//
//  ServiceManager.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Alamofire

class ServiceManager: NSObject {
    static let shared = ServiceManager()
    var hideLoader = false
}

//MARK:- Post Api Request
extension ServiceManager {
    func callPostRequest(url: String, parameters:[String: Any], completion: @escaping (Data) -> Void) {
        if Reachability.isConnectedToNetwork() == false {
            completion(Data())
            return
        }
        
        if hideLoader == false {
            Utility.shared.startLoading()
        }
        self.postRequest(url: url, parameters: parameters) { (jsonData,success) in
            Utility.shared.stopLoading()
            if let data = jsonData {
                completion(data)
            }
        }
    }
    
    func postRequest(url: String, parameters:[String: Any], completion: @escaping (Data?, Bool) -> Void) {
        var request = URLRequest(url: URL(string: ServerUrl.link + url)!)
        print(request)
        print(parameters)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(HeaderKey.customHeaderKey, forHTTPHeaderField: HeaderKey.headerKeyName)
        request.setValue(HeaderKey.secret_key, forHTTPHeaderField: "secret-key")
        request.setValue(HeaderKey.client_id, forHTTPHeaderField: "client_id")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        
        Alamofire.request(request).responseJSON { (response) in
            print(response)
            if let data = response.data {
                if let data = self.checkSuccess(data) {
                    completion(data, true)
                }else {
                    completion(data, false)
                }
            }
        }
    }
}

//MARK:- Get Api Request
extension ServiceManager {
    func callGetRequest(url: String, parameters:[String: Any], completion: @escaping(Data) -> Void) {
        print(url)
        if Reachability.isConnectedToNetwork() == false {
            completion(Data())
            return
        }
        
        if hideLoader == false {
            Utility.shared.startLoading()
        }
        self.getRequest(url: url, parameters: parameters) { (jsonData,success) in
            Utility.shared.stopLoading()
            if let data = jsonData {
                completion(data)
            } else {
                completion(Data())
            }
        }
    }
    
    func getRequest(url: String, parameters:[String: Any], completion: @escaping(Data?, Bool) -> Void) {
        
        let methodName = url.replacingOccurrences(of: " ", with: "%20")
        
        request(URL(string: ServerUrl.link + methodName)!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [HeaderKey.headerKeyName: HeaderKey.customHeaderKey]).responseJSON { (response) in
            print(response)
            
            if let data = response.data {
                if let data = self.checkSuccess(data) {
                    completion(data, true)
                }else {
                    completion(data, false)
                }
            } 
        }
    }
}

//MARK:- Multipart Api Request
extension ServiceManager {
    func uploadImage(url: String, parameters:[String: Any], completion: @escaping(Data) -> Void) {
        
        let url = ServerUrl.link + url
        if hideLoader == false {
            Utility.shared.startLoading()
        }

        let header: HTTPHeaders = [
            "Content-Type" : "multipart/form-data",//"multipart/form-data",//"application/json"
            "Authorization": "Bearer " + HeaderKey.secret_key,
            HeaderKey.headerKeyName: HeaderKey.customHeaderKey
        ]
        
        upload(multipartFormData: { (multipartFormData) in
            if let data = parameters["image"] as? Data {
                multipartFormData.append(data, withName: "user_image", fileName: "image.png", mimeType: "image/jpeg")
            }
        },
               usingThreshold: UInt64.init(),
               to: url,
               method: .post,
               headers: header
            
        ) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    Utility.shared.stopLoading()
                    print(response)
                    if let data = response.data {
                        if let data = self.checkSuccess(data) {
                            completion(data)
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                Utility.shared.stopLoading()
                Utility.shared.showAlertView(message: "encodingError")
                
            }
        }
    }
}

extension ServiceManager {
    func checkSuccess(_ data: Data) -> Data? {
        let errorType = try? JSONDecoder().decode(ErrorType.self, from: data)
        if let success = errorType?.success  {
            if success {
                return data
            } else {
                Utility.shared.showAlertView(message: errorType?.message ?? "")
            }
        }
        
        return nil
    }
}

extension ServiceManager {
    
    func postStripeRequest(url:String, params:NSDictionary?, oncompletion:@escaping (Bool?, String?, NSDictionary?) -> Void) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + UserModel.shared.secret_key]
        
        request(URL(string: url)!, method: .post, parameters: params as? [String : AnyObject], encoding: URLEncoding.default, headers: headers)
            .responseJSON
            { (response) in
               // Utility.shared.stopLoading()
                
                if response.result.value != nil {
                    if let response = response.result.value as? [String : Any] {
                        print(response)
                        
                        if response["error"] != nil {
                            oncompletion(false,(response["error"] as? NSDictionary)?.value(forKey: "message") as? String ?? "" ,response as NSDictionary)
                            return
                        }
                        
                        
                        let message = response["message"] as? String
                        oncompletion(true,message,response as NSDictionary)
                    }
                } else {
                    oncompletion(false, "Server error occured", nil)
                }
                
        }
    }
    
    func getStripeRequest(url:String, params:NSDictionary?, oncompletion:@escaping (Bool?, String?, NSDictionary?) -> Void) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + UserModel.shared.secret_key]
        
        request(URL(string: url)!, method: .get, parameters: params as? [String : AnyObject], encoding: URLEncoding.default, headers: headers)
            .responseJSON
            { (response) in
                if response.result.value != nil {
                    if let response = response.result.value as? [String : Any] {
                        if response["error"] != nil {
                            oncompletion(false,(response["error"] as? NSDictionary)?.value(forKey: "message") as? String ?? "" ,response as NSDictionary)
                            return
                        }
                        print(response)
                        let success = response["status"] as? Bool
                        let message = response["message"] as? String
                        oncompletion(true,message,response as NSDictionary)
                    }
                } else {
                    oncompletion(false, "Server error occured", nil)
                }
        }
    }
    
    
    func deleteStripeRequest(methodName:String, params:NSDictionary?, oncompletion:@escaping (Bool?, String?, NSDictionary?) -> Void) {
        let headers: HTTPHeaders = [
            // "Authorization": "Bearer sk_test_9beXr1Y9N9qXsc7R7zqf2HMv"
            "Authorization": "Bearer " + UserModel.shared.secret_key]
        
        request(URL(string: methodName)!, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON
            {(response) in
                if response.result.value != nil {
                    if let response = response.result.value as? [String : Any] {
                        print(response)
                        let success = response["deleted"] as? Bool
                        let message = response["message"] as? String
                        oncompletion(success,message,response as NSDictionary)
                    }
                } else {
                    oncompletion(false, "Server error occured", nil)
                }
        }
    }
    
    func postRequestWithThreeValues(url: String, parameters:[String: Any], completion: @escaping (Bool?, String?, NSDictionary?) -> Void) {
        
        if Reachability.isConnectedToNetwork() == false {
            completion(false, "Server error occured", nil)
            return
        }

        var request = URLRequest(url: URL(string: ServerUrl.link + url)!)
        print(request)
        print(parameters)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(HeaderKey.customHeaderKey, forHTTPHeaderField: HeaderKey.headerKeyName)
        request.setValue(HeaderKey.secret_key, forHTTPHeaderField: "secret-key")
        request.setValue(HeaderKey.client_id, forHTTPHeaderField: "client_id")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
       // Utility.shared.startLoading()

        Alamofire.request(request).responseJSON { (response) in
            //Utility.shared.stopLoading()
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    print(response)
                    let success = response["success"] as? Bool
                    let message = response["message"] as? String
                    completion(success,message,response as NSDictionary)
                }
            } else {
                completion(false, "Server error occured", nil)
            }
        }
    }
    
    func getRequestWithThreeValues(url: String, parameters:[String: Any], completion: @escaping(Bool?, String?, NSDictionary?) -> Void) {
        
        
        let methodName = url.replacingOccurrences(of: " ", with: "%20")
        
        print(ServerUrl.link + methodName)
        
        
        request(URL(string: ServerUrl.link + methodName)!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [HeaderKey.headerKeyName: HeaderKey.customHeaderKey]).responseJSON { (response) in
            print(response)
            //Utility.shared.stopLoading()
            if response.result.value != nil {
                if let response = response.result.value as? [String : Any] {
                    let success = response["success"] as? Bool
                    let message = response["message"] as? String
                    completion(success,message,response as NSDictionary)
                }
            } else {
                completion(false, "Server error occured", nil)
            }
        }
    }
    
    func loginWithMedia(url: String, parameters:[String: Any], completion: @escaping (Data) -> Void) {
        
        print(parameters)
        
        if Reachability.isConnectedToNetwork() == false {
            completion(Data())
            return
        }
        
        if hideLoader == false {
            Utility.shared.startLoading()
        }

        var request = URLRequest(url: URL(string: ServerUrl.link + url)!)
        print(request)
        
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(HeaderKey.customHeaderKey, forHTTPHeaderField: HeaderKey.headerKeyName)
        request.setValue(HeaderKey.client_id, forHTTPHeaderField: "c81e728d9d4c2f636f067f89cc14862c")
         request.setValue(HeaderKey.secret_key, forHTTPHeaderField: "c81e728d9d4c2f636f067f89cc14862c")
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
//        print(params)
        
        Alamofire.request(request).responseJSON { (response) in
            print(response)
            Utility.shared.stopLoading()
            if let data = response.data {
                if let data = self.checkSuccess(data) {
                    completion(data)
                }else {
                    completion(data)
                }
            }
        }
    }
}
