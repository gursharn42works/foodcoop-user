//
//  AppDelegate.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import Firebase
import FirebaseMessaging
import UserNotifications
import Braintree
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let appUrlScheme = "com.hashtagrestaurants.app.payments"
    let googleApiKey = "AIzaSyCRO59JAn4c77L2zQwAndcJu5iIolV9pr4"
    

    var notificationManager = NotificationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        FirebaseApp.configure()
        GMSServices.provideAPIKey(googleApiKey)

        Messaging.messaging().delegate = notificationManager
        BTAppSwitch.setReturnURLScheme(appUrlScheme)

        self.setCustomUI(application)
        
        return true
    }
    
    func setCustomUI(_ application: UIApplication) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        self.registerNotification(application)

    }

    func registerNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = notificationManager
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.scheme?.localizedCaseInsensitiveCompare(appUrlScheme) == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }

}

