//
//  AddressCell.swift
//  Restaurant42
//
//  Created by Apple1 on 27/03/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

var pickUpTimeStr = String()
var paystr = String()
var DataId = String()
var nameLocation = String()
var cellTappedcard:Bool = true // for disabling stripe


class CartAddressCell: UITableViewCell {
    
    //MARK:- Variables
    var addAddress : (()->())?
    var changeAddress : (()->())?
    
    @IBOutlet weak var btnOnline: UIButton!
    @IBOutlet weak var btncash: UIButton!
    @IBOutlet weak var btnPayPal: UIButton!
    
    //MARK:- IBOutlets
    @IBOutlet weak var changeAddressView : UIView!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var payAtStoreLbl : UILabel!
    @IBOutlet weak var payAtStoreBtn: UIButton!

    var isOrderPickUp = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnSelectAction(_ sender: UIButton) {
        if sender.tag == 1 {
            btnOnline.isSelected = true
            btncash.isSelected = false
            btnPayPal.isSelected = false
            paystr = "Stripe"
            cellTappedcard = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }else if sender.tag == 3{
            btnOnline.isSelected = false
            btncash.isSelected = true
            btnPayPal.isSelected = false
           if payAtStoreLbl.text == "Cash on Delivery" {
            paystr = "Cash on Delivery"
            }else {
                paystr = "pay_on_pickup"
            }
            
            cellTappedcard = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }else{
            
            btnOnline.isSelected = false
            btncash.isSelected = false
            btnPayPal.isSelected = true
            paystr = "Braintree"
            cellTappedcard = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }
        
        UserModel.shared.paymentOption = PaymentOption(rawValue: paystr) ?? .payOnline
    }
    
    //MARK:- Function
    func config(){
        
        if payAtStoreLbl.text == "Cash on Delivery" {
            self.btncash.isSelected = UserModel.shared.paymentOption == .cod
         }else {
            self.btncash.isSelected = UserModel.shared.paymentOption == .payOnPickup
         }
        
        self.btnOnline.isSelected = UserModel.shared.paymentOption == .payOnline
        self.btnPayPal.isSelected = UserModel.shared.paymentOption == .paypal
        
        if isOrderPickUp {
            self.payAtStoreLbl.isHidden = true
            self.payAtStoreBtn.isHidden = true
            self.btncash.isHidden = true
        } else {
            self.payAtStoreLbl.isHidden = false
            self.payAtStoreBtn.isHidden = false
            self.btncash.isHidden = false

        }
        
    }
    
    //MARK:- IBAction
    @IBAction func addAddressTap(){
        addAddress?()
    }
    
    @IBAction func changeAddressTap(){
        changeAddress?()
    }
    
}
