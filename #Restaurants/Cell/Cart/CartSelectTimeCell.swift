//
//  CartSelectTimeCell.swift
//  Magic
//
//  Created by Apple1 on 20/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CartSelectTimeCell: UITableViewCell,UITextFieldDelegate {
    
    //MARK:- IBOutlets
   /* @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectTimeView: UIView!
    @IBOutlet var immediateRadioBtn: UIButton!
    @IBOutlet var laterRadioBtn: UIButton!
    @IBOutlet var selectDateTF: UITextField!
    @IBOutlet var selectTimeTF: UITextField!
    @IBOutlet var changeDateTF: UITextField!
    @IBOutlet var changeTimeTF: UITextField!
    
    //MARK:- Variables
    var onTimePreferenceClick:((Int)->())?
    var hallAvailability:((Bool,String)->())?
    var laterTap:((String)->())?
    let datePicker  : UIDatePicker = UIDatePicker()
    let timePicker  : UIDatePicker = UIDatePicker()
    let monthsToAdd = 0
    let daysToAdd = 14
    let yearsToAdd = 0

    var currentDate = Date()
    var hallTiming = [HallTiming]()
    var hallId : Int?
    var isHallAvailableToday : Bool = true
    var selectedDate : Date?
    var selectedTime : Date?
    var dateComponent = DateComponents()
    var first : Bool = true
    var hallName : String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        immediateRadioBtn.isSelected = true
        laterRadioBtn.isSelected = false
        selectDateView.isHidden = true
        selectTimeView.isHidden = true
        selectDateTF.delegate = self
        selectDateTF.delegate = self
        changeDateTF.delegate = self
        changeTimeTF.delegate = self
        // Set Picker
        setDatePicker()
        setTimePicker()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(){
        currentDate = Date()
        for hall in Utility.sharedInstance.hallArr{
            if hall.id == hallId ?? 0{
               hallTiming = hall.timing ?? [HallTiming]()
                hallName = hall.name ?? ""
            }
        }
        datePicker.setDate(Date(), animated: true)
        selectDateTF.text = ""
        selectTimeTF.text = ""
        //checkAvailability()
    }
    
    @IBAction func checkBoxClicked(_ sender : UIButton)
    {
        if sender.tag == 1
        {
            datePicker.setDate(Date(), animated: true)
            if !isHallAvailableToday{
                hallAvailability?(false, hallName)
               
            }
            immediateRadioBtn.isSelected = true
            laterRadioBtn.isSelected = false
            selectDateView.isHidden = true
            selectTimeView.isHidden = true
            self.onTimePreferenceClick?(1)
        }else{
            immediateRadioBtn.isSelected = false
            laterRadioBtn.isSelected = true
            selectDateView.isHidden = false
            selectTimeView.isHidden = false
            //self.checkAvailability()
            self.onTimePreferenceClick?(2)
            laterBtnTap()
        }
        
    }
    
    @objc func handleDatePicker() {
        let dateToShow = datePicker.date.getDate()
        selectDateTF.text =  "    " + dateToShow.convertDateFormat("yyyy-MM-dd", "MMMM dd, yyyy")
        selectedDate = datePicker.date
    
    }
    @objc func handleTimePicker() {
        let timeToShow = timePicker.date.getTime()
        selectTimeTF.text = "    " + timeToShow.convertDateFormat("HH:mm:ss", "hh:mm a")
      
    }
    
    func setDatePicker(){
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.minimumDate = Date()
        dateComponent.month = monthsToAdd
        dateComponent.day = daysToAdd
        dateComponent.year = yearsToAdd
        datePicker.timeZone = TimeZone(secondsFromGMT: 0)
        datePicker.maximumDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        changeDateTF.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: UIControl.Event.valueChanged)
        
    }
    
    func setTimePicker() {
        changeTimeTF.inputView = timePicker
        timePicker.datePickerMode = UIDatePicker.Mode.time
        timePicker.timeZone = TimeZone(secondsFromGMT: 0)
        timePicker.addTarget(self, action: #selector(handleTimePicker), for: UIControl.Event.valueChanged)
    }
    
    
//    func checkAvailability(){
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "EEE"
//        let currentDay = dateFormatterPrint.string(from: datePicker.date)
//        var days = [String]()
//        for timing in hallTiming{
//            days.append(timing.day ?? "")
//            if(timing.day?.caseInsensitiveCompare(currentDay) == ComparisonResult.orderedSame){
//                let startTime = timing.startTime ?? ""
//                let endTime = timing.endTime ?? ""
//
//                let dateFormatter = DateFormatter()
//                dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
//                dateFormatter.dateFormat = "yyyy-MM-dd 'at' hh:mm a"
//
//                let startDateString = ((datePicker.date.getDate())) + " at " + startTime
//                let finalStartDate = dateFormatter.date(from: startDateString)
//                let endDateString = (datePicker.date.getDate()) + " at " + endTime
//                let finalEndDate = dateFormatter.date(from: endDateString)
//
//
//                if ((finalStartDate ?? Date()) > (finalEndDate ?? Date())){
//                    hallAvailability?(false, hallName)
//                    if datePicker.date.getDate() == Date().getDate(){
//                        isHallAvailableToday = false
//                        selectDateTF.text = ""
//                        selectTimeTF.text = ""
//                    }
//                }
//                else{
//                    selectedDate = datePicker.date
//                    let dateToShow = ((selectedDate?.getDate()) ?? Date().getDate())
//                    selectDateTF.text =  "    " + dateToShow.convertDateFormat("yyyy-MM-dd", "MMMM dd, yyyy")
//                    hallAvailability?(true, hallName)
//                    selectTodayHallTiming()
//                }
//            }
//        }
//
//        if !(days.contains(currentDay.firstUppercased)){
//            hallAvailability?(false, hallName)
//            let dateToShow = (datePicker.date.getDate())
//            let timeToShow = (datePicker.date.getCurrentTime())
//            selectDateTF.text = "    " + dateToShow.convertDateFormat("yyyy-MM-dd", "MMMM dd, yyyy")
//            selectTimeTF.text = "    " + timeToShow.convertDateFormat("HH:mm:ss", "hh:mm a")
//        }
//    }
//
//    func selectTodayHallTiming(){
//        if selectDateTF.text?.trim() == ""{
//            return;
//        }
//        currentDate = Date()
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "EEE"
//        let currentDay = dateFormatterPrint.string(from: datePicker.date)
//        for timing in hallTiming{
//
//            if(timing.day?.caseInsensitiveCompare(currentDay) == ComparisonResult.orderedSame){
//
//                let startTime = timing.startTime ?? ""
//                let endTime = timing.endTime ?? ""
//
//                let dateFormatter = DateFormatter()
//                dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
//                dateFormatter.dateFormat = "yyyy-MM-dd 'at' hh:mm a"
//                let startDateString = ((selectedDate?.getDate()) ?? "") + " at " + startTime
//                let finalStartDate = dateFormatter.date(from: startDateString)
//
//                let endDateString = ((selectedDate?.getDate()) ?? "") + " at " + endTime
//                let finalEndDate = dateFormatter.date(from: endDateString)
//
//                let currentTime = Date().getCurrentDate()
//                let finalCurrentTimeString = currentTime.convertDateFormat("dd-MM-yyyy_HH:mm:ss", "yyyy-MM-dd 'at' hh:mm a")
//                let currentTimeDate = dateFormatter.date(from: finalCurrentTimeString)
//                if selectedDate?.getDate() == Date().getDate()
//                {
//                    let timeToShow = currentDate.getCurrentTime()
//                    selectTimeTF.text = "    " + timeToShow.convertDateFormat("HH:mm:ss", "hh:mm a")
//                    //Show current time if current time is greater than restuarant start time
//                    if ((currentTimeDate ?? Date()) > (finalStartDate ?? Date()))
//                    {
//                    if ((currentTimeDate ?? Date()) > (finalEndDate ?? Date()))
//                    {
//                        hallAvailability?(false, hallName)
//                            isHallAvailableToday = false
//                    }
//                    else{
//                        timePicker.minimumDate = currentTimeDate
//                        timePicker.maximumDate = finalEndDate
//                    }
//                    }else{
//                        timePicker.minimumDate = finalStartDate
//                        timePicker.maximumDate = finalEndDate
//                    }
//
//                }else{
//                    let timeToShow = finalStartDate?.getTime() ?? ""
//                    selectTimeTF.text = "    " + timeToShow.convertDateFormat("HH:mm:ss", "hh:mm a")
//                    timePicker.minimumDate = finalStartDate
//                    timePicker.maximumDate = finalEndDate
//                }
//
//            }
//        }
//
//    }

    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == changeDateTF || textField == changeTimeTF {
//           self.endEditing(true)
//            if textField == changeDateTF{
//                if !isHallAvailableToday{
//                    if datePicker.date.getDate() == currentDate.getDate(){
//                        hallAvailability?(false, hallName)
//                    }
//                }
//                checkAvailability()
//                selectTodayHallTiming()
//            }
//
//           laterBtnTap()
//        }
//    }
    func laterBtnTap()
    {
        let dateToServer = selectDateTF.text?.trim().convertDateFormat("MMMM dd, yyyy","yyyy-MM-dd")
        let timeToServer = selectTimeTF.text?.trim().convertDateFormat("hh:mm a","HH:mm:ss")
        laterTap?((dateToServer ?? "") + " " + (timeToServer ?? ""))
    }
    */
}

