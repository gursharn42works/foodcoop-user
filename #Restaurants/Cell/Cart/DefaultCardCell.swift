//
//  DefaultCardCell.swift
//  Magic
//
//  Created by Apple1 on 03/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol DefaultCardDelegate {
    func changeCardTap()
}

class DefaultCardCell: UITableViewCell {

    @IBOutlet weak var change_addCardBtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    
    var delegate : DefaultCardDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityIndicator.hidesWhenStopped = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ card : CardModel) {
        getCardImage(card)
        
        if UserModel.shared.user_id == 0 {
            cardNumber.text = "XXXX-XXXX-XXXX-XXXX"
            cardImage.image = UIImage(named: "defaultCard")
        } else {
            cardNumber.text = "XXXX-XXXX-XXXX-" + card.cardNumber
        }

    }
    
    
    @IBAction func changeCardTap(_ sender : UIButton){
        delegate?.changeCardTap()
    }
    
    func getCardImage(_ card : CardModel){
        switch card.brand.lowercased(){
        case "visa":
            cardImage.image = CardImage.Visa.image
        case "mastercard":
            cardImage.image = CardImage.MasterCard.image
        case "american express":
            cardImage.image = CardImage.AmericanExpress.image
        case "diners":
            cardImage.image = CardImage.DinersClub.image
        case "discover":
            cardImage.image = CardImage.Discover.image
        case "jcb":
            cardImage.image = CardImage.JCB.image
        case "unionpay":
            cardImage.image = CardImage.UnionPay.image
        case "Mir":
            cardImage.image = CardImage.Mir.image
        default:
            return
        }
    }
}
