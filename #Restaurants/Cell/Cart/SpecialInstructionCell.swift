//
//  SpecialInstructionCell.swift
//  Magic
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class SpecialInstructionCell: UITableViewCell,UITextViewDelegate {

    //MARK:- IBOutlets
    @IBOutlet var specialInstructionTF : KMPlaceholderTextView!
    
    //MARK:- Variables
    var specialInstruction: ((String)-> Void)?
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        specialInstruction?(textView.text ?? "")
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let currentText = textView.text ?? ""
//        guard let stringRange = Range(startTime: range, endTime: currentText) else { return false }
//
//        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
//
//        return changedText.count <= 80
//    }
}
