//
//  PickUpTimeCell.swift
//  Order Up!
//
//  Created by 42Works on 13/05/20.
//  Copyright © 2020 Apple1. All rights reserved.
//

import UIKit
import WWCalendarTimeSelector

class PickUpTimeCell: UITableViewCell,WWCalendarTimeSelectorProtocol {
	
	@IBOutlet weak var immdateLbl: UILabel!
	@IBOutlet weak var laterLbl: UILabel!
	
	@IBOutlet weak var laterBtn: UIButton!
	@IBOutlet weak var imidateBtn: UIButton!
	
	@IBOutlet weak var selectdatetimeTf: UITextField!
	@IBOutlet weak var btnSelect: UIButton!
	@IBOutlet weak var btnSelectHeight: NSLayoutConstraint!
	@IBOutlet weak var tfHeightt: NSLayoutConstraint!
	
	var selectTimeLater : (()->())?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		btnSelectHeight.constant = 0
		btnSelect.isHidden = true
		tfHeightt.constant = 0
		
		self.selectdatetimeTf.delegate = self
		
		self.checkBoxClicked(self.imidateBtn)
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
		
	}
	
	@IBAction func checkBoxClicked(_ sender : UIButton)
	{
		if sender.tag == 1
		{
			imidateBtn.isSelected = true
			laterBtn.isSelected = false
			btnSelectHeight.constant = 0
			btnSelect.isHidden = true
			tfHeightt.constant = 0
//			pickUpTimeStr = "immediate"//FIXME:- //GSD changed "immedate" to "immediate"
		}else{
			imidateBtn.isSelected = false
			laterBtn.isSelected = true
			btnSelectHeight.constant = 20
			btnSelect.isHidden = false
			tfHeightt.constant = 20
			
		}
		
	}
}

extension PickUpTimeCell: UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		self.selectTimeLater?()
		return false
	}
}
