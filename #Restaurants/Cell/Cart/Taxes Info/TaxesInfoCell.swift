//
//  TaxesInfoCell.swift
//  Magic
//
//  Created by Apple1 on 12/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TaxesInfoCell: UITableViewCell {

    @IBOutlet var taxName : UILabel!
    @IBOutlet var taxAmount : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ taxInfo : TaxDescription){

        //taxName.text = taxInfo.title
        taxAmount.smartDecimalText = /* UserSession.shared.currencyCode*/ /*"$"*/ String(format: "%.2f",taxInfo.taxPercentage ?? 0.00) + "%"
        
        taxName.text = taxInfo.title ?? ""
        
        let taxPercentage = " (\(taxAmount.text ?? ""))"
        
        taxName.text =  taxName.text! //+ taxPercentage
        
        taxAmount.smartDecimalText = String(format: "%.2f",taxInfo.amount )
    }
    
}
