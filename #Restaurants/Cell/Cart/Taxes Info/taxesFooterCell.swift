//
//  taxesFooterCell.swift
//  Magic
//
//  Created by Apple1 on 12/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class taxesFooterCell: UITableViewCell {

    @IBOutlet var totalTax : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ taxInfo : [TaxDescription]){
	
		var totalTaxAmount = 0.0
        for tax in taxInfo{
			totalTaxAmount += tax.amount//GSD tax.taxPercentage ?? 0.0
        }
        print(totalTaxAmount)
        totalTax.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + String(format: "%.2f",totalTaxAmount)
    }
}
