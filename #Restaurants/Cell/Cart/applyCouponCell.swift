//
//  applyCouponCell.swift
//  Magic
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class applyCouponCell: UITableViewCell,UITextFieldDelegate {
    //MARK:- Variables
    var couponTap: ((String)-> Void)?
    
    //MARK:- IBOutlets
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var applyCouponBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        couponTextField.delegate = self
		couponTextField.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- IBAction
    @IBAction func applyCouponTap(_ sender : UIButton) { 
        couponTap?(couponTextField.text?.trim() ?? "")
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        couponTextField.resignFirstResponder()
        return true
    }
}
