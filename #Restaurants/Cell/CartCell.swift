//
//  CartCell.swift
//  Magic
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var ingredientsValueLabel: UILabel!
    @IBOutlet weak var dishNameLabel: UILabel!
    @IBOutlet weak var veg_NonVegImage: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var valueTxtField: UITextField!
    @IBOutlet weak var dishUnavailableLabel : UILabel!
    @IBOutlet weak var customizationLabel : UILabel!
    @IBOutlet weak var servingSizeLabel : UILabel!
    @IBOutlet weak var vwSelectedItems: UIView!
    @IBOutlet weak var dishTypeStackView: UIStackView!
    @IBOutlet weak var dishNameLeading: NSLayoutConstraint!
    
    lazy var cartItem = CartItem()
    var count = 1
    var deleteItem: (()-> Void)?
    var updateQuantity: ((CartItem)-> Void)?
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- Functions
    func config(_ item: CartItem, unavailableCustomizations: [Int]) {

        cartItem = item
        dishNameLabel.text = item.dishName
        let priceString = (Double(item.price) ?? 0.0)// * Double(count)
        price.smartDecimalText = item.currencySymbol + (String(format: "%.2f",priceString))
        valueTxtField.text = item.qty
        count = Int(valueTxtField.text ?? "") ?? 0
        dishUnavailableLabel.text = item.status == true ? "" : Messages.dishUnavailable
        customizationLabel.isHidden = cartItem.customOptions.count > 0 || cartItem.addOns.count > 0 || item.variationName != "" || item.variationserving != "" ? false : true
        var optionNameArr = [String]()
        var optionIDArr = [Int]()

        for option in cartItem.customOptions{
            optionNameArr.append(option.option ?? "")
            optionIDArr.append(option.optionId ?? 0)
        }
        
        for addon in cartItem.addOns{
            optionNameArr.append(addon.name ?? "")
            optionIDArr.append(addon.addOnId ?? 0)
        }
        
        let mutableString = NSMutableAttributedString()
        let redColor = [NSAttributedString.Key.foregroundColor: AppColor.primaryColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]

              
        for i in 0..<optionNameArr.count  {
            let option_id = optionIDArr[i]
            let name = optionNameArr[i]

            if unavailableCustomizations.contains(option_id) {
                
                let partOne = NSMutableAttributedString(string: name, attributes: redColor)
                mutableString.append(partOne)
            } else {
                let partTwo = NSMutableAttributedString(string: name, attributes: lightGray)
                mutableString.append(partTwo)
            }
            
            if i < optionNameArr.count-1 {
                let addcomma = NSMutableAttributedString(string: ", ", attributes: lightGray)
                mutableString.append(addcomma)
            }

        }
        
        if item.variationName != "" {
            var variationStr = String()
            if optionNameArr.count > 0 {
                variationStr = ",\(item.variationName!)"
            }else {
                variationStr = "\(item.variationName!)"
            }
            
            let partTwo = NSMutableAttributedString(string: variationStr, attributes: lightGray)
            mutableString.append(partTwo)
        }
        if item.variationserving != "" {
            let varStr = "(\(item.variationserving!))"
            let partTwo = NSMutableAttributedString(string: varStr , attributes: lightGray)
            mutableString.append(partTwo)
        }

        customizationLabel.attributedText = mutableString

        servingSizeLabel.text = "Serving size: " + (cartItem.serving?.ingredient ?? "")
        servingSizeLabel.isHidden = true
        
        for dishIndicator in self.dishTypeStackView.arrangedSubviews{
            dishIndicator.isHidden = true
        }
        
        //var isDishTypeShown = false
        
//        if self.cartItem.showTypesOnApp{
//            for dishType in self.cartItem.dishTypes{
//
//                if dishType.index < self.dishTypeStackView.arrangedSubviews.count && dishType.index != -1{
//                    self.dishTypeStackView.arrangedSubviews[dishType.index].isHidden = false
//                    isDishTypeShown = true
//                }
//            }
//        }
//
        //self.dishNameLeading.constant = isDishTypeShown ? 8 : -10
    }
    
@IBAction func plusBtn_Click(_ sender: UIButton) {

        count += 1
        
        if (count < 100) {
            valueTxtField.text = "\(count)";
        } else {
            valueTxtField.text = "100";
            count = 100;
        }
       cartItem.qty = "\(count)"
       updateQuantity?(cartItem)
       price.smartDecimalText = "$\(((Double(cartItem.price) ?? 0.0)) * Double(count))"
    }
    
    @IBAction func minusBtn_Click(_ sender: UIButton) {
        if count == 1 {
            return
        }
        
        count -= 1
        if (count < 100) {
            valueTxtField.text = "\(count)";
        } else {
            valueTxtField.text = "100";
            count = 100;
        }
        
        cartItem.qty = "\(count)"
        updateQuantity?(cartItem)
        price.smartDecimalText = "$\(((Double(cartItem.price) ?? 0.0)) * Double(count))"
    }
    
    
    
     @IBAction func deleteItemTap(){
        deleteItem?()
    }
}
