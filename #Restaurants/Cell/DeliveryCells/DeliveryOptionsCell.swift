//
//  DeliveryOptionsCell.swift
//  #Restaurants
//
//  Created by Satveer on 05/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol DeliveryOptionsCellDelegate: class {
    func deliveryOptionType(type: String)
    func addBtnClick(isToAdd: Bool)
}

class DeliveryOptionsCell: UITableViewCell {

    @IBOutlet weak var orderPickUpBtn: UIButton!
    @IBOutlet weak var orerDeliveryBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressLblLeading: NSLayoutConstraint!
    @IBOutlet weak var addressBottomLbl: UILabel!
    @IBOutlet weak var homeIcon: UIImageView!
    @IBOutlet weak var addressBottomLblXPOs: NSLayoutConstraint!
    @IBOutlet weak var addBtnWidth: NSLayoutConstraint!

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var inValidAdressLbl: UILabel!

    var delegate: DeliveryOptionsCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDefaultAddressLabelData(_ address: DeliveryAddressModel,isValidDistance: Bool) {
//        delegate?.deliveryOptionType(type: "delivery")
        self.homeIcon.image = UIImage(named: "homeSelectedOne")
        if address.title == "" {
            self.addressLbl.text = ""
            self.addressBottomLbl.text = Constant.shared.emptyAddressMsg
            self.addBtn.setTitle("ADD", for: .normal)
        } else {
            self.addressLbl.text = address.title.capitalizeFirstInEachWord()
            self.addressBottomLbl.text = "\(address.house_no), \(address.street1), \(address.street2), \(address.city.capitalized), \(address.state.capitalized)."
            self.addBtn.setTitle("CHANGE", for: .normal)
        }
        self.addBtnWidth.constant = 76
        self.addressLblLeading.constant = 15

//        if UserModel.shared.user_id == 0 {
//            inValidAdressLbl.isHidden = true
//            homeIcon.isHidden = true
//            addressBottomLblXPOs.constant = -25
//        }else {
            inValidAdressLbl.isHidden = isValidDistance
      //  }
    }
    
    
    func setRestaurnatAddressLabelData(_ address: RestaurantAddressModel) {
//        delegate?.deliveryOptionType(type: "pickup")
        self.addressLbl.text = address.location_title
        let addressfull = "\(address.address_line_1),\(address.city),\(address.state),\(address.zip)"

        self.addressBottomLbl.text = addressfull
        self.addBtnWidth.constant = 0
        self.addressLblLeading.constant = -35
        inValidAdressLbl.isHidden = true
        self.homeIcon.isHidden = true
        self.homeIcon.image = UIImage(named: "selectARestaurant")
    }
    
    @IBAction func btnSelectAction(_ sender: UIButton) {
        if sender.tag == 1 {
            orderPickUpBtn.isSelected = true
            orerDeliveryBtn.isSelected = false
            delegate?.deliveryOptionType(type: "pickup")
        }else if sender.tag == 2 {
            orderPickUpBtn.isSelected = false
            orerDeliveryBtn.isSelected = true
            delegate?.deliveryOptionType(type: "delivery")
        }
        
    }
    
    @IBAction func addBtnClick(_ sender: UIButton) {
        
        if sender.titleLabel?.text! == "ADD" {
            delegate?.addBtnClick(isToAdd: true)
        } else {
            delegate?.addBtnClick(isToAdd: false)
        }
    }
    
}
