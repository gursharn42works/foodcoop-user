//
//  SelectProductCheckboxCell.swift
//  Magic
//
//  Created by Apple1 on 21/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SelectProductCheckboxCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureUIForOption(_ option : Option){
        self.checkBtn.isSelected = (option.isDefault ?? false) ? true : false
        self.checkBtn.alpha = (option.isAvailable ?? false) ?   1.0 : 0.5
        self.checkBtn.isEnabled = (option.isAvailable ?? false)
        self.isUserInteractionEnabled = (option.isAvailable ?? false)
        self.nameLabel.alpha = self.checkBtn.alpha
        self.priceLabel.alpha = self.checkBtn.alpha
        
        
    }
    
}
