//
//  SelectProductRadioCell.swift
//  Magic
//
//  Created by Apple1 on 21/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol SelectProductRadioCellDeleaget: class {
    func paasSelectedServingToDishDetailView(section: Int, row: Int, custIndex: Int)
}

class SelectProductRadioCell: UITableViewCell {

    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var radioButton : UIButton!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var variationTableView : UITableView!
    @IBOutlet var variationTableViewHeight : NSLayoutConstraint!

    var dish = DishModel()
    var radioBtnTap: ((Int)-> Void)?
    var variable = "variable"
    var delegate: SelectProductRadioCellDeleaget?
    var custIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.variationTableView.isHidden = true
//        self.setTableView()
    }
    
    func setVariationTableView(dish: DishModel) {
        self.dish = dish
        if self.dish.dish_type == variable {
            self.variationTableView.register(UINib(nibName: Constant.shared.dishHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.dishHeaderCell)
            self.variationTableView.register(UINib(nibName: Constant.shared.selectProductRadioCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductRadioCell)
            self.variationTableView.dataSource = self
            self.variationTableView.delegate = self
            self.variationTableView.isHidden = false
            self.variationTableView.reloadData()
            self.variationTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)

        } else {
            self.variationTableView.isHidden = true
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey]
            {
                let newsize  = newvalue as! CGSize
                self.variationTableViewHeight.constant = newsize.height
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func radioBtnTap(_ sender : UIButton){
        radioBtnTap?(sender.tag)
    }
    
    func configureUIForOption(_ option : Option) {
        self.radioButton.isSelected = (option.isDefault ?? false) ? true : false
        self.radioButton.alpha = (option.isAvailable ?? false) ?   1.0 : 0.5
        self.radioButton.isEnabled = (option.isAvailable ?? false)
        self.isUserInteractionEnabled = (option.isAvailable ?? false)
        self.nameLabel.alpha = self.radioButton.alpha
        self.priceLabel.alpha = self.radioButton.alpha
        
        if self.dish.dish_type == variable {
            self.priceLabel.isHidden = true
        }
        
        if self.dish.dish_type == variable {
            self.radioButton.isUserInteractionEnabled = false
        }
        
    }
}

extension SelectProductRadioCell: UITableViewDataSource, UITableViewDelegate {
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        self.dish.variations.count
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = tableView.dequeueReusableCell(withIdentifier: "DishHeaderCell") as! DishHeaderCell
//
//        let variation = self.dish.variations[section].variation
//        headerView.customisationHeading.text = variation
//        headerView.customisationSubHeading.isHidden = true
//        headerView.lineView.isHidden = true
//       // headerView.customisationSubHeading.text = dish.customizations[section].allowPricePerItem ?? false ? "(Choose \(maxSelection), additional cost for extras)" : "(Choose \(maxSelection))"
//
//        return headerView
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 30
//    }
//
////    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////        return 100
////    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dish.variations[section].servings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 {
//            let headerView = tableView.dequeueReusableCell(withIdentifier: "DishHeaderCell") as! DishHeaderCell
//            let variation = self.dish.variations[indexPath.section].variation
//            headerView.customisationHeading.text = variation
//            headerView.customisationSubHeading.isHidden = true
//            headerView.lineView.isHidden = true
//            return headerView
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.selectProductRadioCell, for: indexPath)as! SelectProductRadioCell
        let serving  = self.dish.variations[indexPath.section].servings[indexPath.row]
        
        cell.nameLabel.text = serving.serving
        cell.radioButton.isSelected = serving.isDefault
        cell.priceLabel.smartDecimalText = serving.price

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.paasSelectedServingToDishDetailView(section: indexPath.section, row: indexPath.row, custIndex: self.custIndex)
        for i in 0..<self.dish.variations[indexPath.section].servings.count {
            self.dish.variations[indexPath.section].servings[i].isDefault = false
        }
        
        self.dish.variations[indexPath.section].servings[indexPath.row].isDefault = true
        self.variationTableView.reloadData()
        
        
        
    }
}
