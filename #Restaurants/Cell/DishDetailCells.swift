////
////  DishDetailCells.swift
////  #Restaurants
////
////  Created by Satveer on 03/08/20.
////  Copyright © 2020 42works. All rights reserved.
////
//
//import UIKit
//
//class DishHeaderCell: UITableViewCell {
//
//    //MARK:- IBOutlets
//    @IBOutlet weak var customisationHeading : UILabel!
//    @IBOutlet weak var customisationSubHeading : UILabel!
//    @IBOutlet weak var lineView : UIView!
//    @IBOutlet weak var headerViewTop : NSLayoutConstraint!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//
//}
//
//
//class RadioBoxItemCell: UITableViewCell {
//    @IBOutlet weak var radioBtn: UIButton!
//    @IBOutlet weak var nameLbl: UILabel!
//    @IBOutlet weak var priceLbl: UILabel!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//    
//    func setOptionUI(_ option : CartOption) {
//        
//        self.radioBtn.isSelected = option.selected ? true : false
//        self.radioBtn.alpha = option.available ?   1.0 : 0.5
//        
//        self.radioBtn.isEnabled = option.available
//        self.isUserInteractionEnabled = option.available
//        self.nameLbl.alpha = self.radioBtn.alpha
//        self.priceLbl.alpha = self.radioBtn.alpha
//        
//    }
//
//}
//
//class CheckBoxItemCell: UITableViewCell {
//
//    @IBOutlet weak var checkBtn: UIButton!
//    @IBOutlet weak var nameLbl: UILabel!
//    @IBOutlet weak var priceLbl: UILabel!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//    }
//    
//    
//    func setOptionUI(_ option : CartOption) {
//        
//        self.checkBtn.isSelected = option.selected ? true : false
//        self.checkBtn.alpha = option.available ?   1.0 : 0.5
//        
//        self.checkBtn.isEnabled = option.available
//        self.isUserInteractionEnabled = option.available
//        self.nameLbl.alpha = self.checkBtn.alpha
//        self.priceLbl.alpha = self.checkBtn.alpha
//        
//    }
//
//}
//
//class DishDetailDividerCel: UITableViewCell {
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//
//}
