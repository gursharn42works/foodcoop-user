//
//  FilterCell.swift
//  Magic
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet var radioButton : UIButton!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonTrailing: NSLayoutConstraint!
    
    var radioBtnTap: ((Int)-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func radioBtnTap(_ sender : UIButton){
         radioBtnTap?(sender.tag)
    }
}
