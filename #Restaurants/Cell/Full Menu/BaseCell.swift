//
//  BasicCell.swift
//  MyPagerCollView
//
//  Created by Leela Prasad on 08/03/18.
//  Copyright © 2018 Leela Prasad. All rights reserved.
//

import UIKit

class BasicCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    var indicatorView: UIView!
    
    override var isSelected: Bool {
        
        didSet{
            UIView.animate(withDuration: 0.001) {
                self.indicatorView.backgroundColor = self.isSelected ? AppColor.primaryColor: UIColor.clear
                self.titleLabel.textColor = self.isSelected ? AppColor.darkTextColor: AppColor.textColor
                self.titleLabel.font = self.isSelected ? UIFont.init(name: "Roboto-Regular", size: 14) ?? UIFont.systemFont(ofSize: 12) : UIFont.init(name: "Roboto-Light", size: 14) ?? UIFont.systemFont(ofSize: 12)
                self.layoutIfNeeded()
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = UIColor.white
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.init(name: "Roboto-Light", size: 14) ?? UIFont.systemFont(ofSize: 12)
        titleLabel.textColor = AppColor.textColor
        addSubview(titleLabel)
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: titleLabel)
        addConstraintsWithFormatString(formate: "V:|[v0]|", views: titleLabel)
        
        addConstraint(NSLayoutConstraint.init(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint.init(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        setupIndicatorView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
    }
    
    func setupIndicatorView() {
        indicatorView = UIView()
        addSubview(indicatorView)
        
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: indicatorView)
        addConstraintsWithFormatString(formate: "V:[v0(3)]|", views: indicatorView)
    }
    
}
