//
//  HallDishCell.swift
//  Magic
//
//  Created by Arohi Magotra on 18/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class HallDishCell: UITableViewCell {
    
    @IBOutlet weak var veg_NonVegImage: UIImageView!
    @IBOutlet weak var dishName: MediumLabel!
    @IBOutlet weak var calories: UILabel!
    @IBOutlet weak var allergies: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var favDish: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var hallName: RobotoLightLabel!
    @IBOutlet weak var dishImageWidth: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dishTypeStackView: UIStackView!
    @IBOutlet weak var bestSellerImg: UIImageView!
    @IBOutlet weak var bestSellerLbl: UILabel!
    
    @IBOutlet weak var itemCountLbl: UILabel!
    
    @IBOutlet weak var addBtn_Out: UIButton!
    
    @IBOutlet weak var plusBtn_Out: UIButton!
    @IBOutlet weak var minusBtn_Out: UIButton!
    
    @IBOutlet weak var readMoreBtn_Out: UIButton!
    
    @IBOutlet weak var bestSeller_Out: UIImageView!
    
    @IBOutlet weak var bestsellerWidthConst: NSLayoutConstraint!
    
    @IBOutlet weak var recommended_Out: UIImageView!
    
    @IBOutlet weak var ratingLbl_Out: UILabel!
    @IBOutlet weak var starImg_Out: UIImageView!
    
    @IBOutlet weak var starWidthConst: NSLayoutConstraint!
    
    var onFavClick:((DishModel,Int)->())?
    var viewLongPressed:((DishModel)->())?
    var titleAllergies = "Allergens: "
    let titleCalories = "Calories: "
    var dish = DishModel()
    var isFromSearch : Bool?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(_:)))
        addBtn_Out.layer.borderWidth = 1
        addBtn_Out.layer.borderColor = #colorLiteral(red: 0.9725490196, green: 0.07058823529, blue: 0.2509803922, alpha: 1)
        self.contentView.addGestureRecognizer(longPressRecognizer)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func config(_ isSearch : Bool = false){
        
        if isSearch
        {
            hallName.text = dish.hallName
        }
        
        self.containerView.alpha = dish.isDishAvailable == false ? 0.5 : 1

        //GSD dishImageWidth.constant = dish.productImage == "" ? 0 : 83
        favBtn.isSelected = dish.isFavourite ?? false
      //  productImage.set_image(dish.productImage ?? "", placeholder: "dishPlaceholder_300_200")
        
        productImage.set_image(dish.productImage ?? "", placeholder: #imageLiteral(resourceName: "dishPlaceholder_300_200"))
        dishName.text = dish.name?.uppercased()
        if bestSellerLbl != nil && bestSellerLbl != nil {
            bestSellerImg.isHidden = !dish.isBestSeller
            bestSellerLbl.isHidden = true
        } else {
            bestSellerImg.isHidden = true
            bestSellerLbl.isHidden = true
        }

        
        calories.numberOfLines = (dish.isCustomizable ?? false) ?  2 : 3
        
        calories.text = dish.productDescription //GSD
        
        //        calories.text = dish.ingredients //GSD
        
        favDish.image = dish.isFavourite == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
    
        price.smartDecimalText = UserModel.shared.currencyCode  /*"$"*/ + (dish.price ?? "")
//        price.smartDecimalText = UserModel.shared.currencyCode  /*"$"*/ + "\(String(format: "%.2f",dish.price ?? ""))"

        var attributes = "N/A"
        for value in dish.attributes ?? [AttributeModelModel](){
            if value.name?.lowercased() == "allergens"{
                attributes = value.value.joined(separator: ", ")
            }
        }
        
        attributes = attributes.trim() == "" ? "N/A" : attributes
        
        //        let attributedAllergiesString : NSAttributedString = titleAllergies.attributedString(fullText: attributes, size: 12.0)
        //        allergies.attributedText = attributedAllergiesString
        
        allergies.isHidden = dish.isCustomizable == true ? false : true
        allergies.text =  dish.isCustomizable == true ? "customizations available" : "" //GSD
        
        
        for dishIndicator in self.dishTypeStackView.arrangedSubviews{
            dishIndicator.isHidden = true
        }
        if dish.showTypesOnApp {
            for dishType in dish.dishTypes{
                if dishType.index < self.dishTypeStackView.arrangedSubviews.count && dishType.index != -1{
                    self.dishTypeStackView.arrangedSubviews[dishType.index].isHidden = false
                }
            }
        }
    }
    
    @IBAction func favBtnTap(_ sender : UIButton){
        
        if UserModel.shared.user_id != 0 {
            sender.isSelected = !sender.isSelected
            dish.isFavourite = sender.isSelected
            favDish.image = dish.isFavourite == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
            
            BaseVC().likeUnlikeService(dish.id ?? 0) { (succes) in
            }

        }else{
            if !(isFromSearch ?? false){
                let loginController = Router.viewController(with: .loginVC, .login) as! LoginVC
                UserModel.shared.login_Skipped = true
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(loginController , animated: true)
                return
            }
        }
        onFavClick?(dish,favBtn.tag)
    }
    
    
    @IBAction func longPressed(_ sender: UILongPressGestureRecognizer)
    {
        viewLongPressed?(dish)
    }
    
    
    
    
}
