//
//  HomeItemsCell.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol HomeItemsCellDelegate: class {
    func moveToRestDetailView(rest_id: Int,tag_id: Int,type : String)
}
class HomeItemsCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!

    
    var delegate:HomeItemsCellDelegate?
    var category_id = 0

    var restaurantsArr = [Restaurants]()
    
    var dishArr = ["Marrakesh","MoC","Food@U","Hardyz"]
    var itemName = ["Pasta","Sushi","Salad"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.register(UINib(nibName: "RestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension HomeItemsCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize  {
        //CGSize(width: (collectionView.frame.size.width/2)-30, height: collectionView.frame.size.height)
        return CGSize(width: 155, height: 170)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restaurantsArr.count//dishes.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //MARK:- Show See All Cell
//        if indexPath.row == dishes.count {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.shared.seeAllHomeCell, for: indexPath) as! SeeAllHomeCell
//            return cell
//        }
//        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.shared.homeItemCollCell, for: indexPath) as! HomeItemCollCell
            cell.showItemsData(restaurantsArr[indexPath.row])

            return cell
       // }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.row > self.dishes.count-1 {
        let restId = restaurantsArr[indexPath.row].id ?? 0
        let tagID = 0
        self.delegate?.moveToRestDetailView(rest_id: restId, tag_id: tagID, type: "Rest")
//        } else {
//            if  self.dishes[indexPath.row].isDishAvailable == false {
//                Utility.shared.showAlertView(message: Messages.dishNotAvailable)
//            } else {
//                self.delegate?.moveToDishDetailView(dish_id: self.dishes[indexPath.row].id ?? 0, cate_id: 0)
//            }
//        }
    }
    
}



