//
//  HomeBannerCell.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class HomeBannerCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImgView: UIImageView!
    
    func showBanner(banner: HomeBanner) {
        bannerImgView.set_image(banner.bannerImage ?? "", placeholder: "")
    }
}
