//
//  HomeitemCollCell.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class HomeItemCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imagView: UIImageView!
    @IBOutlet weak var restName: UILabel!
    @IBOutlet weak var restratingView: UIView!
    @IBOutlet weak var restrating: UILabel!

    func showItemsData(_ rast: Restaurants)  {
        
        self.imagView.set_image(rast.thumb ?? "", placeholder: "dishBackground_bg")
        self.restName.text = rast.name
        self.restrating.text = "\(rast.rating ?? 0.0)"
        
        var  rating = rast.rating ?? 0.0
        rating = rating.rounded()
        self.restratingView.isHidden = rating == 0.0 ? true:false

        if rating == 0.0 {
            print("dsdsdsdsd")
        }
    }
}
