//
//  DishHeaderCell.swift
//  Restaurant42
//
//  Created by Apple1 on 25/03/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

class DishHeaderCell: UITableViewCell {

    //MARK:- IBOutlets
    @IBOutlet weak var customisationHeading : UILabel!
    @IBOutlet weak var customisationSubHeading : UILabel!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var headerViewTop : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		customisationHeading.clipsToBounds = false
		customisationSubHeading.clipsToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
