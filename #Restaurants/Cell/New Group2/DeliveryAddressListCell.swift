//
//  DeliveryAddressListCell.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol DeliveryAddressListCellDelegate: class {
    func defaultClickFunc(index: Int)
    func crossClickFunc(index: Int)
}

class DeliveryAddressListCell: UITableViewCell {

    @IBOutlet weak var addresstitleLbl: UILabel!
    @IBOutlet weak var addressDescLbl: UILabel!
    @IBOutlet weak var setDefaultBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var selectedCard: UIImageView!
    @IBOutlet weak var tickWidth: NSLayoutConstraint!
    @IBOutlet weak var defaultBtnWidth: NSLayoutConstraint!

    var delegate: DeliveryAddressListCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func defaultClick(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "   Default   " {
            return
        }
        self.delegate?.defaultClickFunc(index: sender.tag)
    }
    
    @IBAction func crossClick(_ sender: UIButton) {
        self.delegate?.crossClickFunc(index: sender.tag)
    }

}
