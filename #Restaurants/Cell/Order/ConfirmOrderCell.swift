//
//  ConfirmOrderCell.swift
//  Magic
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

let attrWithUnderline : [NSAttributedString.Key: Any] = [
    NSAttributedString.Key.font : UIFont.init(name: "Roboto-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12),
    NSAttributedString.Key.foregroundColor : AppColor.darkTextColor,
    NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]

let attrWithoutUnderline : [NSAttributedString.Key: Any] = [
    NSAttributedString.Key.font : UIFont.init(name: "Roboto-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12),
    NSAttributedString.Key.foregroundColor : UIColor.white]

var attributedString = NSMutableAttributedString(string:"")


protocol ConfirmOrderDelegate {
    func deleteCardTap(_ row : Int)
}
class ConfirmOrderCell: UITableViewCell {

    
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var setDefaultBtn: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var selectedCard: UIImageView!
    @IBOutlet weak var tickWidth: NSLayoutConstraint!
 
    var isDefault : Bool = false
    var delegate : ConfirmOrderDelegate?
    var userCardId : String?
    var card : CardModel = CardModel()
    var defaultCardChanged : ((CardModel)-> Void)?
    var selectedCardRow : Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func config(_ card : CardModel){
        self.card = card
        card.isDefault = card.cardId == userCardId ? true : false
        self.changeState(card.isDefault)
        
        let defaultTitle = card.isDefault ? "   Default   " : " Set as Default "
        let attributeString = card.isDefault ? NSMutableAttributedString(string: defaultTitle,
                                                                         attributes: attrWithoutUnderline) : NSMutableAttributedString(string: defaultTitle,
                                                                                                                                       attributes: attrWithUnderline)
        setDefaultBtn.setAttributedTitle(attributeString, for: .normal)
        getCardImage(card)
        
        if UserModel.shared.user_id == 0 {
            cardNumber.text = "XXXX-XXXX-XXXX-XXXX"
            cardImage.image = CardImage.defaultCard.image
        } else {
            cardNumber.text = "XXXX-XXXX-XXXX-" + card.cardNumber
        }

    }
    
    @IBAction func deleteCardTap(_ sender: Any) {
        delegate?.deleteCardTap(((sender as? UIButton)?.tag) ?? 0)
    }
    
    private func changeState(_ isSelected: Bool) {
        if isSelected {
            setDefaultBtn.backgroundColor = AppColor.primaryColor
            setDefaultBtn.setTitleColor(UIColor.white, for: .normal)
        } else {
            setDefaultBtn.backgroundColor = UIColor.white
            setDefaultBtn.setTitleColor(AppColor.primaryColor, for: .normal)
        }
    }
    
    @IBAction func makeCardDefault() {
        defaultCardChanged?(card)
    }
    
    func getCardImage(_ card : CardModel){
        switch card.brand.lowercased(){
        case "visa":
            cardImage.image = CardImage.Visa.image
        case "mastercard":
            cardImage.image = CardImage.MasterCard.image
        case "american express":
            cardImage.image = CardImage.AmericanExpress.image
        case "diners":
            cardImage.image = CardImage.DinersClub.image
        case "discover":
            cardImage.image = CardImage.Discover.image
        case "jcb":
            cardImage.image = CardImage.JCB.image
        case "unionpay":
            cardImage.image = CardImage.UnionPay.image
        case "Mir":
            cardImage.image = CardImage.Mir.image
        default:
            return
        }
    }
}
