//
//  OrderDetailCell.swift
//  Magic
//
//  Created by Apple on 13/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {

    @IBOutlet var dishName : UILabel!
    @IBOutlet var dishPrice : UILabel!
    @IBOutlet var veg_NonVegImage : UIImageView!
    @IBOutlet var servingSize : UILabel!
    @IBOutlet weak var customizationLabel : UILabel!
    @IBOutlet weak var dishTypeStackView: UIStackView!
    @IBOutlet weak var dishNameLeading: NSLayoutConstraint!
    @IBOutlet weak var dividerView: DividerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showCellData(_ dish : CartItem, dividerLineShow: Bool){
            let qtyString = "  (x\(dish.qty))"
            
            let dishAttributedString : NSAttributedString = dish.dishName.attributedStringColored(fullText: qtyString, size: 13.0)
            
            self.dividerView.isHidden = !dividerLineShow
        
            dishName.attributedText = dishAttributedString
            dishPrice.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish.price)"
            customizationLabel.isHidden = dish.customOptions.count > 0 || dish.addOns.count > 0 ? false : true
            var optionNameArr = [String]()
            for option in dish.customOptions{
                optionNameArr.append(option.option ?? "")
            }
            
            for addon in dish.addOns{
                optionNameArr.append(addon.name ?? "")
            }
            customizationLabel.text = optionNameArr.joined(separator: ", ")
          //  servingSize.text = "Serving size: " + (dish.servingName ?? "")
            
            var isDishTypeShown = false
            
            for dishIndicator in self.dishTypeStackView.arrangedSubviews{
                dishIndicator.isHidden = true
            }
            if dish.showTypesOnApp {
                for dishType in dish.dishTypes{
                    if dishType.index < self.dishTypeStackView.arrangedSubviews.count && dishType.index != -1{
                        self.dishTypeStackView.arrangedSubviews[dishType.index].isHidden = false
                        isDishTypeShown = true
                    }
                }
            }
            
            self.dishNameLeading.constant = isDishTypeShown ? 8 : -10
        }
}
