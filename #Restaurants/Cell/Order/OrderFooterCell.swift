//
//  OrderFooterCell.swift
//  Magic
//
//  Created by Apple1 on 13/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class OrderFooterCell: UITableViewCell {

  
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardNumber: MediumLabel!
    @IBOutlet weak var orderStatus: RegularLabel!
    
    @IBOutlet weak var lblCancelText: UILabel!
    @IBOutlet weak var vwCancelReason: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func config(_ card : Card){
        getCardImage(card)
        
        if UserModel.shared.user_id == 0 {
            cardNumber.text = "XXXX-XXXX-XXXX-XXXX"
            cardImage.image = UIImage(named: "defaultCard")
        } else {
            if card.lastFourDigits == "" {
                cardNumber.text = "Online"
            }else {
                cardNumber.text = "XXXX-XXXX-XXXX-" + card.lastFourDigits
            }
            
        }
    }
    
    func getCardImage(_ card : Card){
        switch card.brand.lowercased(){
        case "visa":
            cardImage.image = CardImage.Visa.image
        case "mastercard":
            cardImage.image = CardImage.MasterCard.image
        case "american express":
            cardImage.image = CardImage.AmericanExpress.image
        case "diner":
            cardImage.image = CardImage.DinersClub.image
        case "discover":
            cardImage.image = CardImage.Discover.image
        case "jcb":
            cardImage.image = CardImage.JCB.image
        case "unionpay":
            cardImage.image = CardImage.UnionPay.image
        case "mir":
            cardImage.image = CardImage.Mir.image
        default:
            return
        }
    }
    
    func configurePayAtStore(_ paymentMethod : PaymentMethod, cancelRequest: Int){
        
        if paymentMethod == .payAtStore{
            cardImage.image = CardImage.defaultCard.image
             cardNumber.text =  "Pay At Store"
            
            if cancelRequest == 0 {
                lblCancelText.text = Messages.orderCancelByHallWithoutRequestForUnpaid
            }
            else if cancelRequest == 1 {
                self.lblCancelText.text = Messages.cancelCancelledOnUserRequest
            }
            else if cancelRequest == 2 {
                self.lblCancelText.text = Messages.cancelCancelledOnUserRequest
            }
//            else {
//                self.lblCancelText.text = Messages.orderCancelByHallWithoutRequestForUnpaid
//            }
            
        }
        
        if paymentMethod == .brainTree{
            cardImage.image = CardImage.paypal.image
            cardNumber.text =  "PayPal"
        }
    }
    
}
