//
//  OrdersViewModel.swift
//  Magic
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

/*
1. Pending for confirmation        pending_confirmation
2. Confirmed                                  confirmed
3. Preparing order                         preparing
4. Ready for pickup                       ready
5. Completed                                 delivered
6. Cancelled                                   cancelled
*/

enum OrderStatusEnum : String {

    case pendingConfirmation    = "pending_confirmation"
    case confirmed              = "confirmed"
    case preparingOrder         = "preparing"
    case readyForPickup         = "ready"
    case completed              = "delivered"
    case cancelled              = "cancelled"
    case unattended             = "unattended"
    case out_for_delivery       = "out_for_delivery"
    
    var highlightTillIndex : Int{
        switch self {
        case .pendingConfirmation:
            return 0
        case .confirmed, .cancelled, .preparingOrder, .unattended:
            return 1
//        case .preparingOrder:
//            return 2
        case .readyForPickup,.out_for_delivery:
            return 2
        case .completed:
            return 3
        default:
            return -1
        }
    }

    var detail: String{
        switch self {
        case .pendingConfirmation:
            return "Waiting for confirmation"
        case .confirmed:
            return ""
        case .preparingOrder:
            return "Your order in the Kitchen"
        case .readyForPickup:
            return "Your order is ready for Pickup"
        case .completed:
            return "Your order has been delivered successfully"
        default:
            return "Your order is cancelled"
        }
    }

}

protocol OrdersViewModelDelegate: class {
    func reloadTableView()
}

class OrdersViewModel: BaseVC {
    
    var array: [Order] = [Order]() {
        didSet {
            self.delegate?.reloadTableView()
        }
    }
    var eraseArray: Bool = false
    var limit = 10
    var type: String = "ongoing"
    var isLoadMoreRequired: Bool = true
    var delegate: OrdersViewModelDelegate?
    
    
    func getOrders(_ type: Int, isRefresh: Bool = false,_ isForBottomBar : Bool = false) {
        
        var typeStr = ""
        switch type {
        case 0:
            typeStr = "ongoing"
            break
        case 1:
            typeStr = "previous"
            break
        default:
            break
        }
        
        self.orderService(typeStr, isRefresh,isForBottomBar)
    }
    
    //MARK:- Service call
    func orderService(_ type: String, _ isRefresh: Bool,_ isForBottomBar : Bool = false) {
        
        var page = self.eraseArray ? 1 : ((array.count)/limit) + 1
//        if isRefresh {
//            page = 1
//        }
        let serviceURL = "\(ApiName.orderList)?user_id=\(UserModel.shared.user_id)&type=\(type)&limit=\(limit)&page=\(page)"
        
        ServiceManager.shared.postRequestWithThreeValues(url: serviceURL, parameters: [:], completion: { (status, message, response) in
//            if isRefresh {
//                self.isEndRefresh = true
//            } else {
//                self.isLoading = false
//            }
//            self.showNoInternetScreen = false
            if status ?? false {
                
                if let response = response?.value(forKey: "data") as? NSDictionary {
                    if let arrayOrder = response.value(forKey: "orders") as? NSArray {
                        if isRefresh || self.eraseArray  || type == "ongoing"{
                            self.eraseArray = false
                            self.array.removeAll()
                        }
                        for dic in arrayOrder {
                            let order = Order().getOrder(dic)
                            self.array.append(order)
                        }
                    }
                }
//                if self.array.count == 0 {
//                    self.showEmptyMsg = true
//                }
            } else {
//                if message == AppMessages.noInternetConnection{
//                    self.showNoInternetScreen = true
//                    if isRefresh {
//                        self.isEndRefresh = true
//                    }
//                }else
//                {
//                    self.showNoInternetScreen = false
//                    self.showEmptyMsg = true
//                }
            }
            
            if self.array.count/page % self.limit == 0 {
                self.isLoadMoreRequired = true
            } else {
                self.isLoadMoreRequired = false
            }
        })
    }
    
    func reOrderService(_ orderId : String,oncompletion:@escaping (Bool, String, NSDictionary) -> Void){
        
        let serviceURL = ApiName.reorder
        let dic = ["order_id" : orderId]
        ServiceManager.shared.postRequestWithThreeValues(url: serviceURL, parameters: dic, completion: { (status, message, response) in

            if status ?? false {
                var cartItem = [String:Any]()
                var localCartItem = [[String : Any]]()
                if var data = response?.value(forKey: "data") as? [String : Any]{
                    print(data)
                    cartItem["isCouponApplied"] = false
                    var localCartCount = 0
                    var localcartTotal = 0.0
                    let hallid = data["hall_id"] as? Int
                    for items in data["items"] as? [[String : Any]] ?? [[String : Any]](){
                        localCartCount += (items["qty"] as? Int) ?? 0
                        localcartTotal += (((items["price"] as? Double) ?? 0.0) * (Double((items["qty"] as? Int) ?? 0)))
                        var singleItem = [String:Any]()
                        var localcartVeriation = [String:Any]()
                        localcartVeriation["option_id"] = (items["variation"] as? [String:Any])?["option_id"] as? Int ?? 0
                        localcartVeriation["variation_serving"] = (items["variation"] as? [String:Any])?["variation_serving"] as? String ?? ""
                        localcartVeriation["variation_serving_title"] = (items["variation"] as? [String:Any])?["variation_serving_title"] as? String ?? ""
                        
                        singleItem["variationName"] = (items["variation"] as? [String:Any])?["variation_serving"] as? String ?? ""
                        singleItem["variationserving"] = (items["variation"] as? [String:Any])?["variation_serving_title"] as? String ?? ""
                        
                        singleItem["dish_id"] = items["dish_id"] as? Int ?? 0
                        singleItem["item_name"] = items["item_name"] as? String ?? ""
                        singleItem["qty"] = items["qty"] as? Int ?? 0
                        singleItem["veg"] = items["veg"] as? String ?? ""
                        singleItem["price"] = items["price"] as? Double ?? 0.0
                        singleItem["addons"] = items["addons"] as? [[String : Any]]
                        singleItem["Veriations"] = localcartVeriation
                        singleItem["options"] = items["options"] as? [[String : Any]]
                        singleItem["serving"] = items["serving"] as? [String : Any]
                        
                        localCartItem.append(singleItem)
                    }
                    print(localcartTotal)
                    cartItem["items"] = localCartItem
                    cartItem["cart_total"] = localcartTotal
                    cartItem["cart_count"] = localCartCount
                    cartItem["hall_id"] = hallid
                    print(cartItem)
                    UserModel.shared.cartItemsPrice = localcartTotal
                    UserModel.shared.cartItemsCount = localCartCount
                    UserDefaults.standard.removeObject(forKey: UserDefault.localCart)
                       
                    UserDefaults.standard.set(cartItem, forKey: UserDefault.localCart)
                       
                    
                }
                oncompletion(true, message ?? "", response ?? [:])
            } else {
                oncompletion(true, "", [:])
                Utility.shared.showAlertView(message: message ?? "")
            }
        })
    }
    
}
