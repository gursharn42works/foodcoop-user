//
//  OrderHistoryCell.swift
//  Magic
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol OrderHistoryDelegate {
    func reOrderTap(_ ordert : Order)
    func canceOrderTap(_ ordert : Order)

}

class OrderHistoryCell: UITableViewCell {
    // MARK: - IBoutlets
    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var orderDishName: UILabel!
    @IBOutlet weak var orderTotalPrice: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var hallName : UILabel!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var rateOrder: RobotoMediumButton!
    @IBOutlet weak var orderStatus: RegularLabel!
    @IBOutlet weak var order_delievered: UIImageView!
    @IBOutlet weak var cancellationView: UILabel!
    @IBOutlet weak var cancellationLabel: UILabel!

    // MARK: - Variables
    var delegate: OrderHistoryDelegate?
    var order = Order()
    var rateOrderClick: (()-> Void)?
    var cancelOrderClick: (()-> Void)?
    var pickupTitle = "Order Placed On: "
    //MARK:- Clausre
    var orderStatusClausre:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func config(_ order: Order, _ isOngoing : Bool) {
        self.order = order
        orderNo.text = order.number
        
        //GSD CHANGED  order.delieveryDate -> order.orderPlacedOn
        
        let del_date = order.delieveryDate.components(separatedBy: ",")
        let finaldate = "\(del_date[0])" + "," + "\(del_date[1])" + " " + "at" + "\(del_date[2])"
        print(finaldate)
        
        let attributedOrderDateString : NSAttributedString = pickupTitle.attributedString(fullText: finaldate, size: 12.0)
        orderDate.attributedText = attributedOrderDateString
        
        orderTotalPrice.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + order.totalPrice
        orderDishName.text = order.description
        //hallName.text = order.hall
        
        if !isOngoing
        {
//            thumbImage.isHidden = order.thumb == "" || order.status == OrderStatusEnum.cancelled.rawValue ? true : false
//            thumbImage.image = order.thumb == "up" ? #imageLiteral(resourceName: "Up") : #imageLiteral(resourceName: "Down")
            rateOrder.isHidden = order.thumb == "" && order.status != OrderStatusEnum.cancelled.rawValue ? false : true
            order_delievered.image = order.status == OrderStatusEnum.cancelled.rawValue ? #imageLiteral(resourceName: "cross") : #imageLiteral(resourceName: "Tick In colored").withRenderingMode(.alwaysOriginal)
            order_delievered.isHidden = false
            if "\(order.thumb)" == "up" && order.status == "delivered" {
                rateOrder.isHidden = true
            }
            if "\(order.thumb)" == "down" && order.status == "delivered" {
                rateOrder.isHidden = true
            }

        }else{
            if order.cancelRequest == 1 {
                cancellationLabel.isHidden = false
                cancellationLabel.text = Messages.cancelRequestSent
            }else{
                cancellationLabel.isHidden = true
            }
            
        }
        
        orderStatus.textColor = order.status == "cancelled" ? UIColor.red : AppColor.primaryColor
        var status =  order.status.replacingOccurrences(of: "_", with: " ")
        
        if order.order_type == Constant.shared.DELIVERABLE {
            if status == "ready" {
                status = "being prepared"
            }
            else if status == "picked up" {
                status = "out for delivery"
            }
        }

        orderStatus.text = status.camelCasedString
        
        if order.status == "cancelled"{
            orderStatus.textColor = UIColor.red//AppColor.primaryColor
        }else{
            orderStatus.textColor = AppColor.greenColor
            
        }
        
        if order.delivery_rating > 0 || order.order_rating > 0 {
            self.rateOrder.isHidden = true
        }
    }
    
    // MARK: - IBFunctions
    
    @IBAction func reOrderClicked(){
        self.delegate?.reOrderTap(self.order)
    }
    @IBAction func rateOrderClicked(){
        rateOrderClick?()
    }
    @IBAction func cancelOrderClicked(){
        self.cancelOrderClick?()
    }
    
    @IBAction func orderStatusTap() {
        orderStatusClausre?()
    }
}
