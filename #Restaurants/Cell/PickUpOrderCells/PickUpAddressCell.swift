//
//  PickUpAddressCell.swift
//  #Restaurants
//
//  Created by Satveer on 14/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class PickUpAddressCell: UITableViewCell {
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressBottomLbl: UILabel!
    @IBOutlet weak var homeIcon: UIImageView!
    @IBOutlet weak var addressBottomLblXPOs: NSLayoutConstraint!
    
    var delegate: DeliveryOptionsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDefaultAddressLabelData(_ address: DeliveryAddressModel,isValidDistance: Bool) {
        if address.title == "" {
            self.addressLbl.text = ""
            self.addressBottomLbl.text = Constant.shared.emptyAddressMsg
        } else {
            self.addressLbl.text = address.title.capitalizeFirstInEachWord()
            self.addressBottomLbl.text = "\(address.house_no), \(address.street1), \(address.street2), \(address.city.capitalized), \(address.state.capitalized)."
        }
    }
}
