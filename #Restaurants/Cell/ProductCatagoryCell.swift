//
//  ProductCatagoryCell.swift
//  Magic
//
//  Created by Apple on 8/16/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ProductCatagoryCell: UITableViewCell {

    @IBOutlet var catagoryName : UILabel!
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var arrowDown: UIImageView!
    @IBOutlet weak var nameLedaingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    func getTimeHeading(_ index : Int) -> String{
        let addedIndex = index
        let now = Date()
        var dateComponent = DateComponents()
        dateComponent.month = 0
        dateComponent.day = addedIndex
        dateComponent.year = 0
        let nextDate = Calendar.current.date(byAdding: dateComponent, to: now)
        if addedIndex == 0{
            return "Today"
        }
        else if addedIndex == 1{
            return "Tomorrow"
        }else{
            let dateToShow = nextDate?.getDate().convertDateFormat("yyyy-MM-dd", "dd MMM, EEE")
            return dateToShow ?? ""
        }
    }
    
}
