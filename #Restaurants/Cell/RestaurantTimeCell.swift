//
//  RestaurantTimeCell.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RestaurantTimeCell: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var tickImagView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
