//
//  AboutVC.swift
//  #Restaurants
//
//  Created by Satveer on 25/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class AboutVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func aboutTap(){

    }
    
    @IBAction func privacyPolicyTap(){
        let privacyPolicyVC = Router.viewController(with: .termsConditionVC, .login) as! TermsConditionVC
        privacyPolicyVC.isTerms = false
        self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }
    
    @IBAction func termsAndCondtionTap(){
        let privacyPolicyVC = Router.viewController(with: .termsConditionVC, .login) as! TermsConditionVC
        privacyPolicyVC.isTerms = false
        self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }
    
    @IBAction func back_Click(){
        self.navigationController?.popViewController(animated: true)
    }

}
