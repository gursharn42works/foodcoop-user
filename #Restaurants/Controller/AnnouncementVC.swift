//
//  AnnouncementVC.swift
//  #Restaurants
//
//  Created by Satveer on 25/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class AnnouncementVC: UIViewController {

    @IBOutlet weak var announcementLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    
    var titleStr = ""
    var messageStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showInfo()
    }
    
    @IBAction func hide(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func showInfo() {
        if titleStr.trim() != ""{
            if announcementLbl != nil {
                self.announcementLbl.text = titleStr
            }
        }
        self.messageLbl.text  = messageStr
    }
    

}
