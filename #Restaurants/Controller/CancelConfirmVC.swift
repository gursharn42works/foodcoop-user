//
//  CancelConfirmVC.swift
//  #Restaurants
//
//  Created by Satveer on 12/08/20.
//  Copyright © 2020 Apple1. All rights reserved.
//

import UIKit

protocol CancelConfirmVCDelegate: class {
    func hideOrderCancelView(order_id: String, reason: String, reasonType: String)
}

class CancelConfirmVC: BaseVC, UITextViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reasonTxtView: UITextView!

    var delegeate: CancelConfirmVCDelegate?
    
    var order_id = ""
    var selelectedReason = "I ordered by mistake."
    var cancelReasons = ["I ordered by mistake.", "I no longer wish to receive my order.", "My order is delayed.", "Others"]
    var placeholder = "Write your reason here..."
    var reasonType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reasonTxtView.isHidden = true
        self.reasonTxtView.delegate = self
    }
    
    @IBAction func yesBtn_Clcik(_ sender: UIButton) {
        
        var reason = self.selelectedReason
        if self.reasonTxtView.text! != placeholder && self.reasonTxtView.text! != "" {
            reason = self.reasonTxtView.text!
        }
         if self.reasonTxtView.text! == "" && reason == "Others" {
            Utility.shared.showAlertView(message: placeholder)
            return
        }
         if self.reasonTxtView.text! == placeholder && reason == "Others" {
            Utility.shared.showAlertView(message: placeholder)
            return
        }
        self.dismiss(animated: false, completion: nil)

        self.delegeate?.hideOrderCancelView(order_id: self.order_id, reason: reason, reasonType: reasonType)

    }
    
    @IBAction func noBtn_Clcik(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

}

extension CancelConfirmVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cancelReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelConfirmCell", for: indexPath) as! CancelConfirmCell
        
        cell.reasonLbl.text = self.cancelReasons[indexPath.row]
        if self.selelectedReason == self.cancelReasons[indexPath.row] {
            cell.radioImg.image = UIImage(named: "radioBtnSelected")
        } else {
            cell.radioImg.image = UIImage(named: "radioBtnUnselected")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selelectedReason = self.cancelReasons[indexPath.row]
        tableView.reloadData()
        self.reasonType = selelectedReason

        if selelectedReason == self.cancelReasons.last {
            self.reasonTxtView.isHidden = false
            self.reasonType = "Others"
        }else {
            self.reasonTxtView.isHidden = true
            self.self.reasonTxtView.text = placeholder
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text! ==  placeholder {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text! ==  "" {
            textView.text = placeholder
            textView.textColor = .lightGray
        }
    }
}

class CancelConfirmCell: UITableViewCell {
    @IBOutlet weak var radioImg: UIImageView!
    @IBOutlet weak var reasonLbl: UILabel!

}
