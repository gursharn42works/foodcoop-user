//
//  ConfirmOrderViewController.swift
//  Magic
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AddNewCardViewController: BaseVC {
	
	// MARK: - IBOutlets
	
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var cardNumberTextField: UITextField!
	@IBOutlet weak var expiryDateTextField: UITextField!
	@IBOutlet weak var cvvTextField: UITextField!
	@IBOutlet weak var zipcodeTextField: UITextField!
	@IBOutlet weak var cardImg: UIImageView!
	
	// MARK: - Variables
	lazy var creditCardValidator = CreditCardValidator()
	var cardSaved :(()->())?
	var savedCards = [CardModel]()

	var isCustomerCreated = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
		
	// MARK: - IBActions
    @IBAction func back_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
	@IBAction func addCardTap(_ sender: UIButton) {
		createToken()
	}
	
	@IBAction func cardNumberChanged(_ sender: UITextField) {
		if let number = sender.text {
			detectCardNumberType(number: number)
		}
	}
	
}
extension AddNewCardViewController{
	
	//First create token for adding new card and then add card
	
	func createToken() {
		
		let cardName = nameTextField.text?.trim()
		let cardNumber = cardNumberTextField.text?.trim()
		let expiryDate = expiryDateTextField.text?.trim()
		let cvv = cvvTextField.text?.trim() ?? ""
		
		for card in savedCards {
			if cardNumber?.contains(card.cardNumber) ?? false  {
                Utility.shared.showAlertView(message: "A card with same details is already saved.")
				return
			}
		}
		
		if cardName == ""{
            Utility.shared.showAlertView(message: Messages.emptyName)
			return
		}else if cardNumber == ""{
            Utility.shared.showAlertView(message: Messages.emptyCardNumber)

			return
		}else if expiryDate == ""{
            Utility.shared.showAlertView(message: Messages.emptyExpiryDate)
			return
		}else if cvv == ""{
            Utility.shared.showAlertView(message: Messages.emptyCvvCode)
			return
		}
		
		
		let expiryMonth = expiryDate?.split(separator: "/").first ?? ""
		let expiryYear = expiryDate?.split(separator: "/").last ?? ""
		let dic = ["card[number]": cardNumber ?? "",
				   "card[exp_month]": expiryMonth ,
				   "card[exp_year]": expiryYear ,
				   "card[cvc]": cvv] as [String : Any]
        Utility.shared.startLoading()
        ServiceManager.shared.postStripeRequest(url: "https://api.stripe.com/v1/tokens", params: dic as NSDictionary) { (status, message, dict) in
            Utility.shared.stopLoading()
			if status ?? false {
				let tokenID = dict?.value(forKey: "id") as? String ?? ""
				self.createCard(tokenID)
			} else {
                Utility.shared.showAlertView(message: message ?? "")

			}
		}
	}
	
	func createCard(_ tokenId: String) {
		if tokenId == "" {
			return
		}
        let customerId = UserModel.shared.stripe_id
		if customerId == "" {
			return
		}
		
		let dic = ["source":tokenId] as [String : Any]
        Utility.shared.startLoading()
		ServiceManager.shared.postStripeRequest(url: "https://api.stripe.com/v1/customers/\(customerId)/sources", params: dic as NSDictionary) { (status, message, dict) in
            Utility.shared.stopLoading()
			if status ?? false {
                _ = self.navigationController?.popViewController(animated: true)
				self.cardSaved?()
			} else {
				
                if self.isCustomerCreated == false { //message!.contains("No such customer")
					self.isCustomerCreated = true
                    //UserModel.shared.stripe_id = ""
					StripeApiModel().generateToken { (str) in
						self.createToken()
					}
				} else {
                    Utility.shared.showAlertView(message: message ?? "")
				}
			}
		}
	}
}

extension AddNewCardViewController : UITextFieldDelegate{
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		var range = range
		
		if textField == cardNumberTextField
		{
			if Int(range.location) == 19 {
				return false
			}
			
			let aSet = NSCharacterSet(charactersIn:"0123456789-").inverted
			let compSepByCharInSet = string.components(separatedBy: aSet)
			let numberFiltered = compSepByCharInSet.joined(separator: "")
			if string != numberFiltered {
				return false
			}
			
			//        // Reject appending non-digit characters
			//        if Int(range.length) == 0 && !CharacterSet.decimalDigits.characterIsMember(unichar(string[string.index(string.startIndex, offsetBy: 0)])) {
			//            return false
			//        }
			// Auto-add hyphen before appending 4rd or 7th digit
			
			if Int(range.length) == 0 && (Int(range.location) == 4 || Int(range.location) == 9 || Int(range.location) == 14) {
				textField.text = "\(textField.text ?? "")-\(string)"
				return false
			}
			
			// Delete hyphen when deleting its trailing digit
			
			if Int(range.length) == 1 && (Int(range.location) == 5 || Int(range.location) == 10 || Int(range.location) == 15) {
				range.location -= 1
				range.length = 2
				textField.text = (textField.text as NSString?)?.replacingCharacters(in: range, with: "")
				return false
			}
		}else if textField == expiryDateTextField
		{
			if Int(range.location) == 7 {
				return false
			}
			
			let aSet = NSCharacterSet(charactersIn:"0123456789-").inverted
			let compSepByCharInSet = string.components(separatedBy: aSet)
			let numberFiltered = compSepByCharInSet.joined(separator: "")
			if string != numberFiltered {
				return false
			}
			if Int(range.length) == 0 && (Int(range.location) == 2){
				textField.text = "\(textField.text ?? "")/\(string)"
				return false
			}
			// Delete hyphen when deleting its trailing digit
			if Int(range.length) == 0 && (Int(range.location) == 2) {
				range.location -= 1
				range.length = 2
				textField.text = (textField.text as NSString?)?.replacingCharacters(in: range, with: "")
				return false
			}
		}else if textField == zipcodeTextField || textField == cvvTextField{
			let aSet = NSCharacterSet(charactersIn:"0123456789-").inverted
			let compSepByCharInSet = string.components(separatedBy: aSet)
			let numberFiltered = compSepByCharInSet.joined(separator: "")
			if string != numberFiltered {
				return false
			}
			if textField == cvvTextField{
				if Int(range.location) == 4 {
					return false
				}
			}
		}else{
			let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
			let compSepByCharInSet = string.components(separatedBy: aSet)
			let numberFiltered = compSepByCharInSet.joined(separator: "")
			if string != numberFiltered {
				return false
			}
		}
		return true
	}
	
	//    func textFieldDidEndEditing(_ textField: UITextField) {
	//        if textField == cardNumberTextField{
	//            if textField.text?.count == 0{
	//                cardImg.image = #imageLiteral(resourceName: "defaultCard")
	//            }
	//        }
	//    }
	
	func detectCardNumberType(number: String) {
		if let type = creditCardValidator.type(from: number) {
			//            self.cardTypeLabel.text = type.name
			//            self.cardTypeLabel.textColor = UIColor.green
			//self.cardImg.isHidden = false
			self.cardImg.image = UIImage(named : type.image)
		} else {
			self.cardImg.image = #imageLiteral(resourceName: "defaultCard")
			// self.cardImg.isHidden = true
		}
	}
}
