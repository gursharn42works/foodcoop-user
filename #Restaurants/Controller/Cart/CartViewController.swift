//
//  CartViewController.swift
//  Magic
//
//  Created by Apple on 8/3/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WWCalendarTimeSelector
import BraintreeDropIn
import Braintree
import Stripe
import Razorpay

protocol CartViewControllerDeleagte: class {
    func updateValueOnBottomCartView()
}

class CartViewController: BaseVC,WWCalendarTimeSelectorProtocol,AddMobilePopUpVCDelegate,DeliveryOrderDetailVCDeleagte,CartMessageViewDelegate {
    
    func moveToHomeViewOnBrowseBtnClick() {
        
        if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
            let rootNC = UINavigationController(rootViewController: magicTabBar)
            rootNC.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
    }
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var cartTableHeight: NSLayoutConstraint!
    @IBOutlet weak var placeOrderBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var blurView: UIView!
    var indexRow = Int()
    fileprivate var today: Date = Date()
    var st = String()
    var d1 = String()
    var d2 = String()
    var cartVM = CartVM()
    var isFromDishDetailView = false
    var isFromReOrderView = false
    //MARK:- Variables
    lazy var cart = Cart()
    var expandedSectionHeaderNumber = 1
    lazy var couponDic = NSDictionary()
    var productDetail = DishDetailVC()
    var cardDetail = [CardModel]()
    var cardDetailFetched : Bool = false
    var setCardDefault : Bool = false
    var task = CancellableDelayedTask()
    var selectedTap : Int = 1
    var pickup : String = "immediate"
    var defaultCard : CardModel?
    var itemUnavailable : Bool?
    var stripeApiModel = StripeApiModel()
    var selectedPaymentCard : CardModel?
    var selectedCardFetched : Bool = false
    var hallClosed : Bool?
    var timingUpdated : Bool?
    var stringData = String()
    var unavailableCustomizations: [Int] = []
    
    var strdelegate = ""
    fileprivate var brainTreeClientToken = ""
    fileprivate var brainTreeNonce = ""
    
    var isOrderPickUp = false
    var delieveryAddress = DeliveryAddressModel()
    var isValidDistance = false
    var delegate: CartViewControllerDeleagte?
    var isFromBottomCartView = false
    var isFromreorderView = false
    
    var cartMessageView = CartMessageView()
    var paymentSheet: PaymentSheet?
    var restaurantAddressModel = RestaurantAddressModel()
    var paymentSheetFlowController: PaymentSheet.FlowController!
    
    // razor pay test id rzp_test_NVSMG8QKdPphRU
    var razorpay_Key = ""
    var razorpay: RazorpayCheckout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserModel.shared.paymentOption = .payOnline
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        placeOrderBtn.isExclusiveTouch = true
        if UserModel.shared.user_id != 0 {
            self.initializeData()
        }
        
        cartTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        
        addCartMessageView()
    }
    
    func addCartMessageView() {
        self.cartMessageView = CartMessageView(frame: CGRect(x: 0, y: 55, width: self.view.frame.size.width, height: self.view.frame.size.height-100))
        self.cartMessageView.delegate = self
        self.view.addSubview(cartMessageView)
        self.cartMessageView.isHidden = true
        
    }
    
    func callGetDefaultAddressApi() {
        //MARK: Get saved address from userdefault
        let savedAddress = DeliveryAddressModel().getSavedDeliveryAddress()
        if savedAddress.id == 0 {
            //MARK: If not any saved address then get from api
            self.cartVM.getDefaultAddressList { (defaultAddress) in
                self.delieveryAddress = defaultAddress
                DeliveryAddressModel().saveSelectedDeliveryAddress(address: defaultAddress)
                self.checkOrderIsPossible()
                self.cartTableView.reloadData()
              //  Utility.shared.startLoading()
                
            }
        } else {
            self.delieveryAddress = savedAddress
         //   Utility.shared.startLoading()
            
        }
        
        self.showNointernetView()
    }
    
    func checkDeliveryAddressAvailable()-> Bool {
        if self.delieveryAddress.id == 0 && isOrderPickUp == false  {
            return false
        } else {
            return true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchtxt = ""
        if UserModel.shared.isLoggedin {
            self.initializeData()
            UserModel.shared.card_id = ""
            self.reloadAPI(UIButton())
            UserModel.shared.isLoggedin = false
            
        }
        
        self.backBtn.isHidden = self.isFromDishDetailView || isFromBottomCartView || isFromReOrderView ? false:true
        
        
        self.callGetDefaultAddressApi()
        
    }
    
    func setCustomUI() {
        self.reloadAPI(UIButton())
        //        if self.getDelieveryAddress().id != nil && self.getDelieveryAddress().id != 0{
        //            self.addressvalidationApiCall()
        //        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkOrderIsPossible()
        self.cartTableView.reloadData()
        
        
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.cartTableView.isHidden = true
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.removeEmptyMsg(self.view)
            self.cartTableView.isHidden = false
            self.setCustomUI()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func loadList(notification: NSNotification){
        //load data here
        self.cartTableView.reloadData()
    }
    
    //MARK:- Inititialize
    
    fileprivate func initializeData(){
        
        let clientTokenURL = URL(string: ApiName.brainTreeClientToken)!
        var clientTokenRequest = URLRequest(url: clientTokenURL)
        clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")
        
        cartVM.getBrainTreeClientToken { (token) in
            self.brainTreeClientToken = token
        }
        
    }
    
    //MARK:- Functions
    
    func callStripeServices(){
        stripeApiModel.callApi()
        
        stripeApiModel.responseHandle = { card in
            self.cardDetail = card
            var defaultCardFound : Bool?
            
            for card in self.cardDetail{
                if card.cardId == UserModel.shared.card_id {
                    self.defaultCard = card
                    card.isDefault  = true
                    defaultCardFound = true
                }
            }
            
            if self.cardDetail.count > 0 && !(defaultCardFound ?? false){
                self.defaultCard = self.cardDetail[0]
                self.cardDetail[0].isDefault = true
                self.selectedPaymentCard = self.defaultCard
                self.updateProfileBackground(UserModel.shared.stripe_id,cardId: self.cardDetail[0].cardId)
            }
            
            if self.selectedPaymentCard == nil{
                self.selectedPaymentCard = self.defaultCard
            }
            
            self.sortCards()
            
            self.cartTableView.reloadData()
            self.cartTableView.layoutIfNeeded()
            self.cardDetailFetched = true
        }
    }
    
    //SORT CARDS
    fileprivate func sortCards(){
        
        var cards = [CardModel]()
        
        var defaultCard : CardModel?
        
        for card in self.cardDetail{
            
            if !card.isDefault{
                cards.append(card)
            }else{
                defaultCard = card
            }
        }
        
        if defaultCard != nil{
            cards.insert(defaultCard!, at: 0)
        }
        
        self.cardDetail = cards
    }
    
    func reloadTableView() {
    }
    
    func showEmptyMessage(bool: Bool) {
    }
    
    //    func addressvalidationApiCall() {
    //        // Add Address
    //        // check if restaurant deliever on entered location
    //
    //
    //        addressViewModel.responseRecieved = {[weak self](dic) in
    //            if let status = dic.value(forKey: "success") as? Bool{
    //                if status{
    //                    if let message = dic.value(forKey: "message") as? String{
    //                        if message == "Invalid Address"{
    //                            self?.isDefaultAddressAvailable = false
    //                        }else{
    //                            self?.isDefaultAddressAvailable = true
    //                        }
    //                    }
    //                    self?.checkOrderIsPossible()
    //                }else{
    //                    Utility.shared.showAlertView(message: dic.value(forKey: "message") as? String ?? "")
    //                }
    //            }
    //        }
    //
    //        //let delieveryAddress = self.getDelieveryAddress()
    //
    //        //let addressString = "\(delieveryAddress.apartment ?? ""), \(delieveryAddress.street1 ?? ""), \(delieveryAddress.street2 ?? ""), \(delieveryAddress.state ?? ""), \(delieveryAddress.city ?? ""), \(delieveryAddress.zipcode ?? "")"
    //        //FIXME:- //GSD addressViewModel.validateAddress = addressString
    //
    //    }
    
    func initialise(){
        cartTableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        cartTableView.register(UINib(nibName: Constant.shared.cartCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.cartCell)
        cartTableView.register(UINib(nibName: Constant.shared.applyCouponCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.applyCouponCell)
        cartTableView.register(UINib(nibName: Constant.shared.cartTotalCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.cartTotalCell)
        cartTableView.register(UINib(nibName: Constant.shared.specialInstructionCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.specialInstructionCell)
        cartTableView.register(UINib(nibName: Constant.shared.pickUpTimeCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.pickUpTimeCell)
        cartTableView.register(UINib(nibName: Constant.shared.applyCouponHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.applyCouponHeaderCell)
        cartTableView.register(UINib(nibName:
                                        Constant.shared.defaultCardCell, bundle: nil), forCellReuseIdentifier: Constant.shared.defaultCardCell)
        cartTableView.register(UINib(nibName:
                                        Constant.shared.deliveryOptionsCell, bundle: nil), forCellReuseIdentifier: Constant.shared.deliveryOptionsCell)
        cartTableView.register(UINib(nibName:
                                        Constant.shared.cartAddressCell, bundle: nil), forCellReuseIdentifier: Constant.shared.cartAddressCell)
    }
    
    fileprivate func deleteItem(_ dishName : String,_ item: [Int], index: Int) {
        
        // Delete item from local cart
        
        if self.cart.arr.count > index{
            var carItemArr = self.cart.arr[0].data as? [CartItem]
            carItemArr?.remove(at: index)
            if var localCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]
            {
                if var cartItems = localCart["items"] as? [[String : Any]]
                {
                    cartItems.remove(at: index)
                    localCart["items"] = cartItems
                }
                let cartItems = localCart["items"] as? [[String : Any]]
                if (cartItems?.count) ?? 0 > 0
                {
                    UserDefaults.standard.set(localCart, forKey: UserDefault.localCart)
                }else{
                    UserDefaults.standard.set("", forKey: UserDefault.specialInstruction)
                    UserDefaults.standard.removeObject(forKey: UserDefault.localCart)
                }
                self.cart.arr.removeAll()
                self.placeOrderBtn.isHidden = self.cart.arr.count <= 3 ? true : false
                self.cart.getData(localCart as NSDictionary? ?? NSDictionary())
                UserModel.shared.cartItemsCount = self.cart.cartCount
                UserModel.shared.cartItemsPrice = self.cart.cartTotal
                self.getLocalCartValuesService()
                self.cartTableView.reloadData()
            }
        }
        
        if let cartItems = self.cart.arr[0].data as? [CartItem]{
            itemUnavailable =  self.checkDishAvailability(cartItems)
        }
        
        Utility.shared.showAlertView(message: Messages.cartItemRemoved)
        
        self.cartTableView.reloadData()
    }
    
    private func checkDishAvailability(_ cartItems : [CartItem]) -> Bool{
        var isSomeItemUnavailable : Bool = false
        for item in cartItems{
            if item.status == false{
                isSomeItemUnavailable = true
                return isSomeItemUnavailable
            }
        }
        return isSomeItemUnavailable
    }
    
    
    private func CheckHallStatus() -> Bool {
        var isHallClosed = false
        self.checkRstaurantClosed(timing: UserModel.shared.hallTimings) { (success) in
            isHallClosed = success
        }
        return isHallClosed
    }
    
    private func checkOrderIsPossible() {
        // order can be placed if there is no ongoing order
        // hall is not closed
        // hall is accepting order at current time
        // all the dishes in the cart are available
        
        let isHallClosed = CheckHallStatus()
        let isHallAcceptingOrder = self.cart.isAcceptingOrder
        var dishesUnavailable = false
        if self.cart.arr.count > 0 , let cartItems = self.cart.arr[0].data as? [CartItem]{
            dishesUnavailable = checkDishAvailability(cartItems)
        }
        
        if UserModel.shared.ongoingOrders.count > 4{
            self.placeOrderBtn.backgroundColor = AppColor.primaryColor
        }else if self.checkDeliveryAddressAvailable() == false {
            self.placeOrderBtn.backgroundColor = UIColor.lightGray
        }else if isHallClosed{
            self.placeOrderBtn.backgroundColor = UIColor.lightGray
            self.placeOrderBtn.setTitle(
                "RESTAURANT IS CLOSED", for: .normal)
            self.hallClosed = true
        }else if !(isHallAcceptingOrder ?? true){
            self.placeOrderBtn.backgroundColor = UIColor.lightGray
        }else if dishesUnavailable{
            self.placeOrderBtn.backgroundColor = UIColor.lightGray
            self.itemUnavailable = true
        }else if UserModel.shared.user_id == 0 {
            self.placeOrderBtn.backgroundColor = UIColor.lightGray
        }
        else{
            self.placeOrderBtn.backgroundColor = AppColor.primaryColor
        }
        
        if isHallClosed {
            self.placeOrderBtn.setTitle("RESTAURANT IS CLOSED", for: .normal)
        } else {
            let placeOrderTitle = UserModel.shared.user_id == 0 ? "LOGIN TO PLACE ORDER" : "PLACE ORDER"
            self.placeOrderBtn.setTitle(placeOrderTitle, for: .normal)
        }
        
    }
    
    func callBottomViewMethonOnBack() {
        self.delegate?.updateValueOnBottomCartView()
    }
    
    @IBAction func back_Click(_ sender: UIButton){
        self.delegate?.updateValueOnBottomCartView()
        
        if self.isFromDishDetailView {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: FullMenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- IBAction
    @IBAction func placeOrderTap() {
        
        let loginStatus = self.checkLoginStatus()
        if !loginStatus{
            return
        }
        
        if self.isValidDistance == false && self.isOrderPickUp == false && self.delieveryAddress.id != 0 {
            Utility.shared.showAlertView(message: Messages.orderCannotDeliver)
            return
        }
        if UserModel.shared.ongoingOrders.count > 4 {
            Utility.shared.showAlertView(message: Messages.ongoingOrder)
            return
        }
        else if self.checkDeliveryAddressAvailable() == false {
            Utility.shared.showAlertView(message: Messages.emptyDeliverAddress)
            return
        }else if hallClosed ?? false{
            
            if let restaurantTimeVC = Router.viewController(with: .restaurantTimeVC, .product) as? RestaurantTimeVC {
                restaurantTimeVC.modalPresentationStyle = .overCurrentContext
                restaurantTimeVC.hallTiming = UserModel.shared.hallTimings
                self.window?.rootViewController?.present(restaurantTimeVC, animated: false, completion: nil)
                
            }
            return
        }else if !(self.cart.isAcceptingOrder ?? true){
            Utility.shared.showAlertView(message: Messages.restaurantNotAcceptingOrder)
            return
        }else if cardDetailFetched == false{
            Utility.shared.showAlertView(message: Messages.pleaseWait)
        }
//        else if cardDetail.count == 0 && UserModel.shared.paymentOption == .payOnline {
//            Utility.shared.showAlertView(message: Messages.noCards)
//        }
        else if itemUnavailable ?? false{
            Utility.shared.showAlertView(message: Messages.dishUnavailableUserMessage)
        }
        //        }else if self.getDelieveryAddress().id == 0 || self.getDelieveryAddress().id == nil{
        //            Utility.shared.showAlertView(message: Messages.noAddressAdded)
        //            return
        //        }
        else{
            if UserModel.shared.phone == ""  {
                if let addMobilePopUpVC = Router.viewController(with: .addMobilePopUpVC, .profile) as? AddMobilePopUpVC {
                    addMobilePopUpVC.delegate = self
                    addMobilePopUpVC.modalPresentationStyle = .overCurrentContext
                    self.present(addMobilePopUpVC, animated: true, completion: nil)
                }
                return
            }
            
            taxesInfoTap(true)
        }
    }
    
    func placeOrderAfterMobileAdded() {
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.taxesInfoTap(true)
        }
    }
    
    
    fileprivate func checkLoginStatus() -> Bool{
        if UserModel.shared.user_id == 0 {
            UserModel.shared.login_Skipped = true
            let loginController = Router.viewController(with: .loginVC, .login) as! LoginVC
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(loginController , animated: true)
            return false
        }
        
        return true
    }
    
    func taxesInfoTap(_ orderPlaced : Bool) {
        
        let popController = Router.viewController(with: .taxesInfo, .cart) as! TaxesInfoViewController
        
        popController.shouldShowThemeColor = !orderPlaced
        
        popController.modalPresentationStyle = .overFullScreen
        popController.cancelTap = {
            self.blurView.isHidden = true
        }
        
        print("self.cart.taxDescription: \(self.cart.taxDescription)")
        
        popController.taxesInfo = self.cart.taxDescription
        popController.placingOrder = orderPlaced
        
        self.window?.rootViewController?.present(popController, animated: true, completion: nil)
        self.blurView.isHidden = false
        //  self.addressvalidationApiCall()
        
        self.getRestaurantTimes { (timings, success) in
            if success {
                self.checkOrderIsPossible()
                self.cartTableView.reloadData()
                popController.placeOrderClosure = {
                    if self.itemUnavailable ?? false{
                        Utility.shared.showAlertView(message: Messages.dishUnavailableUserMessage)
                    }else if self.CheckHallStatus() { //self.hallClosed ?? false
                        let popController = Router.viewController(with: .restaurantTimeVC, .product) as! RestaurantTimeVC
                        
                        popController.modalPresentationStyle = .overFullScreen
                        popController.hallName = "Restaurant is Closed"
                        self.window?.rootViewController?.present(popController, animated: false, completion: nil)
                        return
                    }else if !(self.cart.isAcceptingOrder ?? true){
                        Utility.shared.showAlertView(message: Messages.restaurantNotAcceptingOrder)
                        return
                    }
                    else{
                        if UserModel.shared.paymentOption == .paypal{
                            DispatchQueue.main.async {
                                self.showDropIn(clientTokenOrTokenizationKey: self.brainTreeClientToken)
                            }
                        }
                        else{
                            self.placeOrderApi()
                        }
                        
                    }
                }
            }else{
                print("TEST Current implementation")
            }
        }
    }
    
    //MARK:- Observers
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        cartTableHeight.constant = cartTableView.contentSize.height
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        //        if cartTableView.observationInfo != nil {
        //            cartTableView.removeObserver(self, forKeyPath: "contentSize")
        //        }
        self.view.endEditing(true)
    }
    
    
    override func reloadAPI(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            //            Utility.shared.startLoading()
            initialise()
            cardDetailFetched = false
            placeOrderBtn.isEnabled = true
            self.removeEmptyMsg(self.view)
            self.cart.arr.removeAll()
            var localCartDic = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]
            
            //            print(localCartDic)
            if let couponCode = self.cart.couponCode{
                if couponCode.trim() != ""{
                    localCartDic?["coupon_code"] = couponCode
                }
            }
            
            self.cart.getData(localCartDic as NSDictionary? ?? NSDictionary())
            callStripeServices()
            self.getLocalCartValuesService()
            self.cartTableView.reloadData()
        }
    }
    
}

extension CartViewController: UITableViewDataSource ,UITableViewDelegate,CartTotalCellDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return  cart.arr.count <= 3 ? 0 : cart.arr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch cart.arr[section].type {
        case .cartItem:
            return ((cart.arr[section].data as? [CartItem])?.count) ?? 0
        case .cartTotal:
            return 1
        case .specialInstruction:
            return 1
        case .pickUptime:
            return 0//FIXME:- //GSD Check 1
        case .coupon:
            return (UserModel.shared.user_id == 0) ? 0 : (self.expandedSectionHeaderNumber == section ? 1 : 0)
        case .deliveryOptions:
            return 1
        case .address:
            return 1
        case .defaultCard:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = cart.arr[indexPath.section].type
        switch type {
        case .cartItem:
            let cartCell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartCell, for: indexPath) as! CartCell
            let cartItems = cart.arr[indexPath.section].data as? [CartItem] ?? [CartItem()]
            
            cartCell.vwSelectedItems.isHidden = indexPath.row > 0
            
            cartCell.config(cartItems[indexPath.row] as CartItem,unavailableCustomizations: unavailableCustomizations)
            cartCell.deleteItem = {
                self.deleteItem(cartItems[indexPath.row].dishName, [cartItems[indexPath.row].id], index: Int(indexPath.row))
            }
            cartCell.updateQuantity = { cartItem in
                
                if var localCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]
                {
                    if var cartItems = localCart["items"] as? [[String : Any]]
                    {
                        if var item = cartItems[indexPath.row] as? [String : Any]{
                            item["price"] = (Double(cartItem.price) ?? 0.0)
                            item["qty"] = (Int(cartCell.valueTxtField.text ?? "") ?? 0)
                            cartItems.remove(at: indexPath.row)
                            cartItems.insert(item, at: indexPath.row)
                        }
                        
                        localCart["items"] = cartItems
                        
                    }
                    UserDefaults.standard.set(localCart, forKey: UserDefault.localCart)
                    self.cart.arr.removeAll()
                    self.cart.getData(localCart as NSDictionary? ?? NSDictionary())
                    UserModel.shared.cartItemsCount = self.cart.cartCount
                    UserModel.shared.cartItemsPrice = self.cart.cartTotal
                    //self.cartValueUpdate(self.tabBarController ?? MagicTabBar())
                    
                    self.getLocalCartValuesService()
                    self.cartTableView.reloadData()
                }
            }
            return cartCell
            
        case .coupon:
            let couponCell = tableView.dequeueReusableCell(withIdentifier: "applyCouponCell", for: indexPath) as! applyCouponCell
            
            couponCell.couponTextField.text = self.cart.couponCode ?? ""
            couponCell.couponTap = { couponCode in
                if couponCode.length == 0 {
                    Utility.shared.showAlertView(message: Messages.noCouponCode)
                    return
                }
                if couponCell.applyCouponBtn.title(for: .normal) == "APPLY"
                {
                    self.cart.couponCode = couponCell.couponTextField.text
                    self.getLocalCartValuesService()
                }else{
                    self.removeLocalCoupon()
                }
            }
            
            if self.cart.isCouponApplied ?? false
            {
                if couponCell.applyCouponBtn.titleLabel?.text ?? "" != "REMOVE"{
                    self.view.makeToast(Messages.couponApplied, duration: 2.0, point: self.view.center, title: nil, image: nil) { didTap in
                    }
                }
                
                couponCell.applyCouponBtn.setTitle("REMOVE", for: .normal)
            }
            else{
                
                couponCell.applyCouponBtn.setTitle("APPLY", for: .normal)
            }
            return couponCell
            
        case .specialInstruction:
            let specialInstructionCell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.specialInstructionCell, for: indexPath) as! SpecialInstructionCell
            
            specialInstructionCell.specialInstructionTF.placeholderColor =  AppColor.placeHolderColor
            
            specialInstructionCell.specialInstruction = {instructions in
                self.cart.specialInstruction = instructions.trim()
            }
            return specialInstructionCell
        case .pickUptime:
            let Cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.pickUpTimeCell, for: indexPath) as! PickUpTimeCell
            Cell.btnSelect.addTarget(self, action: #selector(CartViewController.onClickedTapButton(_:)), for: .touchUpInside)
            Cell.btnSelect.tag = indexPath.row
            Cell.selectdatetimeTf.text = stringData
            self.indexRow = indexPath.row
            Cell.selectTimeLater = {[weak self] in
                self?.onClickedTapButton(nil)
            }
            return Cell
            
        case .cartTotal:
            let cartTotalCell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartTotalCell, for: indexPath) as! CartTotalCell
            cartTotalCell.config(self.cart,isOrderPickUp: isOrderPickUp)
            cartTotalCell.delegate = self
            cartTotalCell.deliveryChargesView.isHidden = self.isOrderPickUp   ? true : false
            
            if self.delieveryAddress.title == "" || self.isValidDistance == false {
                cartTotalCell.deliveryChargesView.isHidden = true
            }
            //cartTotalCell.dividerLineView.isHidden = true
            return cartTotalCell
            
        case .defaultCard:
            if cellTappedcard == true {
                let defaultCardCell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.defaultCardCell, for: indexPath) as! DefaultCardCell
                defaultCardCell.delegate = self
                defaultCardCell.activityIndicator.startAnimating()
                defaultCardCell.change_addCardBtn.isHidden = true
                if self.cardDetailFetched{
                    defaultCardCell.activityIndicator.stopAnimating()
                    defaultCardCell.change_addCardBtn.isHidden = false
                }
                
                if self.cardDetail.count < 1
                {
                    defaultCardCell.change_addCardBtn.setTitle("ADD CARD", for: .normal)
                    defaultCardCell.cardNumber.text = "XXXX-XXXX-XXXX-XXXX"
                    defaultCardCell.cardImage.image = #imageLiteral(resourceName: "defaultCard")
                }else{
                    defaultCardCell.change_addCardBtn.setTitle("CHANGE", for: .normal)
                    defaultCardCell.config(self.selectedPaymentCard ?? CardModel())
                    defaultCardCell.cardImage.backgroundColor = UIColor.clear
                }
                
                return defaultCardCell
            }else{
                let dummy = UITableViewCell()
                dummy.selectionStyle = .none
                return dummy
            }
        case .deliveryOptions:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.deliveryOptionsCell, for: indexPath) as! DeliveryOptionsCell
            cell.delegate = self
            if self.isOrderPickUp {
                cell.addressBottomLblXPOs.constant = -27
                cell.setRestaurnatAddressLabelData(self.restaurantAddressModel)
                cell.orderPickUpBtn.isSelected = true
                cell.orerDeliveryBtn.isSelected = false

            } else {
                cell.orderPickUpBtn.isSelected = false
                cell.orerDeliveryBtn.isSelected = true
                cell.setDefaultAddressLabelData(delieveryAddress, isValidDistance: self.isValidDistance)
                cell.orderPickUpBtn.isSelected = self.isOrderPickUp
                cell.addBtn.isHidden = self.isOrderPickUp
                cell.homeIcon.isHidden = self.isOrderPickUp
                if self.delieveryAddress.title == "" {
                    cell.inValidAdressLbl.isHidden = true
                    cell.homeIcon.isHidden = true
                    cell.addressBottomLblXPOs.constant = -27
                } else {
                    cell.addressBottomLblXPOs.constant = 12
                }
            }
            return cell
        case .address:
            let addressCell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartAddressCell, for: indexPath) as! CartAddressCell
            addressCell.isOrderPickUp  = self.isOrderPickUp
            addressCell.config()
            
            if self.isOrderPickUp == false {
                addressCell.payAtStoreLbl.text = "Cash on Delivery"
            } else {
                addressCell.payAtStoreLbl.text = "Pay at Store"
            }
            
            return addressCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let type = cart.arr[indexPath.section].type
        return type == .cartItem
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextItem = UIContextualAction(style: .destructive, title: "REMOVE") {  (contextualAction, view, boolValue) in
            //Code I want to do here
            let cartItems = self.cart.arr[indexPath.section].data as? [CartItem] ?? [CartItem()]
            
            self.deleteItem(cartItems[indexPath.row].dishName, [cartItems[indexPath.row].id], index: Int(indexPath.row))
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem])
        
        return swipeActions
    }
    
    
    @objc func onClickedTapButton(_ sender: Any?) {
        print("Tapped")
        let selector = WWCalendarTimeSelector.instantiate()
        
        selector.delegate = self
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        present(selector, animated: true, completion: nil)
        
    }
    
    func openTaxInfoPopUp() {
        self.taxesInfoTap(false)
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        
        print(pickUpTimeStr)
        if strdelegate != "First"
        {
            let for1 = DateFormatter()
            for1.dateFormat = "yyyy-MM-dd"
            d1 = for1.string(from: date)
            print(d1)
            for1.dateFormat = "MMM dd, yyyy"
            st = for1.string(from: date)
            print(st)
        }
        
        let for2  = DateFormatter()
        for2.dateFormat = "HH:mm:ss"
        d2 = for2.string(from: date)
        print(d2)
        for2.dateFormat = "HH:mm:ss"
        let datetoComp = for2.date(from: d2)
        for2.dateFormat = "h:mm a"
        let dateString = for2.string(from: datetoComp!)
        print(dateString)
        
        if strdelegate == "First"
        {
            selector.delegate = nil
            stringData = "\(st) \(dateString)"
            pickUpTimeStr = "\(d1) \(d2)"
            print(pickUpTimeStr)
            
            cartTableView.reloadData()
        }else{
            DispatchQueue.main.async {
                
                let selector = WWCalendarTimeSelector.instantiate()
                selector.delegate = self
                selector.optionStyles.showDateMonth(false)
                selector.optionStyles.showMonth(false)
                selector.optionStyles.showYear(false)
                selector.optionStyles.showTime(true)
                self.present(selector, animated: true, completion: nil)
                self.strdelegate = "First"
            }
            
        }
    }
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool{
        let order = NSCalendar.current.compare(today, to: date, toGranularity: .day)
        if order == .orderedDescending{
            return false
        } else {
            if date > Calendar.current.date(byAdding: .day, value: 5, to:today)!{
                return false
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        if isOrderPickUp && indexPath.section == 5 {
//            return 85
//        }
        if indexPath.section == 6 {
           return 80
        }
        
        let type = cart.arr[indexPath.section].type
        
        if type == .defaultCard  && UserModel.shared.paymentOption != .payOnline {
            return 10
        }
        
        tableView.estimatedRowHeight = 65
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("SELECTED CELL: \(String(describing: tableView.cellForRow(at: indexPath)))")
        
        if cart.arr.count <= indexPath.section{
            return
        }
        
        let type = cart.arr[indexPath.section].type
        if type == .cartItem{
            let cartItems = cart.arr[indexPath.section].data as? [CartItem] ?? [CartItem]()
            let item = cartItems[indexPath.row]
            
            //let prodctDetailVC = UIStoryboard(name: StoryboardName.productDetail, bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            
            let prodctDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC
            
            prodctDetailVC?.dish_id = Int(item.dishId)
            prodctDetailVC?.viewtitle = item.dishName.uppercased()
            prodctDetailVC?.cartAddOns = item.addOns
            prodctDetailVC?.cartCustomOptions = item.customOptions
            prodctDetailVC?.hallId = cart.hallId
            prodctDetailVC?.isFromCartScreen = true
            prodctDetailVC?.cartCountUpdated = { count in
                if self.cart.couponCode != ""{
                    //self.ApplyCouponService(self.cart.couponCode ?? "",showLoader: false)
                }
                self.reloadAPI(UIButton())
            }
            prodctDetailVC?.itemCount = Int(item.qty) ?? 0
            prodctDetailVC?.isLocalCartUpdate = true
            prodctDetailVC?.localCartItemIndex = indexPath.row
            prodctDetailVC?.cartServing = item.serving
            prodctDetailVC?.variationName = item.variationName ?? ""
            prodctDetailVC?.variationService = item.variationserving ?? ""
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(prodctDetailVC ?? DishDetailVC(), animated: true)
        }
    }
    //MARK:- Table View Header
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: Constant.shared.applyCouponHeaderCell) as! ApplyCouponHeaderCell
        headerView.backgroundColor = UIColor.white
        headerView.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(sectionHeaderWasTouched(_:)))
        headerView.addGestureRecognizer(headerTapGesture)
        if section == self.expandedSectionHeaderNumber {
            headerView.arrowImageView.image =  #imageLiteral(resourceName: "coloredDownArrow")
        }else{
            headerView.arrowImageView.image = #imageLiteral(resourceName: "up_arrow")
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 && (UserModel.shared.user_id != 0) ? 44 : 0
    }
    
    @objc fileprivate func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        
        let headerView = sender.view as! ApplyCouponHeaderCell
        let section = headerView.tag
        
        if self.expandedSectionHeaderNumber == section {
            self.expandedSectionHeaderNumber = -1
            headerView.arrowImageView.image = #imageLiteral(resourceName: "up_arrow")
        } else {
            self.expandedSectionHeaderNumber = section
        }
        cartTableView.reloadData()
        cartTableView.layoutIfNeeded()
        
        //test
        
        //        for sectionItem in cart.arr{
        //            print("sectionItem : \(sectionItem)")
        //        }
        
    }
    
    //MARK:- Table View Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        footerView.backgroundColor = AppColor.background
        
        if section == cart.arr.count - 1 || section == cart.arr.count - 2 {
            footerView.backgroundColor = UIColor.white
        }
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5{
            return 0
            
        }else{
            return 5
        }
    }
}

extension CartViewController : DefaultCardDelegate, DeliveryOptionsCellDelegate {
    
    func addBtnClick(isToAdd: Bool) {
        
        if UserModel.shared.user_id == 0 {
            self.login_Click()
            return
        }
        if isToAdd {
            let vc = Router.viewController(with: .addDeliveryAddressVC, .cart) as! AddDeliveryAddressVC
            vc.isFromCartView = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = Router.viewController(with: .deliveryAddressVC, .cart) as! DeliveryAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func deliveryOptionType(type: String)  {
        if type == "pickup" {
            isOrderPickUp = true
        } else {
            isOrderPickUp = false
        }
        
       // Utility.shared.startLoading()
        
        self.getLocalCartValuesService()
    }
    
    func changeCardTap(){
        
        let loginStatus = self.checkLoginStatus()
        
        if !loginStatus{
            return
        }
        
        if self.cardDetail.count == 0
        {
            //let confirmOrder = UIStoryboard(name: StoryboardName.cart, bundle: nil).instantiateViewController(withIdentifier: "AddNewCardViewController") as? AddNewCardViewController
            
            let confirmOrder = Router.viewController(with: .addNewCard, .cart) as? AddNewCardViewController
            confirmOrder?.cardSaved = {
                self.reloadAPI(UIButton())
            }
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(confirmOrder ?? UIViewController(), animated: true)
        }else
        {
            //let confirmOrder = UIStoryboard(name: StoryboardName.cart, bundle: nil).instantiateViewController(withIdentifier: "SavedCardsViewController") as? SavedCardsViewController
            
            let confirmOrder = Router.viewController(with: .savedCards, .cart) as? SavedCardsViewController
            
            confirmOrder?.savedCards = self.cardDetail
            confirmOrder?.selectedPaymentCard = self.selectedPaymentCard
            confirmOrder?.cardValueUpdated = {selectedPaymentCard in
                self.selectedPaymentCard = selectedPaymentCard
                self.reloadAPI(UIButton())
            }
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(confirmOrder ?? UIViewController(), animated: true)
        }
    }
}

extension CartViewController{
    //MARK:- Service
    
    //MARK:- local Cart Apis
    func getLocalCartValuesService() {
        print(cart.cartTotal)
        let localCartDic = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        let hallid = localCartDic["hall_id"] as? Int ?? 0
        print()
        let url = ApiName.localCartTotal
        var dic = [String : Any]()
        let itemDic = self.getCartItemsDic()
        let jsonData = try? JSONSerialization.data(withJSONObject: itemDic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        dic["items"] = jsonString
        dic["hall_id"] = hallid//UserModel.shared.hall_id//UserDefaults.standard.string(forKey: "hallid") ?? ""//GSD 2 Changed hall id to dynamic
        dic["coupon_code"] = self.cart.couponCode?.trim() ?? ""
        dic["user_id"] = UserModel.shared.user_id
        dic["address_id"] = self.delieveryAddress.id
        dic["state"] = self.delieveryAddress.state
        
        if isOrderPickUp == false {
            dic["order_type"] = "DELIVERABLE"
        } else {
            dic["order_type"] = ""
        }
        
        print(itemDic)
        print(dic)
        ServiceManager.shared.postRequestWithThreeValues(url: url, parameters: dic) { (status, message, response) in
            
            if UserModel.shared.paymentOption != .payOnline {
                Utility.shared.stopLoading()
            }
            if status ?? false{
                self.cartTableView.isHidden = false
                if let data = response?.value(forKey: "data") as? NSDictionary{
                    self.cart.couponDiscount = (String(format: "%.2f", (data.value(forKey: "discount") as? Double) ?? 0.0))
                    self.cart.isCouponApplied = self.cart.couponDiscount != "0.00" ? true : false
                    self.cart.tax = (String(format: "%.2f", (data.value(forKey: "tax") as? Double) ?? 0.0))
                    self.cart.taxDescription = TaxDescription().getCartItemAddons(data.value(forKey: "taxes") as? NSArray ?? NSArray())
                    self.cart.grandTotal = (String(format: "%.2f", (data.value(forKey: "grand_total") as? Double) ?? 0.0))
                    
                    if let charges = data["delivery_charges"] as? String {
                        self.cart.delivery_charges = charges
                    }
                    else if let charges = data["delivery_charges"] as? Double {
                        self.cart.delivery_charges = String(format: "%.2f", charges)
                    } else {
                        self.cart.delivery_charges = "0"
                    }
                    
                    self.isValidDistance = data["distance_valid"] as? Bool ?? false
                    self.cart.order_type = data["order_type"] as? String ?? ""
                    
                    self.cart.totalPrice = (String(format: "%.2f", data.value(forKey: "total") as? Double ?? 0.0))
                    self.cart.isAcceptingOrder = data.value(forKey: "is_accepting_orders") as? String == "yes" ? true : false
                    let unAvailableDishesIdArr = data.value(forKey: "unavailable") as? [Int]
                    //unavailableCustomizations
                    let unavailableCustomizati = data.value(forKey: "unavailableCustomizations") as? [Int]
                    self.unavailableCustomizations = unavailableCustomizati ?? []
                    self.restaurantAddressModel = RestaurantAddressModel.setRestaurntAddressData(dic: data["rest"] as? [String: Any] ?? [:])
                    if self.cart.arr.count > 0 && (unAvailableDishesIdArr?.count ?? 0) > 0{
                        let cartItemArr = self.cart.arr[0].data as? [CartItem] ?? [CartItem]()
                        
                        for i in 0..<cartItemArr.count{
                            for id in unAvailableDishesIdArr ?? [Int](){
                                if cartItemArr[i].dishId == id{
                                    cartItemArr[i].status = false
                                }
                            }
                        }
                    }
                    
                    self.razorpay_Key = data["razor_key_id"] as? String ?? ""
                    
                    self.checkOrderIsPossible()
                    self.cartTableView.reloadData()
                    self.placeOrderBtn.isHidden = self.cart.arr.count <= 3 ? true : false
                    self.cartMessageView.isHidden = true
                    if self.cart.arr.count <= 3{
                        
                        
                        self.cartMessageView.isHidden = false
                        //  self.showEmptyScreenMessage(Messages.noCartHeading, Messages.noCartItem, self.view)
                    }else{
                        self.removeEmptyMsg(self.view)
                    }
                }
            }else{
                self.cartMessageView.isHidden = true
                if message == Messages.noInternetConnection {
                    self.removeEmptyMsg(self.view)
                    self.cart.arr.removeAll()
                    self.cartTableView.reloadData()
                    self.cartTableView.isHidden = true
                    self.placeOrderBtn.isHidden = true
                    self.messageWithButton(Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                    
                }else{
                    
                    self.view.makeToast(message, duration: 3.0, point: self.view.center, title: nil, image: nil) { didTap in
                    }
                    
                    //GSD:- Disabled emptying cart for errors.
                    
                    self.cart.couponCode = ""
                    
                    DispatchQueue.main.async {
                        self.cartTableView.reloadData()
                    }
                    
                    return
                    
                    /*self.cart.arr.removeAll()
                     self.cartTableView.reloadData()
                     self.placeOrderBtn.isHidden = true
                     self.showTwoLineEmptyScreen(AppMessages.noCartHeading, message2: AppMessages.noCartItem, superView: self.view)*/
                }
            }
        }
    }
    
    func applyLocalCoupon(){
        var savedCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [String : Any]()
        savedCart["coupon_code"] = self.cart.couponCode ?? ""
        savedCart["discount"] = self.cart.couponDiscount
        UserDefaults.standard.set(savedCart, forKey: UserDefault.localCart)
    }
    
    func removeLocalCoupon(){
        var savedCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [String : Any]()
        savedCart["coupon_code"] = ""
        savedCart["discount"] = "0.00"
        self.cart.isCouponApplied = false
        self.cart.couponCode = ""
        self.cart.couponDiscount = "0.00"
        UserDefaults.standard.set(savedCart, forKey: UserDefault.localCart)
        self.cart.arr.removeAll()
        self.cart.getData(savedCart as NSDictionary? ?? NSDictionary())
        self.getLocalCartValuesService()
        self.cartTableView.reloadData()
        
        //if couponCell.applyCouponBtn.titleLabel?.text ?? "" != "Apply"{
        self.view.makeToast(Messages.couponRemoved, duration: 2.0, point: self.view.center, title: nil, image: nil) { didTap in
        }
        //}
    }
    
    //MARK:- Clear Cart Service
    
    func clearCartService(){
        
        //        self.showLoader(self)
        var itemIdArr = [Int]()
        for cartItem in cart.arr.first?.data as? [CartItem] ?? [CartItem](){
            itemIdArr.append(cartItem.id)
        }
        let jsonData = try? JSONSerialization.data(withJSONObject: itemIdArr, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let dic = ["user_id": UserModel.shared.user_id, "items" : jsonString ?? ""] as [String : Any]
        
        ServiceManager.shared.postRequestWithThreeValues(url: ApiName.removeFromCart, parameters: dic, completion: { (status, message, response) in
            if status ?? false {
                self.cart.arr.removeAll()
                self.cartTableView.reloadData()
                self.placeOrderBtn.isHidden = true
                self.cartMessageView.isHidden = false
                //self.showEmptyScreenMessage(Messages.noCartHeading, Messages.noCartItem, self.view)
                UserModel.shared.cartItemsCount = 0
                UserModel.shared.cartItemsPrice = 0.00
            } else {
                Utility.shared.showAlertView(message: message ?? "")
            }
        })
    }
    
    
    //MARK:- Place Order Api
    func placeOrderApi(){
        
//        else if paystr == "pay_on_pickup" && self.isOrderPickUp == false {
//            UserModel.shared.paymentOption = .cod
//            paystr = UserModel.shared.paymentOption.rawValue
//        }
        var stripeDic = NSDictionary()
        let localCartDic = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        let hallid = localCartDic["hall_id"] as? Int ?? 0
        
        if let card_id = self.selectedPaymentCard?.cardId  {
            stripeDic = ["stripe_customer_id" : UserModel.shared.stripe_id,"stripe_card_id" : card_id]
        } else {
            stripeDic = ["stripe_customer_id" : UserModel.shared.stripe_id,"stripe_card_id" : self.selectedPaymentCard?.cardId ?? ""]
        }
        print(stripeDic)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: stripeDic, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let itemDic = self.getPlaceOrderItemsDic()
        let jsonItemsData = try? JSONSerialization.data(withJSONObject: itemDic, options: [])
        let jsonItemsString = String(data: jsonItemsData!, encoding: .utf8)
        //    let delieveryAddress = self.getDelieveryAddress()
        
        let payment_method = UserModel.shared.paymentOption
        if paystr == "Cash on Delivery" && self.isOrderPickUp{
            UserModel.shared.paymentOption = .payOnPickup
        }

        var dic = ["items" : jsonItemsString ?? "", "user_id" : UserModel.shared.user_id,
                   "hall_id" : hallid,//UserModel.shared.hall_id,//GSD Commented --> 2,
                   "instructions" :  cart.specialInstruction ,
                   "payment_method" : UserModel.shared.paymentOption.rawValue,
                   "payment_details" : jsonString ?? "","coupon_code" : self.cart.couponCode ?? "",
                   "pickup":pickUpTimeStr] as [String : Any]
        
        UserModel.shared.paymentOption = payment_method

        dic["firstname"] = delieveryAddress.firstname
        dic["lastname"] = delieveryAddress.lastname
        dic["email"] = delieveryAddress.email
        dic["house_no"] = delieveryAddress.house_no
        dic["street1"] = delieveryAddress.street1
        dic["street2"] = delieveryAddress.street2
        dic["city"] = delieveryAddress.city
        dic["state"] = delieveryAddress.state
        dic["zipcode"] = delieveryAddress.zipcode
        dic["nonce"] = self.brainTreeNonce
        dic["phone"] = UserModel.shared.phone
        dic["deviceData"] = ""
        dic["address_id"] = self.delieveryAddress.id
        
        if isOrderPickUp == false {
            dic["order_type"] = Constant.shared.DELIVERABLE
        } else {
            dic["order_type"] = "PICKEDUP"
        }
        
        dic["lat"] = delieveryAddress.lat
        dic["lng"] = delieveryAddress.lng
        
        print(dic)
        Utility.shared.startLoading()
        ServiceManager.shared.postRequestWithThreeValues(url: ApiName.placeOrder, parameters: dic, completion: { (status, message, response) in
            if status ?? false {
                self.view.endEditing(true)
                if let orderId = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "order_id")as? Int{
                    let grandTotal = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "grand_total")as? String
                    let razorPayid = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "razorpay_order_id")as? String
                    if UserModel.shared.paymentOption == .payOnline {
                    
//                        let url = ApiName.paymentIntent+"customer_id="+UserModel.shared.stripe_id+"&order_id=\(orderId)"
//                        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
//
//                            let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
//
//                            if let json = responseJSON as? [String: Any], let data = json["data"] as? [String: Any] {
//                                let ephemeralKey = data["ephemeralKey"] as? String ?? ""
//                                let paymentIntent = data["paymentIntent"] as? String ?? ""
//
//                                self.openPaymentSheetFlowController(order_id: "\(orderId)", customerId: UserModel.shared.stripe_id, ephemeralKeySecret: ephemeralKey, clientSecret: paymentIntent,publishableKey: UserModel.shared.publishableKey)
//                            }
//                        }
                        
                        self.showPaymentForm(razorPayorder_id: razorPayid ?? "", grandTotal: grandTotal ?? "0")
                    }
                    else {
                        let orderStatusVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as! DeliveryOrderDetailVC
                        orderStatusVC.delegate = self
                        orderStatusVC.order.id = "\(orderId)"
                        orderStatusVC.isFromCartScreen = true
                        orderStatusVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(orderStatusVC, animated: true)
                        
                        self.removeOrderValueAfterOrderBooking()
                        
                    }
                    
                }
                
            } else {
                Utility.shared.stopLoading()

                self.getLocalCartValuesService()
                let data = response?["data"] as? NSDictionary ?? [:]
                if let errors = data["errors"] as? NSArray {
                    print(errors)
                    if errors.count > 0 {
                        Utility.shared.showAlertView(message: errors[0] as? String ?? "")
                    }
                } else{
                    Utility.shared.showAlertView(message: message ?? "")
                }
            }
        })
        
    }
    
    func removeOrderValueAfterOrderBooking() {
        self.cart.arr.removeAll()
        self.cartTableView.reloadData()
        self.placeOrderBtn.isHidden = true
        self.cartMessageView.isHidden = false
        // self.showEmptyScreenMessage(Messages.noCartHeading, Messages.noCartItem, self.view)
        
        UserModel.shared.cartItemsCount = 0
        UserModel.shared.cartItemsPrice = 0.00
        UserDefaults.standard.removeObject(forKey: UserDefault.localCart)
        UserDefaults.standard.synchronize()
        
    }
    
//    func openPaymentSheetFlowController(order_id: String,customerId: String,ephemeralKeySecret: String, clientSecret: String,publishableKey: String) {
//        STPAPIClient.shared.publishableKey = publishableKey // UserModel.shared.publishableKey
//        Utility.shared.startLoading()
//        // MARK: Create a PaymentSheet.FlowController instance
//        var configuration = PaymentSheet.Configuration()
//        configuration.merchantDisplayName = "Example, Inc."
//        configuration.customer = .init(id: customerId, ephemeralKeySecret: ephemeralKeySecret)
//        PaymentSheet.FlowController.create(paymentIntentClientSecret: clientSecret,
//                                           configuration: configuration) { [weak self] result in
//            Utility.shared.stopLoading()
//
//            switch result {
//            case .failure(let error):
//                print(error)
//            case .success(let paymentSheetFlowController):
//                self?.paymentSheetFlowController = paymentSheetFlowController
//                self?.openStripePaymentView(order_id: "\(order_id)", configuration: configuration, clientSecret: clientSecret)
//            }
//        }
//    }
    
//    func openStripePaymentView(order_id: String, configuration: PaymentSheet.Configuration,clientSecret: String) {
//            // MARK: Set your Stripe publishable key - this allows the SDK to make requests to Stripe for your account
//
//
//            self.paymentSheet = PaymentSheet(
//                paymentIntentClientSecret: clientSecret,
//                configuration: configuration)
//
//            // MARK: Start the checkout process
//            self.paymentSheet?.present(from: self) { paymentResult in
//                // MARK: Handle the payment result
//                switch paymentResult {
//                case .completed:
//                //    self.getFourDigitCardNUmber(order_id: order_id, customerId: customerId, ephemeralKeySecret: ephemeralKeySecret, clientSecret: clientSecret)
//                //                Utility.shared.showAlertView(message: "Your order is confirmed!")
//                    if let paymentOption = self.paymentSheetFlowController.paymentOption {
//                      //  self.callApiAfterStripePayment(order_id: order_id, card_digits: paymentOption.label)
//                    }
//
//                case .canceled:
//                    print("Canceled!")
//                   // Utility.shared.showAlertView(message: "Canceled!")
//                case .failed(let error):
//                    print(error)
//                    Utility.shared.showAlertView(message: "Payment failed: \n\(error.localizedDescription)")
//                }
//            }
//
//    }
    
    // razorpay
    
    internal func showPaymentForm(razorPayorder_id: String,grandTotal: String){
        razorpay = RazorpayCheckout.initWithKey(razorpay_Key, andDelegateWithData: self)
        let finalTotal = Double(grandTotal)! * 100
        let options: [String:Any] = [
                    "amount": "\(finalTotal)", //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "order_id": razorPayorder_id,
                   // "image": "https://url-to-image.png",
                    "name": "FoodCoop",
                    "prefill": [
                        "contact": UserModel.shared.phone,
                        "email": UserModel.shared.email
                    ],
                    "theme": [
                        "color": "#F81240"
                      ]
                ]
        
        razorpay.open(options)
        Utility.shared.stopLoading()
       // razorpay.open(options, displayController: self)
    }
    
    
    
 
    
    
    func callApiAfterRazorpayPayment(razorpay_payment_id: String,razorpay_order_id: String,razorpay_signature: String) {
        
        
        Utility.shared.startLoading()
        ServiceManager.shared.callPostRequest(url: ApiName.updaterazorpayStatus, parameters: ["razorpay_payment_id":razorpay_payment_id,
            "razorpay_order_id": razorpay_order_id,
            "razorpay_signature":razorpay_signature]) { (jsonData) in
            Utility.shared.stopLoading()

            let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
            if let json = responseJSON as? [String: Any], let data = json["data"] as? [String: Any] {
                DispatchQueue.main.async {
                    let orderid = data["order_id"] as? String
                    let orderStatusVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as! DeliveryOrderDetailVC
                    orderStatusVC.delegate = self
                    orderStatusVC.order.id = orderid ?? "0"
                    orderStatusVC.isFromCartScreen = true
                    orderStatusVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(orderStatusVC, animated: true)
                }
                
                self.removeOrderValueAfterOrderBooking()
                
            }else {
                print("error")
            }
            
        }
        
    }
    
    func getCartItemsDic() -> [[String : Any]] {
        var allItemsDic = [[String : Any]]()
        var cartItems = [CartItem]()
        
        if self.cart.arr.count > 0
        {
            cartItems = self.cart.arr[0].data as? [CartItem] ?? [CartItem]()
        }
        
        for item in cartItems{
            var itemDic = [String : Any]()
            var addOns = [[String : Int]]()
            var customization = [[String : Int]]()
            for element in item.addOns{
                addOns.append(["addon_id" : element.addOnId ?? 0])
            }
            
            for element in item.customOptions{
                customization.append(["ingredient_id" : element.ingredientId ?? 0 ,"option_id" : element.optionId ?? 0])
            }
            itemDic["serving"] = ["option_id" : item.serving?.optionId ?? 0]
            itemDic["dish_id"] = item.dishId
            itemDic["price"] = item.price
            itemDic["qty"] = item.qty
            itemDic["addons"] = addOns
            itemDic["options"] = customization
            itemDic["variation"] = item.variations
            allItemsDic.append(itemDic)
        }
        return allItemsDic
    }
    
    func getPlaceOrderItemsDic() -> [[String : Any]]{
        var allItemsDic = [[String : Any]]()
        var cartItems = [CartItem]()
        
        if self.cart.arr.count > 0
        {
            cartItems = self.cart.arr[0].data as? [CartItem] ?? [CartItem]()
        }
        
        for item in cartItems{
            var itemDic = [String : Any]()
            var addOns = [[String : Any]]()
            var customization = [[String : Any]]()
            
            for element in item.addOns{
                addOns.append(["addon_id" : element.addOnId ?? 0,"addon" : element.name ?? "", "price" : element.price ?? 0])
            }
            
            for element in item.customOptions{
                customization.append(["ingredient_id" : element.ingredientId ?? 0 ,"option_id" : element.optionId ?? 0,"ingredient" : element.ingredient ?? "","option_name" : element.option ?? ""])
            }
            itemDic["serving"] = ["option_id" : item.serving?.optionId ?? 0,"ingredient" : item.serving?.ingredient ?? ""]
            itemDic["dish_id"] = item.dishId
            itemDic["price"] = item.price
            itemDic["qty"] = item.qty
            itemDic["addons"] = addOns
            itemDic["options"] = customization
            itemDic["variation"] = item.variations
            allItemsDic.append(itemDic)
        }
        return allItemsDic
    }
    
    
    //    func getLatestTimingApi(oncompletion:@escaping ServiceResponseString) {
    //
    //        ServiceManger.shared.getRequest(methodName: ApiName.getTiming + "?timezone_name=\(TimeZone.current.identifier)", params: nil, oncompletion: { (status, message, response) in
    //            if status ?? false{
    //                if let hallArr = (response?.value(forKey: "data") as? NSDictionary)?.value(forKey: "halls") as? NSArray{
    //                    Utility.sharedInstance.hallArr = Hall().getHallDetail(hallArr)
    //                }
    //                oncompletion(true, message)
    //            }else{
    //                oncompletion(false, message)
    //            }
    //        })
    //    }
}
extension CartViewController{
    func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

// BRAIN TREE

extension CartViewController: BTDropInViewControllerDelegate{
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        
        if let presented = self.presentedViewController {
            presented.removeFromParent()
        }
        
        if self.brainTreeClientToken != ""{
            let request =  BTDropInRequest()
            self.blurView.isHidden = true
            let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
            { (controller, result, error) in
                if (error != nil) {
                    print("ERROR")
                    print(error?.localizedDescription as Any)
                } else if (result?.isCancelled == true) {
                    print("CANCELLED")
                } else if let result = result {
                    
                    if let nonce = result.paymentMethod?.nonce{
                        self.brainTreeNonce = nonce
                        self.placeOrderApi()
                    }
                }
                controller.dismiss(animated: true, completion: nil)
            }
            
            self.present(dropIn!, animated: true, completion: nil)
        }
        
    }
    
    
    func drop(_ viewController: BTDropInViewController, didSucceedWithTokenization paymentMethodNonce: BTPaymentMethodNonce) {
        
        print("didSucceedWithTokenization: \(paymentMethodNonce.nonce)")
        
        dismiss(animated: true, completion: nil)
    }
    
    func drop(inViewControllerDidCancel viewController: BTDropInViewController) {
        print("inViewControllerDidCancel")
        dismiss(animated: true, completion: nil)
    }
    
}

// razorpay delegate methods

//extension CartViewController : RazorpayPaymentCompletionProtocol {
//
//    func onPaymentError(_ code: Int32, description str: String) {
//        print("error: ", code, str)
//
//    }
//
//    func onPaymentSuccess(_ payment_id: String) {
//        print("success: ", payment_id)
//
//    }
//}

// RazorpayPaymentCompletionProtocolWithData - This will returns you the data in both error and success case. On payment failure you will get a code and description. In payment success you will get the payment id.
extension CartViewController: RazorpayPaymentCompletionProtocolWithData {
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
        Utility.shared.showAlertView(message: str)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        print("razorpay_order_id", response!["razorpay_order_id"] as! String)
        print("razorpay_signature", response!["razorpay_signature"] as! String)
        print("razorpay_payment_id",response!["razorpay_payment_id"] as! String)
        callApiAfterRazorpayPayment(razorpay_payment_id: response!["razorpay_payment_id"] as! String, razorpay_order_id: response!["razorpay_order_id"] as! String, razorpay_signature: response!["razorpay_signature"] as! String)
    }
}
