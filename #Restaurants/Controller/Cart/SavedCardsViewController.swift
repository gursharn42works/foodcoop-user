//
//  SavedCardsViewController.swift
//  Magic
//
//  Created by Apple on 8/16/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SavedCardsViewController: BaseVC, ConfirmOrderDelegate {
    
    @IBOutlet var savedCardsTableView: UITableView!
    @IBOutlet var cardsTableViewHeight: NSLayoutConstraint!
    
    var savedCards = [CardModel]()
    let stripeApiModel = StripeApiModel()
    var selectedCardRow : Int?
    var selectedPaymentCard : CardModel?
    var cardValueUpdated :((CardModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stripeApiModel.responseHandle = { card in
            self.savedCards = card
            self.savedCardsTableView.reloadData()
        }
        initiailise()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Functions
    private func initiailise(){
        savedCardsTableView.register(UINib(nibName: "ConfirmOrderCell", bundle: Bundle.main), forCellReuseIdentifier: "ConfirmOrderCell")
    }
    
    @IBAction func back_click(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewCard(){
        if savedCards.count >= 5{
            Utility.shared.showAlertView(message: Messages.savedCards)
            return
        }
        let addCard = Router.viewController(with: .addNewCard, .cart) as? AddNewCardViewController
        addCard?.savedCards = self.savedCards
        addCard?.cardSaved = {
            self.stripeApiModel.callApi()
        }
        self.navigationController?.pushViewController(addCard ?? UIViewController(), animated: true)
        
    }
    
    func backTap() {
        self.cardValueUpdated?(self.selectedPaymentCard ?? CardModel())
    }
    
    //MARK:- Observers
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        cardsTableViewHeight.constant = savedCardsTableView.contentSize.height > self.view.frame.height - 100 ? self.view.frame.height - 100 : savedCardsTableView.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        savedCardsTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if savedCardsTableView.observationInfo != nil {
            savedCardsTableView.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    //MARK:- SORT CARDS
    
    fileprivate func sortCards(){
        
        var cards = [CardModel]()
        
        var defaultCard : CardModel?
        
        for card in self.savedCards{
            
            if !card.isDefault{
                cards.append(card)
            }else{
                defaultCard = card
            }
        }
        
        if defaultCard != nil{
            cards.insert(defaultCard!, at: 0)
        }
        
        self.savedCards = cards
    }
    
    //MARK:- Delete Card
    
    func deleteCardTap(_ row : Int){
        
        let deletionAlert  = UIAlertController(title: "Please Verify!", message: Messages.cartDeletionMessage, preferredStyle: .alert)
        
        deletionAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        deletionAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.deleteCard(at : row)
        }))
        
        
        self.present(deletionAlert, animated: true, completion: nil)
    }
    
    
    private func deleteCard(at row: Int){
        
        let customerId = UserModel.shared.stripe_id
        let deletedCardId = self.savedCards[row].cardId
        if customerId == "" || deletedCardId == ""
        {
            return
        }
        
        Utility.shared.startLoading()

        ServiceManager.shared.deleteStripeRequest(methodName: "https://api.stripe.com/v1/customers/\(customerId)/sources/\(deletedCardId)", params: nil, oncompletion: { (status, message, response) in
            Utility.shared.stopLoading()

            if status ?? false {
                self.savedCards.remove(at: row)
                self.savedCardsTableView.reloadData()
                
                if UserModel.shared.card_id == deletedCardId{
                    if self.savedCards.count != 0{
                        self.saveProfileChanges(UserModel.shared.stripe_id,cardId: self.savedCards[0].cardId){(status, message, dic) in
                            if status == true{
                                self.savedCardsTableView.reloadData()
                            }else{
                                Utility.shared.showAlertView(message: message ?? "")
                            }
                        }
                    }
                    self.cardValueUpdated?(self.selectedPaymentCard ?? CardModel())
                }
            }else{
                self.saveProfileChanges(UserModel.shared.stripe_id,cardId:""){(status, message, dic) in
                    if status == true{
                        self.savedCardsTableView.reloadData()
                    }else{
                        Utility.shared.showAlertView(message: message ?? "")
                    }
                }
            }
        })
    }
    
    
}
extension SavedCardsViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return savedCards.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderCell", for: indexPath)as? ConfirmOrderCell
        cell?.userCardId = UserModel.shared.card_id
        cell?.config(savedCards[indexPath.row])
        if selectedCardRow == nil && selectedPaymentCard == nil{
            if (cell?.card.isDefault) ?? false{
                selectedCardRow = indexPath.row
                cell?.tickWidth.constant = 20
                cell?.cardNumber.textColor = AppColor.primaryColor
                selectedPaymentCard = savedCards[indexPath.row]
            }
        }else if selectedCardRow == nil && selectedPaymentCard != nil{
            if savedCards[indexPath.row].cardId == selectedPaymentCard?.cardId{
                selectedCardRow = indexPath.row
                cell?.tickWidth.constant = 20
                cell?.cardNumber.textColor = AppColor.primaryColor
            }
        }
        
        if selectedCardRow != indexPath.row{
            cell?.tickWidth.constant = 0
            cell?.cardNumber.textColor = AppColor.darkTextColor
        }else{
            cell?.tickWidth.constant = 20
        }
        
        cell?.delegate = self
        cell?.deleteBtn.tag = indexPath.row
        cell?.defaultCardChanged = { changedCard in
            Utility.shared.startLoading()
            self.cardValueUpdated?(self.selectedPaymentCard ?? CardModel())
            self.saveProfileChanges(UserModel.shared.stripe_id,cardId: changedCard.cardId){(status, message, dic) in
                Utility.shared.stopLoading()

                if status == true{
                    
                    for card in self.savedCards{
                        card.isDefault = false
                    }
                    
                    self.savedCards[indexPath.row].isDefault = true
                    UserModel.shared.card_id = self.savedCards[indexPath.row].cardId
                    self.sortCards()
                    self.selectCurrenCard(at: 0)
                    
                    self.savedCardsTableView.reloadData()
                }else{
                    Utility.shared.showAlertView(message: message ?? "")
                }
            }
        }
        return cell ?? ConfirmOrderCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = tableView.cellForRow(at: indexPath) as? ConfirmOrderCell{
            self.selectCurrenCard(at: indexPath.row)
            self.savedCardsTableView.reloadData()
        }
    }
    
    fileprivate func selectCurrenCard(at index: Int){
        //cell.tickWidth.constant = 20
        selectedCardRow = index
        selectedPaymentCard = savedCards[index]
        self.cardValueUpdated?(self.selectedPaymentCard ?? CardModel())
    }
    
}
extension SavedCardsViewController{
    
    func saveProfileChanges(_ stripeId : String = "", cardId : String = "",oncompletion:@escaping (Bool?, String?, NSDictionary?) -> Void){
        let user = UserModel.shared
        let updateProfile = UpdateProfile()
        updateProfile.name = user.user_name
        updateProfile.phone = user.phone
        updateProfile.studentID = user.student_id
        updateProfile.cardId = cardId
        updateProfile.stripeId = stripeId
        let serverDic = updateProfile.serverData()
        updateUserProfile(serverDic) { (success) in
            oncompletion(true, "", nil)
        }
    }
    
    
}

