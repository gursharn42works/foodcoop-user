//
//  TaxesInfoViewController.swift
//  Magic
//
//  Created by Apple1 on 12/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TaxesInfoViewController: BaseVC,UITableViewDelegate,UITableViewDataSource {

     //MARK:- IBOutlets
    @IBOutlet var taxTableView : UITableView!
    @IBOutlet var taxTableViewHeight : NSLayoutConstraint!
    @IBOutlet var progressView : UIProgressView!
    @IBOutlet var taxesView : UIView!
    @IBOutlet var orderProgressView : UIView!
    
     //MARK:- Variables
    var cancelTap: (()-> Void)?
    var placeOrderClosure : (()-> Void)?
    var progressBarCompleted : Bool?
    var task = CancellableDelayedTask()
    var taxesInfo = [TaxDescription]()
    var placingOrder : Bool?
    var shouldShowThemeColor  = false
	
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialise()
        taxesView.isHidden = (placingOrder ?? false) == true ? true : false
        orderProgressView.isHidden = (placingOrder ?? false) == true ? false : true
        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if placingOrder ?? false{
             progressViewReload()
        }
       
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Functions
    
    func initialise(){
        taxTableView.register(UINib(nibName: "taxesFooterCell", bundle: Bundle.main), forCellReuseIdentifier: "taxesFooterCell")
        taxTableView.register(UINib(nibName: "TaxesInfoCell", bundle: Bundle.main), forCellReuseIdentifier: "TaxesInfoCell")
    }
    
    func progressViewReload(){
        self.progressView.progress = 0.0
        
        self.task = CancellableDelayedTask()
        UIView.animate(withDuration: 5.0, animations: {
            self.progressView.setProgress(1.0, animated: true)
        })
        self.task.run(delay: 5.0) { () -> Void in
            self.progressBarCompleted = true
            self.cancelTap?()
            self.placeOrderClosure?()
            self.dismiss(animated: true, completion: nil)
        }
    }

    
    //MARK:- IBAction
    
    @IBAction func crossBtnTap(){
        cancelTap?()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelOrderBtnTap(){
        if !(progressBarCompleted ?? false){
            self.task.cancel()
            crossBtnTap()
        }
    }
    
    //MARK:- Observers
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        taxTableViewHeight.constant = taxTableView.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        taxTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if taxTableView.observationInfo != nil {
            taxTableView.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    
    //MARK:- Table View

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taxesInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxesInfoCell", for: indexPath) as? TaxesInfoCell
        cell?.config(taxesInfo[indexPath.row])
		
        return cell ?? TaxesInfoCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableCell(withIdentifier: "taxesFooterCell") as! taxesFooterCell
        footerView.config(taxesInfo)
		
		//if self.shouldShowThemeColor{
			footerView.totalTax.textColor = AppColor.primaryColor
		//}
		
		
        return footerView
    }
	
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        tableView.estimatedSectionFooterHeight = 50
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    

}
