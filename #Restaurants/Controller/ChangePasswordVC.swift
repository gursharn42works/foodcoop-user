//
//  ChangePasswordVC.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseVC {

    @IBOutlet var currentPasswordTxt : UITextField!
    @IBOutlet var newPasswordTxt : UITextField!
    @IBOutlet var confirmPasswordTxt : UITextField!

    var changePasswordVM = ChangePasswordVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func showPassword_Click(_ sender : UIButton){
        if sender.tag == 1 {
            sender.isSelected = !sender.isSelected
            currentPasswordTxt.isSecureTextEntry = sender.isSelected == true  ? false : true
        }
        else if sender.tag == 2 {
            sender.isSelected = !sender.isSelected
            newPasswordTxt.isSecureTextEntry = sender.isSelected == true  ? false : true
        }
        else {
            sender.isSelected = !sender.isSelected
            confirmPasswordTxt.isSecureTextEntry = sender.isSelected == true  ? false : true
        }
    }
    
    @IBAction func save_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        self.changePasswordVM.changePassword(currentPassword: currentPasswordTxt.text!, newPassword: newPasswordTxt.text!, confirmPassword: confirmPasswordTxt.text!) { (success) in
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func back_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
