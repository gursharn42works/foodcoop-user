//
//  ContactUsVC.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class ContactUsVC: BaseVC {

    @IBOutlet var nameTxt : UITextField!
    @IBOutlet var emailTxt : UITextField!
    @IBOutlet var hallTxt : UITextField!
    @IBOutlet var subjectTxt : UITextField!
    @IBOutlet var messageTxt : KMPlaceholderTextView!

    var contactUsVM = ContactUsVM()
    var hallId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setData()
    }
    
    func setData() {
        self.nameTxt.text = UserModel.shared.user_name
        self.emailTxt.text = UserModel.shared.email
    }

    @IBAction func submit_Click(_ sender: UIButton){
        self.submitConatctUsData()
    }
    
    @IBAction func back_Click(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ContactUsVC {
    
    func submitConatctUsData() {
        contactUsVM.submitContactUs(name: nameTxt.text!, email: emailTxt.text!, subject: subjectTxt.text!, message: messageTxt.text!) { () in
            self.navigationController?.popViewController(animated: true)
        }
    }
}
