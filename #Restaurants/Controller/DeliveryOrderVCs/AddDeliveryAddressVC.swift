//
//  AddDeliveryAddressVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import CoreLocation

class AddDeliveryAddressVC: BaseVC, UITextFieldDelegate {

    @IBOutlet var addressTitleTxt: UITextField!
    @IBOutlet var houseNumTxt: UITextField!
    @IBOutlet var street1Txt: UITextField!
    @IBOutlet var street2Txt: UITextField!
    @IBOutlet var cityTxt: UITextField!
    @IBOutlet var stateTxt: UITextField!
    @IBOutlet var zipTxt: UITextField!
    @IBOutlet var checkBtn: UIButton!
    @IBOutlet var submitBtn: UIButton!

    @IBOutlet weak var stateBtn_Out: UIButton!
    @IBOutlet weak var stateTbl_Out: UITableView!
    @IBOutlet weak var statateBackView: UIView!

    var addDeliveryAddressVM = AddDeliveryAddressVM()
    var address = DeliveryAddressModel()
    
    var isDefault = 0
    var isFromCartView = false
    var selectedState = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustoUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.statateBackView.isHidden = true
    }
    
    func setCustoUI() {

        if self.address.id != 0 {
            self.submitBtn.setTitle("UPDATE ADDRESS", for: .normal)
            self.addressTitleTxt.text = address.title
            self.houseNumTxt.text = address.house_no
            self.street1Txt.text = address.street1
            self.street2Txt.text = address.street2
            self.cityTxt.text = address.city
            self.stateTxt.text = address.state
            self.zipTxt.text = address.zipcode
        } else {
            self.submitBtn.setTitle("ADD ADDRESS", for: .normal)
        }
    }

    @IBAction func defaultClick(_ sender: UIButton){
        if self.isDefault == 1 {
            self.isDefault = 0
            self.checkBtn.setBackgroundImage(UIImage(named: "Check box grey"), for: .normal)
        } else {
            self.isDefault = 1
            self.checkBtn.setBackgroundImage(UIImage(named: "Check box with tik icon"), for: .normal)
        }
    }
    
    @IBAction func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func State_Btn(_ sender: Any) {
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        self.statateBackView.isHidden = false
    }
    
    @IBAction func hideStatusBackView(_ sender: Any) {
        self.statateBackView.isHidden = true
    }
    
    @IBAction func submitClick(){
        
        //MARK:- Check Fields Data
        if self.addDeliveryAddressVM.isValidData(addressTitleTxt.text!, houseNumTxt.text!, street1Txt.text!, street2Txt.text!, cityTxt.text!, stateTxt.text!, zipTxt.text!) { return }

        let geocoder = CLGeocoder()
        let address = "\(addressTitleTxt.text!), \(houseNumTxt.text!), \(street1Txt.text!), \(street2Txt.text!), \(cityTxt.text!), \(stateTxt.text!), \(zipTxt.text!)"
        Utility.shared.startLoading()
        geocoder.geocodeAddressString(address) { placemarks, error in
            let placemark = placemarks?.first
            let lat = placemark?.location?.coordinate.latitude ?? 0.0
            let lon = placemark?.location?.coordinate.longitude ?? 0.0
            print("Lat: \(lat), Lon: \(lon)")
            Utility.shared.stopLoading()
            
            self.saveAddressWithLatLong(lat: lat, long: lon)
        }

    }
    
    func saveAddressWithLatLong(lat: Double, long: Double) {
        self.addDeliveryAddressVM.callAddAddressService(self.address.id,addressTitleTxt.text!, houseNum: houseNumTxt.text!, street1: street1Txt.text!, street2: street2Txt.text!, city: cityTxt.text!, state: selectedState, zip: zipTxt.text!, default_address: self.isDefault, lat: lat, lng: long) { (success) in
            
            if success {
                if self.isFromCartView {
                    let vc = Router.viewController(with: .deliveryAddressVC, .cart) as! DeliveryAddressVC
                    self.navigationController?.pushViewController(vc, animated: true)

                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.statateBackView.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == zipTxt {
            let maxLength = 8
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == stateTxt || textField == cityTxt {
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string != numberFiltered {
                return false
            }

        }
        return true
    }


}

extension AddDeliveryAddressVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addDeliveryAddressVM.states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "stateCell", for: indexPath)
        
        let states = addDeliveryAddressVM.states[indexPath.row].components(separatedBy: ":")
        cell.textLabel?.text = states[1]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let states = addDeliveryAddressVM.states[indexPath.row].components(separatedBy: ":")
        selectedState = states[0]
        stateTxt.text = states[1]
        self.statateBackView.isHidden = true
    }
    
    
}



