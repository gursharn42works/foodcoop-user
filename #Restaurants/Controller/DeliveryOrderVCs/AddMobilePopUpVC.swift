//
//  AddMobilePopUpVC.swift
//  #Restaurants
//
//  Created by Satveer on 08/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol AddMobilePopUpVCDelegate: class {
    func placeOrderAfterMobileAdded()
}

class AddMobilePopUpVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mobileTxt: UITextField!
    var delegate: AddMobilePopUpVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mobileTxt.delegate = self
    }
    

    @IBAction func submitClick() {
        
        if self.isValidData(mobile: self.mobileTxt.text!) { return }
        
        let url = ApiName.updateUserProfile + "\(UserModel.shared.user_id)"
        let parameters = ["phone": self.mobileTxt.text!]
        
        ServiceManager.shared.callPostRequest(url: url, parameters: parameters) { (jsonData) in
            
            self.saveUserData(data: jsonData)
            self.dismiss(animated: true, completion: nil)
            self.delegate?.placeOrderAfterMobileAdded()

        }
    }
    @IBAction func cancelClick() {
        self.dismiss(animated: true, completion: nil)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
extension AddMobilePopUpVC {
    
    private func isValidData(mobile: String) -> Bool {
         if mobile == "" {
            Utility.shared.showAlertView(message: Messages.emptyMobile)
            return true
        }
        else if mobile.count < 10 {
            Utility.shared.showAlertView(message: Messages.invalidMobile)
            return true
        }
        return false
    }
}
