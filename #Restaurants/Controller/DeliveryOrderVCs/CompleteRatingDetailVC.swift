//
//  CompleteRatingDetailVC.swift
//  #Restaurants
//
//  Created by Satveer on 07/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import Cosmos

class CompleteRatingDetailVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deliveryRateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foodRateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var itemViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foodRatingView: CosmosView!
    @IBOutlet weak var deliveryRatingView: CosmosView!
    @IBOutlet weak var deliveryViewBottomLine: UIView!
    @IBOutlet weak var foodrateBottomLine: UIView!
    @IBOutlet weak var foodItemsTitleLbl: UILabel!

    var foodFeedbackPlaceholder = "Write your reviews here..."
    var deliveryFeedbackPlaceholder = "Write your feedback here..."
    var order = Order()
    var ratingsArray: [Dictionary<String, Int>] = []
    var finalrating: [Dictionary<String, Any>] = []
    var deliveryCompleteVM = DeliveryCompleteVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewUI()
        self.setUpData()
    }
    
    func setTableViewUI() {
        self.tableView.delegate  = self
        self.tableView.dataSource = self
        
        if order.order_type != Constant.shared.DELIVERABLE {
            self.deliveryRateViewHeight.constant = 0
        }

    }
    

    
    @IBAction func backBtnClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpData() {
//        self.deliveryRateViewHeight.constant = 0
//        self.foodRateViewHeight.constant = 0

          
        self.foodItemsTitleLbl.text = order.items.count == 1 || order.items.count == 0 ? "Food Item Rating:":"Food Item(s) Rating:"
        
        if self.order.delivery_rating > 0 {
            self.deliveryRateViewHeight.constant = 100
            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
            self.deliveryRatingView.isUserInteractionEnabled = false
        } else {
            self.deliveryRateViewHeight.constant = 0
        }
        
        if self.order.order_rating > 0 {
            self.foodRateViewHeight.constant = 80
            self.foodRatingView.rating = Double(self.order.order_rating)
            self.foodRatingView.isUserInteractionEnabled = false
        } else {
            self.foodRateViewHeight.constant = 0
        }
        
        
        if self.order.is_rating {
            
            //MARK:Order
            if self.order.order_comment != "" && self.order.order_rating == 0 {
                self.foodRateViewHeight.constant = 90
                self.foodRatingView.isHidden = true
            }
            else if self.order.order_comment != "" && self.order.order_rating != 0 {
                self.foodRateViewHeight.constant = 90
            }
            else if self.order.order_comment == "" && self.order.order_rating != 0 {
                self.foodRateViewHeight.constant = 90
                
            }
            
            //MARK:Delivery
            if self.order.delivery_comment != "" && self.order.delivery_rating == 0 {
                self.deliveryRateViewHeight.constant = 82
                self.deliveryRatingView.isHidden = true
            }
            else if self.order.delivery_comment != "" && self.order.delivery_rating != 0 {
                self.deliveryRateViewHeight.constant = 82
            }
            else if self.order.delivery_comment == "" && self.order.delivery_rating != 0 {
                self.deliveryRateViewHeight.constant = 82
            }

            self.foodRatingView.isUserInteractionEnabled = false
            self.deliveryRatingView.isUserInteractionEnabled = false
            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
            self.foodRatingView.rating = Double(self.order.order_rating)

       }
        
        DispatchQueue.main.async {
            self.sizeHeaderToFit(tableView: self.tableView)
        }

        if order.items.count == 0 {
            self.deliveryViewBottomLine.isHidden = true
        }


    }
}

extension CompleteRatingDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StarCell", for: indexPath) as! StarTableViewCell
        let dish = order.items[indexPath.row]
        let qtyString = "  (x\(dish.qty))"
        
        let dishAttributedString : NSAttributedString = dish.dishName.attributedStringColored(fullText: qtyString, size: 13.0)
        
        
        cell.itemnameLbl.attributedText = dishAttributedString
        //  dishPrice.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish.price)"
        cell.itemSubLbl.isHidden = dish.customOptions.count > 0 || dish.addOns.count > 0 ? false : true
        var optionNameArr = [String]()
        for option in dish.customOptions{
            optionNameArr.append(option.option ?? "")
        }
        
        for addon in dish.addOns{
            optionNameArr.append(addon.name ?? "")
        }
        cell.itemSubLbl.text = optionNameArr.joined(separator: ", ")
        cell.starRatingView.rating = Double(dish.rating["rating"] as? Int ?? 0)
//        let arr = ["Rating":1,"index":indexPath.row,"dishId":dish.dishId]
//        ratingsArray.append(arr)
        
//        cell.starRatingView.didFinishTouchingCosmos = { [self] rating in
//
//            // ratingsArray[indexPath.row] = rating
//            if ratingsArray.count != 0 {
//                if let index = ratingsArray.firstIndex(where: {$0["index"] == indexPath.row}) {
//                    ratingsArray[index]["Rating"] = Int(rating)
//                }else {
//                    let arr = ["Rating":Int(rating),"index":indexPath.row,"dishId":dish.dishId]
//                    ratingsArray.append(arr)
//                }
//            }else {
//                let arr = ["Rating":Int(rating),"index":indexPath.row,"dishId":dish.dishId]
//                ratingsArray.append(arr)
//            }
//
//        }
        
        return cell
    }

}
