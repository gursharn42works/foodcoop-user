//
//  DeliveryAddressVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

//protocol DeliveryAddressVCDelegate: class {
//    func showSelectedAddressOnCartView(address: DeliveryAddressModel,selectedIndex: Int)
//    func callTheDefaultAddressApi()
//}

//var isPushedFromAdd = false

class DeliveryAddressVC: UIViewController {

    @IBOutlet var addressListTableView: UITableView!
    var deliveryAddressVM = DeliveryAddressVM()
    
  //  var delegate: DeliveryAddressVCDelegate?
    var isMoveBack = false
    var comingforRrst = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setCustomUI()
        self.addressListTableView.isHidden = true
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getDeliveryAddressList()
    }
    func setCustomUI() {
        addressListTableView.register(UINib(nibName: Constant.shared.confirmOrderCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.confirmOrderCell)
        
        self.deliveryAddressVM.controller = self
        self.addressListTableView.dataSource = deliveryAddressVM
        self.addressListTableView.delegate = deliveryAddressVM
        
    }
    
    func getDeliveryAddressList() {
        self.deliveryAddressVM.getDeliveryAddressesList { (success) in
            self.addressListTableView.isHidden = !success
            self.addressListTableView.reloadData()
            
            if self.isMoveBack {
               // self.delegate?.callTheDefaultAddressApi()
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: CartViewController.self) {
                        
                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func back_click() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: CartViewController.self) {
                if comingforRrst == false {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }else  if controller.isKind(of: RestaurantSearchVC.self) {
                if comingforRrst == true {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }else if controller.isKind(of: HomeVC.self) {
                if comingforRrst == false {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
                
            }else if controller.isKind(of: NewSearchVC.self) {
                if comingforRrst == false {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }

    
//    func callWhenMoveBack() {
//        if deliveryAddressVM.addresses.count > 0 && deliveryAddressVM.selectedAddressRow < deliveryAddressVM.addresses.count {
//            self.delegate?.showSelectedAddressOnCartView(address: deliveryAddressVM.addresses[deliveryAddressVM.selectedAddressRow], selectedIndex: deliveryAddressVM.selectedAddressRow)
//        }
//    }
    
    @IBAction func addNewAddress(){
        
        if self.deliveryAddressVM.addresses.count >= 5 {
            Utility.shared.showAlertView(message: Messages.maximumSavedAddress)
            return
        }
        let addCard = Router.viewController(with: .addDeliveryAddressVC, .cart) as? AddDeliveryAddressVC
        self.navigationController?.pushViewController(addCard ?? UIViewController(), animated: true)
    }
}
