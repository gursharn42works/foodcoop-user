//
//  DeliveryCompleteVC.swift
//  #Restaurants
//
//  Created by Satveer on 08/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import Cosmos

//var deliveryOrderCompleteGlobal: DeliveryCompleteVC?

class DeliveryCompleteVC: BaseVC, UITextViewDelegate {
    @IBOutlet weak var orderDetailTableView: UITableView!
    @IBOutlet weak var orderTableHeight: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var delieveryDate : UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var orderDelieveredImageHeight: NSLayoutConstraint!
    @IBOutlet weak var dishNameLeading: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var cancelOrderStatusLabel: UILabel!
    @IBOutlet weak var order_delievered: UIImageView!
    @IBOutlet weak var cancelOrderReason: UIView!
    @IBOutlet weak var cancelOrderReasonLabel: UILabel!
    @IBOutlet weak var lblOrderPlaceOn: UILabel!
//    @IBOutlet weak var deliveryRateViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var foodRateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var submitBtnViewHeight: NSLayoutConstraint!

//    @IBOutlet weak var foodRatingView: CosmosView!
//    @IBOutlet weak var deliveryRatingView: CosmosView!
//    @IBOutlet weak var foodReviewTxtView: UITextView!
//    @IBOutlet weak var deliveryReviewTxtView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!

    @IBOutlet weak var contactDeliveryGuyView: UIView!
    @IBOutlet weak var contactRestaurantView: UIView!
    @IBOutlet weak var deliveryBoyImageView: UIImageView!
    @IBOutlet weak var restuantImageView: UIImageView!
    @IBOutlet weak var restuantNameLbl: UILabel!
    @IBOutlet weak var deliveryBoyNameLbl: UILabel!
    @IBOutlet weak var restaurantPhoneBtn: UIButton!
    @IBOutlet weak var deliveryBoyPhoneBtn: UIButton!
    @IBOutlet weak var restaurantPhoneIcon: UIImageView!
    @IBOutlet weak var deliveryBoyPhoneIcon: UIImageView!
    
//    @IBOutlet weak var itemRatingTbl: UITableView!
//
//    @IBOutlet weak var starBGHeightConst: NSLayoutConstraint!
    
    var foodFeedbackPlaceholder = "Write your reviews here..."
    var deliveryFeedbackPlaceholder = "Write your feedback here..."

    var pickupTitle = "Order Placed On: "
    var placedForTitle = "Order Placed For: "
    var locationTitle = "Location: "
    var order = Order()
    var deliveryCompleteVM = DeliveryCompleteVM()
    var isFromNotificationTab = false
    
    var ratingsArray: [Dictionary<String, Int>] = []
    var finalrating: [Dictionary<String, Any>] = []
    var isFromCartScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setCustomUI()
        ServiceManager.shared.hideLoader = false
       // deliveryOrderCompleteGlobal = self
    }
    
    private func setUpTableView() {
        self.orderDetailTableView.estimatedRowHeight = 80.0
        self.orderDetailTableView.rowHeight = UITableView.automaticDimension

        self.orderDetailTableView.dataSource = self
        self.orderDetailTableView.delegate = self
        
//        self.itemRatingTbl.dataSource = self
//        self.itemRatingTbl.delegate = self
//
//        if order.order_type != Constant.shared.DELIVERABLE {
//            self.deliveryRateViewHeight.constant = 0
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOrderDetails()
        self.addNotificationCenter()
        orderDetailTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if orderDetailTableView.observationInfo != nil {
            orderDetailTableView.removeObserver(self, forKeyPath: "contentSize")
        }
        NotificationCenter.default.removeObserver(self)

    }

    func setCustomUI() {
        mainScrollView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        orderDetailTableView.register(UINib(nibName: Constant.shared.cartTotalCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.cartTotalCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderDetailCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderDetailCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderFooterCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderFooterCell)
        
//        itemRatingTbl.register(UINib(nibName: "StarTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "StarCell")
        
        self.setUpTableView()
        self.mainScrollView.isHidden = true
    }
    
    
    
    @IBAction func back_Click(_ sender: UIButton) {
        if isFromNotificationTab { //veer
            self.popBack(3)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deliverygyClick(_ sender: UIButton) {
        self.openCallView(number: order.deliveryBoy.phone)
    }
    
    @IBAction func restaurantClick(_ sender: UIButton) {
        self.openCallView(number: order.restaurantDetail.phone)
    }


    @IBAction func reorderClcik(_ sender: UIButton) {
        if UserModel.shared.ongoingOrders.count >= 4 {
            Utility.shared.showAlertView(message: Messages.ongoingOrder)
            return
        }
        Utility.shared.startLoading()
        OrdersViewModel().reOrderService(order.id){(status, message, dic) in
            Utility.shared.stopLoading()
            if status {
                let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController
                cartVC?.isFromReOrderView = true
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(cartVC ?? CartViewController(), animated: true)
            }
        }

    }
    
    @IBAction func submitClick(_ sender: UIButton) {

        if let orderStatusVC = Router.viewController(with: .ratingDetailVC, .profile) as? RatingDetailVC {
            orderStatusVC.order = self.order
            self.navigationController?.pushViewController(orderStatusVC, animated: true)
        }

//        var deliveryRate = deliveryRatingView.rating
//        if order.order_type != Constant.shared.DELIVERABLE {
//             deliveryRate = 0
//        }
//
//        for i in 0..<ratingsArray.count {
//            let rating = [ "dish_id": ratingsArray[i]["dishId"]! as Int,
//                           "user_id": "\(UserModel.shared.user_id)",
//                           "rating": ratingsArray[i]["Rating"]! as Int] as [String : Any]
//            finalrating.append(rating)
//        }
//
//        self.deliveryCompleteVM.callRateDeliveryApi(delivery_id: order.deliveryBoy.delivery_id, order_id: order.id, rating: "\(deliveryRate)", comment: self.checkText(textView: deliveryReviewTxtView), order_rating: "\(foodRatingView.rating)", order_comment: self.checkText(textView: foodReviewTxtView), dishrating: finalrating) { (success) in
//
//            if success {
//                if let orderStatusVC = Router.viewController(with: .deliveryFeedbackCompleteVC, .profile) as? DeliveryFeedbackCompleteVC {
//                    orderStatusVC.order = self.order
//                    self.navigationController?.pushViewController(orderStatusVC, animated: false)
//                }
//
//            }
//        }
    }
    
//    func checkText(textView: UITextView)-> String {
//        if textView.text! ==  foodFeedbackPlaceholder || textView.text! ==  deliveryFeedbackPlaceholder {
//
//            return ""
//        }
//        return textView.text
//    }
//
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text! ==  foodFeedbackPlaceholder || textView.text! ==  deliveryFeedbackPlaceholder {
//            textView.text = ""
//            textView.textColor = .black
//        }
//    }
//
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text! ==  "" && textView == foodReviewTxtView{
//            textView.text = foodFeedbackPlaceholder
//            textView.textColor = .lightGray
//        }
//        else if textView.text! ==  "" && textView == deliveryReviewTxtView {
//            textView.text = deliveryFeedbackPlaceholder
//            textView.textColor = .lightGray
//        }
//
//    }
    
}

extension DeliveryCompleteVC {
    
    private func getOrderDetails() {
        self.getOrderDetails(order_id: order.id) { [self] (success) in
            if success {
                ServiceManager.shared.hideLoader = false
                self.showCompletedDetailData()
                self.orderTableHeight.constant = self.orderDetailTableView.contentSize.height
//                self.starBGHeightConst.constant = CGFloat((self.order.items.count * 85))
                if self.order.is_rating {
                        let vc = Router.viewController(with: .deliveryFeedbackCompleteVC, .profile) as! DeliveryFeedbackCompleteVC
                        vc.order = self.order
                        self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    self.mainScrollView.isHidden = false
                }
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        orderTableHeight.constant = orderDetailTableView.contentSize.height
    }

    
    private func showCompletedDetailData() {
        
        orderNumber.text = order.number
        print(order.orderPlacedOn)
        let del_date = order.orderPlacedOn.components(separatedBy: ",")
        
        if del_date.count > 1 {
            let finaldate = "\(del_date[0])" + "," + "\(del_date[1])" + " " + "at" + "\(del_date[2])"
            self.lblOrderPlaceOn.attributedText = pickupTitle.attributedString(fullText: finaldate, size: 12.0)
            
        }
        
        self.deliveryBoyImageView.set_image(order.deliveryBoy.photo, placeholder: "deliveryicon")
        self.restuantImageView.set_image(order.restaurantDetail.logo, placeholder: "restauranticon")

        
        if order.order_type == Constant.shared.DELIVERABLE {
            self.lblLocation.attributedText = locationTitle.attributedString(fullText: order.locationTitle, size: 12.0)
            self.lblAddress.text = ""
        } else {
            let address = "\(order.locationTitle)\n(\(order.restaurantAddressModel.address_line_1),\(order.restaurantAddressModel.city),\(order.restaurantAddressModel.state),\(order.restaurantAddressModel.zip))"
            self.lblAddress.text = address
        }
        orderDelieveredImageHeight.constant = order.status == "previous" ? 19 : 0
        
        dishNameLeading.constant = order.status == "previous" ? 10 : 0
        
        //cancelOrderView.isHidden = order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        cancelOrderView.isHidden = true//GSD order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        
        cancelOrderReason.isHidden = order.cancellationReason == "" ? true:false

        cancelOrderReasonLabel.text = order.cancellationReason
        
        cancelOrderReasonLabel.textColor = UIColor.red
        
        self.showStatusAccordingPaymentType(order: self.order, statusLabel: cancelOrderStatusLabel)

        let type = order.status == "delivered" || order.status == "cancelled" ? "previous" : "ongoing"
        
        if type == "ongoing"
        {
        }else{
           // thumbImage.isHidden = "\(order.thumb)" == "" || order.status == "cancelled" ? true : false
           // thumbImage.image = "\(order.thumb)" == "up" ? #imageLiteral(resourceName: "Up") : #imageLiteral(resourceName: "Down")
            order_delievered.image = order.status == OrderStatusEnum.cancelled.rawValue ? #imageLiteral(resourceName: "close_icon") : #imageLiteral(resourceName: "order deliverd+")
            
        }
        self.orderDetailTableView.reloadData()
        
//        if self.order.delivery_rating > 0 {
//            self.deliveryRateViewHeight.constant = 100
//            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
//            self.deliveryRatingView.isUserInteractionEnabled = false
//        }
//
//        if self.order.order_rating > 0 {
//            self.foodRateViewHeight.constant = 80
//            self.foodRatingView.rating = Double(self.order.order_rating)
//            self.foodRatingView.isUserInteractionEnabled = false
//        }
        
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation

        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .completed {
            self.contactDeliveryGuyView.isHidden = true
        }
        if order.restaurantDetail.city ==  "" {
            self.contactRestaurantView.isHidden = true
        }
        
        if order.restaurantDetail.phone == "" {
            self.restaurantPhoneBtn.isHidden = true
            self.restaurantPhoneIcon.isHidden = true
        } else {
            self.restaurantPhoneBtn.isHidden = false
            self.restaurantPhoneIcon.isHidden = false
        }
        if order.deliveryBoy.phone == "" {
            self.deliveryBoyPhoneBtn.isHidden = true
            self.deliveryBoyPhoneIcon.isHidden = true
        } else {
            self.deliveryBoyPhoneBtn.isHidden = false
            self.deliveryBoyPhoneIcon.isHidden = false
        }

        self.contactDeliveryGuyView.isHidden = false

        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .completed {
            self.contactDeliveryGuyView.isHidden = true
        }
        
        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .cancelled {
            self.contactDeliveryGuyView.isHidden = true
        }

        self.deliveryBoyNameLbl.text = "Delivery Executive (\(self.order.deliveryBoy.firstname.capitalized) \(self.order.deliveryBoy.lastname.capitalized))"

        if order.paymentMethod == .payAtStore {

        }
//        if self.order.is_rating {
//            self.submitBtnViewHeight.constant = 0
//
//            //MARK:Order
//            if self.order.order_comment != "" && self.order.order_rating == 0 {
//                self.foodRateViewHeight.constant = 150
//                self.foodRatingView.isHidden = true
//            }
//            else if self.order.order_comment != "" && self.order.order_rating != 0 {
//                self.foodRateViewHeight.constant = 150
//                self.foodReviewTxtView.borderColor = .clear
//                self.foodReviewTxtView.text = self.order.order_comment
//            }
//            else if self.order.order_comment == "" && self.order.order_rating != 0 {
//                self.foodRateViewHeight.constant = 60
//
//            }
//
//            //MARK:Delivery
//            if self.order.delivery_comment != "" && self.order.delivery_rating == 0 {
//                self.deliveryRateViewHeight.constant = 150
//                self.deliveryRatingView.isHidden = true
//            }
//            else if self.order.delivery_comment != "" && self.order.delivery_rating != 0 {
//                self.deliveryRateViewHeight.constant = 150
//                self.deliveryReviewTxtView.borderColor = .clear
//                self.deliveryReviewTxtView.text = self.order.delivery_comment
//            }
//            else if self.order.delivery_comment == "" && self.order.delivery_rating != 0 {
//                self.deliveryRateViewHeight.constant = 82
//            }
//
//            self.foodRatingView.isUserInteractionEnabled = false
//            self.deliveryRatingView.isUserInteractionEnabled = false
//            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
//            self.foodRatingView.rating = Double(self.order.order_rating)
//
//       }
        

    }

}


extension DeliveryCompleteVC {
   
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }

    @objc func notificationRecieved(notification: NSNotification) {
        ServiceManager.shared.hideLoader = true
        self.getOrderDetails()

    }
}

extension DeliveryCompleteVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if indexPath.row < order.items.count {
//            if order.items[indexPath.row].dishTypes.count == 2 {
//                return 52
//            }
//            else if order.items[indexPath.row].dishTypes.count == 3 {
//                if indexPath.section == 0 {
//                    return 70
//                }else {
//                    return 130
//                }
//
//            }
//            else if order.items[indexPath.row].dishTypes.count == 4 {
//                return 85
//            }
//        }
//     //   tableView.estimatedRowHeight = 60
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section != 1
        {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 5))
            footerView.backgroundColor = AppColor.background
            return footerView
        }else{
            
            let footerView = tableView.dequeueReusableCell(withIdentifier: "OrderFooterCell") as! OrderFooterCell
            
            footerView.orderStatus.textColor = order.status == OrderStatusEnum.cancelled.rawValue ? UIColor.red : AppColor.greenColor
            
          //  footerView.cancelReasonView.isHidden = true
          //  footerView.cancelReasonLbl.text = order.cancellationReason

            footerView.vwCancelReason.isHidden = order.status != OrderStatusEnum.cancelled.rawValue
            
            
            if order.status == "cancelled"{
                footerView.orderStatus.textColor = UIColor.red//AppColor.primaryColor
                footerView.lblCancelText.isHidden = false
                footerView.lblCancelText.text = cancelOrderStatusLabel.text
              //  footerView.cancelReasonView.isHidden = order.cancellationReason == "" ? true:false

            }else if order.status == "pending_confirmation"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "confirmed"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "delivered" {
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true

            }
            
            var status =  order.status.replacingOccurrences(of: "_", with: " ")
            
            if order.order_type == Constant.shared.DELIVERABLE {
                if status == "ready" {
                    status = "being prepared"
                }
                else if status == "picked up" {
                    status = "out for delivery"
                }
            }
            footerView.orderStatus.text = status.camelCasedString
            footerView.config(order.card)
            footerView.configurePayAtStore(_  : order.paymentMethod, cancelRequest: order.cancelRequest)
            
            if order.order_type == Constant.shared.DELIVERABLE && order.paymentMethod == .payAtStore {
                footerView.cardNumber.text = "Cash on Delivery"
            }
            
            return footerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        print(section)
        if section != 1
        {
            return 5
        }else{
            tableView.estimatedSectionFooterHeight = 103
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0  {
            return order.items.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.orderDetailCell, for: indexPath) as! OrderDetailCell
            if order.items.count > 1 && indexPath.row < order.items.count-1 {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: true)
            } else {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: false)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartTotalCell, for: indexPath) as! CartTotalCell
            cell.config(self.order)
            cell.delegate = self
            return cell
        }
    }
    
    func openTaxInfoPopUp() {
        self.taxesInfoTap()
    }
    
    func taxesInfoTap(){
        let popController = Router.viewController(with: .taxesInfo, .cart) as! TaxesInfoViewController
        
        popController.modalPresentationStyle = .overFullScreen
        popController.cancelTap = {
            self.blurView.isHidden = true
            self.dismiss(animated: true, completion: nil)
        }
        popController.placingOrder = false
        popController.taxesInfo = self.order.taxDescription
        self.window?.rootViewController?.present(popController, animated: true, completion: nil)
        blurView.isHidden = false
    }
    
}

extension DeliveryCompleteVC: CartTotalCellDelegate {
    func getOrderDetails(order_id: String, completion:@escaping (Bool) -> Void){
        
        let url = ApiName.orderDetail + "?\(Constant.shared.order_id)=\(order_id)"
        
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            
            if self.createOrderDetailModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createOrderDetailModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            self.order = Order().getOrder(jsonDic["data"] as? NSDictionary ?? [:])
            print(self.order)
            return true
        }
        return false
    }
}
