//
//  ShowDeliveryDataVC.swift
//  #Restaurants
//
//  Created by Satveer on 11/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import Cosmos

class DeliveryFeedbackCompleteVC: BaseVC {
    @IBOutlet weak var orderDetailTableView: UITableView!
    @IBOutlet weak var orderTableHeight: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var delieveryDate : UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var orderDelieveredImageHeight: NSLayoutConstraint!
    @IBOutlet weak var dishNameLeading: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var cancelOrderStatusLabel: UILabel!
    @IBOutlet weak var order_delievered: UIImageView!
    @IBOutlet weak var cancelOrderReason: UIView!
    @IBOutlet weak var cancelOrderReasonLabel: UILabel!
    @IBOutlet weak var lblOrderPlaceOn: UILabel!

//    @IBOutlet weak var foodRatingView: CosmosView!
//    @IBOutlet weak var deliveryRatingView: CosmosView!
//    @IBOutlet weak var foodReviewTxtLbl: UILabel!
//    @IBOutlet weak var deliveryReviewLbl: UILabel!
    @IBOutlet weak var contactDeliveryGuyView: UIView!
    @IBOutlet weak var contactRestaurantView: UIView!
    @IBOutlet weak var deliveryBoyImageView: UIImageView!
    @IBOutlet weak var restuantImageView: UIImageView!
    @IBOutlet weak var restuantNameLbl: UILabel!
    @IBOutlet weak var deliveryBoyNameLbl: UILabel!
//    @IBOutlet weak var foodRatingStackview: UIStackView!
//    @IBOutlet weak var deliveryRatingStackview: UIStackView!
    @IBOutlet weak var restaurantPhoneBtn: UIButton!
    @IBOutlet weak var deliveryBoyPhoneBtn: UIButton!

    @IBOutlet weak var restaurantPhoneIcon: UIImageView!
    @IBOutlet weak var deliveryBoyPhoneIcon: UIImageView!
    @IBOutlet weak var viewFeedbackBtnViewHeight: NSLayoutConstraint!

//    @IBOutlet weak var foodReviewStaticLblHeight: NSLayoutConstraint!
//    @IBOutlet weak var foodReviewStaticLblTop: NSLayoutConstraint!
//    @IBOutlet weak var deliveryReviewStaticLblHeight: NSLayoutConstraint!

//    @IBOutlet weak var deliveryContentView: UIView!
//    @IBOutlet weak var restaurantContentView: UIView!

    var foodFeedbackPlaceholder = "Write your reviews here..."
    var deliveryFeedbackPlaceholder = "Write your feedback here..."

    var pickupTitle = "Order Placed On: "
    var placedForTitle = "Order Placed For: "
    var locationTitle = "Location: "
    var order = Order()
    var deliveryCompleteVM = DeliveryCompleteVM()
    var isFromCartScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setCustomUI()
        ServiceManager.shared.hideLoader = false
       // deliveryOrderCompleteGlobal = self
    }
    
    private func setUpTableView() {
        self.orderDetailTableView.estimatedRowHeight = 80.0
        self.orderDetailTableView.rowHeight = UITableView.automaticDimension

        self.orderDetailTableView.dataSource = self
        self.orderDetailTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOrderDetails()

        orderDetailTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if orderDetailTableView.observationInfo != nil {
            orderDetailTableView.removeObserver(self, forKeyPath: "contentSize")
        }
    }

    func setCustomUI() {
        mainScrollView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        orderDetailTableView.register(UINib(nibName: Constant.shared.cartTotalCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.cartTotalCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderDetailCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderDetailCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderFooterCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderFooterCell)
        
        self.setUpTableView()
        self.mainScrollView.isHidden = true
        
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .cancelled

        if orderStatus == .cancelled {
            self.viewFeedbackBtnViewHeight.constant = 0
        }
        //self.addNotificationCenter()
    }
    
    //9june
    
    @IBAction func back_Click(_ sender: UIButton) {
        
        if Helper.shared.isFromView == "orderHistory" {
            self.navigationController!.popToRootViewController(animated: true)
        }
        else {
            
            var isMoved = false
            
            if ((self.window?.rootViewController as? UINavigationController)?.viewControllers.first as? MagicTabBar) != nil{
                isMoved = true
                if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
                    let rootNC = UINavigationController(rootViewController: magicTabBar)
                    rootNC.setNavigationBarHidden(true, animated: false)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
                }
             }
            
            
//            if let vc = self.navigationController?.viewControllers.last(where: { $0.isKind(of: HomeVC.self) }) {
//                isMoved = true
//                navigationController?.popToViewController(vc, animated: true)
//            }

            if isMoved  == false {
                self.navigationController?.popViewController(animated: true)
            }

        }

    }
    
    @IBAction func deliverygyClick(_ sender: UIButton) {
        self.openCallView(number: order.deliveryBoy.phone)
    }
    
    @IBAction func restaurantClick(_ sender: UIButton) {
        self.openCallView(number: order.restaurantDetail.phone)
    }
   
    @IBAction func viewRatingBtnClick(_ sender: UIButton) {

        if let orderStatusVC = Router.viewController(with: .completeRatingDetailVC, .profile) as? CompleteRatingDetailVC {
            orderStatusVC.order = self.order
            self.navigationController?.pushViewController(orderStatusVC, animated: true)
        }
    }

    @IBAction func reorderClcik(_ sender: UIButton) {
        if UserModel.shared.ongoingOrders.count >= 4 {
            Utility.shared.showAlertView(message: Messages.ongoingOrder)
            return
        }
        Utility.shared.startLoading()
        OrdersViewModel().reOrderService(order.id){(status, message, dic) in
            Utility.shared.stopLoading()
            if status {
                let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController
                cartVC?.isFromReOrderView = true
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(cartVC ?? CartViewController(), animated: true)
            }
        }

    }
        
}

extension DeliveryFeedbackCompleteVC {
    
    private func getOrderDetails() {
        self.getOrderDetails(order_id: order.id) { (success) in
            if success {
                ServiceManager.shared.hideLoader = false
                self.showCompletedFeedbackDetailData()
                self.mainScrollView.isHidden = false
                self.orderTableHeight.constant = self.orderDetailTableView.contentSize.height
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        orderTableHeight.constant = orderDetailTableView.contentSize.height
    }

    
    private func showCompletedFeedbackDetailData() {
        
        orderNumber.text = order.number
        print(order.orderPlacedOn)
        let del_date = order.orderPlacedOn.components(separatedBy: ",")
        
        if del_date.count > 1 {
            let finaldate = "\(del_date[0])" + "," + "\(del_date[1])" + " " + "at" + "\(del_date[2])"
            self.lblOrderPlaceOn.attributedText = pickupTitle.attributedString(fullText: finaldate, size: 12.0)
            
        }
        
        self.deliveryBoyImageView.set_image(order.deliveryBoy.photo, placeholder: "deliveryicon")
        self.restuantImageView.set_image(order.restaurantDetail.logo, placeholder: "restauranticon")

        
        if order.order_type == Constant.shared.DELIVERABLE {
            self.lblLocation.attributedText = locationTitle.attributedString(fullText: order.locationTitle, size: 12.0)
            self.lblAddress.text = ""
        } else {
            let address = "\(order.locationTitle)\n(\(order.restaurantAddressModel.address_line_1),\(order.restaurantAddressModel.city),\(order.restaurantAddressModel.state),\(order.restaurantAddressModel.zip))"
            self.lblAddress.text = address
        }

        orderDelieveredImageHeight.constant = order.status == "previous" ? 19 : 0
        
        dishNameLeading.constant = order.status == "previous" ? 10 : 0
        
        //cancelOrderView.isHidden = order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        cancelOrderView.isHidden = true//GSD order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        
        cancelOrderReason.isHidden = order.cancellationReason == "" ? true:false

        cancelOrderReasonLabel.text = order.cancellationReason
        
        cancelOrderReasonLabel.textColor = UIColor.red
        
        self.showStatusAccordingPaymentType(order: self.order, statusLabel: cancelOrderStatusLabel)

        let type = order.status == "delivered" || order.status == "cancelled" ? "previous" : "ongoing"
        
        if type == "ongoing"
        {
        }else{
           // thumbImage.isHidden = "\(order.thumb)" == "" || order.status == "cancelled" ? true : false
           // thumbImage.image = "\(order.thumb)" == "up" ? #imageLiteral(resourceName: "Up") : #imageLiteral(resourceName: "Down")
            order_delievered.image = order.status == OrderStatusEnum.cancelled.rawValue ? #imageLiteral(resourceName: "close_icon") : #imageLiteral(resourceName: "Tick In colored")
            
        }
        self.orderDetailTableView.reloadData()
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation

//        if self.order.delivery_rating > 0 {
//            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
//            self.deliveryRatingView.isUserInteractionEnabled = false
//        }
//
//        if self.order.order_rating > 0 {
//            self.foodRatingView.rating = Double(self.order.order_rating)
//            self.foodRatingView.isUserInteractionEnabled = false
//        }
        
        self.contactDeliveryGuyView.isHidden = false

        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .completed {
            self.contactDeliveryGuyView.isHidden = true
        }
        
        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .cancelled {
            self.contactDeliveryGuyView.isHidden = true
        }

        if order.restaurantDetail.city ==  "" {
            self.contactRestaurantView.isHidden = true
        }
        
        if order.restaurantDetail.phone == "" {
            self.restaurantPhoneBtn.isHidden = true
            self.restaurantPhoneIcon.isHidden = true
        } else {
            self.restaurantPhoneBtn.isHidden = false
            self.restaurantPhoneIcon.isHidden = false
        }
        if order.deliveryBoy.phone == "" {
            self.deliveryBoyPhoneBtn.isHidden = true
            self.deliveryBoyPhoneIcon.isHidden = true
        } else {
            self.deliveryBoyPhoneBtn.isHidden = false
            self.deliveryBoyPhoneIcon.isHidden = false
        }

        
        self.deliveryBoyNameLbl.text = "Delivery Executive (\(self.order.deliveryBoy.firstname.capitalized) \(self.order.deliveryBoy.lastname.capitalized))"

        
        if self.order.is_rating {
            //MARK:Order
            
//            if self.order.order_rating == 0 {
//                self.restaurantContentView.isHidden = true
//            } else {
//                self.restaurantContentView.isHidden = false
//            }
//
//            if self.order.delivery_rating == 0 {
//                self.deliveryContentView.isHidden = true
//            } else {
//                self.deliveryContentView.isHidden = false
//            }
//


//            if self.order.order_comment != "" && self.order.order_rating == 0 {
//                self.foodRatingView.isHidden = true
//            }
//            else if self.order.order_comment != "" && self.order.order_rating != 0 {
//                self.foodReviewTxtLbl.text = self.order.order_comment
//            }
//            else if self.order.order_comment == "" && self.order.order_rating != 0 {
//                self.foodReviewTxtLbl.isHidden = true
//                self.foodReviewStaticLblHeight.constant = 0
//                self.foodReviewStaticLblTop.constant = -10
//            }
//            else {
//                self.restaurantContentView.isHidden = true
//            }
            
            //MARK:Delivery
//            if self.order.delivery_comment != "" && self.order.delivery_rating == 0 {
//                self.deliveryRatingView.isHidden = true
//            }
//            else if self.order.delivery_comment != "" && self.order.delivery_rating != 0 {
//                self.deliveryReviewLbl.text = self.order.delivery_comment
//            }
//            else if self.order.delivery_comment == "" && self.order.delivery_rating != 0 {
//                self.deliveryReviewLbl.isHidden = true
//                //self.deliveryReviewStaticLblHeight.constant = 0
//            }
//            else if self.order.delivery_comment == "" && self.order.delivery_rating == 0 {
//                self.deliveryReviewLbl.isHidden = true
//                self.deliveryReviewStaticLblHeight.constant = 0
//            }
//            else {
//                self.deliveryContentView.isHidden = true
//            }
            
//            self.foodRatingView.isUserInteractionEnabled = false
//            self.deliveryRatingView.isUserInteractionEnabled = false
//            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
//            self.foodRatingView.rating = Double(self.order.order_rating)
//
//
//            if self.order.delivery_rating == 0 && order.order_type == Constant.shared.DELIVERABLE {
//                self.deliveryContentView.isHidden = false
//                self.deliveryRatingView.rating = 3.0
//            }
        }
        
//        if self.order.order_type != Constant.shared.DELIVERABLE  {
//            self.deliveryContentView.isHidden = true
//        }
//        if orderStatus == .cancelled {
//            self.restaurantContentView.isHidden = true
//            self.deliveryContentView.isHidden = true
//        }
        
        
    }
    

}

extension DeliveryFeedbackCompleteVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row < order.items.count {
            if order.items[indexPath.row].dishTypes.count == 2 {
                return 52
            }
            else if order.items[indexPath.row].dishTypes.count == 3 {
                if indexPath.section == 0 {
                    return 70
                }else {
                    return 130
                }
                
            }
            else if order.items[indexPath.row].dishTypes.count == 4 {
                return 85
            }
        }
     //   tableView.estimatedRowHeight = 60
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section != 1
        {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 5))
            footerView.backgroundColor = AppColor.background
            return footerView
        }else{
            
            let footerView = tableView.dequeueReusableCell(withIdentifier: "OrderFooterCell") as! OrderFooterCell
            
            footerView.orderStatus.textColor = order.status == OrderStatusEnum.cancelled.rawValue ? UIColor.red : AppColor.greenColor
            
            footerView.vwCancelReason.isHidden = order.status != OrderStatusEnum.cancelled.rawValue
          //  footerView.cancelReasonView.isHidden = true

            if order.status == "cancelled"{
                
                footerView.orderStatus.textColor = UIColor.red//AppColor.primaryColor
                footerView.lblCancelText.isHidden = false
                footerView.lblCancelText.text = cancelOrderStatusLabel.text
//                footerView.cancelReasonView.isHidden = order.cancellationReason == "" ? true:false
//                footerView.cancelReasonLbl.text = order.cancellationReason

            }else if order.status == "pending_confirmation"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "confirmed"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "delivered" {

                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true

            }
            var status =  order.status.replacingOccurrences(of: "_", with: " ")
            if order.order_type == Constant.shared.DELIVERABLE {
                if status == "ready" {
                    status = "being prepared"
                }
                else if status == "picked up" {
                    status = "out for delivery"
                }
            }
            
            footerView.orderStatus.text = status.camelCasedString
            footerView.config(order.card)
            footerView.configurePayAtStore(_  : order.paymentMethod, cancelRequest: order.cancelRequest)
            
            if order.order_type == Constant.shared.DELIVERABLE && order.paymentMethod == .payAtStore {
                footerView.cardNumber.text = "Cash on Delivery"
            }

            return footerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        print(section)
        if section != 1
        {
            return 5
        }else{
            
//            let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation
//
//            if orderStatus != .cancelled {
//                return 100
//            }
            tableView.estimatedSectionFooterHeight = 103
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0  {
            return order.items.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.orderDetailCell, for: indexPath) as! OrderDetailCell
            if order.items.count > 1 && indexPath.row < order.items.count-1 {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: true)
            } else {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: false)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartTotalCell, for: indexPath) as! CartTotalCell
            cell.config(self.order)
            cell.delegate = self
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
//    func tableView(tableView: UITableView,
//        heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
//    {
//
//        return UITableView.automaticDimension
//    }
    
    func openTaxInfoPopUp() {
        self.taxesInfoTap()
    }
    
    func taxesInfoTap(){
        let popController = Router.viewController(with: .taxesInfo, .cart) as! TaxesInfoViewController
        
        popController.modalPresentationStyle = .overFullScreen
        popController.cancelTap = {
            self.blurView.isHidden = true
            self.dismiss(animated: true, completion: nil)
        }
        popController.placingOrder = false
        popController.taxesInfo = self.order.taxDescription
        self.window?.rootViewController?.present(popController, animated: true, completion: nil)
        blurView.isHidden = false
    }
    
}

extension DeliveryFeedbackCompleteVC: CartTotalCellDelegate {
    func getOrderDetails(order_id: String, completion:@escaping (Bool) -> Void){
        
        let url = ApiName.orderDetail + "?\(Constant.shared.order_id)=\(order_id)"
        
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            
            if self.createOrderDetailModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createOrderDetailModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            self.order = Order().getOrder(jsonDic["data"] as? NSDictionary ?? [:])
            print(self.order)
            return true
        }
        return false
    }
}
