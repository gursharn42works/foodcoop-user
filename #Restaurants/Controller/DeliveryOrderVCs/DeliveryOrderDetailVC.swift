//
//  DeliveryOrderDetailVC.swift
//  #Restaurants
//
//  Created by Satveer on 08/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import FirebaseDatabase

protocol DeliveryOrderDetailVCDeleagte: class {
    func callBottomViewMethonOnBack()
}

class DeliveryOrderDetailVC: BaseVC {
    @IBOutlet weak var orderDetailTableView: UITableView!
    @IBOutlet weak var orderTableHeight: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var delieveryDate : UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var orderDelieveredImageHeight: NSLayoutConstraint!
    @IBOutlet weak var dishNameLeading: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var cancelOrderView: UIView!
    @IBOutlet weak var cancelOrderStatusLabel: UILabel!
    @IBOutlet weak var order_delievered: UIImageView!
    @IBOutlet weak var cancelOrderReason: UIView!
    @IBOutlet weak var cancelOrderReasonLabel: UILabel!
    @IBOutlet weak var lblOrderPlaceOn: UILabel!
    @IBOutlet weak var mapView: GMSMapView!

    @IBOutlet weak var deliveryBoyImageView: UIImageView!
    @IBOutlet weak var restuantImageView: UIImageView!
    @IBOutlet weak var reOrderBtn: UIButton!
    @IBOutlet weak var rateThisOrderBtn: UIButton!

    @IBOutlet weak var contactDeliveryGuyView: UIView!
    @IBOutlet weak var contactRestaurantView: UIView!
    @IBOutlet weak var orderDetailViewTop: NSLayoutConstraint!

    @IBOutlet weak var restuantNameLbl: UILabel!
    @IBOutlet weak var deliveryBoyNameLbl: UILabel!

    @IBOutlet weak var restaurantPhoneBtn: UIButton!
    @IBOutlet weak var deliveryBoyPhoneBtn: UIButton!
    @IBOutlet weak var restaurantPhoneIcon: UIImageView!
    @IBOutlet weak var deliveryBoyPhoneIcon: UIImageView!
    @IBOutlet weak var contactViewHeight: NSLayoutConstraint!

    var pickupTitle = "Order Placed On: "
    var placedForTitle = "Order Placed For: "
    var locationTitle = "Location: "
    var order = Order()
    lazy var ordersViewModel = OrdersViewModel()
    var cancelOrder: (()-> Void)?
    var orderRated: ((String)-> Void)?
    var isFromCartScreen = false
    var isFromNotification = false
    var isCurrentScreen = false
   // var isFromNotificationClick = false
    var isPushedToView = false
    var ref: DatabaseReference!
    var firebaseModel = FireBaseDataModel()
    var isFirstTime = false
    var deliveryBoyMarker = GMSMarker()
    var isFromNotificationTab = false
    var delegate: DeliveryOrderDetailVCDeleagte?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setCustomUI()
    }
    
    private func setUpTableView() {
        self.orderDetailTableView.estimatedRowHeight = 80.0
        self.orderDetailTableView.rowHeight = UITableView.automaticDimension


        self.orderDetailTableView.dataSource = self
        self.orderDetailTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getOrderDetails()
        orderDetailTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        
        self.isCurrentScreen = true
        self.isPushedToView = false
        self.addNotificationCenter()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if orderDetailTableView.observationInfo != nil {
            orderDetailTableView.removeObserver(self, forKeyPath: "contentSize")
        }
        
        self.isCurrentScreen = false
        NotificationCenter.default.removeObserver(self)
    }

    func setCustomUI() {
        
        ref = Database.database().reference()
        ServiceManager.shared.hideLoader = false
    
        DeliveryAddressModel().removeSavedDeliveryAddress()

        mainScrollView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        orderDetailTableView.register(UINib(nibName: Constant.shared.cartTotalCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.cartTotalCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderDetailCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderDetailCell)
        orderDetailTableView.register(UINib(nibName: Constant.shared.orderFooterCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderFooterCell)
        
        self.setUpTableView()
        self.mainScrollView.isHidden = true
    }
    
    @IBAction func button_Click(_ sender: UIButton) {
            if sender.title(for: .normal) == "VIEW ORDER STATUS".uppercased(){
                self.trackStatus(order)
            }else if sender.title(for: .normal) == "CANCEL ORDER".uppercased(){
                self.cancelOrderTap()
            }else if sender.title(for: .normal) == "Reorder".uppercased(){
                self.reOrderTap(order)
            }else{
                self.rateOrderTap(order)
            }
    }
    
    @IBAction func deliverygyClick(_ sender: UIButton) {
        self.openCallView(number: order.deliveryBoy.phone)
    }
    
    @IBAction func restaurantClick(_ sender: UIButton) {
        self.openCallView(number: order.restaurantDetail.phone)
    }
    
    @IBAction func back_Click(_ sender: UIButton) {
        
        self.delegate?.callBottomViewMethonOnBack()
        
        DispatchQueue.main.async { //9june
            if self.isFromCartScreen {
                if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
                    let rootNC = UINavigationController(rootViewController: magicTabBar)
                    rootNC.setNavigationBarHidden(true, animated: false)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
                }
//                if ((((self.window?.rootViewController as? UINavigationController)?.viewControllers.first as? MagicTabBar)?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? HomeVC) != nil{
//
//                    self.navigationController?.popToRootViewController(animated: true)
//                 }
                
            }else {
                if let navController = self.navigationController {
                    navController.popViewController(animated: true)
                }
            }
        }
    }
    
    
    func reOrderTap(_ order : Order) {
        if UserModel.shared.ongoingOrders.count >= 4 {
            Utility.shared.showAlertView(message: Messages.ongoingOrder)
            return
        }
        Utility.shared.startLoading()
        ordersViewModel.reOrderService(order.id){(status, message, dic) in
            Utility.shared.stopLoading()
            if status {
                let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(cartVC ?? CartViewController(), animated: true)
            }
        }

    }
    
    func cancelOrderTap() {
        
        if order.cancelRequest == 1 {
            Utility.shared.showAlertView(message: Messages.approvalPending)
            return;
        }
        
        self.openCancelliationPolicyView(order_id: "\(order.id)")

    }
    
    func trackStatus(_ order: Order) {
        if let orderStatusVC = Router.viewController(with: .orderStatusVC, .profile) as? OrderStatusVC {
            orderStatusVC.order.id = order.id
            self.navigationController?.pushViewController(orderStatusVC, animated: true)
        }
    }

    func rateOrderTap(_ order : Order) {
        
        let vc = Router.viewController(with: .deliveryCompleteVC, .profile) as! DeliveryCompleteVC
        
        vc.modalPresentationStyle = .overFullScreen
        vc.order = order
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension DeliveryOrderDetailVC {

    func getDeliveryLocationFromFireBase() {
       
        ref.child("delivery_boy").child("\(order.deliveryBoy.deliveryBoyId)").observe(.value) { (snapshot) in
            // Get user value
            let orderStatus = OrderStatusEnum(rawValue: self.order.status) ?? .pendingConfirmation

            if orderStatus == .completed || orderStatus == .cancelled {
                return
            }
              let value = snapshot.value as? NSDictionary ?? [:]
              self.firebaseModel = self.firebaseModel.setFireBaseModelData(dic: value)

              if self.firebaseModel.lat != 0.0 && self.firebaseModel.lng != 0.0 {
                self.showDeliveryBoyMarkerOnMap(angle: self.firebaseModel.angle, lat: self.firebaseModel.lat, lng: self.firebaseModel.lng,image: "deliveryicon")
              }
            }
    }
    
    func showMarkerOnMap(lat: Double, lng: Double,image: String, isAddress: Bool) {
        let marker = GMSMarker()
        marker.map = self.mapView
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        marker.position = position
        marker.icon = UIImage(named: image)
       // marker.setIconSize(scaledToSize: .init(width: 35, height: 35))

        if isAddress {
            cameraMoveToLocation(position: position)
        }
    }
    
    func showDeliveryBoyMarkerOnMap(angle: Double, lat: Double, lng: Double,image: String) {
        self.deliveryBoyMarker.map = nil
        self.deliveryBoyMarker.map = self.mapView
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        deliveryBoyMarker.position = position
        deliveryBoyMarker.icon = UIImage(named: image)
        deliveryBoyMarker.rotation = angle
       // deliveryBoyMarker.setIconSize(scaledToSize: .init(width: 45, height: 45))


    }
    
    func cameraMoveToLocation(position: CLLocationCoordinate2D?) {
        if position != nil {
            self.mapView.camera = GMSCameraPosition.camera(withTarget: position!, zoom: 10)
        }
    }
    
}

extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}


extension DeliveryOrderDetailVC {
    
    private func getOrderDetails() {
        self.getOrderDetails(order_id: order.id) { (success) in
            if success {
                ServiceManager.shared.hideLoader = false
                self.moveToNextViewAccoringStatus()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                    self.showOrderDetailData()
                    self.mainScrollView.isHidden = false
                    self.orderTableHeight.constant = self.orderDetailTableView.contentSize.height
                }
            }
        }
    }
    
    func moveToNextViewAccoringStatus() {
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation

        if orderStatus == .completed && self.isFromNotification && isCurrentScreen {
            
            self.isFromNotification = false
            if let orderStatusVC = Router.viewController(with: .deliveryCompleteVC, .profile) as? DeliveryCompleteVC {
                orderStatusVC.order = order
                orderStatusVC.isFromCartScreen = self.isFromCartScreen
                self.navigationController?.pushViewController(orderStatusVC, animated: false)
            }
        }
        else if orderStatus == .cancelled && self.isFromNotification && isCurrentScreen {
            
            self.isFromNotification = false
            if let orderStatusVC = Router.viewController(with: .deliveryFeedbackCompleteVC, .profile) as? DeliveryFeedbackCompleteVC {
                orderStatusVC.order = order
                self.navigationController?.pushViewController(orderStatusVC, animated: false)
            }
        }
        else if isFromNotificationTab && orderStatus == .completed {
            if let orderStatusVC = Router.viewController(with: .deliveryCompleteVC, .profile) as? DeliveryCompleteVC {
                orderStatusVC.order = order
                orderStatusVC.isFromCartScreen = self.isFromCartScreen
                orderStatusVC.isFromNotificationTab = self.isFromNotificationTab
                self.navigationController?.pushViewController(orderStatusVC, animated: false)
            }
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        orderTableHeight.constant = orderDetailTableView.contentSize.height
    }

    
    private func showOrderDetailData() {
        
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation
        
        //MARK: Show data on Mapview

        if self.order.order_type == Constant.shared.DELIVERABLE {
            
            if self.order.restaurantDetail.lat != 0.0 && self.order.restaurantDetail.lng != 0.0 {
                
                self.showMarkerOnMap(lat: self.order.restaurantDetail.lat, lng: self.order.restaurantDetail.lng,image: "restauranticon", isAddress: false)
            }
            
            if self.order.order_type == Constant.shared.DELIVERABLE {
                self.showMarkerOnMap(lat: order.deliveryAddress.lat, lng: order.deliveryAddress.lng, image: "userredplaceholder", isAddress: true)
                if orderStatus != .pendingConfirmation && orderStatus != .completed || orderStatus != .cancelled{
                    self.getDeliveryLocationFromFireBase()
                }
            }else {
                //self.locationManager.startUpdatingLocation()
            }
        }
        
        self.deliveryBoyImageView.set_image(order.deliveryBoy.photo, placeholder: "deliveryicon")
        self.restuantImageView.set_image(order.restaurantDetail.logo, placeholder: "restauranticon")

        orderNumber.text = order.number
        print(order.orderPlacedOn)
        let del_date = order.orderPlacedOn.components(separatedBy: ",")
        let finaldate = "\(del_date[0])" + "," + "\(del_date[1])" + " " + "at" + "\(del_date[2])"
        self.lblOrderPlaceOn.attributedText = pickupTitle.attributedString(fullText: finaldate, size: 12.0)
        
        if order.order_type == Constant.shared.DELIVERABLE {
            self.lblLocation.attributedText = locationTitle.attributedString(fullText: order.locationTitle, size: 12.0)
            self.lblAddress.text = ""
        } else {
            let address = "\(order.locationTitle)\n(\(order.restaurantAddressModel.address_line_1),\(order.restaurantAddressModel.city),\(order.restaurantAddressModel.state),\(order.restaurantAddressModel.zip))"
            self.lblAddress.text = address
        }
        

        orderDelieveredImageHeight.constant = order.status == "previous" ? 19 : 0
        
        dishNameLeading.constant = order.status == "previous" ? 10 : 0
        
        //cancelOrderView.isHidden = order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        cancelOrderView.isHidden = true//GSD order.status == OrderStatus.cancelled.rawValue || order.cancelRequest == 1 ? false : true
        
        cancelOrderReason.isHidden = true//GSD order.status == OrderStatus.cancelled.rawValue && !(order.cancelRequest == 2) ? false : true
        
        cancelOrderReasonLabel.text = order.cancellationReason
        
        cancelOrderReasonLabel.textColor = UIColor.red
        
        self.showStatusAccordingPaymentType(order: self.order, statusLabel: cancelOrderStatusLabel)

        let type = order.status == "delivered" || order.status == "cancelled" ? "previous" : "ongoing"
        
        if type == "ongoing"
        {
            reOrderBtn.setTitle("View order status".uppercased(), for: .normal)
            if order.paymentMethod == .payByCard {
                if orderStatus == .completed || orderStatus == .cancelled{
                    self.rateThisOrderBtn.isHidden = true
                } else {
                    rateThisOrderBtn.isHidden = false
                }
            } else {
                rateThisOrderBtn.isHidden = true
            }

            
        }else{
            reOrderBtn.setTitle("Reorder".uppercased(), for: .normal)
            rateThisOrderBtn.setTitle("Rate this order".uppercased(), for: .normal)
           // thumbImage.isHidden = "\(order.thumb)" == "" || order.status == "cancelled" ? true : false
           // thumbImage.image = "\(order.thumb)" == "up" ? #imageLiteral(resourceName: "Up") : #imageLiteral(resourceName: "Down")
            rateThisOrderBtn.isHidden = "\(order.thumb)" == "" && order.status != "cancelled" ? false : true
            order_delievered.image = order.status == OrderStatusEnum.cancelled.rawValue ? #imageLiteral(resourceName: "close_icon") : #imageLiteral(resourceName: "Tick In colored")
            
            if "\(order.thumb)" == "up" && order.status == "delivered"{
                rateThisOrderBtn.isHidden = true
            }
        }
        print(order.status)
        
        if order.status == "cancelled"{
            rateThisOrderBtn.isHidden = true
            reOrderBtn.setTitle("Reorder".uppercased(), for: .normal)
            reOrderBtn.isHidden = false
        }else if order.status == "pending_confirmation" || order.status == "preparing" || order.status == "ready" {
            rateThisOrderBtn.isHidden = false
            rateThisOrderBtn.setTitle("Cancel Order".uppercased(), for: .normal)
            reOrderBtn.isHidden = false
            reOrderBtn.setTitle("View order status".uppercased(), for: .normal)
        }else if order.status == "confirmed"{
            if order.paymentMethod == .payByCard {
                rateThisOrderBtn.isHidden = false
            } else {
                rateThisOrderBtn.isHidden = true
            }
            reOrderBtn.isHidden = false
            reOrderBtn.setTitle("View order status".uppercased(), for: .normal)
        }else if order.status == "delivered" {
            
            reOrderBtn.isHidden = false
            rateThisOrderBtn.setTitle("Rate this order".uppercased(), for: .normal)
            reOrderBtn.setTitle("Reorder".uppercased(), for: .normal)
            
            if "\(order.thumb)" == "up" ||  "\(order.thumb)" == "down" {
                rateThisOrderBtn.isHidden = true
            }
            else {
                rateThisOrderBtn.isHidden = false
            }
            
        }
        
        if order.delivery_rating > 0 || order.order_rating > 0 {
            self.rateThisOrderBtn.isHidden = true
        }
        
        if  orderStatus == .preparingOrder  && order.order_type == Constant.shared.DELIVERABLE {
            self.rateThisOrderBtn.isHidden = true
        }
        
        
        if order.restaurantDetail.city ==  "" {
            self.contactRestaurantView.isHidden = true
        }
        
        if order.restaurantDetail.phone == "" {
            self.restaurantPhoneBtn.isHidden = true
            self.restaurantPhoneIcon.isHidden = true
        } else {
            self.restaurantPhoneBtn.isHidden = false
            self.restaurantPhoneIcon.isHidden = false
        }
        if order.deliveryBoy.phone == "" {
            self.deliveryBoyPhoneBtn.isHidden = true
            self.deliveryBoyPhoneIcon.isHidden = true
        } else {
            self.deliveryBoyPhoneBtn.isHidden = false
            self.deliveryBoyPhoneIcon.isHidden = false
        }

        self.contactDeliveryGuyView.isHidden = false

        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .completed {
            self.contactDeliveryGuyView.isHidden = true
        }
        
        if self.order.deliveryBoy.delivery_boy_id == 0 || orderStatus == .cancelled {
            self.contactDeliveryGuyView.isHidden = true
        }

        self.deliveryBoyNameLbl.text = "Delivery Executive (\(self.order.deliveryBoy.firstname.capitalized) \(self.order.deliveryBoy.lastname.capitalized))"
        
        if orderStatus == .cancelled || self.order.order_type != Constant.shared.DELIVERABLE {
            self.mapView.isHidden = true
            self.orderDetailViewTop.constant = 0
        }
        


        
        self.orderDetailTableView.reloadData()
        
        if orderStatus != .pendingConfirmation && order.order_type == Constant.shared.DELIVERABLE {
            if order.paymentMethod == .payByCard {
                if orderStatus == .completed || orderStatus == .cancelled{
                    self.rateThisOrderBtn.isHidden = true
                } else {
                    self.rateThisOrderBtn.isHidden = false
                }
            } else {
                self.rateThisOrderBtn.isHidden = true
            }
        }
        else if orderStatus != .pendingConfirmation && order.paymentMethod == .payAtStore {
            self.rateThisOrderBtn.isHidden = true
        }

        if self.order.deliveryBoy.phone == "" && self.order.restaurantDetail.phone == "" {
            self.contactViewHeight.constant = 0
        }
        
        if order.paymentMethod == .payAtStore {
            self.rateThisOrderBtn.isHidden = true
        }

    }
    
    //put this code in your viewController class
     func drawImageWithProfilePic(pp: UIImage, image: UIImage) -> UIImage {

            let imgView = UIImageView(image: image)
             imgView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)

            let picImgView = UIImageView(image: pp)
            picImgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)

            imgView.addSubview(picImgView)
            picImgView.center.x = imgView.center.x
            picImgView.center.y = imgView.center.y - 7
            picImgView.layer.cornerRadius = picImgView.frame.width/2
            picImgView.clipsToBounds = true
            imgView.setNeedsLayout()
            picImgView.setNeedsLayout()

            let newImage = imageWithView(view: imgView)
            return newImage
        }

        func imageWithView(view: UIView) -> UIImage {
            var image: UIImage?
            UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
            if let context = UIGraphicsGetCurrentContext() {
                view.layer.render(in: context)
                image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            }
            return image ?? UIImage()
        }

}


extension DeliveryOrderDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if indexPath.row < order.items.count {
//            if order.items[indexPath.row].dishTypes.count == 2 {
//                return 52
//            }
//            else if order.items[indexPath.row].dishTypes.count == 3 {
//                if indexPath.section == 0 {
//                    return 70
//                }else {
//                    return 130
//                }
//
//            }
//            else if order.items[indexPath.row].dishTypes.count == 4 {
//                return 85
//            }
//        }
//     //   tableView.estimatedRowHeight = 60
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section != 1
        {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 5))
            footerView.backgroundColor = AppColor.background
            return footerView
        }else{
            
            let footerView = tableView.dequeueReusableCell(withIdentifier: "OrderFooterCell") as! OrderFooterCell
            
            footerView.orderStatus.textColor = order.status == OrderStatusEnum.cancelled.rawValue ? UIColor.red : AppColor.greenColor
            
          //  footerView.cancelReasonView.isHidden = true
          //  footerView.cancelReasonLbl.text = order.cancellationReason

            footerView.vwCancelReason.isHidden = order.status != OrderStatusEnum.cancelled.rawValue
            
            
            if order.status == "cancelled"{
                footerView.orderStatus.textColor = UIColor.red//AppColor.primaryColor
                footerView.lblCancelText.isHidden = false
                footerView.lblCancelText.text = cancelOrderStatusLabel.text
              //  footerView.cancelReasonView.isHidden = order.cancellationReason == "" ? true:false

            }else if order.status == "pending_confirmation"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "confirmed"{
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true


                }else if order.status == "delivered" {
                    footerView.orderStatus.textColor = AppColor.greenColor
                    footerView.lblCancelText.isHidden = true

            }
            
            var status =  order.status.replacingOccurrences(of: "_", with: " ")
            
            if order.order_type == Constant.shared.DELIVERABLE {
                if status == "ready" {
                    status = "being prepared"
                }
                else if status == "picked up" {
                    status = "out for delivery"
                }
            }
            footerView.orderStatus.text = status.camelCasedString
            footerView.config(order.card)
            footerView.configurePayAtStore(_  : order.paymentMethod, cancelRequest: order.cancelRequest)
            
            if order.order_type == Constant.shared.DELIVERABLE && order.paymentMethod == .payAtStore {
                footerView.cardNumber.text = "Cash on Delivery"
            }
            
            return footerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        print(section)
        if section != 1
        {
            return 5
        }else{
            tableView.estimatedSectionFooterHeight = 103
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0  {
            return order.items.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.orderDetailCell, for: indexPath) as! OrderDetailCell
            if order.items.count > 1 && indexPath.row < order.items.count-1 {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: true)
            } else {
                cell.showCellData(order.items[indexPath.row], dividerLineShow: false)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.cartTotalCell, for: indexPath) as! CartTotalCell
            cell.config(self.order)
            cell.delegate = self
            return cell
        }
    }
    
    func openTaxInfoPopUp() {
        self.taxesInfoTap()
    }
    
    func taxesInfoTap(){
        let popController = Router.viewController(with: .taxesInfo, .cart) as! TaxesInfoViewController
        
        popController.modalPresentationStyle = .overFullScreen
        popController.cancelTap = {
            self.blurView.isHidden = true
            self.dismiss(animated: true, completion: nil)
        }
        popController.placingOrder = false
        popController.taxesInfo = self.order.taxDescription
        self.window?.rootViewController?.present(popController, animated: true, completion: nil)
        blurView.isHidden = false
    }
    
}

extension DeliveryOrderDetailVC: CartTotalCellDelegate {
    func getOrderDetails(order_id: String, completion:@escaping (Bool) -> Void){
        
        let url = ApiName.orderDetail + "?\(Constant.shared.order_id)=\(order_id)"
        
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            
            if self.createOrderDetailModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createOrderDetailModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            self.order = Order().getOrder(jsonDic["data"] as? NSDictionary ?? [:])
            print(self.order)
            return true
        }
        return false
    }
}

extension DeliveryOrderDetailVC {
   
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }

    @objc func notificationRecieved(notification: NSNotification) {
        ServiceManager.shared.hideLoader = true
        self.isFromNotification = true
        self.getOrderDetails()

    }

}
