//
//  RatingDetailVC.swift
//  #Restaurants
//
//  Created by Satveer on 07/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import Cosmos

class RatingDetailVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deliveryRateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foodRateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var itemViewHeight: NSLayoutConstraint!
    @IBOutlet weak var foodRatingView: CosmosView!
    @IBOutlet weak var deliveryRatingView: CosmosView!
    @IBOutlet weak var foodReviewTxtView: UITextView!
    @IBOutlet weak var deliveryReviewTxtView: UITextView!
    @IBOutlet weak var foodrateBottomLine: UIView!
    @IBOutlet weak var deliveryViewBottomLine: UIView!
    @IBOutlet weak var foodItemsTitleLbl: UILabel!
    
    @IBOutlet weak var reasonBackView_Out: UIView!
    
    @IBOutlet weak var popupBackView_Out: UIView!
    
    @IBOutlet weak var reasonTbl_Out: UITableView!
    

    var foodFeedbackPlaceholder = "Write your reviews here..."
    var deliveryFeedbackPlaceholder = "Write your feedback here..."
    var order = Order()
    var ratingsArray: [Dictionary<String, Int>] = []
    var finalrating: [Dictionary<String, String>] = []
    var deliveryCompleteVM = DeliveryCompleteVM()
    var selelectedReason = ""
    var reasonArray = ["My order was delayed","Packaging/Spillage Issues","Food Quality was not good","Incorrect/Less items were sent","Found an object in my order"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableViewUI()
        self.setUpData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.reasonBackView_Out.isHidden = true
        self.popupBackView_Out.isHidden = true
    }
    
    func setTableViewUI() {
        self.tableView.delegate  = self
        self.tableView.dataSource = self
    }
    

    
    @IBAction func backBtnClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func popupCross_Btn(_ sender: Any) {
        self.reasonBackView_Out.isHidden = true
        self.popupBackView_Out.isHidden = true
    }
    
    @IBAction func popupSubmit_Btn(_ sender: Any) {
        
        if selelectedReason == "" {
            Utility.shared.showAlertView(message: "Please select reason")
        }else {
            self.reasonBackView_Out.isHidden = true
            self.popupBackView_Out.isHidden = true
            SubmitRating()
        }
        
        
    }
    
    func setUpData() {
        self.foodItemsTitleLbl.text = order.items.count == 1 || order.items.count == 0 ? "Food Item Rating:":"Food Item(s) Rating:"
        if order.order_type != Constant.shared.DELIVERABLE {
            self.deliveryRateViewHeight.constant = 0
        }

        if self.order.delivery_rating > 0 {
            self.deliveryRateViewHeight.constant = 100
            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
            self.deliveryRatingView.isUserInteractionEnabled = false
        }
        
        if self.order.order_rating > 0 {
            self.foodRateViewHeight.constant = 80
            self.foodRatingView.rating = Double(self.order.order_rating)
            self.foodRatingView.isUserInteractionEnabled = false
        }
        
        
        if self.order.is_rating {
            
            //MARK:Order
            if self.order.order_comment != "" && self.order.order_rating == 0 {
                self.foodRateViewHeight.constant = 150
                self.foodRatingView.isHidden = true
            }
            else if self.order.order_comment != "" && self.order.order_rating != 0 {
                self.foodRateViewHeight.constant = 150
                self.foodReviewTxtView.borderColor = .clear
                self.foodReviewTxtView.text = self.order.order_comment
            }
            else if self.order.order_comment == "" && self.order.order_rating != 0 {
                self.foodRateViewHeight.constant = 60
                
            }
            
            //MARK:Delivery
            if self.order.delivery_comment != "" && self.order.delivery_rating == 0 {
                self.deliveryRateViewHeight.constant = 150
                self.deliveryRatingView.isHidden = true
            }
            else if self.order.delivery_comment != "" && self.order.delivery_rating != 0 {
                self.deliveryRateViewHeight.constant = 150
                self.deliveryReviewTxtView.borderColor = .clear
                self.deliveryReviewTxtView.text = self.order.delivery_comment
            }
            else if self.order.delivery_comment == "" && self.order.delivery_rating != 0 {
                self.deliveryRateViewHeight.constant = 82
            }

            self.foodRatingView.isUserInteractionEnabled = false
            self.deliveryRatingView.isUserInteractionEnabled = false
            self.deliveryRatingView.rating = Double(self.order.delivery_rating)
            self.foodRatingView.rating = Double(self.order.order_rating)

       }
        
        DispatchQueue.main.async {
            self.sizeHeaderToFit(tableView: self.tableView)
            if self.order.items.count == 0 {
                self.deliveryViewBottomLine.isHidden = true
            }
        }



    }
}

extension RatingDetailVC: UITextViewDelegate {
    
    func checkText(textView: UITextView)-> String {
        if textView.text! ==  foodFeedbackPlaceholder || textView.text! ==  deliveryFeedbackPlaceholder {
            
            return ""
        }
        return textView.text
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text! ==  foodFeedbackPlaceholder || textView.text! ==  deliveryFeedbackPlaceholder {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text! ==  "" && textView == foodReviewTxtView{
            textView.text = foodFeedbackPlaceholder
            textView.textColor = .lightGray
        }
        else if textView.text! ==  "" && textView == deliveryReviewTxtView {
            textView.text = deliveryFeedbackPlaceholder
            textView.textColor = .lightGray
        }
        
    }
    
   @IBAction func submitRatingBtnClick() {
    
    if foodRatingView.rating <= 3.0 {
        self.reasonBackView_Out.isHidden = false
        self.popupBackView_Out.isHidden = false
    }else {
        SubmitRating()
      }

    }
    
    
    func SubmitRating() {
        let deliveryRate = deliveryRatingView.rating
           
        for i in 0..<ratingsArray.count {
                   let rating = [ "dish_id": "\(ratingsArray[i]["dishId"]! as Int)",
                                  "user_id": "\(UserModel.shared.user_id)",
                                  "rating": "\(ratingsArray[i]["Rating"]! as Int)",
                                  "order_id": "\(self.order.id)",
                                  "order_item_id": "\(ratingsArray[i]["itemId"] ?? 0)"] as [String : String]
                   finalrating.append(rating)
               }
           let abc = ["dish_ratings":finalrating]
           let jsonItemsData = try? JSONSerialization.data(withJSONObject: abc, options: [])
           let jsonItemsString = String(data: jsonItemsData!, encoding: .utf8)
       
            
         let deliveryId = "\(order.deliveryBoy.delivery_id)" == "0" ? "": "\(order.deliveryBoy.delivery_id)"
        
        self.deliveryCompleteVM.callRateDeliveryApi(delivery_id: deliveryId, order_id: order.id, rating: "\(deliveryRate)", comment: self.checkText(textView: deliveryReviewTxtView), order_rating: "\(foodRatingView.rating)", order_comment: self.checkText(textView: foodReviewTxtView), dishrating: jsonItemsString!,rest_Id: "\(order.hallId)",subject: selelectedReason,message:"") { (success) in
                
                if success {
                    if let orderStatusVC = Router.viewController(with: .deliveryFeedbackCompleteVC, .profile) as? DeliveryFeedbackCompleteVC {
                        orderStatusVC.order = self.order
                        self.navigationController?.pushViewController(orderStatusVC, animated: false)
                    }    }
            }
    }
    
    
}

extension RatingDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == reasonTbl_Out {
            return reasonArray.count
        }else {
            return order.items.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == reasonTbl_Out {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonCell", for: indexPath) as! ReasonCell
            cell.reasonLbl.text = self.reasonArray[indexPath.row]
            if self.selelectedReason == self.reasonArray[indexPath.row] {
                cell.radioImg.image = UIImage(named: "radioBtnSelected")
            } else {
                cell.radioImg.image = UIImage(named: "radioBtnUnselected")
            }
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StarCell", for: indexPath) as! StarTableViewCell
            let dish = order.items[indexPath.row]
            let qtyString = "  (x\(dish.qty))"
            
            let dishAttributedString : NSAttributedString = dish.dishName.attributedStringColored(fullText: qtyString, size: 13.0)
            
            
            cell.itemnameLbl.attributedText = dishAttributedString
            //  dishPrice.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish.price)"
            cell.itemSubLbl.isHidden = dish.customOptions.count > 0 || dish.addOns.count > 0 ? false : true
            var optionNameArr = [String]()
            for option in dish.customOptions{
                optionNameArr.append(option.option ?? "")
            }
            
            for addon in dish.addOns{
                optionNameArr.append(addon.name ?? "")
            }
            cell.itemSubLbl.text = optionNameArr.joined(separator: ", ")
            let arr = ["Rating":3,"index":indexPath.row,"dishId":dish.dishId,"itemId" :order.items[indexPath.row].id]
            ratingsArray.append(arr)
            
            cell.starRatingView.didFinishTouchingCosmos = { [self] rating in
                
                // ratingsArray[indexPath.row] = rating
                if ratingsArray.count != 0 {
                    if let index = ratingsArray.firstIndex(where: {$0["index"] == indexPath.row}) {
                        ratingsArray[index]["Rating"] = Int(rating)
                    }else {
                        let arr = ["Rating":Int(rating),"index":indexPath.row,"dishId":dish.dishId,"itemId" :order.items[indexPath.row].id]
                        ratingsArray.append(arr)
                    }
                }else {
                    let arr = ["Rating":Int(rating),"index":indexPath.row,"dishId":dish.dishId,"itemId" :order.items[indexPath.row].id]
                    ratingsArray.append(arr)
                }
                
            }
            
            return cell
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == reasonTbl_Out {
            self.selelectedReason = self.reasonArray[indexPath.row]
            reasonTbl_Out.reloadData()
        }
        
        }
    
}

class ReasonCell: UITableViewCell {
    @IBOutlet weak var radioImg: UIImageView!
    @IBOutlet weak var reasonLbl: UILabel!

}


