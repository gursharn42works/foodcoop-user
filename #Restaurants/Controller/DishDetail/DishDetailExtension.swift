//
//  DishDetailExtension.swift
//  #Restaurants
//
//  Created by Satveer on 09/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

extension DishDetailVC {
   
    func setUpTableView() {
        customizationTable.register(UINib(nibName: Constant.shared.selectProductRadioCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductRadioCell)
        customizationTable.register(UINib(nibName: Constant.shared.selectProductCheckboxCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductCheckboxCell)
        customizationTable.register(UINib(nibName: Constant.shared.dishHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.dishHeaderCell)
        addOnTable.register(UINib(nibName: Constant.shared.selectProductCheckboxCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductCheckboxCell)
        addOnTable.register(UINib(nibName: Constant.shared.dishHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.dishHeaderCell)
        servingTable.register(UINib(nibName: Constant.shared.selectProductRadioCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductRadioCell)
        servingTable.register(UINib(nibName: Constant.shared.dishHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.dishHeaderCell)
        
        variationTableView.register(UINib(nibName: Constant.shared.dishHeaderCell, bundle: nil), forCellReuseIdentifier: Constant.shared.dishHeaderCell)
        variationTableView.register(UINib(nibName: Constant.shared.selectProductRadioCell, bundle: nil), forCellReuseIdentifier: Constant.shared.selectProductRadioCell)

        
        
        mainScrollView.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 0)
    }
    
    @IBAction func cartBtn_Click(_ sender: UIButton){
        if let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController {
            cartVC.isFromDishDetailView = true
            self.navigationController?.pushViewController(cartVC, animated: true)
        }
    }

    @IBAction func plusBtn_Click(_ sender: UIButton) {
        itemCount = itemCount + 1
        if (itemCount < 100) {
            valueTextField.text = "\(itemCount)";
        } else {
            valueTextField.text = "100";
            itemCount = 100;
        }
        self.calculateDishPrice()
    }

    @IBAction func minusBtn_Click(_ sender: UIButton) {
        if itemCount == 1 {
            return
        }
        itemCount = itemCount - 1
        if itemCount < 100 {
            valueTextField.text = "\(itemCount)";
        } else {
            valueTextField.text = "100";
            itemCount = 100;
        }
        self.calculateDishPrice()

    }
    
//    func calculateDishPrice() {
//
//        //TODO:-
//        let dish = dishDetailVM.dish
//
//        print("Dish Price : \(String(describing: dish.price))")
//
//        let actualPrice = dish.servingPrice()
//
//        let totalPrice = Double(self.itemCount) * dish.totalPrice()
//
//        totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f",totalPrice))//"\(totalPrice)"
//        //price.text = UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", actualPrice))
//
//        price.smartDecimalText = UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", actualPrice))
//
//        return
//
//        //
//
//
//        var customizationPrice = 0.0
//        var servingPrice = Double(dish.price ?? "") ?? 0.0
//        var addOnPrice = 0.0
//
//
//        // Calculate customizations price
//
//        var optionsValue = 0.0
//
//        for i in 0..<dish.customizations.count{
//            var selectedCount = 0
//            for j in 0..<dish.customizations[i].options.count{
//
//                let option =  dish.customizations[i].options[j]
//
//                if option.isDefault ?? false{
//                    selectedCount = selectedCount + 1
//                }
//
//                //GSD print("Option title: \(option.name)")
//
//                if selectedCount > (dish.customizations[i].maxSelectionAllowed ?? 0){
//                    let extraOptions = selectedCount - (dish.customizations[i].maxSelectionAllowed ?? 0)
//                    optionsValue = customizationPrice + (Double(extraOptions) * (Double(option.price ?? "") ?? 0.0))
//                }
//                //GSD print(optionsValue, "optionsValue")
//            }
//            customizationPrice = optionsValue
//        }
//
//        //GSD print(customizationPrice, "customizationPrice")
//
//        // Calculate serving price
//        for i in 0..<(dish.serving?.count ?? 0){
//            if dish.serving?[i].isDefault ?? false{
//                servingPrice = Double(dish.serving?[i].price ?? "") ?? 0.0
//            }
//        }
//
//        // Calculate addons price
//        for i in 0..<dish.addons.count{
//            if dish.addons[i].isSelected ?? false{
//                addOnPrice += Double(dish.addons[i].price ?? "") ?? 0.0
//            }
//        }
//
//        //Total price
//
//        dishTotalPrice = customizationPrice + addOnPrice + servingPrice
//        let dishTotalPriceString = (String(format: "%.2f",((dishTotalPrice ) * Double(self.itemCount ))))
//
//        //        totalPriceLabel.text = "$" + dishTotalPriceString
//
//        totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + dishTotalPriceString
//        price.text =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", servingPrice))
//
//    }
}


extension DishDetailVC : UITableViewDelegate, UITableViewDataSource{
    //MARK:- Table View DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case variationTableView:
            if dish.variations.count > 0 {
                customizationViewHeight.constant = 50
                customizationView.isHidden = false
            }
            if self.isOnlyVariation{
                return 1
            }
            return dish.variations.count
        case customizationTable:
            
            if dish.customizations.count > 0{
                customizationViewHeight.constant = 50
                customizationView.isHidden = false
            } else {
                if dish.serving?.count ?? 0 == 0 || dish.serving?.count ?? 0 == 1 {
                    customizationViewHeight.constant = 0
                    customizationView.isHidden = true
                }
            }
//             = dish.customizations.count > 0 ?  false : true
            divider.isHidden = dish.customizations.count > 0 ?  false : true
            customizationView.layoutIfNeeded()
            return dish.customizations.count
        case servingTable:
            
            if dish.serving?.count ?? 0 > 1 {
                customizationViewHeight.constant = 50
                customizationView.isHidden = false
            } else {
                if dish.customizations.count == 0{
                    customizationViewHeight.constant = 0
                    customizationView.isHidden = true
                }
            }
            customizationView.layoutIfNeeded()

            return dish.serving?.count ?? 0 > 1 ? 1 : 0
        case addOnTable:
            return dish.addons.count > 0 ? 1 : 0
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case variationTableView:
            if self.isOnlyVariation{
                return dish.variations.count
            }
            return dish.variations[section].servings.count
        case customizationTable:
            return dish.customizations[section].options.count > 0 ? dish.customizations[section].options.count : 0
        case servingTable:
            return dish.serving?.count ?? 0 > 1 ? dish.serving?.count ?? 0 : 0
        case addOnTable:
            return dish.addons.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        
        case variationTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductRadioCell", for: indexPath)as? SelectProductRadioCell
    
            if self.isOnlyVariation {
                let variation  = dish.variations[indexPath.row]
                cell?.nameLabel.text = variation.variation
                cell?.radioButton.isSelected = variation.isDefault ? true : false
                cell?.priceLabel.smartDecimalText = variation.price
            } else {
                let servings  = dish.variations[indexPath.section].servings[indexPath.row]
                cell?.nameLabel.text = servings.serving
                cell?.radioButton.isSelected = servings.isDefault ? true : false
                cell?.priceLabel.smartDecimalText = servings.price
            }
            cell?.custIndex = indexPath.section
            return cell ?? SelectProductRadioCell()
        case customizationTable:
            let type = dish.customizations[indexPath.section].type
            switch type{
            case .singleSelection?:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductRadioCell", for: indexPath)as? SelectProductRadioCell
                let customizationOption  = dish.customizations[indexPath.section].options[indexPath.row]
                cell?.nameLabel.text = customizationOption.name
                cell?.radioButton.isSelected = (customizationOption.isDefault ?? false) ? true : false
                cell?.priceLabel.smartDecimalText = customizationOption.price ?? ""
                cell?.configureUIForOption(customizationOption)
                cell?.custIndex = indexPath.section
                cell?.priceLabel.isHidden = !(dish.customizations[indexPath.section].allowPricePerItem ?? false)
                return cell ?? SelectProductRadioCell()
            case .multipleSelection?:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductCheckboxCell", for: indexPath)as? SelectProductCheckboxCell ?? SelectProductCheckboxCell()
                
                let name = dish.customizations[indexPath.section].options[indexPath.row].name ?? ""
                cell.nameLabel.text = name.capitalizeFirstInEachWord()
                
                if let price =  dish.customizations[indexPath.section].options[indexPath.row].price{
                    if price != ""{
                        cell.priceLabel.smartDecimalText = price
                    }
                }
                
                cell.checkBtn.isSelected = (dish.customizations[indexPath.section].options[indexPath.row].isDefault ?? false) ? true : false
                cell.configureUIForOption(dish.customizations[indexPath.section].options[indexPath.row])
                cell.priceLabel.isHidden = !(dish.customizations[indexPath.section].allowPricePerItem ?? false)
                return cell
            case .none:
                return UITableViewCell()
            }
            
        case servingTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductRadioCell", for: indexPath)as? SelectProductRadioCell
            cell?.nameLabel.text = dish.serving?[indexPath.row].serving ?? ""
            cell?.radioButton.isSelected = dish.serving?[indexPath.row].isDefault == true ? true : false
            cell?.priceLabel.smartDecimalText = dish.serving?[indexPath.row].price != "" ?  UserModel.shared.currencyCode /*"$"*/ + (dish.serving?[indexPath.row].price ?? "") : ""
//            cell?.delegate = self
            return cell ?? SelectProductRadioCell()
            
        case addOnTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductCheckboxCell", for: indexPath)as? SelectProductCheckboxCell ?? SelectProductCheckboxCell()
            let dishAddOn = (dish.addons[indexPath.row].addon ?? "")
            cell.nameLabel.text =  dishAddOn + " "
            cell.priceLabel.smartDecimalText =  UserModel.shared.currencyCode  /*"$"*/ + (dish.addons[indexPath.row].price ?? "")
            cell.checkBtn.isSelected = dish.addons[indexPath.row].isSelected == true ? true : false
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
}
extension DishDetailVC {
    
    //MARK:- Table View Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 34
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "DishHeaderCell") as? DishHeaderCell
        if tableView == variationTableView {
            if self.isOnlyVariation == false {
            var vari = dish.variations[section].variation
//                if section == 0 {
//                    vari = "Select\n\n\(vari)"
//                }
                headerView?.customisationHeading.text = vari
            } else {
                headerView?.customisationHeading.text = "Select"
            }
          //  }
            headerView?.customisationSubHeading.isHidden = true
            headerView?.lineView.isHidden = true
        }
        else if tableView == addOnTable {
            headerView?.customisationHeading.text = "ADD ONS"
            headerView?.customisationSubHeading.isHidden = true
            
        }else if tableView == customizationTable
        {
            let maxSelection = "\(dish.customizations[section].maxSelectionAllowed ?? 0)"
            headerView?.customisationHeading.text = (dish.customizations[section].ingredient ?? "")
            headerView?.customisationSubHeading.text = dish.customizations[section].allowPricePerItem ?? false ? "(Choose \(maxSelection), additional cost for extras)" : "(Choose \(maxSelection))"
            //GSD
            headerView?.customisationSubHeading.isHidden = dish.customizations[section].groupType == .addOn
            
            if dish.customizations[section].ingredient ?? "" == "Choice of Sauces" || maxSelection == "10" {
                 headerView?.headerViewTop.constant = 11
            }
            
            if dish.customizations[section].ingredient ?? "" == "Upgrade to Peri-Peri Fries" {
                headerView?.headerViewTop.constant = 5
            }

        }else{
            headerView?.customisationHeading.text = "Serving Size"
            headerView?.customisationSubHeading.text = dish.serving?.count ?? 1 > 1 ? "(Choose 1)" : ""
            headerView?.customisationSubHeading.isHidden = dish.serving?.count ?? 1 > 1 ? false : true
        }
        return headerView ?? DishHeaderCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch tableView {
        case variationTableView:
            if self.isOnlyVariation {
                if section == 0 {
                    return 40
                }
                return 0
            }
            if section == 0 {
                return 65
            }
            return 40
        case customizationTable:
            customizationViewHeight.constant = dish.customizations[section].options.count > 0 ?  45 : 0
            customizationView.isHidden = dish.customizations[section].options.count > 0 ? false : true
            divider.isHidden = dish.customizations[section].options.count > 0 ? false : true
            customizationView.layoutIfNeeded()
            
            let maxSelection = "\(dish.customizations[section].maxSelectionAllowed ?? 0)"

            if dish.customizations[section].ingredient ?? "" == "Choice of Sauces" || maxSelection == "10" {
                return dish.customizations[section].options.count > 0 ? 48 : 0
            }
            if dish.customizations[section].ingredient ?? "" == "Upgrade to Peri-Peri Fries" {
                return dish.customizations[section].options.count > 0 ? 48 : 0
            }

            return dish.customizations[section].options.count > 0 ? 70 : 0
        case servingTable:
            return dish.serving?.count ?? 1 > 1 ? 50 : 40
        case addOnTable:
            return dish.addons.count > 0 ? 40 : 0
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        select_UnselectRadioBtn(tableView, indexPath: indexPath)
    }
}

extension DishDetailVC {
    
    func select_UnselectRadioBtn(_ tableView : UITableView , indexPath : IndexPath) {
        
        if tableView == variationTableView {
            
            if self.isOnlyVariation {
                for i in 0..<dish.variations.count {
                    if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? SelectProductRadioCell{
                        cell.radioButton.isSelected = i == indexPath.row ? true : false
                        dish.variations[i].isDefault = cell.radioButton.isSelected
                    }
                }
            } else {
                //MARK:- Make All Variation Unselected
                for section in 0..<dish.variations.count {
                    for index in 0..<dish.variations[section].servings.count {
                        if let cell = tableView.cellForRow(at: IndexPath(row: index, section: section)) as? SelectProductRadioCell{
                            cell.radioButton.isSelected = false
                            dish.variations[section].servings[index].isDefault = cell.radioButton.isSelected
                        }
                    }
                }
                
                //MARK:- Selected variation
                for i in 0..<dish.variations[indexPath.section].servings.count {
                    if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? SelectProductRadioCell{
                        cell.radioButton.isSelected = i == indexPath.row ? true : false
                        dish.variations[indexPath.section].servings[i].isDefault = cell.radioButton.isSelected
                    }
                }
            }
        }
        
        if tableView == customizationTable
        {
            if dish.customizations[indexPath.section].type == .multipleSelection
            {
                if let cell = tableView.cellForRow(at: indexPath) as? SelectProductCheckboxCell{
                    
//                    // check if the allowPricePerItem is false
                    // if the variable is false we need to check the max selection
                    if dish.customizations[indexPath.section].allowPricePerItem == false {
                        var count = 0
                        for i in 0..<dish.customizations[indexPath.section].options.count {
                            if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? SelectProductCheckboxCell{
                                if cell.checkBtn.isSelected{
                                    count += 1
                                }
                            }
                        }
                        if count >= dish.customizations[indexPath.section].maxSelectionAllowed ?? 0{
                            if !cell.checkBtn.isSelected{
                                return
                            }
                        }
                    }else{
                        //if the variable is true no need to check max selection
                        //user can select all the toppings and extra price will be charged for that
                    }
                    
                    cell.checkBtn.isSelected = !cell.checkBtn.isSelected
                    dish.customizations[indexPath.section].options[indexPath.row].isDefault = cell.checkBtn.isSelected
                }
            }else{
                
                for i in 0..<dish.customizations[indexPath.section].options.count {
                    if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? SelectProductRadioCell{
                        
//                        if cell.radioButton.alpha == 0.5 {
//                            cell.radioButton.isSelected = false
//                        } else {
                            cell.radioButton.isSelected = i == indexPath.row ? true : false
                        //}

                        dish.customizations[indexPath.section].options[i].isDefault = cell.radioButton.isSelected
                    }
                }
            }
        }else if tableView == servingTable {
            for i in 0..<(dish.serving?.count ?? 0){
                if let cell = tableView.cellForRow(at: IndexPath(row: i, section: indexPath.section)) as? SelectProductRadioCell{
//                    if cell.radioButton.alpha == 0.5 {
//                        cell.radioButton.isSelected = false
//                    } else {
                        cell.radioButton.isSelected = i == indexPath.row ? true : false
                    //}

                    //cell.radioButton.isSelected = i == indexPath.row ? true : false
                    dish.serving?[i].isDefault = cell.radioButton.isSelected
                }
            }
        }else{
            if let cell = tableView.cellForRow(at: indexPath) as? SelectProductCheckboxCell{
//                if cell.checkBtn.alpha == 0.5 {
//                    return
//                }
                cell.checkBtn.isSelected = !cell.checkBtn.isSelected
                dish.addons[indexPath.row].isSelected = cell.checkBtn.isSelected
            }
        }
        
        self.calculateDishPrice()
    }
    
    func calculateVariationServingPrice() -> Double {

        if self.isOnlyVariation {
            var price = 0.0
            for variation in self.dish.variations {
                if variation.isDefault {
                    price += variation.priceInDouble
                }
            }
            return price
        } else {
            var price = 0.0
            for variation in self.dish.variations {
                for serve in variation.servings {
                    if serve.isDefault {
                        price = serve.priceInDouble
                    }
                }
            }
            return price
        }
    }
    
    func calculateDishPrice() {
        
//        let variationPrice = dish.calculateVariationServingPrice()//self.calculateVariationServingPrice()
        var actualPrice = dish.servingPrice()
   
        if dish.variations.count != 0 && actualPrice == 0 {
            actualPrice = Double(self.dish.price ?? "0.0") ?? 0.0
        }
        
        var totalPrice = Double(self.itemCount) * dish.totalPrice() //+variationPrice
        if totalPrice == 0 && actualPrice == 0 {
            totalPrice = Double(dish.price ?? "") ?? 0.0
            actualPrice = Double(dish.price ?? "") ?? 0.0
        }
        totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f",totalPrice))
        price.smartDecimalText = UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", actualPrice))
                
    }
    
//    func calculateDishPrice() {
//
//        //TODO:-
//
//
//
//        print("Dish Price : \(String(describing: dish.price))")
//
//        var actualPrice = dish.servingPrice()
//
//        var variationPrice = self.calculateVariationServingPrice()
//
//        var totalPrice = 0.0
//        var servingsAvailable = Bool()
//        if dish.dish_type == "variable" {
//            for variation in self.dish.variations {
//                if variation.servings.count > 0 {
//                    servingsAvailable = true
//                }else {
//                    servingsAvailable = false
//                }
//            }
//            if servingsAvailable == true {
//                totalPrice = Double(self.itemCount) * (variationPrice)
//            }else {
//                if variationPrice >= actualPrice {
//                    variationPrice = variationPrice-Double(actualPrice)
//                }
//                totalPrice = Double(self.itemCount) * (dish.totalPrice()+variationPrice)
//            }
//        }else {
//            if variationPrice >= actualPrice {
//                variationPrice = variationPrice-Double(actualPrice)
//            }
//                totalPrice = Double(self.itemCount) * (dish.totalPrice()+variationPrice)
//
//
//        }
//
//
//
//        dishTotalPrice = totalPrice
//        if totalPrice == 0 && actualPrice == 0 {
//            totalPrice = Double(dish.price ?? "") ?? 0.0
//            actualPrice = Double(dish.price ?? "") ?? 0.0
//        }
//        totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f",totalPrice))//"\(totalPrice)"
//        //price.text = UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", actualPrice))
//
//    //    price.smartDecimalText = UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", actualPrice))
//
//
//        return
//
//        //
//
//
//        var customizationPrice = 0.0
//        var servingPrice = Double(dish.price ?? "") ?? 0.0
//        var addOnPrice = 0.0
//
//
//        // Calculate customizations price
//
//        var optionsValue = 0.0
//
//        for i in 0..<dish.customizations.count{
//            var selectedCount = 0
//            for j in 0..<dish.customizations[i].options.count{
//
//                let option =  dish.customizations[i].options[j]
//
//                if option.isDefault ?? false{
//                    selectedCount = selectedCount + 1
//                }
//
//                //GSD print("Option title: \(option.name)")
//
//                if selectedCount > (dish.customizations[i].maxSelectionAllowed ?? 0){
//                    let extraOptions = selectedCount - (dish.customizations[i].maxSelectionAllowed ?? 0)
//                    optionsValue = customizationPrice + (Double(extraOptions) * (Double(option.price ?? "") ?? 0.0))
//                }
//                //GSD print(optionsValue, "optionsValue")
//            }
//            customizationPrice = optionsValue
//        }
//
//        //GSD print(customizationPrice, "customizationPrice")
//
//        // Calculate serving price
//        for i in 0..<(dish.serving?.count ?? 0){
//            if dish.serving?[i].isDefault ?? false{
//                servingPrice = Double(dish.serving?[i].price ?? "") ?? 0.0
//            }
//        }
//
//        // Calculate addons price
//        for i in 0..<dish.addons.count{
//            if dish.addons[i].isSelected ?? false{
//                addOnPrice += Double(dish.addons[i].price ?? "") ?? 0.0
//            }
//        }
//
//        //Total price
//
//        dishTotalPrice = customizationPrice + addOnPrice + servingPrice + self.calculateVariationServingPrice()
//        let dishTotalPriceString = (String(format: "%.2f",((dishTotalPrice ?? 0.0) * Double(self.itemCount))))
//
//        //        totalPriceLabel.text = "$" + dishTotalPriceString
//
//        totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + dishTotalPriceString
////        price.text =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", servingPrice))
//
//    }
    
    
}
