//
//  DishNotAvaiableVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class DishNotAvaiableVC: BaseVC {
    
    //MARK:- Variables
    var isShowDishInformation : Bool?{
        didSet{
           selectedValueClause?(isShowDishInformation ?? false)
           self.dismiss(animated: false, completion: nil)
        }
    }
    var selectedValueClause:((Bool)->())?
    var popUpTitle : String?
    var notificationPopup : Bool?
    
    //MARK:- IBOutlets
    @IBOutlet weak var popUpTileLabel : UILabel!{
        didSet{
            popUpTileLabel.text = popUpTitle ?? ""
            if notificationPopup ?? false{
                popUpHeadingLabel.text = ""
            }
        }
    }
    
    @IBOutlet weak var popUpHeadingLabel : UILabel!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- IBAction
    @IBAction func yesBtnTapped(){
        isShowDishInformation = true
    }
    
    @IBAction func noBtnTapped(){
        isShowDishInformation = false
    }
    
    @IBAction func crossBtnTapped(){
        isShowDishInformation = false
    }

}
