//
//  FavoriteDishVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FavoriteDishVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    
    var limit = 10
    var page = 1
    var favoriteDishVM = FavoriteDishVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showBottomBar()
    }
    
    func setCustomUI() {
        self.getFavoriteDetail()
        self.setUpTableView()
    }
    
    override func reloadAPI(_ sender: UIButton) {
        self.getFavoriteDetail()
    }
    
    
    private func setUpTableView() {
        self.tableView.register(UINib(nibName: Constant.shared.hallDishCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.hallDishCell)
        
        self.tableView.dataSource = favoriteDishVM
        self.tableView.delegate = favoriteDishVM
        self.favoriteDishVM.tableView = self.tableView
        self.favoriteDishVM.delegate = self
    }
    
    @IBAction func back_Click(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func showBottomBar() {
        let heightConstant : CGFloat = UserModel.shared.ongoingOrders.count > 0 ? 0 : self.isDeviceIsIphoneX() ? 50 : 0
        addBottomCartView(self.view,heightConstant)
        for view in self.view.subviews {
            if let temp = view as? BottomCartView , temp.tag == -10004 {
                tableViewBottom.constant = 50
                
            }else{
                tableViewBottom.constant = 0
            }
        }
        self.showNointernetView()
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.tableView.isHidden = true
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.tableView.isHidden = false
            self.removeEmptyMsg(self.view)
        }
    }
    
    override func updateValueOnBottomCartView() {
        self.showBottomBar()

    }

}

extension FavoriteDishVC: FavoriteDishVMDelegate {
    
    func getFavoriteDetail() {
        favoriteDishVM.getFavoriteList(limit: self.limit, page: self.page) { (success) in
            
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            } else {
                self.removeEmptyMsg(self.view)
            }
            
            if success {
                self.reloadFavouriteTableView()
            }
        }
    }
    
    func showNoFavoriteItemPopUp() {
        if self.favoriteDishVM.favoriteDishs.count == 0{
            self.showEmptyScreenMessage(Messages.noFavouriteHeading, Messages.noFavouriteDishes, self.view, #imageLiteral(resourceName: "No-Favourites"), self.view)
        }else{
            self.removeEmptyMsg(self.view)
        }
    }
    
    
    
    func reloadFavouriteTableView() {
        self.tableView.reloadData()
        self.tableView.isHidden = false
        self.showNoFavoriteItemPopUp()
    }
    
    func moveToDishDetailFromFavorite(dish_id: Int) {
        if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
            dishDetailVC.hidesBottomBarWhenPushed = true
            dishDetailVC.dish_id = dish_id
            self.navigationController?.pushViewController(dishDetailVC, animated: true)
        }
    }
}

