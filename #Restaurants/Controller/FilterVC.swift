//
//  FilterVC.swift
//  #Restaurants
//
//  Created by Satveer on 30/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol FilterVCDelegate: class {
    func getMenuData()
}

class FilterVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var backView: UIView!
    @IBOutlet var containerView: UIView!

    var delegate: FilterVCDelegate?
    
    var filterVM = FilterVM()
    var filters: MenuFilters?
    var appliedFilter = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setCustomUI()
    }
    
    func setCustomUI() {
        filterVM.filters = filters
        filterVM.selectedValue = UserModel.shared.appliedFilter

        tableView.register(UINib(nibName: Constant.shared.filterCell, bundle: nil), forCellReuseIdentifier: Constant.shared.filterCell)
        
        self.tableView.dataSource = filterVM
        self.tableView.delegate = filterVM
        self.filterVM.delegate = self
        self.tableView.reloadData()
        
        self.addTabGesture()
    }
    
    func addTabGesture() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(recognizer:)))
        self.backView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func handleTap(recognizer : UITapGestureRecognizer) {
        self.dissmissView()
    }
    
    @IBAction func applied_Click(_ sender: Any) {
        UserModel.shared.appliedFilter = filterVM.selectedValue//appliedFilter

        self.delegate?.getMenuData()
        self.dissmissView()
    }
    
    @IBAction func reset_Click(_ sender: Any) {
        UserModel.shared.appliedFilter = ""
        self.delegate?.getMenuData()
        self.dissmissView()
    }

   private func dissmissView() {
        self.dismiss(animated: false, completion: nil)
    }
}

extension FilterVC: FilterVMDelegate {
    func reloadTableView(selectedFilter: String) {
        self.appliedFilter = selectedFilter
        self.tableView.reloadData()
    }
}
