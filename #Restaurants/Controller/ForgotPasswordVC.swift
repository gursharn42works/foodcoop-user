//
//  ForgotPasswordVC.swift
//  #Restaurants
//
//  Created by Satveer on 11/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseVC {
 
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet weak var emailHeightConst: NSLayoutConstraint!
    
    var forgotPasswordVM = ForgotPasswordVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        emailTxt.delegate = self
    }
   
    
    
    
    @IBAction func resetPassword_Click(_ sender: UIButton) {
        forgotPasswordVM.callForgotPasswordService(email: emailTxt.text!) { (success) in
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        }

    }

    @IBAction func back_Click(_ sender: UIButton) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

}

extension ForgotPasswordVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        emailHeightConst.constant = 1.5
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if emailTxt.text == "" {
            emailHeightConst.constant = 0.5
        }else {
            emailHeightConst.constant = 1.5
        }
    }
}

