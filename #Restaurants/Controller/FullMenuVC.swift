//
//  FullMenuVC.swift
//  #Restaurants
//
//  Created by Satveer on 29/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip


var selectedHallid = Int()
class FullMenuVC: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var viewMenuTab: UIView!
    @IBOutlet weak var imageUnderline: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noInternetView : UIView!
    @IBOutlet weak var bottomContainerView: NSLayoutConstraint!
    @IBOutlet weak var resetViewHeight: NSLayoutConstraint!

    var repeatItemView = RepeatItemView()
    var fullMenuSearchView = FullMenuSearchView()
    var tabs = [String]()
    var fullMenuVM = FullMenuVM()
    var isResponse: Bool = false
    var categories = [MenuCategories]()
    var restId = 0
    var tagid = 0
    var dishes: [MenuDishes] = []
    var selectedIndex = 0
    var itemCount = 1
    var tableViewController: UITableView?
    var searchTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
        self.addMenuSearchView()
        self.addRepeatItemView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBar(color: UIColor.white)
        self.tabBarController?.tabBar.isHidden = true
      //  Utility.shared.isSearchViewController = true
       // self.addBottomBars()
        selectedHallid = restId
        self.getMenuData()
        self.addNotificationCenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func addMenuSearchView() {
        self.fullMenuSearchView = FullMenuSearchView(frame: self.view.frame)
        sceneDelegate?.window?.addSubview(fullMenuSearchView)
        self.fullMenuSearchView.delegate = self
        self.fullMenuSearchView.isHidden = true
    }
    func addRepeatItemView() {
        self.repeatItemView = RepeatItemView(frame: self.view.frame)
        sceneDelegate?.window?.addSubview(repeatItemView)
        self.repeatItemView.delegate = self
        self.repeatItemView.isHidden = true
    }
    
    func setCustomUI() {
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = AppColor.primaryColor
        self.noInternetView.isHidden = true
        self.prepareForPresentation()
    }
    
    func addBottomBars() {
        Utility.shared.isSearchViewController = true
        addBottomCartView(self.view,-15)
        bottomContainerView.constant = UserModel.shared.cartItemsCount > 0 ? 40:0

        Utility.shared.isSearchViewController = false
        
        self.showNointernetView()
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.noInternetView.isHidden = false
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.noInternetView.isHidden = true
            self.removeEmptyMsg(self.view)

        }
    }
    
    fileprivate func prepareForPresentation(){
        buttonBarView.removeFromSuperview()
        buttonBarView.frame = CGRect(x: 0, y: 0, width: viewMenuTab.frame.size.width, height: viewMenuTab.frame.size.height)
        viewMenuTab.addSubview(buttonBarView)
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            self.setCellItems(oldCell,newCell,progressPercentage,changeCurrentIndex,animated)
        }
    }
    
    func setCellItems(_ oldCell: ButtonBarViewCell?, _ newCell: ButtonBarViewCell?, _ progressPercentage: CGFloat, _ changeCurrentIndex: Bool, _ animated: Bool) {
        guard changeCurrentIndex == true else { return }
        // if Utility.sharedInstance.isFavourite == true
        //            {
        //                self.fullMenuApi(false)
        //                Utility.sharedInstance.isFavourite = false
        //            }
        oldCell?.label.textColor = UIColor.black// UIColor(white: 1, alpha: 0.6)
        newCell?.label.textColor = AppColor.primaryColor//.white
        oldCell?.label.font = UIFont.init(name: "Roboto-Medium", size: 13)
        newCell?.label.font = UIFont.init(name: "Roboto-Medium", size: 13)
        if animated {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                oldCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        } else {
            newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            oldCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    @IBAction func filter_Click(_ sender: Any) {
        
        if let filterVC = Router.viewController(with: .filter, .home) as? FilterVC {
            filterVC.modalPresentationStyle = .overFullScreen
            filterVC.filters = fullMenuVM.fullMenuModel?.filters
            filterVC.delegate = self
            self.present(filterVC, animated: false, completion: nil)
        }
        
    }
    
    @IBAction func resetFilter_Click(_ sender : Any){
        
    }
    
    @IBAction func search_Click(_ sender: UIButton) {
        self.fullMenuSearchView.isHideView = false
        self.fullMenuSearchView.getMenuSearchData("")
        self.fullMenuSearchView.emptyText = ""
        self.fullMenuSearchView.isHidden = false
    }
    
    @IBAction func backBtn_Click(_ sender: Any) {
        UserModel.shared.appliedFilter = ""
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func tryAgainBtnTap(_ sender: Any) {
        self.getMenuData()
    }
    
    override func reloadAPI(_ sender: UIButton) {
        self.getMenuData()
    }
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        var tabArray = [UITableViewController]()
        if isResponse {
            viewMenuTab.isHidden = false
            containerView.isHidden = false
            imageUnderline.isHidden = false
                let baseVC = BaseVC()
                baseVC.removeEmptyMsg(self.view)
                var isDish = false
                
                for subcat in categories {
                    let child = TableChildExampleViewController(style: .plain, itemInfo: IndicatorInfo(title: subcat.name))
                    child.delegate = self
                    if subcat.dishes.count > 0 {
                        child.dishes = subcat.dishes
                        child.restaurantName = self.titleLabel.text ?? ""
                        tabArray.append(child)
                        isDish = true
                    } else {
                        if isDish == false {
                            baseVC.showEmptyScreenMessage("No dishes found.", "", self.view, #imageLiteral(resourceName: "search"), self.view)
                            viewMenuTab.isHidden = true
                            imageUnderline.isHidden = true
                            break
                        }
                    }
            }
        } else {
            viewMenuTab.isHidden = true
            containerView.isHidden = true
            imageUnderline.isHidden = true
            let child = TableChildExampleViewController(style: .plain, itemInfo: "Table View")
            tabArray.append(child)
        }
        return tabArray
    }
    
    override func reloadPagerTabStripView() {
        //           isReload = true
        
        self.buttonBarView.performBatchUpdates(nil, completion: {
            (result) in
            super.reloadPagerTabStripView()
        })
    }
    
    override func configureCell(_ cell: ButtonBarViewCell, indicatorInfo: IndicatorInfo) {
        super.configureCell(cell, indicatorInfo: indicatorInfo)
        cell.backgroundColor = .clear
    }
    
    override func updateValueOnBottomCartView() {
        self.addBottomBars()
        self.reloadPagerTabStripView()
    }
}

extension FullMenuVC: FilterVCDelegate {
    
    func getMenuData() {
        ServiceManager.shared.hideLoader = false
        fullMenuVM.getFullMenuData(filter: UserModel.shared.appliedFilter,rest_id: restId,tag_id: tagid, searchTxt: searchTxt) { (success) in
            self.removeEmptyMsg(self.view)
            if Reachability.isConnectedToNetwork() == false {
                self.noInternetView.isHidden = false
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            }
            self.noInternetView.isHidden = true
            self.titleLabel.text = self.fullMenuVM.fullMenuModel?.RestDetails.name
            if let category = self.fullMenuVM.fullMenuModel?.categories , category.count > 0 {
                
                self.categories = category
                
                self.isResponse = true
                self.reloadPagerTabStripView()
                self.addBottomBars()
            }
        }
    }
    
    
}

extension FullMenuVC: RepeatItemViewDelegate, TableChildExampleViewControllerDelegate,FullMenuSearchViewDelegate, DishDetailVCDelegate   {
    func reloadView() {                                    
        super.reloadPagerTabStripView()
        self.addBottomBars()
    }
    
    

    
    func updateBottomView() {
        self.addBottomBars()
    }
    
    func openSearchViewControllerOnBack() {
        if self.fullMenuSearchView.isHideView == false && fullMenuSearchView.fullMenuSearchVM.dishes.count > 0 {
            self.fullMenuSearchView.getMenuSearchData(self.fullMenuSearchView.searchString)
            self.fullMenuSearchView.isHidden = false
        }
    }
    
    func moveToMenuDetailView(dishId: Int) {
        self.fullMenuSearchView.isHidden = true
        if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
            dishDetailVC.hidesBottomBarWhenPushed = true
            dishDetailVC.dish_id = dishId
            dishDetailVC.delegate = self
            self.navigationController?.pushViewController(dishDetailVC, animated: true)
        }
    }
    
    func updateViewDataOnMinusBtnClick() {
        self.addBottomBars()
    }
 
    func openReapeatItemView(dishs: [MenuDishes], index: Int, itemCount: Int, childController: UITableView) {
        self.dishes = dishs
        self.selectedIndex = index
        self.itemCount = itemCount
        self.tableViewController = childController
        self.repeatItemView.isHidden = false
        
        self.repeatItemView.showRepeatViewData(dish: dishs[index])
        //AMRK:Show View Data
    }
    
    func moveToDishDetailViewFromReapeatItemView(isAddNew: Bool) {
        self.repeatItemView.isHidden = true
        if isAddNew {
            self.fullMenuSearchView.isHidden = true
            if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
                dishDetailVC.hidesBottomBarWhenPushed = true
                dishDetailVC.dish_id = dishes[self.selectedIndex].id
                dishDetailVC.delegate = self
                self.navigationController?.pushViewController(dishDetailVC, animated: true)
            }
        } else {
            let indexPath = IndexPath(row: self.selectedIndex, section: 0)
            
            if let cell = self.tableViewController?.cellForRow(at: indexPath) as? HallDishCell {
               
                let dish = self.dishes[indexPath.row]
                var count = Int(cell.itemCountLbl.text!)
                      count = count! + 1
                    if itemCount < 101{
                        cell.itemCountLbl.text = "\(count!)";
                        self.fullMenuSearchView.localAddToCart(dish,count,"")
                    }
            }
            
            self.addBottomBars()

        }
    }
}

extension FullMenuVC {

    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }
    
    @objc func notificationRecieved(notification: NSNotification) {
        self.getOnGoingOrdersList { () in
            self.addBottomBars()
        }
    }

}
