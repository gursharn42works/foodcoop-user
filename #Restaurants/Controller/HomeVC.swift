//
//  HomeVC.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import CoreLocation

class HomeVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableBottom: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bannerBackView: UIView!
    @IBOutlet weak var btnSelectLocation: UIButton!
    
    @IBOutlet weak var userCurrentlocation: UILabel!
    
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation!
    var refreshControl = UIRefreshControl()
    
    var homeVM = HomeVM()
    var orderHistoryVM = OrderHistoryVM()
    var addressModel = RestaurantListModel()
    var delieveryAddress = DeliveryAddressModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        self.setCustomUI()
        tableView.register(UINib(nibName: "RestItemsTableCell", bundle: nil), forCellReuseIdentifier: "itemsCell")
        tableView.register(UINib(nibName: "ShowAlertTableCell", bundle: nil), forCellReuseIdentifier: "AlertCell")
    }
    

    func setCustomUI() {
        //MARK:- UI Customization
        homeVM.tableHeight = 230//(self.view.frame.size.width/2) + 40
        self.pageControl.isHidden = true
        self.tableView.isHidden = true
        self.bannerBackView.isHidden = true
        self.bannerBackView.frame.size.height = (self.view.frame.size.height/5)+15
        self.setUpTableView()
        self.setUpCollectionView()
        self.reloadBanners()
        //MARK:- Apis Call
        self.getOrderingFormData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setStatusBar(color: UIColor.white)
        self.tabBarController?.tabBar.isHidden = false
       self.locationManager.delegate = self
        searchtxt = ""
       // self.showAdressAndRestaurantInfo()
        self.addBottomBars()
        self.showNointernetView()
       // DeliveryAddressModel().removeSavedDeliveryAddress()
        self.addNotificationCenter()
        self.callGetDefaultAddressApi()
//        if UserDefaults.standard.object(forKey: "CurrentAddress") != nil {
//            userCurrentlocation.text = (UserDefaults.standard.object(forKey: "CurrentAddress") as! String)
//        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }
    
    
    
    override func reloadAPI(_ sender: UIButton) {
        self.getOrderingFormData()
    }
    
    @IBAction func locationBtn_Click(_ sender: UIButton) {
        if UserModel.shared.user_id == 0 {
            self.getCurrentLoaction ()
            return
        }
        if userCurrentlocation.text == "Add an Address." {
            let vc = Router.viewController(with: .addDeliveryAddressVC, .cart) as! AddDeliveryAddressVC
            vc.isFromCartView = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = Router.viewController(with: .deliveryAddressVC, .cart) as! DeliveryAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    

    
    private func moveToSearchView() {
        if let tab = self.tabBarController {
            tab.selectedIndex = 1
        }
        let navigateView = Router.viewController(with: .fullMenuVC, .home) as? FullMenuVC
        self.navigationController?.pushViewController(navigateView ?? UIViewController(), animated: true)
    }
    
    override func updateValueOnBottomCartView() {
        self.addBottomBars()
    }
    
  
}

extension HomeVC: HomeVMDelegate, RestaurantListVCDelegate { // Dish
    
    func reloadHomeScreenDataAccordingSelectedLocation() {
        self.getHomeData()
    }
    
    func moveToDishDetailView(rest_id: Int, tag_Id: Int, type: String) {
        if type == "Dish" {
           // self.moveToSearchView()
            if let restSearchVC = Router.viewController(with: .Restsearch, .home) as? RestaurantSearchVC {
                restSearchVC.RestTagId = tag_Id
                restSearchVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(restSearchVC, animated: true)
            }
        } else {
            if let dishDetailVC = Router.viewController(with: .fullMenuVC, .home) as? FullMenuVC {
                dishDetailVC.restId = rest_id
                dishDetailVC.tagid = tag_Id
                selectedHallid = rest_id
                dishDetailVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(dishDetailVC, animated: true)
            }
        }
    }
    func reloadPageControl(index: Int) {
        pageControl.currentPage = index
    }
    
    private func setUpTableView() {
        self.tableView.dataSource = homeVM
        self.tableView.delegate = homeVM
        self.homeVM.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        ServiceManager.shared.hideLoader = true
        self.getOrderingFormData()
    }
    
    
    func setUpCollectionView() {
        self.collectionView.dataSource = homeVM
        self.collectionView.delegate = homeVM
    }
    
    func reloadBanners() {
        _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        if let coll  = self.collectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < (homeVM.homeModel?.banners.count ?? 0) - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
            }
        }
    }
    
}

extension HomeVC {
    
    private func getHomeData() {
        homeVM.getHomeData { (success) in
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            } else {
                self.removeEmptyMsg(self.view)
            }
    
            if success {
                self.getOnGoingOrdersList { () in
                    self.updateHomeViewData()
                }
                
            }
        }
    }
    
    func updateHomeViewData() {
        //MARK:- Get Ongoing Orders
        self.tableView.reloadData()
        self.collectionView.reloadData()
        if self.homeVM.homeModel?.banners.count ?? 0 > 0 {
            self.pageControl.numberOfPages = self.homeVM.homeModel?.banners.count ?? 0
            self.pageControl.isHidden = false
            
        }
        self.tableView.isHidden = false
        self.bannerBackView.isHidden = false
        
        
        if let secretKey = self.homeVM.homeModel?.secretKey {
            UserModel.shared.secret_key = secretKey
        }
        
       // if let annoucement = self.homeVM.homeModel?.announcement {
            // self.showAnnouncementPopUp(title: annoucement.title ?? "", message: annoucement.message ?? "")
        //}
        
     //   self.showAdressAndRestaurantInfo()
        self.addBottomBars()
        self.refreshControl.endRefreshing()
        ServiceManager.shared.hideLoader = false
        
        
    }
    
    func getOrderingFormData() {
            self.getHomeData()
    }
    
    private func showAddress(_ addressModel: RestaurantListModel) {
        if addressModel.locations.count > 0 {
            //MARK:- Set up curency
            UserModel.shared.currencyCode = addressModel.currency
            
            if addressModel.locations.count > 0 {
                
                 
                self.btnSelectLocation.isHidden = false
                
                let location = addressModel.locations[UserModel.shared.selectedLocation]
                UserModel.shared.hall_id = location.id
                UserModel.shared.hall_name = location.name
              //  self.lblOrderingFrom.text = location.locationTitle
                let addressLine1 = location.addressLine1
                let addressLine2 = location.addressLine2 ?? ""
                
                if addressLine1 != "" && addressLine2 != "" {
                  //  self.addressLabel.text = "\(addressLine1),\(addressLine2)"
                } else if addressLine1 != "" {
                  //  self.addressLabel.text = addressLine1
                } else {
                  //  self.addressLabel.text = addressLine2
                }
            }
        }
    }
    
    // func getOrdersList(completion: @escaping(Bool) -> Void) {
    
    //        self.getOnGoingOrdersList { (jsonData) in
    //            self.showCustomOrderData(jsonData: jsonData)
    //        }
    //MARK:- Ongoing Order Api call
    //   orderHistoryVM.getHistoryOrders("ongoing", limit: 40, page: 1) { (jsonData) in
    //
    //           let orders = self.createOngoingOrderModel(jsonData: jsonData)
    //
    //            UserModel.shared.ongoingOrders.removeAll()
    //
    //            if orders.count > 0 {
    //                UserModel.shared.ongoingOrders = orders
    //            } else {
    //                UserModel.shared.ongoingOrders = []
    //            }
    //
    //            //self.showCartItems()
    //            self.addBottomBars()
    //            self.showCustomData()
    //            ServiceManager.shared.hideLoader = false
    //
    //            completion(true)
    //           // self.reloadAllControllerPopUps()
    //        }
    //    }
    
    //    func reloadAllControllerPopUps() {
    //        self.showCartItems()
    //
    //        if isNotificationCalled == false {
    //            return
    //        }
    //
    //        if let menu = fullMenuVC {
    //            menu.addBottomBars()
    //        }
    //
    //        if let profile = profileVC {
    //            profile.addBottomBars()
    //        }
    //
    //        if let notification = notificationVC {
    //            notification.addBottomBars()
    //        }
    //        isNotificationCalled = false
    //
    ////        self.showCartItems()
    ////        self.addBottomBars()
    //
    //    }
    
    //    func showCustomOrderData(jsonData: Data) {
    //
    //
    //        //self.showCartItems()
    //        self.addBottomBars()
    //        self.showCustomData()
    //        ServiceManager.shared.hideLoader = false
    //
    //    }
    
    
    func showAdressAndRestaurantInfo() {
        self.showAddress(self.addressModel)
        if let halls = homeVM.homeModel?.halls {
            self.showRestaurantClosed(halls: halls)
        }
        
        self.addBottomBars()
        
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.tableView.isHidden = true
            self.bannerBackView.isHidden = true
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.removeEmptyMsg(self.view)
            self.getOrderingFormData()

        }
    }
    
    private func showRestaurantClosed(halls: [Hall],_ shouldShow : Bool = true) {
        
        self.getSelectedHallTime(halls) { (timings,success) in
            self.removeRestaurantClosed(self.view)
            if success {
                
                self.addRestaurantClosed(self.view, 10, shouldShow) {
                    let popController = Router.viewController(with: .restaurantTimeVC, .product) as! RestaurantTimeVC
                    popController.modalPresentationStyle = .overFullScreen
                    popController.hallName = "Restaurant is Closed."
                    self.present(popController, animated: false, completion: nil)
                }
            }
        }
    }
    
    private func addBottomBars(){
        self.showCartItems()
        if UserModel.shared.ongoingOrders.count > 0 && UserModel.shared.cartItemsCount > 0{
            self.addBottomOrderView(self.view , 10)
            self.addBottomCartView(self.view, 5) //GSD uncommented
            self.tableBottom.constant = 50 + 40
        }else if UserModel.shared.cartItemsCount > 0{
            self.addBottomCartView(self.view, 0)
            self.removeBottomOrderView(self.view)
            self.tableBottom.constant = 50
        }else if UserModel.shared.ongoingOrders.count > 0{
            self.addBottomOrderView(self.view , 10)
            self.tableBottom.constant = 70
            self.removeCartBottomView(self.view)
        }else{
            self.removeBottomOrderView(self.view)
            self.removeCartBottomView(self.view)
            self.tableBottom.constant = 0
        }
    }
}

extension HomeVC {
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadBottomView(notification:)), name:NSNotification.Name(rawValue: AppNotification.reloadBottomView), object: nil)

    }
    
    @objc func reloadBottomView(notification: NSNotification) {
        self.addBottomBars()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppNotification.reloadBottomView), object: nil)

    }

    @objc func notificationRecieved(notification: NSNotification) {
        self.getNotificationCount { (success) in
            if success {
                BaseVC().notificationBadgeUpdate(self.tabBarController ?? UITabBarController())
            }
        }
        ServiceManager.shared.hideLoader = true
        
        self.getHomeData()
    }
}

extension HomeVC : CLLocationManagerDelegate {
    
    
    func showAnnouncementPopUp(title: String, message: String){
        if UserModel.shared.isAnnounceMentShown == false{
            UserModel.shared.isAnnounceMentShown = true
            if let announcementVC = Router.viewController(with: .announcementVC, .home) as? AnnouncementVC{
                announcementVC.modalPresentationStyle = .overFullScreen
                announcementVC.modalTransitionStyle = .crossDissolve
                announcementVC.titleStr = title
                announcementVC.messageStr = message
                self.present(announcementVC, animated: true) {}
            }
        }
        
    }
    
    
    func callGetDefaultAddressApi() {
        //MARK: Get saved address from userdefault
        let savedAddress = DeliveryAddressModel().getSavedDeliveryAddress()
        if savedAddress.id == 0 {
            //MARK: If not any saved address then get from api
            self.homeVM.getDefaultAddressList { [self] (defaultAddress) in
                self.delieveryAddress = defaultAddress
                DeliveryAddressModel().saveSelectedDeliveryAddress(address: defaultAddress)
                if delieveryAddress.title == "" {
                    getCurrentLoaction ()
                }else {
                    self.userCurrentlocation.text = "\(delieveryAddress.title.capitalizeFirstInEachWord())-\(delieveryAddress.house_no), \(delieveryAddress.street1), \(delieveryAddress.street2), \(delieveryAddress.city.capitalized), \(delieveryAddress.state.capitalized)."
                }
            }
        } else {
            self.delieveryAddress = savedAddress
            if delieveryAddress.title == "" {
                getCurrentLoaction ()
            }else {
                self.userCurrentlocation.text = "\(delieveryAddress.title.capitalizeFirstInEachWord())-\(delieveryAddress.house_no), \(delieveryAddress.street1), \(delieveryAddress.street2), \(delieveryAddress.city.capitalized), \(delieveryAddress.state.capitalized)."
            }
            
        }
        
        self.showNointernetView()
    }
    
    func getCurrentLoaction () {
        
        DispatchQueue.main.async { [self] in
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
        CLLocationManager.authorizationStatus() == .authorizedAlways) {
            if locationManager.location == nil {
                return
            }
            currentLoc = locationManager.location
            print(currentLoc.coordinate.latitude)
            print(currentLoc.coordinate.longitude)
            self.getAddressFromLatLon(loc: currentLoc)
        }else {
            print("no lat long" )
        }
      }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
        switch status {
        case .notDetermined:
            print("notDetermined")
        case .restricted:
            print("restricted")
        case .denied:
            print("denied")
        case .authorizedAlways:
            print("authorizedAlways")
            self.getCurrentLoaction ()
        case .authorizedWhenInUse:
            print("authorizedWhenInUse")
            self.getCurrentLoaction ()
        @unknown default:
            print("not")
        }
    }
    
    func getAddressFromLatLon(loc: CLLocation) {
            
        CLGeocoder().reverseGeocodeLocation(loc, completionHandler:
                                                { [self](placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = (placemarks ?? []) as [CLPlacemark]
                    var addressString : String = ""
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country ?? "")
                        print(pm.locality ?? "")
                        print(pm.subLocality ?? "")
                        print(pm.thoroughfare ?? "")
                        print(pm.postalCode ?? "")
                        print(pm.subThoroughfare ?? "")
                        
                        if pm.subThoroughfare != nil {
                            addressString = addressString + pm.subThoroughfare! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        print(addressString)
                       
                  }
                 if delieveryAddress.title == "" {
                    self.userCurrentlocation.text = addressString
                 }
                UserDefaults.standard.set(addressString, forKey: "CurrentAddress")
            })

        }
    
    
}
