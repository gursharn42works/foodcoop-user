//
//  LoginVC.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import AuthenticationServices

class LoginVC: BaseVC {
    
    @IBOutlet var registerLabel: UILabel!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var pwdTxtField: UITextField!
    
    @IBOutlet weak var emailHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var pwdHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var skipLoginLabel: UILabel!
    @IBOutlet weak var skipLoginBtn : UIButton!
    @IBOutlet weak var stackVew: UIStackView!
    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var appleSignInButton: ASAuthorizationAppleIDButton!

    var loginVM = LoginVM()
    var isFomSignUpSkip = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBar(color: UIColor.colorFromHex("#f74e61"))
    }
    
    func setCustomUI() {
        self.appleSignInButton.addTarget(self, action: #selector(callAppleSignin), for: .touchUpInside)
        self.setCustomViews()
    }
    
    func setCustomViews() {
        skipLoginLabel.isHidden = UserModel.shared.login_Skipped ? true : false
        skipLoginBtn.isHidden =  skipLoginLabel.isHidden
        if UserModel.shared.login_Skipped {
            topBar.isHidden = false
        } else {
            topBar.isHidden = true
        }
    }
    
//    @IBAction func appleSignin_Click(_ sender : Any) {
//        let appleIDProvider = ASAuthorizationAppleIDProvider()
//        let request = appleIDProvider.createRequest()
//        request.requestedScopes = [.fullName, .email]
//
//        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//        authorizationController.delegate = self
//        authorizationController.presentationContextProvider = self
//        authorizationController.performRequests()
//    }
}

extension LoginVC: UITextFieldDelegate {
    
    @IBAction func loginBtn_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        loginVM.callLoginService(emailTxtField.text!, pwdTxtField.text!) { (success) in
            if success {
                self.moveToHome()
            }
        }
    }
    
    func moveToHome() {
        let isSkipped = UserModel.shared.login_Skipped
        if isSkipped {
            if isFomSignUpSkip {
                self.popBack(3)
            } else {
                UserModel.shared.isLoggedin = true
                self.navigationController?.popViewController(animated: false)
            }
        } else {
            UserModel.shared.isLoggedin = true
            if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
                let rootNC = UINavigationController(rootViewController: magicTabBar)
                rootNC.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = rootNC
                self.window?.makeKeyAndVisible()
            }
        }
        UserModel.shared.removeValueFromSkipLogin()
    }
    
    @IBAction func facebookBtn_Click(_ sender: UIButton) {
        FacebookManager.openFacebookManager { (dic) in
            if dic.count == 0 {
                return
            }
            self.loginVM.facebookLoginService(dic: dic) { (success) in
                if success {
                    self.moveToHome()
                }
            }
        }
    }
    
    @IBAction func registerBtn_Click(_ sender: UIButton) {
        if let signUpVC = Router.viewController(with: .register, .login) as? SignUpVC {
            signUpVC.isFromLogin = true
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    @IBAction func forgotPassword_Click(_ sender: UIButton) {
        if let forgotPasswordVC = Router.viewController(with: .forgotPasswordVC, .login) as? ForgotPasswordVC {
            self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
        }
    }
    
    @IBAction func skipBtn_Click(_ sender: UIButton) {
        if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
            
            UserModel.shared.removeUserData()
            let rootNC = UINavigationController(rootViewController: magicTabBar)
            rootNC.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
    }
    @IBAction func back_Click(_ sender: UIButton) {
        if let navController = self.navigationController {
            self.setStatusBar(color: UIColor.white)
            navController.popViewController(animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTxtField:
            pwdTxtField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case emailTxtField:
            emailHeightConst.constant = 1.5
            
        case pwdTxtField:
            
            pwdHeightConst.constant = 1.5
            
        default:
            emailHeightConst.constant = 0.5
            pwdHeightConst.constant = 0.5
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTxtField:
            if emailTxtField.text == "" {
                emailHeightConst.constant = 0.5
            }else {
                emailHeightConst.constant = 1.5
            }
        case pwdTxtField:
            if pwdTxtField.text == "" {
                pwdHeightConst.constant = 0.5
            }else {
                pwdHeightConst.constant = 1.5
            }
        default:
            emailHeightConst.constant = 0.5
            pwdHeightConst.constant = 0.5
        }
    }
}

extension LoginVC {
    @objc func callAppleSignin() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
        
    }
}

extension LoginVC : ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension LoginVC : ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account as per your requirement
            let appleId = appleIDCredential.user
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            let appleUserLastName = appleIDCredential.fullName?.familyName
            let appleUserEmail = appleIDCredential.email
            
            var strEmail = ""
            var strName = ""
            
            if let email = appleUserEmail {

                if email.contains("privaterelay.appleid.com"){
                    strEmail = appleUserEmail ?? ""
                }else{
                    strEmail = appleUserEmail ?? ""
                }
                
                if let firstName = appleUserFirstName, let lastName = appleUserLastName{
                    strName = "\(firstName) \(lastName)"
                }
                
                if strEmail == "" {
                    strEmail = "\(appleId)@gmail.com"
                }
                
                if strName == "" {
                    strName = "\(appleId)"
                }

                let params = [
                    "email":strEmail,
                    "name": strName,
                    "social_profile_pic":"",
                    "social_token":appleId,
                    "device_id":UserDefaults.standard.value(forKey: UserDefault.deviceId)as? String ?? "",
                    "device_type":"Ios",
                    "device_token":UserDefaults.standard.value(forKey: UserDefault.fcmToken) as? String ?? ""]
                
                
               print(params)
                
                do {
                    let dataExample: Data =  try NSKeyedArchiver.archivedData(withRootObject: params, requiringSecureCoding: true)
                    
                    let status = KeyChain.save(key: appleId, data: dataExample)
                    print("status: ", status)
                    
                }catch{
                    
                }
                
                print(strEmail,strName)
                
                UserModel.shared.apple_id = appleId
                
                loginVM.appleLoginService(strName, strEmail, "", appleId) { (success) in
                    if success {
                        self.moveToHome()
                    }
                }

            }else{
                
                if let receivedData = KeyChain.load(key: appleId) {
                    
                    do {
                        let dictionary: NSDictionary? = try NSKeyedUnarchiver.unarchivedObject(ofClass: NSDictionary.self, from: receivedData)
                        
                        strEmail = dictionary!["email"] as? String ?? ""
                        strName = dictionary!["username"] as? String ?? ""
                        
                        if strEmail == "" {
                            strEmail = "\(appleId)@mail.com"
                        }
                        
                        if strName == "" {
                            strName = "\(appleId)"
                        }

                        let mutableDictionary = NSMutableDictionary(dictionary: dictionary!)
                        
                        mutableDictionary["device_token"] = UserDefaults.standard.value(forKey: UserDefault.fcmToken) as? String ?? ""
                        
                        print("result: ", mutableDictionary)
                        UserModel.shared.apple_id = appleId

                        loginVM.appleLoginService(strName, strEmail, "", appleId) { (success) in
                            if success {
                                self.moveToHome()
                            }
                        }


                    }catch{
                        
                    }
                }
                
            }
            
            //Write your code
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            //Write your code
        }
        
    }
    
}
