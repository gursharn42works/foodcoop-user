//
//  LogoutVC.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class LogoutVC: UIViewController {
    
    var logoutVM = LogoutVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGesture()
        self.hideShowTabBar(true)
    }
    
    @IBAction func yesBtn_Click(_ sender: UIButton) {
        self.logoutVM.logoutUser { (success) in
            if let magicTabBar = Router.viewController(with: .loginVC, .login) as? LoginVC {
                
                UserModel.shared.removeUserData()
                DeliveryAddressModel().removeSavedDeliveryAddress()
                let rootNC = UINavigationController(rootViewController: magicTabBar)
                rootNC.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = rootNC
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func noBtn_Click(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
