//
//  DishDetailVC.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
//import WWCalendarTimeSelector
protocol DishDetailVCDelegate: class {
    func openSearchViewControllerOnBack()
}

class DishDetailVC: BaseVC,DishDetailVMDelage {
    @IBOutlet weak var productDetailTableHeight: NSLayoutConstraint!
    @IBOutlet weak var addOnTableHeight: NSLayoutConstraint!
    @IBOutlet weak var customizationTable: UITableView!
    @IBOutlet weak var addOnTable: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var calories: UILabel!
    @IBOutlet weak var allergies: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var veg_NonVegImage: UIImageView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var customizationView: UIView!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var customizationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var divider: DividerView!
    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var dishImageWidth: NSLayoutConstraint!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var servingTable : UITableView!
    @IBOutlet weak var servingTableHeight : NSLayoutConstraint!
    @IBOutlet weak var imageBottom: NSLayoutConstraint!
    @IBOutlet weak var priceBottom: NSLayoutConstraint!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var topPriceLabel: UILabel!
    @IBOutlet weak var variationTableView: UITableView!
    @IBOutlet weak var variationTableViewHeight : NSLayoutConstraint!

    @IBOutlet weak var vwCartInfo: UIView!
    
    @IBOutlet weak var priceTrailing: NSLayoutConstraint!
    @IBOutlet weak var lblShortDescription: UILabel!
    @IBOutlet weak var lblDescriptionHeading: UILabel!
    
    @IBOutlet weak var stckLongDescription: UIStackView!
    @IBOutlet weak var dishTypeStackView: UIStackView!
    @IBOutlet weak var totalItemPrize: UILabel!
    @IBOutlet weak var customizationHeadingLbl: UILabel!

    var delegate: DishDetailVCDelegate?
    
    var dishDetailVM = DishDetailVM()
    var dish_id = 0
    var viewtitle : String?
    var hall : Hall?
    var hallId : Int?
    var dish = DishModel()
    var itemCount = 1
    var dishTotalPrice : Double?
    var isHallOpen = false
    var isDishAvailable = false
    var isFromCartScreen = false
    var cartCustomOptions = [CartItemCustomOption]()
    var cartAddOns = [cartItemAddOn]()
    var cartServing : CartItemCustomOption?
    var isLocalCartUpdate: Bool = false
    var variationName = String()
    var variationService = String()

    var maxSelectionAllowed = 0
    var cartCountUpdated: ((Int)-> Void)?
    var localCartItemIndex : Int?
    var isOnlyVariation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomUI()
    }
    
    func setCustomUI() {
        self.getDishDetail()
        self.setUpTableView()
        self.dishDetailVM.delegate = self
        self.mainScrollView.isHidden = true
    }
    
    @IBAction func back_Clcik() {
        self.delegate?.openSearchViewControllerOnBack()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addToCart_Clcik() {
        
        if addToCart.backgroundColor == UIColor.lightGray {
            print("Disable add to cart if button is disabled")

            self.checkRestaurantClosed()

            if UserModel.shared.ongoingOrders.count >= 4 {
                Utility.shared.showAlertView(message: Messages.orderMaxLimit)
                return
            }
            if isDishAvailable == false {
                Utility.shared.showAlertView(message: Messages.dishNotAvailable)
                return
            }

            return
        }

        let customizationsStatus = dish.areAllRequiredCustomizationsSelected()
        
        if !customizationsStatus.success{
            
            self.view.makeToast(customizationsStatus.1, duration: 2.0, point: self.view.center, title: nil, image: nil) { didTap in
            }
            
            return
        }
        
        //MARK:- Checkvariations
        let variation = dish.calculateVariationServingPrice().1
        if variation == false {
            self.view.makeToast(Messages.selectRequiredCustomizations, duration: 2.0, point: self.view.center, title: nil, image: nil) { didTap in
            }
            return
        }

        //priyuchanges

        if UserModel.shared.cartItemsCount == 0   {
            print(UserModel.shared.cartItemsCount)
            print(UserDefaults.standard.string(forKey: "hallid") ?? "")
            let valuesave = UserDefaults.standard.string(forKey: "hallid") ?? ""
            print(valuesave)
            UserDefaults.standard.set(valuesave, forKey: "compareid")
            print(UserDefaults.standard.string(forKey: "compareid") ?? "")
            dishDetailVM.localAddToCart(dish,self.itemCount, self.dishTotalPrice ?? 0.0)
            if isFromCartScreen {
                self.navigationController?.popViewController(animated: true)
            }

        }else{
            //FIXME:- REMOVED location check for #Restaurant if UserDefaults.standard.string(forKey: "compareid") ?? "" == UserDefaults.standard.string(forKey: "hallid") ?? ""{
            print("match")
            if UserModel.shared.ongoingOrders.count > 4{
                Utility.shared.showAlertView(message: Messages.orderMaxLimit)
                return
            }else if UserModel.shared.ongoingOrders.count == 4{
                Utility.shared.showAlertView(message: Messages.orderMaxLimit)
                return
            }
            else if !isHallOpen {

                self.checkRestaurantClosed()
                //let popController = UIStoryboard(name: StoryboardName.productDetail, bundle: nil).instantiateViewController(withIdentifier: "TimingViewController") as! TimingViewController

//                let popController = Router.viewController(with: .timing, .productDetails) as! TimingViewController
//
//                popController.modalPresentationStyle = .overFullScreen
//                popController.hallNme = "Restaurant is Closed"
//                popController.hallId = self.dish.hallId
//                //print(self.dish.hallId)
//                self.present(popController, animated: false, completion: nil)
                return
            }

            else if dish.isAcceptingOrder == false {
                Utility.shared.showAlertView(message: Messages.restaurantNotAcceptingOrder)
                return
            }else if !isDishAvailable {
                Utility.shared.showAlertView(message: Messages.dishNotAvailable)
                return
            }

            dishDetailVM.localAddToCart(dish,self.itemCount, dishTotalPrice ?? 0.0)
            if isFromCartScreen {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateCartData(isCartUpdate: false)
        self.addTableObservers()
        self.updateDishDetailVM()
    }
    
    func updateDishDetailVM() {
        dishDetailVM.isLocalCartUpdate = self.isLocalCartUpdate
        dishDetailVM.localCartItemIndex = self.localCartItemIndex
        self.showNointernetView()
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.mainScrollView.isHidden = true
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.removeEmptyMsg(self.view)
          //  self.mainScrollView.isHidden = false
            self.getDishDetail()
        }
    }
    
    override func reloadAPI(_ sender: UIButton){
        self.getDishDetail()
    }
    
    func addTableObservers() {
        customizationTable.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        addOnTable.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        servingTable.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        productDetailTableHeight.constant = customizationTable.contentSize.height
        addOnTableHeight.constant = addOnTable.contentSize.height
        servingTableHeight.constant = servingTable.contentSize.height
        variationTableViewHeight.constant = variationTableView.contentSize.height

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if customizationTable.observationInfo != nil {
            customizationTable.removeObserver(self, forKeyPath: "contentSize")
        }
        if addOnTable.observationInfo != nil {
            addOnTable.removeObserver(self, forKeyPath: "contentSize")
        }
        if servingTable.observationInfo != nil {
            servingTable.removeObserver(self, forKeyPath: "contentSize")
        }
    }
}

extension DishDetailVC {
    
    func getDishDetail() {
        ServiceManager.shared.hideLoader = false
        dishDetailVM.getDishDetail(dish_id) { (success) in
            
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            } else {
                self.removeEmptyMsg(self.view)
            }
            
            if success {
                self.dish = self.dishDetailVM.dish
                self.showData()
              //  self.tableViewHeight()
            }
        }
    }
    
//    func tableViewHeight() {
//        DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
//            self.productDetailTableHeight.constant = self.customizationTable.contentSize.height
//            self.addOnTableHeight.constant = self.addOnTable.contentSize.height
//            self.servingTableHeight.constant = self.servingTable.contentSize.height
//        })
//    }
    
    
    func updateCartData(isCartUpdate: Bool) {
        self.cartCountLabel.isHidden = UserModel.shared.cartItemsCount > 0 ? false : true
        self.cartCountLabel.text = "\(UserModel.shared.cartItemsCount)"
         // self.topPriceLabel.isHidden = UserModel.shared.cartItemsCount > 0 ? false : true
        
        self.topPriceLabel.smartDecimalText =  UserModel.shared.currencyCode  /*"$"*/ + (String(format: "%.2f", UserModel.shared.cartItemsPrice))

        self.priceTrailing.constant = UserModel.shared.cartItemsCount > 0 ? 10 : 0
        self.cartCountUpdated?(UserModel.shared.cartItemsCount)
        
        if isCartUpdate {
            let message = (self.addToCart.titleLabel?.text ?? "") == AppStrings.updateCart ? Messages.itemUpdatedSuccessfully : Messages.itemAdded
            
            Utility.shared.showAlertView(message: message)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Item Added"), object: nil)
        }
        
    }
    

    
    func showData() {
        // handle product detail response
        let name = dish.name ?? ""
        titleLabel.text = name.uppercased()
        dishTotalPrice = Double(dish.price ?? "") ?? 0.0
        isDishAvailable = dish.isDishAvailable ?? false
        
        price.smartDecimalText = UserModel.shared.currencyCode  /*"$"*/ + (dish.price ?? "")

//        descriptionLabel.text = dish.longDescription
        self.lblShortDescription.text = dish.productDescription
//
//        self.lblDescriptionHeading.isHidden = (dish.longDescription ?? "" == "")
      // self.stckLongDescription.isHidden = self.lblDescriptionHeading.isHidden
        
      //  let hallId = Int(UserModel.shared.hall_id)
        
        var hallStatus = false
        
        self.checkRstaurantClosed(timing: UserModel.shared.hallTimings) { (success) in
            hallStatus = success
        }

        if UserModel.shared.ongoingOrders.count > 3 {//GSD UPDATED ORDER LIMIT TO 4
            addToCart.backgroundColor = UIColor.lightGray
        }else if !(isDishAvailable ){
            addToCart.backgroundColor = UIColor.lightGray
        }else if hallStatus{
            print(hallStatus)
            addToCart.backgroundColor = UIColor.lightGray
            isHallOpen = false
        }else if !(dish.isAcceptingOrder ?? true){
            addToCart.backgroundColor = UIColor.lightGray
        }
        else{
            addToCart.backgroundColor = AppColor.primaryColor
            isHallOpen = true
        }
        
        if isFromCartScreen{
            for i in 0..<dish.addons.count {
                for element in cartAddOns{
                    if element.addOnId == dish.addons[i].id{
                        dish.addons[i].isSelected = true
                    }
                }
            }
            for i in 0..<dish.customizations.count{
                for j in 0..<dish.customizations[i].options.count{
                    for element in cartCustomOptions{
                        if element.optionId == dish.customizations[i].options[j].id{
                            dish.customizations[i].options[j].isDefault = true
                        }
                    }
                }
            }
            
            for i in 0..<(dish.serving?.count ?? 0){
                if cartServing?.optionId == dish.serving?[i].id{
                    dish.serving?[i].isDefault = true
                }else{
                    dish.serving?[i].isDefault = false
                }
            }
            
            for i in 0..<dish.variations.count {
                
                if dish.variations[i].variation == variationName {
                    dish.variations[i].isDefault = true
                    if dish.variations[i].servings.count > 0 {
                        for j in 0..<dish.variations[i].servings.count {
                           if dish.variations[i].servings[j].serving == self.variationService {
                            dish.variations[i].servings[j].isDefault = true
                            }
                        }
                            
                    }
                    
                }
                
                
            }
             self.calculateDishPrice()
            addToCart.setTitle("Update Cart".uppercased(), for: .normal)
        }else{
            if dish.variations.count == 0 {
                self.calculateDishPrice()
            } else {
                totalItemPrize.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + "\(0)"

            }
            itemCount = 1
            addToCart.setTitle("ADD TO CART", for: .normal)
        }
        
        valueTextField.text = "\(itemCount)"
        self.addToCart.isHidden = false
        
        let servings = dish.variations.filter({$0.servings.count > 0 })
        self.isOnlyVariation = servings.count == 0 ? true:false
        customizationTable.reloadData()
        addOnTable.reloadData()
        servingTable.reloadData()
        self.variationTableView.reloadData()

        //FIXME: - dishImageWidth.constant = dish.productImage == "" ? 0 : 100 //GSD
        productImage.set_image(dish.productImage ?? "", placeholder: AppImage.dishPlaceholderSmall.rawValue)
                dishName.text = dish.name//?.uppercased()
        if dish.type == .veg{
            veg_NonVegImage.image = #imageLiteral(resourceName: "veg")
        }else if dish.type == .nonVeg{
            veg_NonVegImage.image = #imageLiteral(resourceName: "nonveg")
        }else{
            veg_NonVegImage.image = #imageLiteral(resourceName: "contains_egg")
        }
        
        allergies.text = dish.ingredients
        mainScrollView.isHidden = false
       
        
        for dishIndicator in self.dishTypeStackView.arrangedSubviews{
            dishIndicator.isHidden = true
        }
        if dish.showTypesOnApp{
            for dishType in dish.dishTypes{
                if dishType.index < self.dishTypeStackView.arrangedSubviews.count && dishType.index != -1{
                    self.dishTypeStackView.arrangedSubviews[dishType.index].isHidden = false
                }
            }
        }
        
    }
}
