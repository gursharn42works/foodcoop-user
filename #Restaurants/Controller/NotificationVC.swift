//
//  NotificationVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class NotificationVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var deleteImg: UIImageView!
    @IBOutlet weak var dontLBl: UILabel!
    var refreshControl = UIRefreshControl()
    
    var notificationVM = NotificationVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
        self.setLableTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Utility.shared.hideStatusBar = true
        self.addBottomBars()
        self.showLoginView()
        self.addNotificationCenter()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        Utility.shared.hideStatusBar = false
        NotificationCenter.default.removeObserver(self)
    }
    
    func addBottomBars() {
        self.showCartItems()
        if UserModel.shared.ongoingOrders.count > 0 && UserModel.shared.cartItemsCount > 0{
            self.addBottomOrderView(self.view , 10)
            self.addBottomCartView(self.view, 5)
            self.tableViewBottom.constant = 50 + 70
        }else if UserModel.shared.cartItemsCount > 0{
            self.addBottomCartView(self.view, 0)
            self.removeBottomOrderView(self.view)
            self.tableViewBottom.constant = 50
        }else if UserModel.shared.ongoingOrders.count > 0{
            self.addBottomOrderView(self.view , 10)
            self.tableViewBottom.constant = 70
            self.removeCartBottomView(self.view)
        }else{
            self.removeBottomOrderView(self.view)
            self.removeCartBottomView(self.view)
            self.tableViewBottom.constant = 0
        }
        
        self.showNointernetView()
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.tableView.isHidden = true
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.tableView.isHidden = false
            self.removeEmptyMsg(self.view)
        }
    }
    
    func showLoginView() {
        if UserModel.shared.user_id == 0 {
            self.loginView.isHidden = false
        } else {
            self.loginView.isHidden = true
            self.getNotificationList()
        }
        
        self.deleteBtn.isHidden = UserModel.shared.user_id == 0 ? true : false
        self.deleteImg.isHidden = UserModel.shared.user_id == 0 ? true : false
    }
    
    override func reloadAPI(_ sender: UIButton){
        self.notificationVM.notifications = []
        self.tableView.reloadData()
        self.getNotificationList()
    }
    
    func setCustomUI() {
        self.setUpTableView()
        self.notificationVM.delegate = self
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()
        
        
    }
    
    private func setUpTableView() {
        self.tableView.dataSource = notificationVM
        self.tableView.delegate = notificationVM
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
    }
    
    @objc func refresh(_ sender: AnyObject) {
        ServiceManager.shared.hideLoader = true
        self.getNotificationList()
        
    }
    
    @IBAction func delete_click(_ sender: UIButton) {
        let dishNotAvailableVC = Router.viewController(with: .dishNotAvaiableVC, .fullMenu) as? DishNotAvaiableVC
        dishNotAvailableVC?.modalPresentationStyle = .overFullScreen
        dishNotAvailableVC?.notificationPopup = true
        dishNotAvailableVC?.popUpTitle = Messages.clearNotification
        dishNotAvailableVC?.selectedValueClause = {status in
            if status{
                self.deleteNotifications() }else{  return }
        }
        self.window?.rootViewController?.present(dishNotAvailableVC ?? DishNotAvaiableVC(), animated: false, completion: nil)
    }
    
    override func updateValueOnBottomCartView() {
        self.addBottomBars()
    }
}

//MARK:- Call apis
extension NotificationVC: NotificationVMDelagte {
    
    func moveToOrderDetailOnClick(order_id: Int,status: String) {
        
        let status = OrderStatusEnum(rawValue: status) ?? .pendingConfirmation

        if status == .completed {
            let deliveryCompleteVC = Router.viewController(with: .deliveryCompleteVC, .profile) as! DeliveryCompleteVC
            deliveryCompleteVC.order.id = "\(order_id)"
                self.navigationController?.pushViewController(deliveryCompleteVC, animated: true)
        } else {
            let deliveryOrderDetailVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as! DeliveryOrderDetailVC
                deliveryOrderDetailVC.order.id = "\(order_id)"
            deliveryOrderDetailVC.isFromNotificationTab = true
                self.navigationController?.pushViewController(deliveryOrderDetailVC, animated: true)
        }
    }
    
    func getNotificationList() {
        notificationVM.getNotifications(limit: 10,page: 1) { (success) in
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            } else {
                self.removeEmptyMsg(self.view)
            }
            
            self.tableView.reloadData()
            self.tableView.isHidden = false
            ServiceManager.shared.hideLoader = false
            self.refreshControl.endRefreshing()
            self.addBottomBars()
            
            self.deleteBtn.isEnabled = self.notificationVM.notifications.count == 0 ? false : true
            self.deleteImg.isHidden = self.notificationVM.notifications.count == 0 ? true : false
            self.showEmptyNotificationView()
        }
    }
    
    func deleteNotifications() {
        notificationVM.deleteNotifications { (success) in
            if success {
                self.tableView.reloadData()
                self.getNotificationList()
            }
            
        }
    }
    
    func setLableTitle() {
        let newUserStr = "Don't have an account? "
        let lightAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Light", size: 13.0)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let newAttStr = NSAttributedString(string: newUserStr, attributes: lightAttribute)
        
        let registerStr = " Register"
        let boldAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 13.0)!, NSAttributedString.Key.foregroundColor:  AppColor.primaryColor]
        let regAttrStr = NSAttributedString(string: registerStr, attributes: boldAttribute)
        
        let finalStr = NSMutableAttributedString(attributedString: newAttStr)
        finalStr.append(regAttrStr)
        
        dontLBl.attributedText = finalStr
    }
    
}

//MARK:- Show Popup View Messages
extension NotificationVC {
    
    func showEmptyNotificationView() {
        if self.notificationVM.notifications.count > 0 {
            self.noNotificationsAvailable(false)
        } else {
            self.noNotificationsAvailable(true)
        }
    }
    
    func noNotificationsAvailable(_ bool: Bool) {
        if bool {
            self.showEmptyScreenMessage(Messages.noNotificationHeading, Messages.noNotification, self.view, #imageLiteral(resourceName: "No-Notification"))
        } else {
            self.removeEmptyMsg(self.view)
        }
    }
}

extension NotificationVC {
    
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)

    }
    
    @objc func notificationRecieved(notification: NSNotification) {
        self.getOnGoingOrdersList { () in
            self.addBottomBars()
        }
    }
}
