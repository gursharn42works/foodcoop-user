//
//  OrderHistoryVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class BaseOrderHistoryVC: BaseVC {
    
    @IBOutlet var topButtons: [UIButton]!
    @IBOutlet var mainScrollView: UIScrollView! {
        didSet {
            mainScrollView.delegate = self
        }
    }
    
    @IBOutlet weak var scrollLineLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ongoningContainerView: UIView!
    @IBOutlet weak var previousContainerView: UIView!

    var selectedTap = 0
//    lazy var 0rderHistoryVC = [OrderHistoryVC]()
    
    var isResponse: Bool = false
    var currentController = UIViewController()
    
    var ongoingOrder: [Order] = []
    var previousOrder: [Order] = []

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOrderList()
        self.showNointernetView()
        self.addNotificationCenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

        
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.removeEmptyMsg(self.view)

        }
    }
    override func reloadAPI(_ sender: UIButton) {
        self.getOrderList()
    }
    
    @IBAction func back_Click(_ sender: UIButton) {
        Utility.shared.hideStatusBar = false
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func topBtns_Click(_ sender: UIButton) {
        for button in topButtons {
            button.setTitleColor(UIColor.black, for: .normal)
        }
        
        selectedTap = sender.tag - 1
        mainScrollView.setContentOffset(CGPoint(x: (CGFloat(selectedTap) * (self.view.frame.size.width)), y: 0), animated: true)
        sender.setTitleColor(AppColor.primaryColor, for: .normal)
    }
    
    //MARK:- Navigation Segue
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ongoing" {
//            if let vc = segue.destination as? OrderHistoryVC {
//                vc.isPreviousOrders = false
//                vc.orderHistoryVM = self.orderHistoryVM
//                refProductController.append(vc)
//            }
//        }else {
//            if let vc = segue.destination as? OrderHistoryVC {
//                vc.isPreviousOrders = true
//                vc.orderHistoryVM = self.orderHistoryVM
//                refProductController.append(vc)
//            }
//        }
//    }
    
    @IBAction func topTaps(_ sender: UIButton) {
        
        DispatchQueue.main.async {
        for button in self.topButtons {
            button.setTitleColor(UIColor.black, for: .normal)
        }
        
//        self.currentController.removeFromParent()
        
        if sender.currentTitle! == "PREVIOUS ORDERS" {
            if let orderHistoryVC = Router.viewController(with: .orderHistoryVC, .profile) as? OrderHistoryVC {
                orderHistoryVC.isPreviousOrders = true
                orderHistoryVC.previousOrder = self.previousOrder
                self.addChildView(viewController: orderHistoryVC, in: self.previousContainerView)
                self.currentController = orderHistoryVC
                orderHistoryVC.refreshControl.endRefreshing()
                orderHistoryVC.delegate = self
                orderHistoryVC.historyTableView.reloadData()
            }
        } else {
            if let orderHistoryVC = Router.viewController(with: .orderHistoryVC, .profile) as? OrderHistoryVC {
                orderHistoryVC.isPreviousOrders = false
                orderHistoryVC.ongoingOrder = self.ongoingOrder
                self.addChildView(viewController: orderHistoryVC, in: self.ongoningContainerView)
                self.currentController = orderHistoryVC
                orderHistoryVC.refreshControl.endRefreshing()
                orderHistoryVC.delegate = self
                orderHistoryVC.historyTableView.reloadData()

            }
        }
        
            self.selectedTap = sender.tag - 1
            self.mainScrollView.setContentOffset(CGPoint(x: (CGFloat(self.selectedTap) * (self.view.frame.size.width)), y: 0), animated: true)
        
            
        sender.setTitleColor(AppColor.primaryColor, for: .normal)
        }
    }
    
}

//MARK:- UIScrollViewDelegate
extension BaseOrderHistoryVC : UIScrollViewDelegate,OrderHistoryVCDelegate {
    
    func moveLine() {
        scrollLineLeadingConstraint.constant = mainScrollView.contentOffset.x/2
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.moveLine()
        if scrollView == mainScrollView {
            if scrollView.contentOffset.x <= scrollView.frame.size.width/2 {
                scrolltopTaps(topButtons[0])
            } else if scrollView.contentOffset.x <= (scrollView.frame.size.width + scrollView.frame.size.width/2) {
                scrolltopTaps(topButtons[1])
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == mainScrollView {
            let xAxis = Int(scrollView.contentOffset.x/self.view.frame.size.width)
            topTaps(topButtons[xAxis])
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if scrollView == mainScrollView {
                let xAxis = Int(scrollView.contentOffset.x/self.view.frame.size.width)
                topTaps(topButtons[xAxis])
            }
        }
    }
    
    func scrolltopTaps(_ sender: UIButton) {
        for button in self.topButtons {
            button.setTitleColor(UIColor.black, for: .normal)
        }
        self.selectedTap = sender.tag - 1
        sender.setTitleColor(AppColor.primaryColor, for: .normal)
    }
    
    func callApiToGetData() {
        self.getOrderList()
    }
    func getOrderList() {
        //MARK:- Ongoing Order Api call
        Utility.shared.hideStatusBar = false

        OrderHistoryVM().getHistoryOrders("ongoing", limit: 20, page: 1) { (jsonData) in
            
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
            if let response = responseJSON as? [String: Any] {
                if let data = response["data"] as? [String: Any], let orderArr = data["orders"] as? NSArray {
                    self.ongoingOrder = []
                    for dic in orderArr {
                        let order = Order().getOrder(dic)
                        self.ongoingOrder.append(order)
                    }
                }
                
                //MARK:- Previous Order Api call
                OrderHistoryVM().getHistoryOrders("previous", limit: 20, page: 1) { (jsonData) in
                    ServiceManager.shared.hideLoader = true

                    let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
                    if let response = responseJSON as? [String: Any] {
                        if let data = response["data"] as? [String: Any], let orderArr = data["orders"] as? NSArray {
                            self.previousOrder = []

                            for dic in orderArr {
                                let order = Order().getOrder(dic)
                                self.previousOrder.append(order)
                            }
                        }
                    }
                    if let controller = self.currentController as? OrderHistoryVC{
                        controller.refreshControl.endRefreshing()
                        if let refresh = controller.historyTableView.viewWithTag(-1111) as? UIRefreshControl {
                            
                            
                            
                            refresh.endRefreshing()
                        }
                    }
                    
                    if let orderHistoryVC = Router.viewController(with: .orderHistoryVC, .profile) as? OrderHistoryVC {
                        orderHistoryVC.ongoingOrder = self.ongoingOrder
                        self.addChildView(viewController: orderHistoryVC, in: self.ongoningContainerView)
                        orderHistoryVC.refreshControl.endRefreshing()
                        orderHistoryVC.delegate = self
                        orderHistoryVC.historyTableView.reloadData()
                        self.currentController = orderHistoryVC
                    }
                    
                    if let orderHistoryVC = Router.viewController(with: .orderHistoryVC, .profile) as? OrderHistoryVC {
                        orderHistoryVC.isPreviousOrders = true
                        orderHistoryVC.previousOrder = self.previousOrder
                        self.addChildView(viewController: orderHistoryVC, in: self.previousContainerView)
                        self.currentController = orderHistoryVC
                        orderHistoryVC.refreshControl.endRefreshing()
                        orderHistoryVC.delegate = self
                        orderHistoryVC.historyTableView.reloadData()
                    }

                }
            }
        }
    }
    
    private func addChildView(viewController: UIViewController, in view: UIView) {
        viewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        viewController.view.frame = view.bounds
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
}

extension BaseOrderHistoryVC {
   
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }

    @objc func notificationRecieved(notification: NSNotification) {
        ServiceManager.shared.hideLoader = true
        self.getOrderList()
    }
}


