////
////  OrderHistoryVC.swift
////  Magic
////
////  Created by Apple on 06/08/18.
////  Copyright © 2018 Apple. All rights reserved.
////
//

import UIKit

protocol OrderHistoryVCDelegate: class {
    func callApiToGetData()
}

class OrderHistoryVC : BaseVC {
    
    // MARK: - Variables
    var isPreviousOrders = false
    var delegate: OrderHistoryVCDelegate?
    var ongoingOrder: [Order] = []
    var previousOrder: [Order] = []
    var previousPageCount = 1
    var isDataLoading = false
    
    @IBOutlet var historyTableView : UITableView! {
        didSet {
            if isPreviousOrders == false
            {
                historyTableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
                historyTableView.register(UINib(nibName: Constant.shared.ongoingOrdersCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.ongoingOrdersCell)
            }else{
                historyTableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
                historyTableView.register(UINib(nibName: Constant.shared.orderHistoryCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.orderHistoryCell)
            }
            setRefreshControl = true
            historyTableView.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 0)
        }
    }
    var refreshControl: UIRefreshControl!
    var setRefreshControl : Bool? {
        didSet {
            refreshControl = UIRefreshControl()
            refreshControl.tag = -1111
            refreshControl.backgroundColor = UIColor.clear
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action:#selector(refreshTable(_:)), for: UIControl.Event.valueChanged)
            historyTableView.addSubview(self.refreshControl)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Helper.shared.isFromView = ""
        self.setCustomUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Utility.shared.hideStatusBar = false
    }
    
    func setCustomUI() {
        if Reachability.isConnectedToNetwork() {
            if self.ongoingOrder.count == 0 && isPreviousOrders == false {
                self.showEmptyScreenMessage(Messages.noOrderHeading, Messages.noOrder, self.historyTableView,#imageLiteral(resourceName: "No-order"))
            }
            else if self.previousOrder.count == 0 && isPreviousOrders {
                self.showEmptyScreenMessage(Messages.noOrderHeading, Messages.noOrder, self.historyTableView,#imageLiteral(resourceName: "No-order"))
            }
        } else {
            self.ongoingOrder = []
            self.previousOrder = []
            self.delegate?.callApiToGetData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Void
    @objc func refreshTable(_ sender : Any) {
        ServiceManager.shared.hideLoader = true
        self.delegate?.callApiToGetData()
        //ordersViewModel.getOrders(tabIndex, isRefresh: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.refreshControl.endRefreshing()
        }
    }
    
    override func reloadAPI(_ sender: UIButton) {
        self.delegate?.callApiToGetData()
    }
    
    func trackStatus(_ order: Order) {
        if let orderStatusVC = Router.viewController(with: .orderStatusVC, .profile) as? OrderStatusVC {
            orderStatusVC.order.id = "\(order.id)"
            self.navigationController?.pushViewController(orderStatusVC, animated: true)
        }
    }
    
}

//MARK:- UITableViewDataSource
extension OrderHistoryVC : UITableViewDataSource {
    func moveToViewOrderStatus(_ index: Int) {
        if let orderStatusVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as? DeliveryOrderDetailVC {
            if self.isPreviousOrders {
                orderStatusVC.order.id = "\(self.previousOrder[index].id)"
            } else {
                orderStatusVC.order.id = "\(self.ongoingOrder[index].id)"
            }
            self.navigationController?.pushViewController(orderStatusVC, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isPreviousOrders {
            return self.previousOrder.count
        } else {
            return self.ongoingOrder.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isPreviousOrders {
            let order = self.previousOrder[indexPath.row]
            
            let cell  = historyTableView.dequeueReusableCell(withIdentifier: Constant.shared.orderHistoryCell, for: indexPath) as! OrderHistoryCell
            cell.delegate = self
            cell.rateOrderClick = {
                self.rateOrderTap(order , indexPath: indexPath)
            }
            cell.config(order,false)
            cell.rateOrder.isHidden = true
            return cell
        } else {
            let order = self.ongoingOrder[indexPath.row]
            let cell  = historyTableView.dequeueReusableCell(withIdentifier: Constant.shared.ongoingOrdersCell, for: indexPath) as! OrderHistoryCell
            cell.config(order,true)
            cell.orderStatusClausre = {
                self.trackStatus(order)
            }
            cell.cancelOrderClick = {
                self.cancelOrderTap(order, indexPath: indexPath)
            }
            
            return cell
            
        }
    }
}

//MARK:- UITableViewDelegate
extension OrderHistoryVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 40.0
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var orders: [Order] = []
        
        if self.isPreviousOrders {
            orders = self.previousOrder
        } else {
            orders = self.ongoingOrder
        }
        
        let orderStatus = OrderStatusEnum(rawValue: orders[indexPath.row].status) ?? .pendingConfirmation
        Helper.shared.isFromView = "orderHistory"
        if orderStatus == .completed {
            let vc = Router.viewController(with: .deliveryCompleteVC, .profile) as! DeliveryCompleteVC
            vc.order = orders[indexPath.row]
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = Router.viewController(with: .deliveryOrderDetailVC, .profile) as! DeliveryOrderDetailVC
            vc.order = orders[indexPath.row]
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension OrderHistoryVC: OrderHistoryDelegate{
    
    func canceOrderTap(_ ordert: Order) {
        self.callCancelationOrderPolicyApi { (dic) in
            
        }
    }
    
    func cancelOrderTap(_ order : Order, indexPath : IndexPath) {
        if order.cancelRequest == 1{
            Utility.shared.showAlertView(message: Messages.approvalPending)
            return;
        }
        let dic = ["name" : UserModel.shared.user_id ,"order_id" : order.id, "hall_id" : order.hallId,"email" : UserModel.shared.email,"subject" : "Request for cancellation of order \(order.id)","message" : "Request for order cancellation"] as [String : Any]
        self.ongoingOrder[indexPath.row].cancelRequest = 1
        contactUsApi(dic)
    }
    
    func rateOrderTap(_ order : Order, indexPath : IndexPath) {
        let vc = Router.viewController(with: .deliveryCompleteVC, .profile) as! DeliveryCompleteVC
        vc.order = order
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func reOrderTap(_ order : Order) {
        if UserModel.shared.ongoingOrders.count >= 4 {
            Utility.shared.showAlertView(message: Messages.ongoingOrder)
            return
        }
        Utility.shared.startLoading()
        OrdersViewModel().reOrderService(order.id){(status, message, dic) in
            if status {
                Utility.shared.stopLoading()
                
                let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController
                cartVC?.isFromReOrderView = true
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(cartVC ?? CartViewController(), animated: true)
                
            }
        }
    }
    
    
    func contactUsApi(_ dic : [String : Any]) {
        Utility.shared.startLoading()
        ServiceManager.shared.postRequestWithThreeValues(url: ApiName.contactUs, parameters: dic , completion: { (status, message, response) in
            Utility.shared.stopLoading()
            
            if status ?? false {
                Utility.shared.showAlertView(message: Messages.cancelOrder)
            } else {
                Utility.shared.showAlertView(message: message ?? "")
            }
        })
    }
    
}
extension OrderHistoryVC: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if isPreviousOrders {
            print("scrollViewDidEndDragging")
            if ((self.historyTableView.contentOffset.y + self.historyTableView.frame.size.height) >= self.historyTableView.contentSize.height)
            {
                if !isDataLoading{
                    isDataLoading = true
                    self.previousPageCount=self.previousPageCount+1
                    self.getPreviousOderWithPagination()
                }
            }
        }
        
    }
    
    func getPreviousOderWithPagination() {
        
        Utility.shared.startLoading()
        //MARK:- Previous Order Api call
        OrderHistoryVM().getHistoryOrders("previous", limit: 20, page: self.previousPageCount) { (jsonData) in
            Utility.shared.stopLoading()

            let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
            if let response = responseJSON as? [String: Any] {
                if let data = response["data"] as? [String: Any], let orderArr = data["orders"] as? NSArray {
                    for dic in orderArr {
                        let order = Order().getOrder(dic)
                        self.previousOrder.append(order)
                    }
                    self.historyTableView.reloadData()
                }
            }
        }
    }
}
