//
//  OrderStatusVC.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class OrderStatusVC: BaseVC {
    
    @IBOutlet weak var labelOrderDescription: UILabel!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var pickupTimeLabel : UILabel!
    @IBOutlet weak var orderPrepareStaticTxtLbl : UILabel!
    @IBOutlet weak var orderPrepareBottomLbl : UILabel!

    @IBOutlet weak var orderReadyStaticTxtLbl : UILabel!
    @IBOutlet weak var orderReadyBottomLbl : UILabel!

    @IBOutlet var orderStatusImageViews: [UIImageView]!
    @IBOutlet weak var lblConfirmedStatus: UILabel!
    @IBOutlet weak var vwCancel: UIView!
    var refreshControl = UIRefreshControl()

    @IBOutlet var cancelOrderBtn: UIButton!
    
    var orderStatus: OrderStatusEnum = .pendingConfirmation
    var orderStatusVM = OrderStatusVM()
    var order = Order()
    var isFromCartScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addNotificationCenter()
    }
    
    func setCustomUI() {
        self.getOrderDetail(dish_id: self.order.id)
        self.addRefreshControl()
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(refreshPage(_:)), for: UIControl.Event.valueChanged)
        
        mainScrollView.refreshControl = refreshControl

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func refreshPage(_ sender : Any){
        ServiceManager.shared.hideLoader = true
        self.getOrderDetail(dish_id: self.order.id)
    }
    
    
    @IBAction func back_Click(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.isFromCartScreen {
                self.navigationController?.popToRootViewController(animated: true)
//                if let tabBar = self.tabBarController {
//                    tabBar.selectedIndex = 0
//                }
            }else {
                if let navController = self.navigationController {
                    navController.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func cancelOrder_Click(_ sender: UIButton) {
        self.openCancelliationPolicyView(order_id: "\(order.id)")
    }
}

extension OrderStatusVC {
    
    func getOrderDetail(dish_id: String) {
        orderStatusVM.getOrderDetails(dish_id) { (order) in
            self.order = order
            self.showLabelData()
            self.refreshControl.endRefreshing()
            ServiceManager.shared.hideLoader = false
        }
    }
    
    private func showCurrentOrderStatus(){
        
        if orderStatus != .cancelled && orderStatus != .unattended{
            for iv in self.orderStatusImageViews{
                iv.image = iv.tag <= orderStatus.highlightTillIndex ?  #imageLiteral(resourceName: "Tick In colored") :  #imageLiteral(resourceName: "Tick In Grey")
            }
            self.vwCancel.isHidden = true
        }else{
            for iv in self.orderStatusImageViews{
                if iv.tag != 4 {
                    iv.image = #imageLiteral(resourceName: "Tick In Grey")
                }
            }
            self.vwCancel.isHidden = false
        }
        
        if orderStatus == .cancelled || orderStatus == .completed || orderStatus == .unattended {
            cancelOrderBtn.isHidden = true
        }
        
        if order.paymentMethod == .payAtStore {
            cancelOrderBtn.isHidden = true
        }
    }
    
    func showLabelData() {
        let statusTitle = String(format: Messages.orderStatus, order.statusOrderId ?? "")
        let ranges = statusTitle.ranges(of: "#" + (order.statusOrderId ?? ""))
        
        let attribute = NSMutableAttributedString.init(string: statusTitle)
        
        for range in ranges {
            attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13) , range: range.nsRange)
        }
        
      //  let status =  order.status.replacingOccurrences(of: "_", with: " ")

        if order.order_type == Constant.shared.DELIVERABLE {
                    orderReadyStaticTxtLbl.text = "Out For Delivery"
                    orderReadyBottomLbl.text = "Your order is out for delivery."
        }
        
        labelOrderDescription.numberOfLines = 0
        labelOrderDescription.attributedText = attribute
        
        if order.order_type == Constant.shared.DELIVERABLE {
            let pickupTitle = order.orderStartTime != nil && order.orderEndTime != nil ? "Estimated Delivery Time: \(order.orderStartTime ?? "") - \(order.orderEndTime ?? "")" : ""
            if pickupTitle != ""{
                let ranges = pickupTitle.ranges(of: "Estimated Delivery Time:")
                let attribute = NSMutableAttributedString.init(string: pickupTitle)
                for range in ranges {
                    attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13) , range: range.nsRange)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.primaryColor, range: range.nsRange)
                }
                pickupTimeLabel.attributedText = attribute
            }
        } else {
            let pickupTitle = order.orderStartTime != nil && order.orderEndTime != nil ? "Estimated Pickup Time: \(order.orderStartTime ?? "") - \(order.orderEndTime ?? "")" : ""
            if pickupTitle != ""{
                let ranges = pickupTitle.ranges(of: "Estimated Pickup Time:")
                let attribute = NSMutableAttributedString.init(string: pickupTitle)
                for range in ranges {
                    attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13) , range: range.nsRange)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.primaryColor, range: range.nsRange)
                }
                pickupTimeLabel.attributedText = attribute
            }
        }
        //FIXME:- self.checkStatus() // Change it to new status logic
        
        //FIXME:- Test new logic
        print(order.status)
        self.orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation
        self.showCurrentOrderStatus()
        
        if self.orderStatus == .cancelled || self.orderStatus == .completed {
            cancelOrderBtn.isHidden = true
        } else {
            self.cancelOrderBtn.isHidden = false
        }
        
        
        if orderStatus != .pendingConfirmation && order.order_type == Constant.shared.DELIVERABLE {
            if order.paymentMethod == .payByCard {
                if orderStatus == .completed || orderStatus == .cancelled{
                    self.cancelOrderBtn.isHidden = true
                } else {
                    self.cancelOrderBtn.isHidden = false
                }
            } else {
                self.cancelOrderBtn.isHidden = true
            }
        }
        else if orderStatus != .pendingConfirmation && order.paymentMethod == .payAtStore {
            self.cancelOrderBtn.isHidden = true

        }
        
        if order.paymentMethod == .payAtStore {
            cancelOrderBtn.isHidden = true
        }

    }
}

extension OrderStatusVC {
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }
    
    @objc func notificationRecieved(notification: NSNotification) {
        getOrderDetail(dish_id: self.order.id)
        
    }
}
