//
//  PersonalInfoVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PersonalInfoVC: BaseVC {

    @IBOutlet var nameTxt : UITextField!
    @IBOutlet var mobileTxt : UITextField!
    @IBOutlet var emailTxt : UITextField!
    @IBOutlet var studentTxt : UITextField!
    @IBOutlet weak var userProfileImg: UIImageView!
    
    var personalInfoVM = PersonalInfoVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showData()
        ServiceManager.shared.hideLoader = false
    }
    
   private func showData() {
        self.emailTxt.text = UserModel.shared.email
    self.nameTxt.text = UserModel.shared.user_name.capitalized
        self.mobileTxt.text = UserModel.shared.phone
    self.userProfileImg.set_image(UserModel.shared.profile_pic, placeholder: UIImage(named: "profilePicPlaceholder")!)
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func profile_Click(_ sender: UIButton){
        ImagePicker().chooseImage(on: self, ofType: .image)
    }

    @IBAction func back_Click(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func saveChanges_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        personalInfoVM.saveChangesInformation(nameTxt.text!, mobileTxt.text!) { (model) in
           // self.updateUserData(data: model)
            self.view.makeToast(Messages.profileUpdated, duration: 1.0, point: self.view.center, title: nil, image: nil) { didTap in
                self.popView()
            }
        }
        
    }
}

extension PersonalInfoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.userProfileImg.image = pickedImage
            
            if let data = pickedImage.jpegData(compressionQuality: 0.9){
                self.personalInfoVM.uploadUserImage(data: data) { (success) in
                    if success {
                        
                    }
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }

}

extension PersonalInfoVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let range = range
        if textField == mobileTxt {
            if Int(range.location) == 15 {
                return false
            }
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string != numberFiltered {
                return false
            }
        }else if textField == nameTxt {
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string != numberFiltered {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTxt {
            mobileTxt.becomeFirstResponder()
        }else if textField == mobileTxt{
            studentTxt.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return true
    }
}
