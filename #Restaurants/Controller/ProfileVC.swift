//
//  ProfileVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
class ProfileVC: BaseVC {


    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var dontLBl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewBottom : NSLayoutConstraint!

    var orderHistoryVM = OrderHistoryVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLableTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchtxt = ""
        self.setStatusBar(color: UIColor.white)
        self.tabBarController?.tabBar.isHidden = false
       // self.addBottomBar()
        self.showLoginView()
        self.addNotificationCenter()
        self.showProfileData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func showLoginView() {
        if UserModel.shared.user_id == 0 {
            loginView.isHidden = false
            UserModel.shared.login_Skipped = false
            if let loginVC = Router.viewController(with: .loginVC, .login) as? LoginVC {
                loginVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        } else {
            loginView.isHidden = true
        }
    }

    func showProfileData() {
        self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.height/2
        self.emailLbl.text = UserModel.shared.email
        self.nameLbl.text = UserModel.shared.user_name.firstUppercased
        self.phoneLbl.text = UserModel.shared.phone
        self.profileImgView.set_image(UserModel.shared.profile_pic, placeholder: UIImage(named: "profilePicPlaceholder")!)
    }
    
    
    @IBAction func personalInfo_Click() {
        if let personalInfoVC = Router.viewController(with: .personalInfoVC, .profile) as? PersonalInfoVC {
            personalInfoVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(personalInfoVC , animated: true)
        }
    }

    @IBAction func settings_Click() {
        if let settingsVC = Router.viewController(with: .settingsVC, .settings) as? SettingsVC {
            settingsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    
    @IBAction func editProfile(_ sender: Any) {
        
        if let personalInfoVC = Router.viewController(with: .personalInfoVC, .profile) as? PersonalInfoVC {
            personalInfoVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(personalInfoVC , animated: true)
        }
    }
    
    func setLableTitle() {
        let newUserStr = "Don't have an account? "
        let lightAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Light", size: 13.0)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let newAttStr = NSAttributedString(string: newUserStr, attributes: lightAttribute)
        
        let registerStr = " Register"
        let boldAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 13.0)!, NSAttributedString.Key.foregroundColor:  AppColor.primaryColor]
        let regAttrStr = NSAttributedString(string: registerStr, attributes: boldAttribute)
        
        let finalStr = NSMutableAttributedString(attributedString: newAttStr)
        finalStr.append(regAttrStr)
        
        dontLBl.attributedText = finalStr
    }
    
    private func addBottomBar() {
        let heightConstant : CGFloat = UserModel.shared.cartItemsCount > 0 ? -72:0
        addBottomCartView(self.view,heightConstant)
        containerViewBottom.constant = UserModel.shared.cartItemsCount > 0 ? -40:0

    }
    override func updateValueOnBottomCartView() {
        self.addBottomBar()
    }
    
    //MARK:- Navigation Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "orderhistory" {
            if let vc = segue.destination as? OrderHistoryVC {
                vc.isPreviousOrders = false
            }
        }
    }
}

extension ProfileVC {
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved(notification:)), name:NSNotification.Name(rawValue: AppNotification.received), object: nil)
    }

    @objc func notificationRecieved(notification: NSNotification) {
        self.getOnGoingOrdersList { () in
            self.addBottomBar()
        }
    }
}
