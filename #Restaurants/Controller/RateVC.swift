//
//  RateVC.swift
//  #Restaurants
//
//  Created by Satveer on 26/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RateVC: BaseVC {

    // MARK: - Variables
    var rateNextScreen : Bool = false
    var selectedRatingParams = [String]()
    var thumb : String = ""
    var order = Order()

    var dummy = 1
    var orderRated: ((String, String)-> Void)?
    
    // MARK: - Outlets
    @IBOutlet var rateNextView: UIView!
    @IBOutlet var ratingParameters : [UIButton]!
    @IBOutlet var thumbRateBtn : [UIButton]!
    @IBOutlet var notesTV : UITextView!
    @IBOutlet var notesTvVariantHeight : NSLayoutConstraint!
    @IBOutlet var notesTvFixedHeight : NSLayoutConstraint!
    @IBOutlet var orderNumber : UILabel!
    @IBOutlet var orderDishes : UILabel!
    var order_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderNumber.text = "Order No. #" + "\(order.id)"
        orderDishes.text = order.description
        self.order_id = "\(order.id)"
        if rateNextScreen == true{
            rateNextView.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func blurViewBtn()
    {
      self.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction func nextRatingScreenTap()
    {
        if thumb == ""
        {
            Utility.shared.showAlertView(message: "Please select your vote")
            return;
        }
        rateNextView.isHidden = false
    }
    
    @IBAction func submitTap()
    {
        ratingServiceCall()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func rateParameterSelected(_ sender : UIButton)
    {
        // Select paramters on which you are giving ratings
        let parameterSelected = sender.title(for: .normal) ?? ""
        if !selectedRatingParams.contains(parameterSelected){
            selectedRatingParams.append(parameterSelected)
            sender.isSelected = true
            stateChanged(sender)
            print(selectedRatingParams)
        }else{
            let index = selectedRatingParams.firstIndex(of: parameterSelected) ?? 0
            selectedRatingParams.remove(at: index)
            sender.isSelected = false
            stateChanged(sender)
            print(selectedRatingParams)
        }
    }


    // MARK :- Functions
    
    func stateChanged(_ sender : UIButton){
        //sender.layer.borderColor = sender.isSelected ? AppColor.primaryColor.cgColor : AppColor.darkTextColor.cgColor
        sender.layer.backgroundColor = sender.isSelected ? AppColor.primaryColor.cgColor : UIColor.white.cgColor
        sender.layer.borderWidth = sender.isSelected ? 0 : 1
        if sender.isSelected
        {
            sender.setTitleColor(UIColor.white, for: .normal)
        }else{
           sender.setTitleColor(AppColor.darkTextColor, for: .normal)
        }
    }
    
    @IBAction func ratingBtnTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected && sender.tag == 1{
            thumb = "up"
            for btn in thumbRateBtn
            {
                if btn.tag != sender.tag{
                    btn.isSelected = false
                }
            }
        }else if sender.isSelected && sender.tag == 2{
            thumb = "down"
            for btn in thumbRateBtn
            {
                if btn.tag != sender.tag{
                    btn.isSelected = false
                }
            }
        }else{
            thumb = ""
        }
    }
    
    //MARK:- Textfield Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        print(textView.contentSize.height)
        if notesTV.contentSize.height > 55 {
            if dummy == 1{
                notesTvFixedHeight.constant = notesTV.contentSize.height
            }
            notesTvFixedHeight.isActive = true
            notesTvVariantHeight.isActive = false
            notesTV.isScrollEnabled = true
            dummy = 0
            print(notesTvFixedHeight.constant)
        } else {
            
            notesTvFixedHeight.isActive = false
            notesTvVariantHeight.isActive = true
            notesTV.isScrollEnabled = false
            if dummy == 0 {
                notesTV.text = notesTV.text + " "
            }
            dummy = 1
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 80
    }
}

extension RateVC {
     // MARK :- Service Call
    func ratingServiceCall() {
        var dic = ["order_id" : self.order_id ,"thumb" : thumb] as [String : Any]
        var ratingParams = ""
        if selectedRatingParams.count != 0 {
            ratingParams = selectedRatingParams.joined(separator: ",")
            dic["area"] = ratingParams
        }
        if notesTV.text.trim() != "" {
            dic["notes"] = notesTV.text
        }
        
        Utility.shared.startLoading()
        ServiceManager.shared.postRequestWithThreeValues(url: ApiName.rateOrder, parameters: dic, completion: { (status, message, response) in
            Utility.shared.stopLoading()
            if status ?? false {
                self.orderRated?(self.thumb, message ?? "")
            } else {
              
            }
        })
    }
    

    
}
