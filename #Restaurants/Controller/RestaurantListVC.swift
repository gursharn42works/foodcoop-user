//
//  RestaurantListVC.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol RestaurantListVCDelegate: class {
    func reloadHomeScreenDataAccordingSelectedLocation()
}

class RestaurantListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backViewYPos: NSLayoutConstraint!
    
    var delegate: RestaurantListVCDelegate?
    
    var restaurantListVM = RestaurantListVM()
    var addressModel: RestaurantListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpTableView()
        self.addGesture()
        
    }

    
    func setUpTableView() {
        self.restaurantListVM.addressModel = addressModel
        self.tableView.dataSource = restaurantListVM
        self.tableView.delegate = restaurantListVM
        self.tableView.reloadData()
        
    }

    @IBAction func cellBtn_Click(sender: UIButton) {
        if let location = addressModel?.locations[sender.tag] {
            UserModel.shared.hall_id = location.id
            UserModel.shared.hall_name = location.name
        }
        UserModel.shared.selectedLocation = sender.tag
        self.tableView.reloadData()
        self.delegate?.reloadHomeScreenDataAccordingSelectedLocation()
        self.dismiss(animated: true, completion: nil)

    }
}


