//
//  RestaurantTimeVC.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RestaurantTimeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hallNameLbl: UILabel!

    var restaurantTimeVM = RestaurantTimeVM()
    var hallTiming: [HallTiming] = []
    var hallName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hallTiming = UserModel.shared.hallTimings
        self.setUpCustomUI()
    }

    func setUpCustomUI() {
        self.sortTime()

        tableView.register(UINib(nibName: Constant.shared.productCatagoryCell, bundle: nil), forCellReuseIdentifier: Constant.shared.productCatagoryCell)
        self.setUpTableView()
        
        if hallName != "" { hallNameLbl.text = hallName }
        
    }
    
    func sortTime() {
        var timeArr: [HallTiming] = []
        for i in 0..<7 {
            let date = Calendar.current.date(byAdding: .day, value: i, to: Date()) ?? Date()
            let dayname = date.dayNameOfWeek() ?? ""
            for time in hallTiming {
                if time.day == dayname {
                    timeArr.append(time)
                    break
                }
            }
        }
        self.hallTiming = timeArr
    }
    
    private func setUpTableView() {
        
        self.restaurantTimeVM.hallTiming = self.hallTiming
        self.tableView.dataSource = restaurantTimeVM
        self.tableView.delegate = restaurantTimeVM
        self.restaurantTimeVM.tableView = self.tableView
    }
    
    @IBAction func closeBtn_Click(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

}

extension Date {
    func dayNameOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self)
    }
}
