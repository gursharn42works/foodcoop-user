//
//  SearchVC.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Alamofire

class SearchVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var searchLogo: UIImageView!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var topSpaceSearchTV: NSLayoutConstraint!
    @IBOutlet weak var searchTableBottom : NSLayoutConstraint!
    
    var searchVM = SearchVM()
    var task = CancellableDelayedTask()
    
    var category_id = 0
    var filterSearch = false
    var timer:Timer!
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomUI()
        self.callApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Utility.shared.isSearchViewController = true
        self.addBottomBar()
    }
    
//    private func showCartPopUp() {
//        searchTableBottom.constant = self.showHideOnlyCartView(isShow: UserModel.shared.cartItemsCount > 0 ? true : false)
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Utility.shared.isSearchViewController = false
    }
    
    private func setCustomUI() {
        self.searchTextField.setPlaceholderColor("Type here to search")
        self.setUpTableView()
//        self.getSearchData(self.searchTextField.text!)
        

    }
    
    private func setUpTableView() {
        self.tableView.register(UINib(nibName: Constant.shared.hallDishCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.hallDishCell)
        
        self.tableView.dataSource = searchVM
        self.tableView.delegate = searchVM
        self.searchVM.tableView = self.tableView
       // self.searchVM.delegate = self
        
    }
    
    
    
    @IBAction func back_Click(_ sender: UIButton) {
        self.popView()
    }
    
    func moveToDishDetailFromSearch(dish_id: Int) {
        if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
            dishDetailVC.hidesBottomBarWhenPushed = true
            dishDetailVC.dish_id = dish_id
            self.navigationController?.pushViewController(dishDetailVC, animated: true)
        }
    }
    
    func reloadSearchViewTableView() {
        self.tableView.reloadData()
        self.updateLabel()
    }
    
    private func addBottomBar() {
        let heightConstant : CGFloat = UserModel.shared.ongoingOrders.count > 0 ? 0 : self.isDeviceIsIphoneX() ? 50 : 0
        addBottomCartView(self.view,heightConstant)
        for view in self.view.subviews {
            if let temp = view as? BottomCartView , temp.tag == -10004 {
                searchTableBottom.constant = 50
            }else{
                searchTableBottom.constant = 0
            }
        }
    }
    
    override func updateValueOnBottomCartView() {
        self.addBottomBar()
    }
}

extension SearchVC: UITextFieldDelegate {
    
    func callApi() {
        if filterSearch{
            search()
        }else{
            searchApiCall("", showDefault: true,true)
        }
    }
    
    func searchApiCall(_ query : String ,showDefault : Bool, _ resetFilter : Bool = false){
        self.addLoaderOnImage(self.searchLogo)
        searchVM.getSearchData(query, showDefault: showDefault,resetFilter) { (success) in
            if success {
                self.tableView.reloadData()
                self.updateLabel()
//                self.showNoDataView()
            }
        }
    }
    
    private func search() {
        clearBtn.isHidden = true
        if filterSearch
        {
            searchApiCall("", showDefault: true,true)
        }else{
            searchLabel.text = ""
            topSpaceSearchTV.constant = 0
            tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            self.searchVM.dishesArr.removeAll()
            self.searchVM.allDishesArr.removeAll()
            self.tableView.reloadData()
            searchApiCall("", showDefault: true,true)
            self.updateLabel()
        }
    }
    
    func updateLabel(){
        self.removeLoaderOnImage(self.searchLogo, Constant.shared.searchLogo)

        // Show empty screen in case of no data
        if searchTextField.text?.trim() == ""{
            if self.searchVM.dishesArr.count != 0 {
                //self.dishesArr = self.allDishesArr
                self.removeEmptyMsg(self.view)
                self.tableView.reloadData()
            }else{
                self.showEmptyScreenMessage("No matches", "Try broadening your search.", self.view, #imageLiteral(resourceName: "search"))
            }
        } else {
            if self.searchVM.dishesArr.count == 0{
                self.showEmptyScreenMessage("No matches", "Try broadening your search.", self.view, #imageLiteral(resourceName: "search"))

            }else{
                self.removeEmptyMsg(self.view)
            }
        }
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        
//        guard sender.text?.trim().length != 0 else {
//
//         //   DispatchQueue.main.async {
//                self.searchVM.dishesArr.removeAll()
//                self.searchVM.dishesArr = self.searchVM.allDishesArr
//                self.tableView.reloadData()
//                self.updateLabel()
//                // Cancel all hits in queue
//                Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in sessionDataTask.forEach({ (session) in
//                    session.cancel()
//                })
//                    uploadData.forEach({ (uploadSession) in
//                        uploadSession.cancel()
//                    })
//                }
//           // }
//            return
//        }
        
//        self.task.cancel()
//        self.task = CancellableDelayedTask()
      //  self.task.run(delay: 0.5) { () -> Void in
           // self.searchVM.page = 1
           // self.searchApiCall(sender.text?.trim() ?? "", showDefault: false)
       // }
        
        searchText = sender.text ?? ""
        if self.timer != nil {
            self.timer.invalidate()
        }
        if searchText == "" {
            print("noooooo")
            self.searchVM.dishesArr.removeAll()
            self.searchVM.dishesArr = self.searchVM.allDishesArr
            self.tableView.reloadData()
            self.updateLabel()

        } else {
            print("yessss")
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.callSearchTimerApi), userInfo: searchText, repeats: false)
        }

    }
    
    @objc func callSearchTimerApi() {
        if timer.userInfo != nil {
            print(timer.userInfo ?? "")
        }
        timer.invalidate()
         self.searchVM.page = 1
         self.searchApiCall(searchText, showDefault: false)

    }

    func callRequest() {
        self.task.run(delay: 0.5) { () -> Void in
            self.searchVM.page = 1
            self.searchApiCall((self.searchTextField.text!.trim()), showDefault: false)
        }
    }
    
    func onFavClickTap(dishModel: DishModel) {
        // If user is not logged in
        if UserModel.shared.user_id == 0{
            UserModel.shared.login_Skipped = true
            let loginController = Router.viewController(with: .loginVC, .login) as! LoginVC
            self.navigationController?.pushViewController(loginController , animated: true)
            return
        }
        // Update fav on home screen
       // if let homeVC = (((self.window?.rootViewController as? UINavigationController)?.viewControllers.first as? MagicTabBar)?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? HomeVC{
//            if homeVC.tableView != nil{
//                ServiceManager.shared.hideLoader = true
//                homeVC.homeVM.getHomeData { (success) in
//                    ServiceManager.shared.hideLoader = false
//                }
//            }
       // }
        // Update fav on Menu Detail
//        if let VCArr = (((self.window?.rootViewController as? UINavigationController)?.viewControllers.first as? MagicTabBar)?.viewControllers){
//            for navCon in VCArr {
//                if let navCon = navCon as? UINavigationController{
//                    for vc in navCon.viewControllers{
//                        if vc is FullMenuVC {
//                            if (vc as? FullMenuVC)?.buttonBarView != nil {
//                                ServiceManager.shared.hideLoader = true
//                                (vc as? FullMenuVC)?.fullMenuVM.getFullMenuData(filter: UserModel.shared.appliedFilter, completion: { (success) in
//                                    ServiceManager.shared.hideLoader = false
//                                })
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }

//    func showNoDataView() {
//
//        self.removeLoaderOnImage(self.searchLogo, Constant.shared.searchLogo)
//        if searchTextField.text?.trim() == "" {
//            if self.searchVM.dishesArr.count != 0  {
//                self.removeEmptyMsg(self.view)
//                self.tableView.reloadData()
//            }else{
//                self.showMessage()
//            }
//        } else {
//            if self.searchVM.dishesArr.count == 0{
//                self.showMessage()
//            }else{
//                self.removeEmptyMsg(self.view)
//            }
//        }
//    }
    
    func showMessage() {
        self.showEmptyScreenMessage("No matches", "Try broadening your search.", self.view, #imageLiteral(resourceName: "search"), self.view)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldShouldClear")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.removeLoaderOnImage(self.searchLogo, Constant.shared.searchLogo)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldChanged")
       searchTextField.text = ""
        self.searchText = ""
        self.searchVM.dishesArr.removeAll()
       self.searchVM.dishesArr = self.searchVM.allDishesArr
       tableView.reloadData()
       updateLabel()
        self.removeLoaderOnImage(self.searchLogo, Constant.shared.searchLogo)
        return true
    }
    
//    @IBAction func crossBtnAction(_ sender: Any){
//        searchTextField.text = ""
//        crossBtn.isHidden = true
//        crossImage.isHidden = true
//        dishesArr.removeAll()
//        dishesArr = allDishesArr
//        searchTablevView.reloadData()
//        updateLabel()
//    }
}
//extension SearchVC : UIScrollViewDelegate,SearchVMDelegate {
//    func getSearchPaginationData() {
//        if searchVM.page != Int(self.searchVM.dishesArr.count/searchVM.limit + 1){
//            searchVM.page = Int(self.searchVM.dishesArr.count/searchVM.limit + 1)
//            if searchTextField.text! == "" {
//                self.searchApiCall(searchTextField.text ?? "", showDefault: true, true)
//            } else {
//                self.searchApiCall(searchTextField.text ?? "", showDefault: false, true)
//            }
//        }else{
//            return
//        }
//
//    }
//
//}
