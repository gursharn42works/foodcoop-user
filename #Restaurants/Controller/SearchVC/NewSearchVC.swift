//
//  NewSearchVC.swift
//  #Restaurants
//
//  Created by 42works on 26/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import CoreLocation

var searchtxt = ""
class NewSearchVC: BaseVC{
    
    
    
    @IBOutlet weak var searchVW_Out: UISearchBar!{
        didSet {
            searchVW_Out.returnKeyType = .done
            searchVW_Out.enablesReturnKeyAutomatically = false
        }
    }
    @IBOutlet weak var serachBGView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var addressLbl_Out: UILabel!
    var delieveryAddress = DeliveryAddressModel()
    
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation!
    var searchVM = SearchVM()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchTable.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchTableCell")
        self.searchTable.dataSource = searchVM
        self.searchTable.delegate = searchVM
        self.searchVM.tableView = self.searchTable
        self.searchVM.delegate = self
        self.searchTable.separatorColor = UIColor.clear
        setSearchbarUI()
        
    }
    
    func setSearchbarUI() {
        searchVW_Out.searchTextField.backgroundColor = .white
        searchVW_Out.setImage(UIImage(named: "SearchIcon"), for: .search, state: .normal)
        serachBGView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        bgView.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        searchVW_Out.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBar(color: UIColor.white)
        if searchtxt == "grill" {
            searchtxt = ""
        }
         searchVW_Out.text = searchtxt
        tabBarController?.tabBar.isHidden = false
        let address = delieveryAddress.getSavedDeliveryAddress()
        if address.id != 0 || address.title != "" {
            self.addressLbl_Out.text = "\(address.title.capitalizeFirstInEachWord())-\(address.house_no), \(address.street1), \(address.street2), \(address.city.capitalized), \(address.state.capitalized)."
        }else {
            self.getCurrentLoaction ()
        }
        
        callApi()
    }
    
    func callApi() {
            searchApiCall(searchtxt, showDefault: true,true)
    }
    
    func searchApiCall(_ query : String ,showDefault : Bool, _ resetFilter : Bool = false){
       // self.addLoaderOnImage(self.searchLogo)
        searchVM.getSearchData(query, showDefault: showDefault,resetFilter) { (success) in
            if success {
                self.searchTable.reloadData()
            
//                self.showNoDataView()
            }
        }
    }
    

    @IBAction func AddressBtn_Click(_ sender: Any) {
        if UserModel.shared.user_id == 0 {
            self.getCurrentLoaction ()
            return
        }
        if addressLbl_Out.text == "Add an Address." {
            let vc = Router.viewController(with: .addDeliveryAddressVC, .cart) as! AddDeliveryAddressVC
            vc.isFromCartView = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = Router.viewController(with: .deliveryAddressVC, .cart) as! DeliveryAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getCurrentLoaction () {
        
        DispatchQueue.main.async { [self] in
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
        CLLocationManager.authorizationStatus() == .authorizedAlways) {
            currentLoc = locationManager.location
            print(currentLoc.coordinate.latitude)
            print(currentLoc.coordinate.longitude)
            self.getAddressFromLatLon(loc: currentLoc)
        }else {
            print("no lat long" )
        }
      }
        
    }
    
    func getAddressFromLatLon(loc: CLLocation) {
            
        CLGeocoder().reverseGeocodeLocation(loc, completionHandler:
                                                { [self](placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]
                    var addressString : String = ""
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country ?? "")
                        print(pm.locality ?? "")
                        print(pm.subLocality ?? "")
                        print(pm.thoroughfare ?? "")
                        print(pm.postalCode ?? "")
                        print(pm.subThoroughfare ?? "")
                        
                        if pm.subThoroughfare != nil {
                            addressString = addressString + pm.subThoroughfare! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        print(addressString)
                       
                  }
                self.addressLbl_Out.text = addressString
                
            })

        }
    
}

extension NewSearchVC : UISearchBarDelegate {

    // This method updates filteredData based on the text in the Search Box
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            searchtxt = searchText
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                self.searchApiCall(searchtxt, showDefault: true,true)
            }
//            if searchText == "" {
//                FilterArr.removeAll()
//                FilterArr = SearchArr
//            }else {
//                FilterArr = SearchArr.filter { ($0["name"]?.range(of: searchText, options: .caseInsensitive) != nil) }
//            }
//
//            searchTable.reloadData()
        }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
}

extension NewSearchVC : SearchVMDelegate{
   
    func moveToDishDetailFromSearch(dish_id: Int, type: String, itemname: String) {
        jumptoNextView(type: type, id: dish_id, itemname : itemname)
    }
    
    func reloadSearchViewTableView() {
        print("Hello")
    }
    
    func onFavClickTap(dishModel: DishModel) {
        print("Hello")
    }
    
    func getSearchPaginationData() {
        print("Hello")
    }
    
    func jumptoNextView(type: String,id: Int, itemname : String) {
        if type == "tag" {
            if let restSearchVC = Router.viewController(with: .Restsearch, .home) as? RestaurantSearchVC {
                restSearchVC.RestTagId = id
                restSearchVC.searchTxt = ""
                
                restSearchVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(restSearchVC, animated: true)
            }
        }else if type == "dish" {
            if let restSearchVC = Router.viewController(with: .Restsearch, .home) as? RestaurantSearchVC {
                restSearchVC.RestTagId = 0
                if searchtxt == "" {
                    searchtxt = itemname
                }
                restSearchVC.searchTxt = searchtxt
                
                restSearchVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(restSearchVC, animated: true)
            }
        }else {
            if let dishDetailVC = Router.viewController(with: .fullMenuVC, .home) as? FullMenuVC {
                dishDetailVC.restId = id
                dishDetailVC.searchTxt = searchtxt
                dishDetailVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(dishDetailVC, animated: true)
            }
        }
        
    }
    
}
