//
//  RestaurantSearchVC.swift
//  #Restaurants
//
//  Created by 42works on 31/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit


class RestaurantSearchVC: UIViewController {
    
    @IBOutlet weak var addressLbl_Out: UILabel!
    @IBOutlet weak var restSearchTable: UITableView!
    var RestTagId = 0
    var searchTxt = ""
    
    
    
    var restaurantVM = RestaurantSearchVM()
    var delieveryAddress = DeliveryAddressModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        restSearchTable.register(UINib(nibName: "RestaurantsTableViewCell", bundle: nil), forCellReuseIdentifier: "RestaurantCell")
        self.restSearchTable.dataSource = restaurantVM
        self.restSearchTable.delegate = restaurantVM
        self.restaurantVM.tableView = self.restSearchTable
        self.restaurantVM.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let address = delieveryAddress.getSavedDeliveryAddress()
        if address.id != 0 || address.title != "" {
            self.addressLbl_Out.text = "\(address.title.capitalizeFirstInEachWord())-\(address.house_no), \(address.street1), \(address.street2), \(address.city.capitalized), \(address.state.capitalized)."
        }else {
            if UserDefaults.standard.object(forKey: "CurrentAddress") != nil {
                addressLbl_Out.text = (UserDefaults.standard.object(forKey: "CurrentAddress") as! String)
            }
        }
        
        callApi()
    }
    
    func callApi() {
            searchApiCall(RestTagId, showDefault: true,true)
    }
    
    func searchApiCall(_ tagId : Int ,showDefault : Bool, _ resetFilter : Bool = false){
        Utility.shared.startLoading()
        restaurantVM.getSearchData(tagId, restStr: searchTxt, showDefault: showDefault,resetFilter) { (success) in
            Utility.shared.stopLoading()
            if success {
                self.restSearchTable.reloadData()
            
//                self.showNoDataView()
            }
        }
    }

    @IBAction func AddressBtn_Click(_ sender: Any) {
        if UserModel.shared.user_id == 0 {
            if UserDefaults.standard.object(forKey: "CurrentAddress") != nil {
                addressLbl_Out.text = (UserDefaults.standard.object(forKey: "CurrentAddress") as! String)
            }
            return
        }
        if addressLbl_Out.text == "Add an Address." {
            let vc = Router.viewController(with: .addDeliveryAddressVC, .cart) as! AddDeliveryAddressVC
            vc.isFromCartView = true

            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = Router.viewController(with: .deliveryAddressVC, .cart) as! DeliveryAddressVC
            vc.comingforRrst = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func BackBtn_Click(_ sender: Any) {
        
            self.navigationController?.popViewController(animated: true)
    }
    
}

extension RestaurantSearchVC : UISearchBarDelegate {

    // This method updates filteredData based on the text in the Search Box
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
               // self.searchApiCall(searchText, showDefault: true,true)
            }

        }
    
    
}

extension RestaurantSearchVC : RestaurantSearchVMDelegate {
    func jumpToMenu(rest_Id: Int) {
        if let dishDetailVC = Router.viewController(with: .fullMenuVC, .home) as? FullMenuVC {
            dishDetailVC.restId = rest_Id
            dishDetailVC.tagid = RestTagId
            if RestTagId != 0 {
                searchtxt = ""
            }
            dishDetailVC.searchTxt = searchtxt
            dishDetailVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(dishDetailVC, animated: true)
        }
    }
    
    
    
    func moveToDishDetailFromSearch(dish_id: Int) {
        print("Hello")
    }
    
    func reloadSearchViewTableView() {
        print("Hello")
    }
    
    func onFavClickTap(dishModel: DishModel) {
        print("Hello")
    }
    
    func getSearchPaginationData() {
        print("Hello")
    }
}
