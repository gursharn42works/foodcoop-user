//
//  SettingsVC.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {
    
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var changePassHeight: NSLayoutConstraint!
    @IBOutlet weak var descLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.makeAttributedLblTxt()
        
    }
    

    private func makeAttributedLblTxt() {
        let stringValue = "Be it a bug you found or an area you want to serve,shoot us an email here and we will try our best."
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textToFind: "Be it a bug you found or an area you want to serve,shoot us an email", withColor: AppColor.darkTextColor)
        attributedString.setColorForText(textToFind: "here", withColor: AppColor.primaryColor)
        attributedString.setColorForText(textToFind: "and we will try our best.", withColor:  AppColor.darkTextColor)
        descLbl.attributedText = attributedString

    }
    @IBAction func support_Click(_ sender: UIView){
        if let contactUsVC = Router.viewController(with: .contactUsVC, .settings) as? ContactUsVC {
            self.navigationController?.pushViewController(contactUsVC, animated: true)
        }
    }
    
    @IBAction func termsCond_Click(_ sender: UIView){
        if let termsConditionVC = Router.viewController(with: .termsConditionVC, .login) as? TermsConditionVC {
            termsConditionVC.isTerms = true
            self.navigationController?.pushViewController(termsConditionVC, animated: true)
        }
    }
    
    @IBAction func changePass_Click(_ sender: UIView){
        if let changePasswordVC = Router.viewController(with: .changePasswordVC, .settings) as? ChangePasswordVC {
            self.navigationController?.pushViewController(changePasswordVC, animated: true)
        }
    }
    
    @IBAction func back_Click(_ sender: UIView){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutBtnClick(_ sender: UIView){
        if let dishDetailVC = Router.viewController(with: .logoutVC, .profile) as? LogoutVC {
            dishDetailVC.modalPresentationStyle = .overCurrentContext
            self.window?.rootViewController?.present(dishDetailVC, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func here_Click(_ sender: UIView){
        if let contactUsVC = Router.viewController(with: .contactUsVC, .settings) as? ContactUsVC {
            self.navigationController?.pushViewController(contactUsVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NSMutableAttributedString {
        func setColorForText(textToFind: String, withColor color: UIColor) {
         let range: NSRange = self.mutableString.range(of: textToFind, options: .caseInsensitive)
            self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
}
