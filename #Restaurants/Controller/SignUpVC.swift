//
//  SignUpVC.swift
//  #Restaurants
//
//  Created by 42works on 24/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SignUpVC: BaseVC {
    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPassTxt: UITextField!
    @IBOutlet weak var termsCheckImg: UIImageView!
    @IBOutlet weak var termsTxtView: UITextView!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var nameHeightConst: NSLayoutConstraint!
    @IBOutlet weak var emailHeightConst: NSLayoutConstraint!
    @IBOutlet weak var phoneHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var passHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var confirmPassheightConst: NSLayoutConstraint!
    
    var isTermsAccepted = false
    var isFromLogin = false
    var isFromSkip = false
    
    var signUpVM = SignUpVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCustomUI()
    }
    
    func setCustomUI() {
       // termsTxtView.textColor = .white
    }
    
    @IBAction func check_Clcik(_ sender: UIButton) {
        
        termsCheckImg.image = isTermsAccepted ? UIImage(named: Constant.shared.uncheckImg):UIImage(named: Constant.shared.checkedImg)
        isTermsAccepted = !isTermsAccepted
        
    }
    
    @IBAction func terms_Clcik(_ sender: UIButton) {
        let termsConditionVC = Router.viewController(with: .termsConditionVC, .login) as? TermsConditionVC
        termsConditionVC?.isTerms = true
        self.navigationController?.pushViewController(termsConditionVC ?? UIViewController(), animated: true)
        
    }
    
    @IBAction func register_Clcik(_ sender: UIButton) {
        self.registerUser()
    }
    
    @IBAction func login_Clcik(_ sender: UIButton)  {
        if isFromSkip {
             let loginVC = Router.viewController(with: .loginVC, .login) as! LoginVC
            loginVC.isFomSignUpSkip = true
            self.navigationController?.pushViewController(loginVC, animated: true)
            return
        }
        self.popView()
    }
    
    @IBAction func back_Click(_ sender: UIButton) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
}

extension SignUpVC:UITextViewDelegate, UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTxt {
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTxt:
            emailTxt.becomeFirstResponder()
        case emailTxt:
            passwordTxt.becomeFirstResponder()
        case passwordTxt:
            confirmPassTxt.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case nameTxt:
            nameHeightConst.constant = 1.5
        case emailTxt:
            emailHeightConst.constant = 1.5
        case phoneTxt:
            phoneHeightConst.constant = 1.5
        case passwordTxt:
            passHeightConst.constant = 1.5
        case confirmPassTxt:
            confirmPassheightConst.constant = 1.5
        default:
            nameHeightConst.constant = 0.5
            emailHeightConst.constant = 0.5
            phoneHeightConst.constant = 0.5
            passHeightConst.constant = 0.5
            confirmPassheightConst.constant = 0.5
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case nameTxt:
            if nameTxt.text == "" {
                nameHeightConst.constant = 0.5
            }else {
                nameHeightConst.constant = 1.5
            }
        case emailTxt:
            if emailTxt.text == "" {
                emailHeightConst.constant = 0.5
            }else {
                emailHeightConst.constant = 1.5
            }
        case phoneTxt:
            if phoneTxt.text == "" {
                phoneHeightConst.constant = 0.5
            }else {
                phoneHeightConst.constant = 1.5
            }
        case passwordTxt:
            if passwordTxt.text == "" {
                passHeightConst.constant = 0.5
            }else {
                passHeightConst.constant = 1.5
            }
        case confirmPassTxt:
            if confirmPassTxt.text == "" {
                confirmPassheightConst.constant = 0.5
            }else {
                confirmPassheightConst.constant = 1.5
            }
        default:
            nameHeightConst.constant = 0.5
            emailHeightConst.constant = 0.5
            phoneHeightConst.constant = 0.5
            passHeightConst.constant = 0.5
            confirmPassheightConst.constant = 0.5
        }
    }
    
    private func registerUser() {
        let isSkipped = UserModel.shared.login_Skipped

        signUpVM.callSignUpService(nameTxt.text!, emailTxt.text!, phone: phoneTxt.text!, password: passwordTxt.text!, confirmPassword: confirmPassTxt.text!,isTermsAccepted: self.isTermsAccepted) { (success) in
            if success {
                
                if isSkipped {
                    UserModel.shared.isLoggedin = true
                    if self.isFromLogin {
                        self.popBack(3)
                    } else {
                        self.navigationController?.popViewController(animated: false)
                    }
                } else {
                    if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
                        let rootNC = UINavigationController(rootViewController: magicTabBar)
                        rootNC.setNavigationBarHidden(true, animated: false)
                        self.window?.rootViewController = rootNC
                        self.window?.makeKeyAndVisible()
                    }
                }
                
                UserModel.shared.removeValueFromSkipLogin()

            }
        }
    }
}

