//
//  SplashVC.swift
//  #Restaurants
//
//  Created by 42works on 24/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class SplashVC: BaseVC {
    
    @IBOutlet weak var driverImg_Out: UIImageView!
    
    var splashTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        driverImg_Out.image = UIImage.gif(name: "Driver")
        splashTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    @objc func runTimedCode() {
        splashTimer?.invalidate()
        setRootViewController()
        
    }
    
    func setRootViewController() {
        if UserModel.shared.getUserData() || UserModel.shared.login_Skipped {
            self.moveToRootView()
        } else {
            self.moveToLoginView()
        }
    }
    
    func moveToRootView() {
        DispatchQueue.main.async {
            
                if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
                    let rootNC = UINavigationController(rootViewController: magicTabBar)
                    rootNC.setNavigationBarHidden(true, animated: false)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
                }
            
        }
    }
    
    func moveToLoginView() {
        DispatchQueue.main.async {
                if let magicTabBar = Router.viewController(with: .loginVC, .login) as? LoginVC {
                    let rootNC = UINavigationController(rootViewController: magicTabBar)
                    rootNC.setNavigationBarHidden(true, animated: false)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
                }
            
        }
    }
    

}
