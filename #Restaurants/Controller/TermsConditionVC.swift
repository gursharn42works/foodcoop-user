//
//  TermsConditionVC.swift
//  #Restaurants
//
//  Created by 42works on 24/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import WebKit

class TermsConditionVC: BaseVC {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var termsConditionVM = TermsConditionVM()
    var isTerms = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCustomUI()
    }
    
    func setCustomUI() {
        webView.navigationDelegate = self
        
        if isTerms {
            titleLbl.text = Constant.shared.termsTitle
            self.getContent(ApiName.terms)
        } else {
            titleLbl.text = Constant.shared.privacyTitle
            self.getContent(ApiName.privacy)
        }
    }
    
    func getContent(_ string: String) {
        termsConditionVM.getContent(string) { (content) in
            if Reachability.isConnectedToNetwork() == false {
                self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
                return
            } else {
                self.removeEmptyMsg(self.view)
            }
            if let con = content{
                self.webView.loadHTMLString(con.data.page.htmlData, baseURL: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNointernetView()
    }
    
    func showNointernetView() {
        if Reachability.isConnectedToNetwork() == false {
            self.messageWithButton (Messages.noInternetHeading, message2: Messages.noInternetSubHeading, superView: self.view, image: #imageLiteral(resourceName: "NoInternet"))
            return
        } else {
            self.removeEmptyMsg(self.view)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func back_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension TermsConditionVC: WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        var action: WKNavigationActionPolicy?
        defer {
            decisionHandler(action ?? .allow)
        }
        guard let url = navigationAction.request.url else { return }
        if navigationAction.navigationType == .linkActivated {
            action = .cancel                  // Stop in WebView
            UIApplication.shared.open(url, options: [:])
        }
    }
    
}
