//
//  Extensions.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView

var loaderView = LoaderView()
var imageViewLoader: NVActivityIndicatorView?

extension UIImageView {
   
   func set_image(_ url: String, placeholder: Any) {
       let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    
    if let imgStr = placeholder as? String {
        
        if url.contains("%20") {
           // self.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: imgStr))
        } else {
            if urlString == "https://chickenhub.hashtagrestaurants.com/admin/" {
                self.image = UIImage(named: "dishPlaceholder_300_200")

            } else {
             //   self.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: imgStr))
            }

        }
    } else if let img = placeholder as? UIImage {
       // self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.sd_setImage(with: URL(string: urlString), placeholderImage: img)
    }

   }
   
    func stopImageLoader() {
        imageViewLoader?.removeFromSuperview()
        if let imageView = self.viewWithTag(-15) {
            imageView.removeFromSuperview()
        }
    }
   
   func showImageLoader() {
      let size = self.frame.size
       imageViewLoader = NVActivityIndicatorView(frame: CGRect(x:(size.width/2)-20 , y: (size.height/2)-20, width: 40, height: 40), type: .ballSpinFadeLoader, color: .white, padding: 6)
        imageViewLoader?.tag = -15
       imageViewLoader?.startAnimating()
       if let imageLoader = imageViewLoader {
           self.addSubview(imageLoader)
       }
   }
    
    
}

extension UIWindow {
    var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
    
    func showLoader() {
        if let frame =  key?.rootViewController?.view.frame {
            loaderView = LoaderView(frame:frame)
            loaderView.tag = -100
            loaderView.isHidden = false
            key?.addSubview(loaderView)
            loaderView.addLoader()
        }
    }
    func stopLoader() {
        loaderView.removeFromSuperview()
        if let view = key?.viewWithTag(-100) {
            view.removeFromSuperview()
        }
    }
    
    func showAlert(message: String) {
        key?.rootViewController?.view.makeToast(message, duration: 3.0, position: .bottom)
    }
    
    //MARK:- EmptyMessage
    func showEmptyMsg(_ message: String) {
        self.removeEmptyMsg(superView: key?.rootViewController?.view)
        let label = UILabel()
        label.tag = -10
        label.text = message
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)
        label.frame = CGRect(x: CGFloat(10 - 0), y: 0.0, width: key?.rootViewController?.view.frame.width ?? 100 - 20, height: key?.rootViewController?.view.frame.height ?? 0.0)
        label.textAlignment = .center
        label.textColor = AppColor.textColor
        label.numberOfLines = 0
        key?.rootViewController?.view.addSubview(label)
        
    }
    func removeEmptyMsg(superView: UIView?) {
        guard let superView = superView else {
            return
        }
        
        if let emptyMessage = superView.viewWithTag(999){
            emptyMessage.removeFromSuperview()
        }
        //return
        
        for view in superView.subviews {
            if let temp = view as? UILabel, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UILabel, temp.tag == -20 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIImageView, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIButton, temp.tag == -10 {
                view.removeFromSuperview()
            }
        }
    }
}


extension UIStackView {
    
    func showDishTypes(_ string: String) {
        var dishTypes = [DishType]()
        for dishType in string.components(separatedBy: ",")  {
            let dishType  = DishType(rawValue: dishType) ?? .undefined
            dishTypes.append(dishType)
        }
        
        for dishIndicator in self.arrangedSubviews{
            dishIndicator.isHidden = true
        }
        
        for type in dishTypes {
            if type.index < self.arrangedSubviews.count && type.index != -1{
                self.arrangedSubviews[type.index].isHidden = false
            }
        }
    }
}

extension UIViewController {
        
    var window: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }

    func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func alert_image(_ name: String) -> UIImage {
        return UIImage(named: name)!
    }
    
    func addLoaderOnImage(_ imageView: UIImageView) {
        imageView.image = nil
        imageView.showImageLoader()
    }

    func removeLoaderOnImage(_ imageView: UIImageView,_ image: String) {
        imageView.stopImageLoader()
        imageView.image = UIImage(named: image)
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func hideShowTabBar(_ bool: Bool) {
        DispatchQueue.main.async {
            if let tabBar = self.tabBarController?.tabBar {
                tabBar.isHidden = bool
            }
        }
    }
    
    func isDeviceIsIphoneX() -> Bool {
        if self.view.frame.width == 414 && self.view.frame.height == 896 || self.view.frame.width == 375 && self.view.frame.height == 812  {

            print("iPhone X")
            return true
        } else {

            print("not iPhone X")
            return false

        }

    }
}

extension NSObject {
    func addFilters(_ filter: String) -> String {
        return "&dietary=\("")&cuisine=\("")&sort=\(filter)"
    }
    
    
    func saveUserData(data: Data?) {
        let responseJSON = try? JSONSerialization.jsonObject(with: data!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let dic = response["data"] as? [String: Any]{
                UserModel.shared.savePersonalInfoData(user_name: dic["name"] as? String ?? "", profile_pic: dic["profile_pic"] as? String ?? "", phone: dic["phone"] as? String ?? "")
            }
        }
    }
    
    //MARK:- Like/Unlike Service
    func likeUnlikeService(_ dish_id: Int, completion: @escaping(Bool)-> Void) {
        let parameters = [Constant.shared.user_id : UserModel.shared.user_id , Constant.shared.dish_id : dish_id] as [String : Any]
        
        ServiceManager.shared.postRequest(url: ApiName.favouriteDish, parameters: parameters) { (data,success) in
            
        }
    }
    
    
}

extension UITextField {
    func setPlaceholderColor(_ placeholder: String) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
         get {
             return self.placeHolderColor
         }
         set {
             self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
         }
     }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = newValue
        }
    }
}

extension UIView {
    
    func rightValidAccessoryView() -> UIView {
        let imgView = UIImageView(image: UIImage(named: "check_valid"))
        imgView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        imgView.backgroundColor = UIColor.clear
        return imgView
    }
    
    func rightInValidAccessoryView() -> UIView {
        let imgView = UIImageView(image: UIImage(named: "check_invalid"))
        imgView.frame = CGRect(x: self.cornerRadius, y: self.cornerRadius, width: 20, height: 20)
        imgView.backgroundColor = UIColor.clear
        return imgView
    }
}

extension UIView {

    private struct AssociatedKeys {
        static var descriptiveName = "AssociatedKeys.DescriptiveName.blurView"
    }
    
    private (set) var blurView: BlurView {
        get {
            if let blurView = objc_getAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName
                ) as? BlurView {
                return blurView
            }
            self.blurView = BlurView(to: self)
            return self.blurView
        }
        
        set(blurView) {
            objc_setAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName,
                blurView,
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
        
    class BlurView {

        private var superview: UIView
        private var blur: UIVisualEffectView?
        private var editing: Bool = false
        private (set) var blurContentView: UIView?
        private (set) var vibrancyContentView: UIView?
        var animationDuration: TimeInterval = 0.1
        
        var style: UIBlurEffect.Style = .light {
            didSet {
                guard oldValue != style,
                    !editing else { return }
                applyBlurEffect()
            }
        }
        
        var alpha: CGFloat = 0 {
            didSet {
                guard !editing else { return }
                if blur == nil {
                    applyBlurEffect()
                }
                let alpha = self.alpha
                UIView.animate(withDuration: animationDuration) {
                    self.blur?.alpha = alpha
                }
            }
        }
        
        init(to view: UIView) {
            self.superview = view
        }
        
        func setup(style: UIBlurEffect.Style, alpha: CGFloat) -> Self {
            self.editing = true
            self.style = style
            self.alpha = alpha
            self.editing = false
            return self
        }

        func enable(isHidden: Bool = false) {
            if blur == nil {
                applyBlurEffect()
            }
            self.blur?.isHidden = isHidden
        }
                
        private func applyBlurEffect() {
            blur?.removeFromSuperview()
            applyBlurEffect(
                style: style,
                blurAlpha: alpha
            )
        }

        private func applyBlurEffect(style: UIBlurEffect.Style,blurAlpha: CGFloat) {
            superview.backgroundColor = UIColor.clear
            let blurEffect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
            let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
            blurEffectView.contentView.addSubview(vibrancyView)
            blurEffectView.alpha = blurAlpha
            superview.insertSubview(blurEffectView, at: 0)
            blurEffectView.addAlignedConstrains()
            vibrancyView.addAlignedConstrains()
            self.blur = blurEffectView
            self.blurContentView = blurEffectView.contentView
            self.vibrancyContentView = vibrancyView.contentView
        }
    }

    private func addAlignedConstrains() {
        translatesAutoresizingMaskIntoConstraints = false
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.top)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.leading)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.trailing)
        addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute.bottom)
    }
    
    private func addAlignConstraintToSuperview(attribute: NSLayoutConstraint.Attribute) {
            superview?.addConstraint(
            NSLayoutConstraint(
                item: self,
                attribute: attribute,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: superview,
                attribute: attribute,
                multiplier: 1,
                constant: 0
            )
        )
    }
}

extension UIView {
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}


extension UIColor {
    
    static func rgbColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
    }
    
    static func colorFromHex(_ hex: String) -> UIColor {
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if hexString.hasPrefix("#") {
            hexString.remove(at: hexString.startIndex)
        }
        
        if hexString.count != 6 {
            return UIColor.magenta
        }
        
        var rgb: UInt32 = 0
        Scanner.init(string: hexString).scanHexInt32(&rgb)
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16)/255,
                            green: CGFloat((rgb & 0x00FF00) >> 8)/255,
                            blue: CGFloat(rgb & 0x0000FF)/255,
                            alpha: 1.0)
    }
}

fileprivate extension UIView {
    
    var csSafeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return self.safeAreaInsets
        } else {
            return .zero
        }
    }
    
}


extension String {
    var length: Int {
        return self.count
    }
    
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let numberRegEx  = "^(?=.*\\d)(?=.*[a-zA-Z]).{8,20}$"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: self)
        return numberresult
    }
    
    func isValidPhone() -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    func attributedString(fullText: String , size : CGFloat) -> NSMutableAttributedString {
        let lightString = fullText
        let lightAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Light", size: size)!, NSAttributedString.Key.foregroundColor: AppColor.darkTextColor]
        let newAttStr = NSAttributedString(string: lightString, attributes: lightAttribute)
        
        let titleStr = self
        let boldAttribute = [ NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: size)!, NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleAttrString = NSAttributedString(string: titleStr, attributes: boldAttribute)
        let finalStr = NSMutableAttributedString(attributedString: titleAttrString)
        finalStr.append(newAttStr)
        return finalStr
    }
    
    
    func attributedStringColored(fullText: String , size : CGFloat) -> NSMutableAttributedString {
        let lightString = fullText
        let lightAttribute = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: size), NSAttributedString.Key.foregroundColor: AppColor.primaryColor]
        let newAttStr = NSAttributedString(string: lightString, attributes: lightAttribute)
        
        let titleStr = self
        let boldAttribute = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: size), NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleAttrString = NSAttributedString(string: titleStr, attributes: boldAttribute)
        let finalStr = NSMutableAttributedString(attributedString: titleAttrString)
        finalStr.append(newAttStr)
        return finalStr
    }
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale) {
            ranges.append(range)
        }
        return ranges
    }
    
    func convertDateFormat(_ from: String, _ to: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = from
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = to
        return  dateFormatter.string(from: date)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    var camelCasedString: String {
        return self.components(separatedBy: " ")
            .map { return $0.lowercased().firstUppercased }
            .joined(separator: " ")
    }
    
    func getTimeWithDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd 'at' hh:mm a"
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
 
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    func capitalizeFirstInEachWord() -> String {
        
        let words = self.components(separatedBy: " ")
        var processedWords = [String]()
        
        for word in words {
            
            if checkTextSufficientComplexity(text: word) {
                processedWords.append(word)
            } else {
                processedWords.append(word.firstUppercased)

            }
        }
        
        return processedWords.joined(separator: " ")
    }
    
    func checkTextSufficientComplexity( text : String) -> Bool{
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        return capitalresult || numberresult || specialresult
    }
    
    func UTCToLocal() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self) ?? Date()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt)
    }
    
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}


extension Date {
    
    func getDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: self)
        return date
    }
    
    func getTime() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.string(from: self)
        return date
    }

    
}

extension UILabel {
    var smartDecimalText : String {
        set{
            self.handleDecimalText(_ : newValue)
        }
        get{
            return self.text ?? ""
        }
    }
    
    private func handleDecimalText(_ text : String){
        var textCandidate = text
        textCandidate = textCandidate.replacingOccurrences(of: ".00", with: "")
        self.text = textCandidate
    }
}
