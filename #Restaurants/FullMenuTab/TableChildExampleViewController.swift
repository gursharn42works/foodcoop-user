//  TableChildExampleViewController.swift
//  XLPagerTabStrip ( https://github.com/xmartlabs/XLPagerTabStrip )
//
//  Copyright (c) 2017 Xmartlabs ( http://xmartlabs.com )
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation
import XLPagerTabStrip

protocol TableChildExampleViewControllerDelegate: class {
    func openReapeatItemView(dishs: [MenuDishes], index: Int, itemCount: Int, childController: UITableView)
    func updateViewDataOnMinusBtnClick()
    func updateBottomView()
}

class TableChildExampleViewController: UITableViewController, IndicatorInfoProvider {
    
    var pageIndex = 0
    var dishes: [MenuDishes] = []
    var restaurantName = String()
    var dietary = [Dietary]()
    var blackTheme = false
    var itemInfo = IndicatorInfo(title: "View")
    var isFromFilterScreen = false
    var childFrame : CGRect?
    var dietaryArr = [String]()
    var selectedDietaryArray : ((String,Bool)-> Void)?
    var hallFilters: MenuFilters?
    var isFilterApplied : Bool?
    var itemCount = 1
    var selectedIndex: Int?
    var delegate: TableChildExampleViewControllerDelegate?
    var readmoreindex = NSMutableArray()
    
    init(style: UITableView.Style, itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true
        
        tableView.register(UINib(nibName: Constant.shared.hallDishCell, bundle: Bundle.main), forCellReuseIdentifier: Constant.shared.hallDishCell)
        tableView.register(UINib(nibName: Constant.shared.filterCell, bundle: nil), forCellReuseIdentifier: Constant.shared.filterCell)
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = true
        tableView.backgroundColor = UIColor.clear
        tableView.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
    }
    
    
    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromFilterScreen{
            if dietary.count == 0 {
                Utility.shared.showEmptyMsgView(message: Messages.noDietary)
            }
            return dietary.count
        }
        return dishes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromFilterScreen{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.filterCell, for: indexPath) as? FilterCell
            cell?.nameLabel.text = dietary[indexPath.row].name
            cell?.leadingConstraint.constant = 30
            cell?.constraintButtonTrailing.constant = 30
            cell?.radioButton.isSelected = false
            if dietaryArr.contains("\(dietary[indexPath.row].id ?? 0)") {
                cell?.radioButton.isSelected = true
            }
            cell?.radioBtnTap = { index in
                self.select_UnselectRadioBtn(tableView, indexPath: indexPath)
            }
            return cell ?? FilterCell()
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.hallDishCell, for: indexPath) as? HallDishCell
            
            self.showCellData(dishes[indexPath.row], cell, indexPath: indexPath)
            cell?.favBtn.tag = indexPath.row
            cell?.addBtn_Out.tag = indexPath.row
            cell?.minusBtn_Out.tag = indexPath.row
            cell?.plusBtn_Out.tag = indexPath.row
            cell?.readMoreBtn_Out.tag = indexPath.row
            
            return cell ?? HallDishCell()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isFromFilterScreen {
            if dishes[indexPath.row].status == 0 {
                Utility.shared.showAlertView(message: Messages.dishNotAvailable)
            } else {
                if dishes[indexPath.row].customizable {
                    let dish = self.dishes[indexPath.row]
                    let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
                    let cartData = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
                    if cartData.contains(where: { ($0["dish_id"] as! Int) == dish.id })  {
                        self.delegate?.openReapeatItemView(dishs: self.dishes, index: indexPath.row, itemCount: self.itemCount, childController: self.tableView)
                    }else {
                        
                        if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
                            dishDetailVC.hidesBottomBarWhenPushed = true
                            dishDetailVC.dish_id = dishes[indexPath.row].id
                            self.navigationController?.pushViewController(dishDetailVC, animated: true)
                        }
                    }
                    
                   
                }
                
            }
            
        }else{
            self.select_UnselectRadioBtn(tableView, indexPath: indexPath)
        }
    }
    
    private func showCellData(_ dish: MenuDishes?, _ cell: HallDishCell?,indexPath: IndexPath) {
        
        let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        
        if savedLocalCart.count > 0 {
            
            let hallId = savedLocalCart["hall_id"] as? Int ?? 0
            let cartData = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
            if hallId == dish!.hall_id {
                if cartData.count > 0 {
                    if cartData.contains(where: { ($0["dish_id"] as! Int) == dish?.id }) {
                        let cartArr = cartData.filter{ ($0["dish_id"] as! Int) == dish?.id }
                        cell?.addBtn_Out.isHidden = true
                        let qty = cartArr[0]["qty"] as! Int
                        cell?.itemCountLbl.text = "\(qty)"
                        print(true)
                    }else {
                        cell?.addBtn_Out.isHidden = false
                        print(false)
                    }
                }
            }
            
        }
        
        cell?.containerView.alpha = dish?.status == 0 ? 0.5 : 1
        cell?.favBtn.addTarget(self, action: #selector(self.favBtn_Click(_:)), for: .touchUpInside)
        cell?.plusBtn_Out.addTarget(self, action: #selector(self.plusBtnClick(_:)), for: .touchUpInside)
        cell?.minusBtn_Out.addTarget(self, action: #selector(self.minusBtn_Click(_:)), for: .touchUpInside)
        cell?.addBtn_Out.addTarget(self, action: #selector(self.addBtnClick(_:)), for: .touchUpInside)
        cell?.readMoreBtn_Out.addTarget(self, action: #selector(self.readMoreClick(_:)), for: .touchUpInside)
        
        cell?.favBtn.isSelected = dish?.favorited ?? false
        cell?.productImage.set_image(dish?.image ?? "", placeholder: #imageLiteral(resourceName: "dishPlaceholder_300_200"))
        cell?.dishName.text = dish?.name.firstUppercased ?? ""
        cell?.price.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish?.price ?? "")"
        
        //cell?.calories.numberOfLines = dish?.customizable == false ?  3 : 2
        cell?.calories.text = dish?.descript_ion
        if readmoreindex.contains(dish?.id ?? -1) {
            cell?.calories.numberOfLines = 0
            cell?.readMoreBtn_Out.isHidden = true
        } else {
            cell?.calories.numberOfLines = 2
            cell?.readMoreBtn_Out.isHidden = false
            
        }
        
        if (cell?.calories.retrieveTextHeight())! < 40.0 {
            cell?.readMoreBtn_Out.isHidden = true
        }
        //        if (cell?.calories.retrieveTextHeight())! > 40.0 {
        //            cell?.readMoreBtn_Out.isHidden = false
        //        }else {
        //            cell?.readMoreBtn_Out.isHidden = true
        //        }
        
        cell?.favDish.image = dish?.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
        cell?.bestSeller_Out.isHidden = dish?.best_seller == 0 ? true : false
        cell?.bestsellerWidthConst.constant = cell?.bestSeller_Out.isHidden == true ? 0 : 50
        cell?.recommended_Out.isHidden = dish?.recommended == 0 ? true : false
        cell?.ratingLbl_Out.isHidden = dish?.rating == 0.0 ? true : false
        cell?.starImg_Out.isHidden = dish?.rating == 0.0 ? true : false
        cell?.starWidthConst.constant = dish?.rating == 0.0 ? 0 : 15
        cell?.ratingLbl_Out.text = String(dish?.rating ?? 0.0)
        // }
        // cell?.bestSellerImg.isHidden = dish.isBestSeller
        
        cell?.allergies.isHidden = dish?.customizable == true ? false : true
        cell?.allergies.text =  dish?.customizable == true ? "customisable" : "" //GSD
        
        if let subviews = cell?.dishTypeStackView.arrangedSubviews {
            for dishIndicator in subviews {
                dishIndicator.isHidden = true
            }
        }
        
        if let veg = dish?.veg ,let isOn = dish?.showtypeonapp, isOn == 1 {
            cell?.dishTypeStackView.showDishTypes(veg)
        }
    }
    
    @objc func favBtn_Click(_ sender : UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
            if UserModel.shared.user_id != 0 {
                let dish = self.dishes[sender.tag]
                
                sender.isSelected = !sender.isSelected
                cell.favDish.image = dish.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
                
                //MARK:- Remove unlike dish from array
                //  self.dishes.remove(at: indexPath.row)
                
                //                if Utility.shared.favDishArr.contains(dish.id) {
                //                    Utility.shared.favDishArr.remove(dish.id)
                //
                //                } else {
                //                    Utility.shared.favDishArr.add(dish.id)
                //                }
                self.dishes[sender.tag].favorited = !self.dishes[sender.tag].favorited
                
                self.tableView.reloadData()
                self.likeUnlikeService(dish.id) { (succes) in
                }
            }
        }
    }
    
    @objc func readMoreClick(_ sender : UIButton) {
        
        let dishid = self.dishes[sender.tag].id
        self.readmoreindex.add(dishid)
        self.tableView.reloadData()
        
    }
    
    @objc func addBtnClick(_ sender : UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let dish = self.dishes[sender.tag]
        
        let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        if savedLocalCart.count > 0 {
            let hallId = savedLocalCart["hall_id"] as? Int ?? 0
            if hallId != dish.hall_id {
                self.showAlert(dishIndex: sender.tag)
                return
            }
        }
        
        if dishes[sender.tag].customizable {
            if let dishDetailVC = Router.viewController(with: .dishDetailVC, .product) as? DishDetailVC {
                dishDetailVC.hidesBottomBarWhenPushed = true
                dishDetailVC.dish_id = dishes[indexPath.row].id
                self.navigationController?.pushViewController(dishDetailVC, animated: true)
            }
        }else {
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
                
                let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
                if savedLocalCart.count > 0 {
                    let hallId = savedLocalCart["hall_id"] as? Int ?? 0
                    if hallId != dish.hall_id {
                        return
                    }
                }
                cell.addBtn_Out.isHidden = true
                cell.itemCountLbl.text = "1"
                self.localAddToCart(dish,itemCount,"add")
                
                //   self.tableView.reloadData()
                
            }
        }
        
        
        
        
    }
    
    @objc func plusBtnClick(_ sender : UIButton) {
        if dishes[sender.tag].customizable {
            self.delegate?.openReapeatItemView(dishs: self.dishes, index: sender.tag, itemCount: self.itemCount, childController: self.tableView)
        }else {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
                
                let dish = self.dishes[sender.tag]
                var count = Int(cell.itemCountLbl.text!)
                count = count! + 1
                
                if (itemCount < 101) {
                    cell.itemCountLbl.text = "\(count!)";
                    self.localAddToCart(dish,count,"add")
                } else {
                    
                }
            }
            
        }
        
    }
    
    @objc func minusBtn_Click(_ sender : UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
            
            let dish = self.dishes[sender.tag]
            
            var count = Int(cell.itemCountLbl.text!)
            
            count = count! - 1
            
            if count == 0 {
                cell.addBtn_Out.isHidden = false
                
                self.localAddToCart(dish,count,"sub")
                self.delegate?.updateViewDataOnMinusBtnClick()
                
                return
            }
            if itemCount < 100 {
                cell.itemCountLbl.text = "\(count!)";
                // if dishes[sender.tag].customizable {
                self.delegate?.updateViewDataOnMinusBtnClick()
                // }else {
                self.localAddToCart(dish,count,"sub")
                //  }
            }
            
        }
        
        
    }
    
    //    @objc func favBtn_Click(_ sender : UIButton){
    //
    //        if let dish = category?.dishes[sender.tag] {
    //
    //            if Utility.shared.favDishArr.contains(dish.id) {
    //                Utility.shared.favDishArr.remove(dish.id)
    //
    //            } else {
    //                Utility.shared.favDishArr.add(dish.id)
    //            }
    //        }
    //
    //        self.tableView.reloadData()
    //
    //    }
    
    //MARK:- Add to Local cart
    func localAddToCart(_ dish : MenuDishes, _ count : Int?, _ type : String) {
        
        DispatchQueue.main.async {
            
            
            var localCart = [String : Any]()
            var localCartItem = [[String : Any]]()
            // var commonMealIds = [String]()
            
            if let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]{
                
                localCart = savedLocalCart
                localCartItem = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
            }
            
            var cartItem = [String : Any]()
            
            cartItem["dish_id"] = dish.id
            cartItem["item_name"] = dish.name
            cartItem["price"] = Double(dish.price) //GSD price
            cartItem["qty"] = count ?? 0
            // cartItem["status"] = dish.isDishAvailable
            cartItem["showtypeonapp"] = dish.showtypeonapp
            cartItem["veg"] = dish.veg
            cartItem["Veriations"] = ["option_id":0]
            
            // cartItem["veg"] = dish.type == .veg ? "Veg" : "Non-veg"
            if localCartItem.count != 0 {
                
                if type == "add" {
                    
                    var Fresult = Bool()
                    _ = localCartItem.filter { dict in
                        if (cartItem["dish_id"] as? Int) == dict["dish_id"] as? Int {
                            if let index = localCartItem.firstIndex(where: {$0["dish_id"] as! Int == (cartItem["dish_id"] as? Int)!}) {
                                var item = localCartItem[index]
                                let newQty = (count ?? 0)
                                item["qty"] = newQty
                                localCartItem[index] = item
                            }
                            Fresult = true
                            return true
                        }
                        Fresult = false
                        return false
                    }
                    
                    if Fresult == false {
                        localCartItem.append(cartItem)
                    }
                    
                    
                    
                }else {
                    
                    _ = localCartItem.filter { dict in
                        if (cartItem["dish_id"] as? Int) == dict["dish_id"] as? Int {
                            if let index = localCartItem.firstIndex(where: {$0["dish_id"] as! Int == (cartItem["dish_id"] as? Int)!}) {
                                var item = localCartItem[index]
                                let newQty = (count ?? 0)
                                if count == 0 {
                                    localCartItem.remove(at:index)
                                }else {
                                    item["qty"] = newQty
                                    localCartItem[index] = item
                                }
                            }
                            
                            return true
                        }
                        
                        return false
                    }
                    
                }
                
            }else {
                localCartItem.append(cartItem)
            }
            
            
            localCart["items"] = localCartItem
            var localCartCount = 0
            var localcartTotal = 0.0
            for items in localCartItem{
                localCartCount += (items["qty"] as? Int) ?? 0
                localcartTotal += (((items["price"] as? Double) ?? 0.0) * (Double((items["qty"] as? Int) ?? 0)))
            }
            print(localcartTotal)
            localCart["cart_total"] = localcartTotal
            localCart["cart_count"] = localCartCount
            localCart["hall_id"] = dish.hall_id
            localCart["hall_name"] = self.restaurantName
            localCart["isCouponApplied"] = false
            //  localCart["is_accepting_orders"] = dish.isAcceptingOrder == true ? "yes" : "no"
            
            print(localCart)
            UserModel.shared.cartItemsCount = localCartCount
            UserModel.shared.cartItemsPrice = localCart["cart_total"] as? Double ?? 0.0
            UserDefaults.standard.set(localCart, forKey: UserDefault.localCart)
            UserDefaults.standard.synchronize()
            self.delegate?.updateBottomView()
            
            if localCartItem.count == 0 {
                UserModel.shared.cartItemsCount = 0
                UserModel.shared.cartItemsPrice = 0
                UserDefaults.standard.removeObject(forKey: "localCart")
                UserDefaults.standard.synchronize()
            }
            //self.delegate?.updateCartData(isCartUpdate: true)
            
        }
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func select_UnselectRadioBtn(_ tableView : UITableView , indexPath : IndexPath){
        if let cell = tableView.cellForRow(at: indexPath) as? FilterCell{
            cell.radioButton.isSelected = !cell.radioButton.isSelected
            if cell.radioButton.isSelected
            {
                if !dietaryArr.contains("\(dietary[indexPath.row].id ?? 0)") {
                    dietaryArr.append("\(dietary[indexPath.row].id ?? 0)")
                }
                selectedDietaryArray?("\(dietary[indexPath.row].id ?? 0)", true)
            }else{
                if let index = dietaryArr.firstIndex(of:"\(dietary[indexPath.row].id ?? 0)"), index < dietaryArr.count {
                    dietaryArr.remove(at: index)
                }
                selectedDietaryArray?("\(dietary[indexPath.row].id ?? 0)", false)
            }
        }
    }
    
    func showAlert(dishIndex: Int) {
        let dish = self.dishes[dishIndex]

        let refreshAlert = UIAlertController(title: "Item already in cart", message: "Your cart contains items from different restaurant. Would you like to reset your cart.", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            UserModel.shared.cartItemsCount = 0
            UserModel.shared.cartItemsPrice = 0
            UserDefaults.standard.removeObject(forKey: UserDefault.localCart)
            UserDefaults.standard.synchronize()
            self.localAddToCart(dish,self.itemCount,"add")

            self.delegate?.updateBottomView()
            let indexPath = IndexPath(row: dishIndex, section: 0)
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
                cell.addBtn_Out.isHidden = true
                cell.itemCountLbl.text = "1"
            }
            print("Handle Ok logic here")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        refreshAlert.view.tintColor = .black
        present(refreshAlert, animated: true, completion: nil)
    }
    
}

extension UILabel {
    func retrieveTextHeight () -> CGFloat {
        let attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:self.font as Any])
        
        let rect = attributedText.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(rect.size.height)
    }
    
}

