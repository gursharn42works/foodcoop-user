//
//  BaseVC.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

open class BaseVC: UIViewController,CartViewControllerDeleagte {
    
    var cancellationPolicyView = CancellationPolicyView()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.cancellationPolicyView = CancellationPolicyView(frame: self.view.frame)
        self.window?.addSubview(self.cancellationPolicyView)
        self.cancellationPolicyView.isHidden = true
        self.cancellationPolicyView.delegate = self
        
       // self.setStatusBar(color: UIColor.colorFromHex("#f74e61"))
       // self.setStatusBar(color: UIColor.white)
    }
    
    func pushToViewWhenClcikOnNotifcation(order_id: String,isFeedbackView: Bool) {
        
        if isFeedbackView == false {
            let orderStatusVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as! DeliveryOrderDetailVC
            orderStatusVC.order.id = order_id
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(orderStatusVC, animated: true)
        } else {
            let orderStatusVC = Router.viewController(with: .deliveryCompleteVC, .profile) as! DeliveryCompleteVC
            orderStatusVC.order.id = order_id
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(orderStatusVC, animated: true)
        }
    }
    
    func getTabBarHeight() -> CGFloat {
        if let tabBar = self.tabBarController?.tabBar {
            return tabBar.frame.size.height
        }
        return 0.0
    }
    
    func setStatusBar(color : UIColor) {
        if #available(iOS 13, *) {
            let statusBar = UIView(frame: (self.window?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = color//AppColor.primaryColor
            statusBar.tag = -50
            self.window?.rootViewController?.view.addSubview(statusBar)
        } else {
            // ADD THE STATUS BAR AND SET A CUSTOM COLOR
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = color//AppColor.primaryColor
            }
            UIApplication.shared.statusBarStyle = .lightContent
        }
        
    }
    func checkRestaurantClosed() {
        self.checkRstaurantClosed(timing: UserModel.shared.hallTimings) { (success) in
            if success {
                if let restaurantTimeVC = Router.viewController(with: .restaurantTimeVC, .product) as? RestaurantTimeVC {
                    restaurantTimeVC.modalPresentationStyle = .overCurrentContext
                    restaurantTimeVC.hallTiming = UserModel.shared.hallTimings
                    self.window?.rootViewController?.present(restaurantTimeVC, animated: false, completion: nil)
                }
            }
        }
    }
    
    func showCartItems( _ tabBarHeight : CGFloat = 50) {
        let localCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [String : Any]()
        if let cartItems = localCart["items"] as? [[String : Any]]{
            var localCartCount = 0
            var localCartPrice = 0.0

            for items in cartItems{
                localCartCount += (items["qty"] as? Int) ?? 0

                let price = items["price"] as? Double ?? 0.0
                let qty = items["qty"] as? Double ?? 0.0
                localCartPrice += price*qty
            }
                        
            UserModel.shared.cartItemsCount = localCartCount
            UserModel.shared.cartItemsPrice = localCartPrice
        }
    }
    
     func yConstantForRestaurantClosed() -> CGFloat{
        var yConstant : CGFloat = 72 //res
        
        if UserModel.shared.ongoingOrders.count > 0 && UserModel.shared.cartItemsCount > 0{
            yConstant += (120+5)
            return yConstant
        }
        else if UserModel.shared.ongoingOrders.count > 0 {
            yConstant += 65
            return yConstant
        }
        else if UserModel.shared.cartItemsCount > 0{
            yConstant += 50
            return yConstant
        }
        
        return 62
    }
    
    func showEmptyScreenMessage(_ heading: String, _ subtitle : String, _ superView: UIView?, _ image: UIImage = #imageLiteral(resourceName: "cartImage"), _ mainView: UIView?  = nil) {
        
        self.removeEmptyMsg(superView)
        //#imageLiteral(resourceName: "cartImage")
        //let emptyMessageVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "EmptyMessageVC") as! EmptyMessageViewController
        guard let emptyMessageVC = Router.viewController(with: .messagesVC, .main) as? MessagesVC else{return}
        
        emptyMessageVC.view.tag = 999
        superView?.addSubview(emptyMessageVC.view)
        emptyMessageVC.view.translatesAutoresizingMaskIntoConstraints = false
        //emptyMessageVC.browseBtn_Out.addTarget(self, action: #selector(callHome), for: .touchUpInside)
        emptyMessageVC.browseBtn_Out.isHidden = true
        emptyMessageVC.imageView.image = image
        emptyMessageVC.headingLbl.text = heading
        emptyMessageVC.subTitleLbl.text = subtitle
        
        let horizontalConstraint = NSLayoutConstraint(item: emptyMessageVC.view!, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: superView!, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: emptyMessageVC.view!, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: superView!, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.8, constant: 0)
        
        if mainView == nil{
            view.addConstraints([horizontalConstraint, verticalConstraint])
        }else{
            mainView!.addConstraints([horizontalConstraint, verticalConstraint])
        }
        
    }
    
    @objc func callHome() {
        if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
            let rootNC = UINavigationController(rootViewController: magicTabBar)
            rootNC.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
    }
    
    func removeEmptyMsg(_ superView: UIView?) {
        guard let superView = superView else {
            return
        }
        
        if let emptyMessage = superView.viewWithTag(999){
            emptyMessage.removeFromSuperview()
        }
        //return
        
        for view in superView.subviews {
            if let temp = view as? UILabel, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UILabel, temp.tag == -20 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIImageView, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIButton, temp.tag == -10 {
                view.removeFromSuperview()
            }
        }
    }
    
    func messageWithButton(_ message: String, message2 : String, superView: UIView?, image: UIImage = #imageLiteral(resourceName: "NoInternet")) {
        
        self.removeEmptyMsg(superView)
        let emptyImage = UIImageView()
        emptyImage.tag = -10
        emptyImage.image = image
        emptyImage.contentMode = .scaleAspectFit
        emptyImage.clipsToBounds = true
        emptyImage.frame = CGRect(x: ((superView?.frame.size.width) ?? 100)/2 - 75, y: ((superView?.frame.size.height) ?? 100)/2 - 120, width: 150, height: 150)
        superView?.addSubview(emptyImage)
        
        let label = UILabel()
        label.tag = -10
        label.text = message
        label.font = UIFont(name: "Roboto-Regular", size: 16.0)
        label.frame = CGRect(x: 10.0, y: 0.0 + 60, width: (superView?.frame.size.width ?? 100) - 20, height: (superView?.frame.size.height ?? 100))
        label.textAlignment = .center
        label.textColor = AppColor.darkTextColor
        label.numberOfLines = 1
        superView?.addSubview(label)
        
        let label2 = UILabel()
        label2.tag = -10
        label2.text = message2
        label2.font = UIFont(name: "Roboto-Light", size: 14.0)
        label2.frame = CGRect(x: 10.0, y: label.frame.minY + 30, width: (superView?.frame.size.width ?? 100) - 20, height: (superView?.frame.size.height ?? 100))
        label2.textAlignment = .center
        label2.textColor = AppColor.textColor
        label2.numberOfLines = 0
        superView?.addSubview(label2)
        
        let button = UIButton()
        button.tag = -10
        button.setTitle(Messages.tryAgain, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = AppColor.primaryColor
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        button.cornerRadius = 45/2
        let imageY = emptyImage.frame.minY
        let totalY = imageY + label.frame.minY + label2.frame.minY + 100
        let btnY = totalY < (superView?.frame.size.height) ?? 100 - 100 ? totalY : (superView?.frame.size.height) ?? 100 - 100
        button.frame = CGRect(x: (superView?.frame.size.width ?? 100)/2 - 50, y: btnY, width: 100, height: 45)
        button.addTarget(self, action: #selector(reloadAPI(_:)), for: .touchUpInside)
        superView?.addSubview(button)
    }
    
    @IBAction func reloadAPI(_ sender : UIButton){
        
    }
    
    func fullScreenLoaderWithTap(_ viewController: UIViewController, message: String, onCompletion:@escaping (Bool?) -> Void) {
        viewController.view.makeToast(message, duration: 2.0, point: viewController.view.center, title: nil, image: nil) { didTap in
            onCompletion(didTap)
        }
    }
    
    func getOnGoingOrdersList(completion: @escaping()-> Void){
        //MARK:- Call Api
        let url = createCustomUrl()
        ServiceManager.shared.getRequest(url: url, parameters: [:]) { (jsonData,success) in
              UserModel.shared.ongoingOrders.removeAll()
            let orders = self.createOngoingOrderModel(jsonData: jsonData)
              if orders.count > 0 {
                  UserModel.shared.ongoingOrders = orders
              } else {
                  UserModel.shared.ongoingOrders = []
              }
              
            completion()
        }
    }
    
     func createOngoingOrderModel(jsonData: Data?) -> [Order] {
        var ongoingOrder: [Order] = []
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any], let orderArr = data["orders"] as? NSArray {
                for dic in orderArr {
                    let order = Order().getOrder(dic)
                    ongoingOrder.append(order)
                }
            }
        }
        return ongoingOrder
    }
    
    //MARK:- Create Url
    func createCustomUrl() -> String {
        if UserModel.shared.user_id != 0 {
            return "\(ApiName.orderList)?\(Constant.shared.user_id)=\(UserModel.shared.user_id)&\(Constant.shared.type)=\("ongoing")&\(Constant.shared.limit)=\(10)&\(Constant.shared.page)=\(1)"
        }
        return ""
    }
    
    
    func updateProfileBackground(_ stripeId : String = "", cardId : String = "", isBackground : Bool = true){
        if UserModel.shared.user_id == 0 {
            return
        }
        let user = UserModel.shared
        let updateProfile = UpdateProfile()
        updateProfile.name = user.user_name
        updateProfile.phone = user.phone
        updateProfile.studentID = user.student_id
        updateProfile.cardId = cardId
        updateProfile.stripeId = stripeId
        
        let serverDic = updateProfile.serverData()
        updateUserProfile(serverDic) { (success) in
            
        }
    }
    
    func updateUserProfile(_ dic : [String: Any], completion: @escaping (Bool) -> Void) {
        
        let url = ApiName.updateUserProfile + "\(UserModel.shared.user_id)"
        ServiceManager.shared.postRequest(url: url, parameters: dic) { (jsonData,success) in
            if let data = jsonData {
                do {
                let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                    if let dic = jsonDic {
                        
                        let model = PersonalInfoModel.setData(dic: dic["data"] as? [String: Any] ?? [:])
                        self.updateUserData(data: model)
                    }

                } catch {
                    
                }
            }
            
            completion(true)
        }
    }
    
    func updateUserData(data: PersonalInfoModel) {
        UserModel.shared.saveUserData(user_id: data.id, apple_id: "", currency_code: "", user_name: data.name, country: data.country, is_accepting_orders: data.isAcceptingOrder, profile_pic: data.profilePic, phone: data.phone, email: data.email,stripe_id:data.stripeID, card_id: data.defaultCardID)
    }
    
    
    func openCallView(number: String) {
        guard let url = URL(string: "tel://\(number)") else {
            Utility.shared.showAlertView(message: "Invalid Phone number.")
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func showStatusAccordingPaymentType(order: Order,statusLabel: UILabel) {
        
        if order.paymentMethod == .brainTree {
            if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 0{
                statusLabel.text = Messages.orderCancelByHallWithoutRequest
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 1{
                statusLabel.text = Messages.orderCancelledWithpayPalBraintree
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 2{
                statusLabel.text = Messages.orderCancelledDueToNoPickup
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 3 {
                statusLabel.text = Messages.orderCancelledWithpayPalBraintreeWithStatusCancelStatus3
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 4 {
                statusLabel.text = "Your order has been cancelled. Please contact restaurant."
            }
            else if order.cancelRequest == 1 && order.status != "previous" {
                statusLabel.text = Messages.cancelRequestSent
                statusLabel.textColor = UIColor.red
            }else{
                statusLabel.text = ""
            }

            
        }
       else if order.paymentMethod == .payByCard {
            if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 0{
                statusLabel.text = Messages.orderCancelByHallWithoutRequest
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 1{
                statusLabel.text = Messages.orderCancelledWithpayPayByCard
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 2{
                statusLabel.text = Messages.orderCancelledDueToNoPickup
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 3 {
                statusLabel.text = Messages.orderCancelledWithpayPayByCardWithCancelStatus3
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 4 {
                statusLabel.text = "Your order has been cancelled. Please contact restaurant."
            }
            else if order.cancelRequest == 1 && order.status != "previous" {
                statusLabel.text = Messages.cancelRequestSent
                statusLabel.textColor = UIColor.red
            }else{
                statusLabel.text = ""
            }

        }
        else if order.paymentMethod == .payAtStore {
            if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 0{
                statusLabel.text = Messages.orderCancelByHallWithoutRequestForUnpaid
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 1{
                statusLabel.text = Messages.cancelCancelledOnUserRequest
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 2{
                statusLabel.text = Messages.orderCancelledDueToNoPickup
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 4 {
                statusLabel.text = "Your order has been cancelled. Please contact restaurant."
            }
            else if order.cancelRequest == 1 && order.status != "previous" {
                statusLabel.text = Messages.cancelRequestSent
                statusLabel.textColor = UIColor.red
            }else{
                statusLabel.text = ""
            }
        }
        else if order.paymentMethod == .Razorypay {
            if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 0{
                statusLabel.text = Messages.orderCancelByHallWithoutRequest
            }else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 1{
                statusLabel.text = Messages.orderCancelledWithpayPalBraintreeWithStatusCancelStatus3
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 2 {
                statusLabel.text = Messages.orderCancelledDueToNoPickup
            }
            else if order.status == OrderStatusEnum.cancelled.rawValue && order.cancelRequest == 4 {
                statusLabel.text = "Your order has been cancelled. Please contact restaurant."
            }
        }
    }
    
    func sizeHeaderToFit(tableView: UITableView) {
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            tableView.tableHeaderView = headerView
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
        }
    }
    
}

extension BaseVC: BottomOrderViewDelegate,CancellationPolicyViewDelegate, CancelConfirmVCDelegate {
    
    func callCancelliationApi(order_id: String) {
        self.cancellationPolicyView.isHidden = true
        
        let cancelConfirmVC = Router.viewController(with: .cancelConfirmVC, .profile) as! CancelConfirmVC
        cancelConfirmVC.order_id = order_id
        cancelConfirmVC.delegeate = self
        cancelConfirmVC.modalPresentationStyle = .overFullScreen
        
        self.window?.rootViewController?.present(cancelConfirmVC, animated: false, completion: nil)
        
    }
    
    func openCancelliationPolicyView(order_id: String) {
//        self.cancellationPolicyView.delegate = self
        self.cancellationPolicyView.order_id = order_id
        self.callCancelationOrderPolicyApi { (model) in
            self.cancellationPolicyView.isHidden = false
            self.cancellationPolicyView.showHtmlDataOnWebView(string: model.cancellationPolicy)
            self.cancellationPolicyView.isHidden = false
        }
    }
    
    func callCancelationOrderPolicyApi(completion: @escaping(CancelPolicyModel) -> Void) {
        
        ServiceManager.shared.callGetRequest(url: ApiName.cancellationPolicy, parameters: [:], completion: { (jsonData) in
            completion(self.setCancelPolicyModelData(jsonData: jsonData))
        })
    }
    
    private func setCancelPolicyModelData(jsonData: Data?) -> CancelPolicyModel {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? NSDictionary {
            if let data = response["data"] as? NSDictionary{
                if response["success"] as? Int ?? 0 != 0 {
                   return CancelPolicyModel().setCancelPolicyModelData(dic: data)
                }
                
            }
        }
        return CancelPolicyModel()
    }
    
    func hideOrderCancelView(order_id: String, reason: String,reasonType: String) {
        self.cancellationPolicyView.isHidden = true
        self.callCancelOrderApi(order_id: order_id, reason: reason,reason_type: reasonType) { (dic) in
            
        }
    }
    
    func callCancelOrderApi(order_id: String, reason: String, reason_type: String,completion: @escaping(NSDictionary) -> Void) {
        
        let parameters = ["order_id": order_id, "status": "cancelled", "user_id": UserModel.shared.user_id, "reason": reason, "reason_type": reason_type] as [String : Any]
        print(parameters)
        ServiceManager.shared.callPostRequest(url: ApiName.cancelOrder, parameters: parameters, completion: { (JsonData) in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: nil)
            
        })
    }
}

extension BaseVC {
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    @IBAction func login_Click() {
        UserModel.shared.login_Skipped = true
        if let loginVC = Router.viewController(with: .loginVC, .login) as? LoginVC {
            loginVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    @IBAction func register_Click() {
        UserModel.shared.login_Skipped = true
        if let signUpVC = Router.viewController(with: .register, .login) as? SignUpVC {
            signUpVC.isFromSkip = true
            signUpVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    
    
    func getNotificationCount(completion:@escaping (Bool) -> Void){
        
        ServiceManager.shared.getRequest(url: ApiName.getNotificationCount + "?\(Constant.shared.user_id)=\(UserModel.shared.user_id)", parameters: [:]) { (jsonData, success) in
            
        }
        //        WebServices().getRequest(methodName: ServiceName.getNotificationCount + "?user_id=\(getUser().id ?? 0)", params: nil, oncompletion: { (status, message, response) in
        //            if status ?? false {
        //                if let data = response?.value(forKey: "data") as? NSDictionary{
        //                    AppDel.notificationCount = data.value(forKey: "notification_count") as? Int ?? 0
        //                }
        //                oncompletion(true, message ?? "", nil)
        //            }else{
        //                oncompletion(false, message ?? "", nil)
        //            }
        //        })
    }
    
    func notificationBadgeUpdate(_ tabbar : UITabBarController) {
        if let magicTabBar = tabbar as? MagicTabBar{
            if let tabBarItems = magicTabBar.tabBar.items , tabBarItems.count > 3 {
                if UserModel.shared.notificationCount == 0
                {
                    tabBarItems[2].badgeValue = nil
                }else{
                    print(UserModel.shared.notificationCount)
                    tabBarItems[2].badgeValue = "\(UserModel.shared.notificationCount)"
                    tabBarItems[2].badgeColor = AppColor.primaryColor
                }
            }
        }
    }
    
}

extension UITableViewController {
    
    
    func showEmptyScreenMessage(_ heading: String, _ subtitle : String, _ superView: UIView?, _ image: UIImage = #imageLiteral(resourceName: "cartImage"), _ mainView: UIView?  = nil) {
        
        self.removeEmptyMsg(superView)
        
        guard let emptyMessageVC = Router.viewController(with: .messagesVC, .main) as? MessagesVC else{return}
        
        emptyMessageVC.view.tag = 999
        superView?.addSubview(emptyMessageVC.view)
        emptyMessageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        emptyMessageVC.imageView.image = image
        emptyMessageVC.headingLbl.text = heading
        emptyMessageVC.subTitleLbl.text = subtitle
        
        let horizontalConstraint = NSLayoutConstraint(item: emptyMessageVC.view!, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: superView!, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: emptyMessageVC.view!, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: superView!, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        
        if mainView == nil{
            view.addConstraints([horizontalConstraint, verticalConstraint])
        }else{
            mainView!.addConstraints([horizontalConstraint, verticalConstraint])
        }
        
    }
    
    func removeEmptyMsg(_ superView: UIView?) {
        guard let superView = superView else {
            return
        }
        
        if let emptyMessage = superView.viewWithTag(999){
            emptyMessage.removeFromSuperview()
        }
        //return
        
        for view in superView.subviews {
            if let temp = view as? UILabel, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UILabel, temp.tag == -20 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIImageView, temp.tag == -10 {
                view.removeFromSuperview()
            }
            if let temp = view as? UIButton, temp.tag == -10 {
                view.removeFromSuperview()
            }
        }
    }
    
    
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
