//
//  Constant.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation

class Constant {
    static let shared = Constant()
    
    //MARK:- Parameter key
    let email = "email"
    let name = "name"
    let password = "password"
    let device_id = "device_id"
    let device_type = "device_type"
    let device_token = "device_token"
    let role = "ROLE_RESTAURANT"
    let data = "data"
    let iOS = "Ios"
    let user_id = "user_id"
    let dish_id = "dish_id"
    let hall_id = "hall_id"
    let limit = "limit"
    let page = "page"
    let type = "type"
    let order_id = "order_id"
    let subject = "subject"
    let message = "message"
    let timezone = "timezone_name"
    let restId  = "rest_id"
    let tagId = "tag_id"
    let sort = "sort"
    
    //MARK:- Cell identifiers
    let homeBannerCell = "HomeBannerCell"
    let homeItemsCell = "HomeItemsCell"
    let homeItemCollCell = "HomeItemCollCell"
    let seeAllHomeCell = "SeeAllHomeCell"
    let restaurantListCell = "RestaurantListCell"
    let hallDishCell = "HallDishCell"
    let filterCell = "FilterCell"
    let notificationCell = "NotificationCell"
    let orderHistoryCell = "OrderHistoryCell"
    let ongoingOrdersCell = "OngoingOrdersCell"
    let restaurantTimeCell = "RestaurantTimeCell"
    let productCatagoryCell = "ProductCatagoryCell"
    let cartTotalCell = "CartTotalCell"
    let orderDetailCell = "OrderDetailCell"
    let orderFooterCell = "OrderFooterCell"
    let cartCell = "CartCell"
    let applyCouponCell = "applyCouponCell"
    let specialInstructionCell = "SpecialInstructionCell"
    let pickUpTimeCell = "PickUpTimeCell"
    let applyCouponHeaderCell = "ApplyCouponHeaderCell"
    let defaultCardCell = "DefaultCardCell"
    let cartAddressCell = "CartAddressCell"
    let addressCell = "AddressCell"
    let deliveryAddressListCell = "DeliveryAddressListCell"
    //Dish Detail view Cell
    let dishHeaderCell = "DishHeaderCell"
    let selectProductRadioCell = "SelectProductRadioCell"
    let selectProductCheckboxCell = "SelectProductCheckboxCell"
    let dishDetailDividerCel = "DishDetailDividerCel"
    let deliveryOptionsCell = "DeliveryOptionsCell"
    let confirmOrderCell = "ConfirmOrderCell"
    
    let alertCell = "AlertCell"
    //MARK:- Images name
    let checkedImg = "checked"
    let uncheckImg = "uncheck"
    let noDishes = "No_Dishes"
    let searchLogo = "Search Icon"
    
    //MARK:- Controller title
    let termsTitle = "TERMS AND CONDITIONS"
    let privacyTitle = "PRIVACY POLICY"
    let emptyAddressMsg = "No address found. Add new address."
    
    let DELIVERABLE = "DELIVERY"
}

// MARK: - ErrorType
struct ErrorType: Codable {
    let success: Bool
    let message: String
}

struct ServerUrl {
   static let link = "https://foodcoop.hashtagrestaurants.com/api/v1/"
    //"https://demo.hashtagrestaurants.com/admin/api/v1/"
    //static let link = "https://dimsumbox.hashtagrestaurants.com/api/v1/"

 }

struct AppNotification {
    static let received = "notificationRecieved"
    static let reloadBottomView = "reloadBottomView"

}

struct AppKeys {
    static let google = "522306509680-qki3v89ssofh449l0qfifk689l6e09e8.apps.googleusercontent.com"
    static let twitterConsumerKey = "YzU51Q0Wycuv0NkycWuGi5knd"
    static let twitterConsumerSecret = "TF7YEfe3tWnxPz7TD3K8QhcRMtEOBiWPpdbti7K9rvAh2ZRrnk"
    static let googleMaps = "AIzaSyDSEaV_HVD2D5yYZRfjL65ErCCn2zfEBBI"
    static let googleGeocode = "AIzaSyDSEaV_HVD2D5yYZRfjL65ErCCn2zfEBBI"
}

struct ApiName {
    
    static let brainTreeClientToken = "braintree/token"
    static let register = "user"
    static let terms = "terms"
    static let privacy = "privacy"
    static let login = "user/login"
    static let loginWithMedia = "user/socialLogin"
    static let forgotPwd = "user/forgetPassword"
    static let userProfileDetail = "user/detail/"
    static let logoutUser = "user/"
    static let getHomeProduct = "home"
    static let homeFilter = "filter"
    static let fullMenu = "menu?"
    static let RestFullMenu = "rest-detail?"
    static let favouriteDish = "dishes/favorite"
    static let favouriteDishList = "dishes/favorites/list"
    static let getDishDetail = "dishes/view/"
    static let changePassword = "user/changePassword"
    static let updateUserProfile = "user/update/"
    static let addToCart = "cart/add"
    static let removeFromCart = "cart/remove"
    static let updateCartQty = "cart/updateqty"
    static let viewCart = "cart/view"
    static let applyCoupon = "cart/apply_coupon"
    static let searchDish = "search"
    
    static let notificationList = "notification/list/"
    static let getStripeCustomerId = "https://api.stripe.com/v1/customers"
    static let placeOrder = "order/create"
    static let orderList = "orders/list"
    static let removeCoupon = "cart/remove_coupon"
    static let paymentIntent = "orders/payment_intent?"
    static let updateStripeOrderStatus = "cart/update_stripe_order_status"
    static let updaterazorpayStatus = "cart/update_razorpay_order_status"
    static let staticPages = "pages"
    static let contactUs = "contact"
    static let rateOrder = "order/rate"
    static let reorder = "order/reorder"
    static let localCartTotal = "cart/total"
    static let orderDetail = "order/detail"
    static let getNotificationCount = "api/notification/unread/count/"
    static let nutritionInfo = "dishes/nutritionalinfo"
    static let updateFcmToken = "user/token/update"
    static let notificationStatus = "api/notification/status/"
    static let getTiming = "timings"
    static let addAddress = "addresses/add"
    static let updateAddress = "addresses/update"
    static let getAddressList = "addresses/list"
    static let deleteAddress = "addresses/delete"
    static let validateAddress = "addresses/validate"
    static let restrurantList = "restaurants/list"
    static let locationList = "locations/list"
    static let uploadImage = "user/updateImage/"
    static let cancellationPolicy = "orders/cancellation_policy"
    static let cancelOrder = "orders/cancel"

    static let deliveryAddressList = "addresses/list"
    static let deliveryAddressAdd = "addresses/add"
    static let deliveryAddressUpdate = "addresses/update"
    static let deliveryDeleteAddress = "addresses/delete"
    static let defaultDeliveryAddress = "address/default"
    static let distance = "addresses/get/distance"
    
    static let rateDelivery = "deliveryorder/rate/delivery"
    static let RestWithtag = "tag-detail?"

}

struct HeaderKey{
    static let customHeaderKey = "7890abcdefghijkl"
    static let headerKeyName = "X-custom-header"
    static let client_id = "c81e728d9d4c2f636f067f89cc14862c"
    static let secret_key = "c81e728d9d4c2f636f067f89cc14862c"
}

public struct AppColor {
    //static let primaryColor = UIColor(red: 255.0/255.0, green: 115.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    static let primaryColor = UIColor(named: "themeColor") ??  UIColor.colorFromHex("#DA2315")//("#c4161c") //FIXME:- ("#DA2315") // themeColor
    static let dividerColor = UIColor(red: 228/255.0, green: 229/255.0, blue: 231/255.0, alpha: 1.0)
    static let textColor = UIColor(red: 88/255.0, green: 89/255.0, blue: 91/255.0, alpha: 1.0)
    static let darkTextColor = UIColor(red: 73/255.0, green: 73/255.0, blue: 73/255.0, alpha: 1.0)
    static let placeHolderColor = UIColor(red: 165/255.0, green: 165/255.0, blue: 165/255.0, alpha: 1.0)
    static let background = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1.0)
    static let topColor = UIColor(red: 229/255.0, green: 92/255.0, blue: 68/255.0, alpha: 1.0)
    static let checkoutUnselectedColor = UIColor(displayP3Red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha: 1.0)
    static let greyColor = UIColor(red: 156/255.0, green: 156/255.0, blue: 156/255.0, alpha: 1.0)
    static let greenColor = UIColor(red: 48/255.0, green: 173/255.0, blue: 35/255.0, alpha: 1.0)
    static let lightGreen = UIColor(red: 90/255.0, green: 150/255.0, blue: 81/255.0, alpha: 1.0)
}

struct StoryboardName {
    
    static let home = "Home"
    static let login = "Login"
    static let tabbar = "Tabbar"
    static let cart = "Cart"
    static let setting = "Settings"
    static let profile = "Profile"
    static let main = "Main"
    static let productDetail = "ProductDetail"
    static let fullMenu = "FullMenu"
}

struct Messages {
    
    static let allRequiredFields = "All fields are required."
    static let tryAgain = "Try again."
    static let emptyEmail = "Please enter email address."
    static let emptyPwd = "Please enter password."
    static let emptyNewPwd = "Please enter new password."
    static let emptyConfirmPwd = "Please confirm your password."
    static let emptyStudentId = "Please enter your Student Id."
    static let emptyName = "Please enter name."
    static let emptyMobile = "Please enter mobile number."
    static let invalidPhone = "Please enter 10 digits phone number."

    static let invalidMobile = "Please enter 10 digits mobile number."
    static let newPwdLength = "New password should be min. 8 characters long."
    static let minCharPWd = "Password should be min. 8 characters long."
    static let invalidEmail = "Please enter valid email address."
    static let confirmPwdAlertMsg = "Password and confirm password are not same."
    static let createConfirmPwd = "Create password and confirm new password doesn't match. Please try again."
    static let noCouponCode = "Please enter a coupon code."
    static let termsNotSelected = "Please select \"I agree to the Terms of Use\" to proceed further."
    static let pwdNotMatched = "New password and current new password doesn't match. Please try again."
    static let emptyHall = "Please select hall."
    static let emptySubject = "Please enter subject."
    static let emptyMessage = "Please enter message."
    static let emptyFirstName = "Please enter first name."
    static let emptyLastName = "Please enter last name."
    static let emptyAddressTitle = "Please enter address title."
    static let emptyApartment = "Please enter apartment number."
    static let emptyHouseNum = "Please enter house number."
    static let emptyStreet1 = "Please enter street address."
    static let emptyStreetOne = "Please enter street1 address."
    static let emptyStreetTwo = "Please enter street2 address."
    static let emptyCity = "Please enter city."
    static let emptyState = "Please enter state."
    static let emptyZipcode = "Please enter zipcode."
    static let emptyCardNumber = "Please enter card number."
    static let emptyCvvCode = "Please enter cvv number."
    static let emptyExpiryDate = "Please enter expiry date."
    static let selectAllCustomisations = "Please select all required customizations."
    static let noDietary = "No Dietary."
    static let clearNotification = "Are you sure you want to clear all notifications?"
    
    static let noProduct = "No products were found matching your selection."
    static let noInternetConnection = "No internet connection. Please try again."
    static let noReviews = "There are no reviews yet."
    static let noOrderHeading = "HOOK YOURSELF UP WITH SOME GOOD FOOD."
    static let noOrder = "You haven’t placed any order yet."
    static let logout = "You have been logged out. Please login to continue."
    static let profileUpdated = "Profile updated successfully."
    static let savedCards = "Maximum saved cards limit reached."
    static let maximumSavedAddress = "Maximum saved addresses limit reached."

    static let approvalPending = "Cancel request has already been sent to the admin. Please wait for approval."
    static let noFilterSelected = "Please choose any option to apply."
    static let enableLocation = "We don't have access to location services on your device. Please go to settings and enable services to use this feature."
    static let orderStatus = "Your order #%@ has been placed. Please check the progress below."
    static let itemAdded = "Item added to cart."
    static let itemUpdatedSuccessfully = "Item updated successfully."
    static let hallClosed = "%@ is closed for selected date/time."
    static let cancelRequestSent = "Order pending for cancellation."
    static let cancelOrder = "Order status has been sent to Restaurant. Please wait for the confirmation from the Restaurant's end."
    static let orderCancelByHallWithoutRequest = "Unfortunately, Restaurant has declined your order. Your refund will be initiated within 7-10 working days."
    
    static let orderCancelledWithpayPalBraintree = "Order has been cancelled successfully."
    
    static let orderCancelledWithpayPalBraintreeWithStatusCancelStatus3 = "Order has been cancelled successfully. Your refund will be initiated within 7-10 working days."

    static let orderCancelledWithpayPayByCard = "Order has been cancelled successfully."

    static let orderCancelledWithpayPayByCardWithCancelStatus3 = "Order has been cancelled successfully. Your refund will be initiated within 7-10 working days."

    
    static let orderCancelByHallWithoutRequestForUnpaid = "Unfortunately, Restaurant has declined your order, please contact Restaurant."
    
    static let cancelCancelledOnUserRequest = "Order has been cancelled successfully."
    static let orderCancelledDueToNoPickup = "The order has not been picked up, hence the order is cancelled by the Restaurant."
    static let noNotificationHeading = "OOPS! ITS EMPTY."
    static let noNotification = "You have no notifications right now."
    static let creditCardNotSelected = "Please select the credit card option to add a card."
    static let noFavouriteHeading = "FOOD IS LOVE MADE VISIBLE."
    static let noFavouriteDishes = "Once you favorite a item, it will appear here."
    static let removeDishFromFavourites = "Do you want to remove the item from favorites?"
    static let noCartItem = "Your cart is empty. Add something from the menu."
    static let noCartHeading = "GOOD FOOD MAKES LIFE GREAT."
    static let dishNotAvailable = "Item is currently unavailable."
    static let dishUnavailable = "CURRENTLY UNAVAILABLE"
    static let dishUnavailableUserMessage = "Some of the item(s) in your cart are currently unavailable. Please remove the unavailable item(s) to proceed."
    static let invalidLoginDetail = "Login details don't match with our database. Please try logging with other credentials."
    static let pleaseWait = "Please wait for a while as we are fetching your card details."
    static let noCards = "Please add a payment method to continue with the order."
    static let noInternetHeading = "There is no internet to browse."
    static let noInternetSubHeading = "Seems like you are offline. Please check your internet settings."
    static let couponApplied = "Coupon applied successfully."
    static let mealDishesUnavailable = "Some of the item(s) in your cart are currently unavailable. Please clear the cart to proceed."
    static let deleteAddress = "Are you sure you want to delete the address?"
    static let noAddressHeading = "YOU HAVEN'T ADDED ANY ADDRESS YET!"
    static let noAddressSubHeading = "Your addresses will appear here."
    static let restaurantNotAcceptingOrder = "Restaurant is currently not accepting online orders."
    static let noAddressAdded = "Please add your delivery address to place order."
    static let ongoingOrder = "You already have an ongoing order."
    static let invalidAddress = "We no longer deliver to this address."
    static let emptyDeliverAddress = "Please add a address to place an order."
    static let orderCannotDeliver = "We cannot deliver order to this location."

    static let orderMaxLimit = "You have reached the max order limit."

    static let couponRemoved = "Coupon removed successfully."
    static let changePasswordSuccess = "Password updated successfully."
    static let selectRequiredCustomizations = "Please choose required customizations."
    
    static let cartDeletionMessage = "Are you sure you want to delete the card?"
    static let addressDeletionMessage = "Are you sure you want to delete the address?"

    static let cartItemRemoved = "Item removed successfully."
    
    static let restaurantClosed = "The restaurant is currently closed or is not accepting orders. Try selecting a different location or check the business timings here."
    
    static let disclaimerMessage = "I have read and agreed to the Privacy Policy, Terms and Conditions"
    static let comingSoon = "Coming Soon."
}

struct AppStrings {
    static let  appName = "FoodCoop"
    static let  updateCart = "UPDATE CART"
}


enum AppImage : String {
    case dishPlaceholder = "dishPlaceholder"
    case dishPlaceholderSmall = "dishPlaceholder_300_200"
    case dishPlaceholderBG = "dishBackground_bg"
}

struct UserDefault {
    
    static let isUserLoggedIn = "isUserLoggedIn"
    static let user = "user"
    static let localCart = "localCart"
    static let catagoryArray = "catagoryArray"
    static let deviceId = "deviceId"
    static let userId = "id"
    static let customerId = "customerId"
    static let secretKey = "secretKey"
    static let fcmToken = "fcmToken"
    static let recentSearches = "recentSearches"
    static let specialInstruction = "specialInstruction"
    static let couponCode = "couponCode"
    static let socialMediaLogin = "socialMediaLogin"
    static let defaultAddressId = "defaultAddressId"
    static let delieveryAddress = "delieveryAddress"
}
