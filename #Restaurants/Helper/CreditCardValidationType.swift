//
//  CreditCardValidationType.swift
//
//  Created by Vitaliy Kuzmenko on 02/06/15.
//  Copyright (c) 2015. All rights reserved.
//

import Foundation
import UIKit

enum CardImage : String {
    
	case Visa = "Visa"
    case AmericanExpress = "american-express-logo"
    case MasterCard = "MasterCardLogo"
    case DinersClub = "Diners_Club_Logo-880x625"
    case JCB = "JCBLogo"
    case Discover = "discoverLogo"
    case UnionPay = "UnionPayLogo"
    case Mir = "Mirlogo"
	case defaultCard = "defaultCard"
	case paypal = "paypal"
    
    var image: UIImage {
        switch self {
        case .Visa: return #imageLiteral(resourceName: "visaCardLogo")
        case .AmericanExpress: return #imageLiteral(resourceName: "american-express-logo")
        case .MasterCard: return #imageLiteral(resourceName: "MasterCardLogo")
        case .DinersClub: return #imageLiteral(resourceName: "Diners_Club_Logo-880x625")
        case .JCB : return #imageLiteral(resourceName: "JCBLogo")
        case .Discover : return #imageLiteral(resourceName: "discoverLogo")
        case .UnionPay : return #imageLiteral(resourceName: "UnionPayLogo")
        case .Mir : return #imageLiteral(resourceName: "Mirlogo")
		case .defaultCard : return  #imageLiteral(resourceName: "money")
		case .paypal : return #imageLiteral(resourceName: "paypal")
        }
    }
}

public func ==(lhs: CreditCardValidationType, rhs: CreditCardValidationType) -> Bool {
    return lhs.name == rhs.name
}

public struct CreditCardValidationType: Equatable {
    
    public var name: String
    
    public var regex: String
    public var image: String

    public init(dict: [String: Any]) {
        if let name = dict["name"] as? String {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let regex = dict["regex"] as? String {
            self.regex = regex
        } else {
            self.regex = ""
        }
        
        if let image = dict["image"] as? String {
            self.image = image
        } else {
            self.image = ""
        }
    }
}

