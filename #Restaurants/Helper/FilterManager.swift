//
//  FilterManager.swift
//  #Restaurants
//
//  Created by Satveer on 29/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

enum FilterCase {
    case sort
    case dietary
    case cuisine
}

class FilterManager: NSObject {
    
    var sort = NSArray()
    var dietary = [Dietary]()
    var cuisines = [Cuisines]()
    var page = 1
    
    init(page : Int, sort : NSArray, dietary : [Dietary],cuisines : [Cuisines]) {
       self.page = page
       self.sort = sort
       self.dietary = dietary
       self.cuisines = cuisines
    }
    override init(){
        super.init()
    }
    
    func filterManager(_ dic: NSDictionary) -> FilterManager {
        let filter = FilterManager()
        filter.page = 1
        filter.sort = dic.value(forKey: "sort") as? NSArray ?? NSArray()
        filter.dietary = Dietary().getDietaryAttribute(dic.value(forKey: "dietary") as? NSArray ?? NSArray())
        filter.cuisines = Cuisines().getcuisineAttribute(dic.value(forKey: "cuisines") as? NSArray ?? NSArray())
        return filter
    }
    
}
struct  Dietary{
    var id : Int?
    var name : String?
    var options = [Option]()
    
    func dietaryItems(_ dic: NSDictionary) -> Dietary {
        var item = Dietary()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.name = dic.value(forKey: "name") as? String ?? ""
        item.options = Option().getCustomizationOptions(dic.value(forKey: "options") as? NSArray ?? NSArray())
        return item
    }
    
    func getDietaryAttribute(_ arr: NSArray) -> [Dietary] {
        var DietaryArr = [Dietary]()
        for attribute in arr {
            let attributeDic = self.dietaryItems(attribute as? NSDictionary ?? NSDictionary())
            DietaryArr.append(attributeDic)
        }
        return DietaryArr
    }
}

struct Cuisines{
    var id : Int?
    var name : String?
    
    func cuisineItems(_ dic: NSDictionary) -> Cuisines {
        var item = Cuisines()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.name = dic.value(forKey: "name") as? String ?? ""
        return item
    }
    
    func getcuisineAttribute(_ arr: NSArray) -> [Cuisines] {
        var cuisineArr = [Cuisines]()
        for attribute in arr {
            let attributeDic = self.cuisineItems(attribute as? NSDictionary ?? NSDictionary())
            cuisineArr.append(attributeDic)
        }
        return cuisineArr
    }
    
    
}

struct FilterProduct {
    var sortArr: String?
    var dietaryArr: [String]?
    var cuisineArr: [String]?
    
    func serviceUrl(_ this : FilterProduct) -> String {
        var url = ""
        let sortParam = this.sortArr
        let dietaryParam = (this.dietaryArr?.count) ?? 0 > 0 ? this.dietaryArr?.joined(separator: ",") : ""
        let cuisineParam = (this.cuisineArr?.count) ?? 0 > 0 ? this.cuisineArr?.joined(separator: ",") : ""
        url = "?dietary=\(dietaryParam ?? "")&cuisine=\(cuisineParam ?? "")&sort=\(sortParam ?? "")"
        return url
  }
}

