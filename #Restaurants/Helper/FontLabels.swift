//
//  FontLabels.swift
//  PetConnect
//
//  Created by 42Works-Worksys2 on 21/09/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Foundation

//MARK:- Label classes

class RegularLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Regular", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
       // self.textColor = AppColor.darkTextColor
    }
}

class RobotoLightLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.init(name: "Roboto-Light", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.textColor
    }
}

class MediumLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.init(name: "Roboto-Medium", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
    }
}

class CustomLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.init(name: "Roboto-Light", size: 12) ?? UIFont.systemFont(ofSize: 12)
        self.textColor = AppColor.darkTextColor
    }
}

//MARK:- Button classes
class RegularButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Regular", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class RobotoNormalButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Medium", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
        self.setTitleColor(AppColor.textColor, for: .normal)
    }
}

class MediumButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Medium", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class RobotoMediumButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.backgroundColor = AppColor.primaryColor        
        self.titleLabel?.font = UIFont.init(name: "Roboto-Medium", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

//MARK:- Textfield classes

class loginTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.textColor = UIColor.white
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}

class RegularTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Regular", size: self.font?.pointSize ?? 13) ?? UIFont.systemFont(ofSize: self.font?.pointSize ?? 13)
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.placeHolderColor])
        self.textColor = AppColor.darkTextColor
    }
}

class RobotoTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Light", size: 14) ?? UIFont.systemFont(ofSize: 14)
        self.textColor = AppColor.darkTextColor
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.placeHolderColor])
    }
}


class CustomTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
       
        let attributes = [
            NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 17),NSAttributedString.Key.foregroundColor: AppColor.placeHolderColor
        ]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes:attributes)
        self.font = UIFont.init(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
    }
}

