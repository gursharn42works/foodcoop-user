//
//  Helper.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit

class Helper: NSObject {
    
    static let shared = Helper()
    var isFromView = ""
    var loaderView = LoaderView()
    var window: UIWindow?
    
    func showLoader() {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            if let win = sd.window {
                loaderView = LoaderView(frame: win.frame )
                win.addSubview(loaderView)
            }
            
        }
    }
    
    func stopLoader() {
        loaderView.removeFromSuperview()
    }
    
}
extension Collection {
    func firstIndexOfMaxElement<T: Comparable>(_ predicate: (Element) -> T) -> Index? {
        zip(indices, self).max(by: { predicate($0.1) < predicate($1.1) })?.0
    }
    func firstIndexOfMinElement<T: Comparable>(_ predicate: (Element) -> T) -> Index? {
        zip(indices, self).min(by: { predicate($0.1) < predicate($1.1) })?.0
    }
}
