//
//  ImageCompression.h
//  UISApp
//
//  Created by Neeraj on 29/04/16.
//  Copyright © 2016 isquare2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageCompression : NSObject

+(UIImage*)compressImage:(UIImage*)originalImage toWidth:(CGFloat)width;

+(UIImage *)compressImage:(UIImage *)image;

+ (UIImage *)resizedImage:(UIImage *)image;

@end
