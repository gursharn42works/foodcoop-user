//
//  ImageCompression.m
//  UISApp
//
//  Created by Neeraj on 29/04/16.
//  Copyright © 2016 isquare2. All rights reserved.
//

#import "ImageCompression.h"

@implementation ImageCompression

+(UIImage *)compressImage:(UIImage *)originalImage toWidth:(CGFloat)width
{

//    width = width * 2;
//    
//    float oldWidth = originalImage.size.width;
//    float scaleFactor = width / oldWidth;
//    
//    float newHeight = originalImage.size.height * scaleFactor;
//    
//    float newWidth = oldWidth * scaleFactor;
//    
//    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
//    
//    [originalImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
//    
//    UIImage * compressedImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
//    
//
//    NSData *beforeCompression = UIImagePNGRepresentation(originalImage);
//    
//    NSLog(@"Size of your image is %lu bytes", (unsigned long)[beforeCompression length]);
//    
//    NSData *afterCompression = UIImagePNGRepresentation(compressedImage);
//    
//    NSLog(@"Size of your image is %lu bytes", (unsigned long)[afterCompression length]);
    
    //************Disabled compressing
    
     NSData *imageAsData = UIImageJPEGRepresentation(originalImage, 0.3);
     
     UIImage *compressedImage = [UIImage imageWithData:imageAsData];

    
    return compressedImage;
}


+(UIImage *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    
    float actualWidth = image.size.width;
    
  //  NSLog(@"Dimesions  width: %f  height : %f",actualWidth, actualHeight);
    
    float maxHeight = 360.0;
    
    float maxWidth = 480.0;
    
    float imgRatio = actualWidth/actualHeight;
    
    float maxRatio = maxWidth/maxHeight;
  
    float compressionQuality = 0.9;
    
    //50 percent compression
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        { //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        { //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {   actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight); // UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
    //UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSLog(@"After compression Image size  : %.2f MB",(float)imageData.length/1024.0f/1024.0f);

    
    UIGraphicsEndImageContext();
    
    UIImage * compressedImage= [UIImage imageWithData:imageData];
    
    NSLog(@"Dimesions  width: %f  height : %f", compressedImage.size.width, compressedImage.size.height);
    
    
    return compressedImage;
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size
{
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
//    {
//        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
//    }
//    else
//    {
        UIGraphicsBeginImageContext(size);
//    }

    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(newImage, 0.5);
    
    UIImage * resized = [UIImage imageWithData:imageData];
   
    return resized;//newImage;
}

+ (UIImage *)resizedImage:(UIImage *)image; {
    
//    float maxHeight = 540.0;
//    
//    float maxWidth = 720.0;
//    
//    CGFloat originalWidth = image.size.width;
//    
//    CGFloat originalHeight = image.size.height;
//    
////    NSData *originalImgData = UIImageJPEGRepresentation(image, 1.0);
//    
//   // NSLog(@"Original Size of Image(bytes):%lu",(unsigned long)[originalImgData length]);
////    
////    NSLog(@"REsized Dimesion width  :  %f,  height   :  %f", originalWidth, originalHeight);
//   
//    CGFloat scaleFactor = (originalWidth > originalHeight) ? maxWidth / originalWidth : maxHeight / originalHeight;
//    
//    CGFloat newHeight = originalHeight * scaleFactor;
//    CGFloat newWidth = originalWidth * scaleFactor;
//    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    CGSize resized = [ImageCompression resize:image];
    
    UIImage * resizedImg = [ImageCompression imageWithImage:image scaledToSize:resized];
    
    return resizedImg; //newSize
}


+(CGSize)resize:(UIImage*)image
{
    float maxHeight = 360;

    float maxWidth = 480;
    
//    float maxHeight = 540;
//
//    float maxWidth = 720;
    
    CGFloat originalWidth = image.size.width;
    
    CGFloat originalHeight = image.size.height;
    
    CGFloat originalRatio = originalWidth/originalHeight;
    
    
    CGSize resized = CGSizeMake(originalWidth, originalHeight);
    
    // LandScape
    
    if (originalWidth > originalHeight && originalWidth > maxWidth)
    {
        NSLog(@"LanScape : \n\nWidth : %f Height : %f", originalWidth, originalHeight);
        
        resized.width = maxWidth;
        
        resized.height = maxWidth/originalRatio;
        
        NSLog(@"Resized LanScape : \n\nWidth : %f Height : %f", resized.width, resized.height);
        
        return resized;
    }
    else if (originalHeight > originalWidth && originalHeight > maxWidth) //Portrait
    {
        NSLog(@"Portrait : \n\nWidth : %f Height : %f", originalWidth, originalHeight);
        
        resized.height = maxWidth;
        
        resized.width = originalRatio * maxWidth;
        
        NSLog(@"Resized Portrait : \n\nWidth : %f Height : %f", resized.width, resized.height);
        
        return resized;
    }
    else if (originalHeight == originalWidth && originalHeight > maxHeight)
    {
        resized.height = maxHeight;
        
        resized.width = maxHeight;
    }
    
    return resized;
}

@end
