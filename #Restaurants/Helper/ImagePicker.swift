//
//  ImagePicker.swift
//  #Restaurants
//
//  Created by Thirtyfour on 19/06/20.
//  Copyright © 2020 Apple1. All rights reserved.
//

import Foundation
import UIKit

enum AttachementType : String
{
    case video
    case image
    case document
}

class ImagePicker : NSObject
{
    var imagePicker = UIImagePickerController()
 
    var mediaType = AttachementType.image
    
    func chooseImage(on viewController : UIViewController, ofType : AttachementType) {
        
        imagePicker.delegate = viewController as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        let mediaType = ofType == .image ? "Image" : "Video"
        
        self.mediaType = ofType
        
        let alert = UIAlertController(title: "Choose \(mediaType)", message: nil, preferredStyle: .actionSheet)
        
        if self.mediaType == .image
        {
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera(viewController)
            }))
        }
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
             self.openGallary(viewController)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = viewController.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: viewController.view.bounds.midX, y: viewController.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = []

        default:
            break
        }
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera(_ viewController : UIViewController)
    {
		if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
			imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            viewController.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary(_ viewController : UIViewController)
    {
		imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.mediaTypes = self.mediaType == .image ? ["public.image"] : ["public.movie"]
        imagePicker.allowsEditing = true
        viewController.present(imagePicker, animated: true, completion: nil)
    }
}
