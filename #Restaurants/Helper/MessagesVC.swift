//
//  MessagesVC.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class MessagesVC: BaseVC {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var headingLbl: UILabel!
    
    @IBOutlet weak var subTitleLbl: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var browseBtn_Out: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  .clear
        // Do any additional setup after loading the view.
        self.setStatusBar(color: UIColor.white)
    }
    
    @IBAction func BrowseRest_Btn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        if let magicTabBar = Router.viewController(with: .tabBar, .home) as? MagicTabBar {
            let rootNC = UINavigationController(rootViewController: magicTabBar)
            rootNC.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        }
        
    }
    


}
