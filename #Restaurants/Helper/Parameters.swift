//
//  Parameters.swift
//  #Restaurants
//
//  Created by 42works on 24/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Parameters: NSObject {
    
    //MARK:- SignUp
    class func signUpParameters(_ name: String,_ email: String,_ phone: String, password: String, _ confirmPassword: String) -> [String: Any] {
        return ["email": email,"phone": phone, "password": password, "student_id": "", "name": name, "password_confirmation": confirmPassword, "device_id": UserModel.shared.device_id,"device_type": "Ios","device_token":UserModel.shared.fcm_token,"stripe_id": ""]
    }

    
    //AMRK:- Login
    class func loginParameters(_ email: String, password: String) -> [String: Any] {
        return ["email": email, "password": password, "device_id": UserModel.shared.device_id,"device_type": "Ios","device_token":UserModel.shared.fcm_token]
    }
    
    //AMRK:- Apple
    class func appleLoginParameters(_ email: String, name: String,_ profile_pic: String, _ social_token: String) -> [String: Any] {
        return ["email": email, "name": name, "social_profile_pic": profile_pic, "social_token":social_token, "device_id": UserModel.shared.fcm_token,"device_type": "Ios","device_token":UserModel.shared.fcm_token]
    }
    
    //AMRK:- Facebook
    class func facebookLoginParameters(data: NSDictionary) -> [String: Any] {
        
        var serverDic = [String: String]()
        serverDic["email"] = data.value(forKey: "email") as? String ?? ""
        serverDic["name"] = data.value(forKey: "name") as? String ?? ""
        serverDic["social_profile_pic"] = (((data.value(forKey: "picture") as? NSDictionary)?.value(forKey: "data") as? NSDictionary)?.value(forKey: "url")) as? String ?? ""
        serverDic["social_token"] = data.value(forKey: "id") as? String ?? ""
        serverDic["device_id"] = UserModel.shared.device_id
        serverDic["device_type"] = "Ios"
        serverDic["device_token"] = UserModel.shared.fcm_token

        return serverDic
    }
    
    //AMRK:- Login
    class func changePasswordParameters(_ password: String, oldpassord: String,confirmNewPwd: String) -> [String: Any] {
        return ["password": password, "oldpassord": oldpassord, "password_confirmation": confirmNewPwd,"id":UserModel.shared.user_id]
    }
    
    class func personalInfoParameters(_ name: String, _ phone: String) -> [String: Any] {
        var serverDic = [String: String]()
        serverDic["name"] = name
        serverDic["phone"] = phone
        serverDic["student_id"] = UserModel.shared.student_id
        serverDic["stripe_id"] = UserModel.shared.stripe_id
        serverDic["default_card_id"] = UserModel.shared.card_id
        return serverDic
    }
    
    class func addDeliveryAddressParameters(_ addressId: Int, addressTitle: String, _ houseNum: String, _ street1: String, _ street2: String, _ city: String, _ state: String, _ zip: String, _ default_address: Int, lat: Double, lng: Double) -> [String: Any] {
        var serverDic = [String: Any]()
        if addressId != 0 {
            serverDic["id"] = addressId
        }
        serverDic["user_id"] = UserModel.shared.user_id
        serverDic["firstname"] = UserModel.shared.user_name
        serverDic["lastname"] = UserModel.shared.user_name
        serverDic["title"] = addressTitle
        serverDic["house_no"] = houseNum
        serverDic["street1"] = street1
        serverDic["street2"] = street2
        serverDic["city"] = city.capitalized
        serverDic["state"] = state.capitalized
        serverDic["zipcode"] = zip
        serverDic["default_address"] = default_address
        serverDic["lat"] = lat
        serverDic["lng"] = lng

        if lng != 0.0 && lng != 0.0 {
            serverDic["isLatLng"] = 1
        } else {
            serverDic["isLatLng"] = 0
        }

        return serverDic
    }
    
    class func rateDeliveryParameters(_ delivery_id: String,_ order_id: String, _ rating: String, _ comment: String, _ order_rating: String, _ order_comment: String, _ dishRating: String,_ rest_Id: String, _ subject: String, _ message:String) -> [String: Any] {
        return ["delivery_id": delivery_id,"order_id": order_id, "rating": rating, "comment": comment, "order_rating": order_rating, "order_comment": order_comment, "dish_ratings":dishRating, "rest_id": rest_Id, "subject":subject, "message":message]
    }
}

