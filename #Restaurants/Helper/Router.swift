
  //Router.swift
  //#Restaurants

 //Created by 42works on 22/07/20.
 //Copyright © 2020 42works. All rights reserved.


import UIKit

enum Storyboard : String{
    case main             = "Main"
    case login            = "Login"
    case settings         = "Settings"
    case home             = "Home"
    case product          = "ProductDetail"
    case cart             = "Cart"
    case fullMenu         = "FullMenu"
    case profile          = "Profile"
}

enum VCId : String {
    
    //MARK:- Login
    
    case loginVC          = "LoginVC"
    case register       = "SignUpVC"
    case forgotPasswordVC = "ForgotPasswordVC"
    case termsConditionVC = "TermsConditionVC"
    case splash           = "SplashVC"
    
    //MARK:- Settings
    
    case changePasswordVC         = "ChangePasswordVC"
    case privacyPolicy          = "PrivacyPolicyController"
    case contactUsVC            = "ContactUsVC"
    case report                 = "ReportViewController"
    case settingsVC               = "SettingsVC"
    case about                    = "AboutViewController"
    case language                = "LanguageViewController"
    
    //MARK:- Home
    
    case notificationVC            = "NotificationVC"
    case cartVC                    = "CartViewController"
    case tabBar                    = "MagicTabBar"
    case profile                = "ProfileController"
    case announcementVC            = "AnnouncementVC"
    case filter                    = "FilterVC"
    case filterDietary            = "FilterDietaryViewController"
    case addressesVC                = "AddressesVC"
    case addAddress                = "AddAddressViewController"
    case addressPopUp           = "AddressPopupController"
    case restaurantListVC        = "RestaurantListVC"
    case dropDown                = "DropDownViewController"
    case home                    = "HomeVC"
    case fullMenuVC              = "FullMenuVC"
    case Restsearch              = "RestaurantSearchVC"
    
    //MARK:- Product Details
    case productDetailVC        = "ProductDetailVC"
    case dishDetailVC            = "DishDetailVC"
    case searchVC                = "SearchVC"
    case navSearchVC            = "NavSearchViewController"
    case restaurantTimeVC        = "RestaurantTimeVC"
    case stripeCustomPaymentVC        = "StripeCustomPaymentVC"

    //MARK:- Cart
    
    case addNewCard                = "AddNewCardViewController"
    case savedCards                = "SavedCardsViewController"
    case taxesInfo                = "TaxesInfoViewController"
    
    //MARK:- Full Menu
    
    case menuDetails            = "MenuDetailViewController"
    case dishNotAvaiableVC        = "DishNotAvaiableVC"
    
    case deliveryAddressVC      = "DeliveryAddressVC"
    case addDeliveryAddressVC   = "AddDeliveryAddressVC"
    case fullMenuSearchVC       = "FullMenuSearchVC"

    //MARK:- Profile
    
    case favoriteDishVC        = "FavoriteDishVC"
    case logoutVC                    = "LogoutVC"
    case rateVC                    = "RateVC"
    //case orderDetailVC            = "OrderDetailVC"
    case deliveryOrderDetailVC            = "DeliveryOrderDetailVC"
    case checkoutViewController            = "CheckoutViewController"
    case deliveryCompleteVC            = "DeliveryCompleteVC"
    case deliveryFeedbackCompleteVC  = "DeliveryFeedbackCompleteVC"
    case personalInfoVC    = "PersonalInfoVC"
    case orderHistoryVC            = "OrderHistoryVC"
    case baseOrderHistoryVC        = "BaseOrderHistoryVC"
    case orderStatusOld            = "OrderStatusViewController_Old"
    case orderStatusVC            = "OrderStatusVC"
    case orderStatusMapViewVC = "OrderStatusMapViewVC"
    case addMobilePopUpVC   = "AddMobilePopUpVC"
    case ratingDetailVC  = "RatingDetailVC"
    case completeRatingDetailVC  = "CompleteRatingDetailVC"
    
    ////MARK:- Main
    case messagesVC            = "MessagesVC"
    case cancelConfirmVC       = "CancelConfirmVC"
}


class Router {
    
    private init() {
        
    }
    
    static func viewController(with id: VCId,_ storyboard : Storyboard) -> UIViewController?{
        
        return UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(identifier: id.rawValue)
    }
    
    static func push(_ id: VCId,_ storyboard: Storyboard,  fromVC: UIViewController){
     
        if let vc = Router.viewController(with: id, storyboard){
            Router.push(vc, fromVC)
        }
    }
    
    static func push(_ vc: UIViewController, _ fromVC : UIViewController){
        fromVC.navigationController?.pushViewController(vc, animated: true)
    }
}
