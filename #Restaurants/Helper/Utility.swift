//
//  Utility.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Utility: NSObject {
  
    static let shared = Utility()
    
    var favDishArr = NSMutableArray()
    var isSearchViewController = false
    var hideStatusBar = false
    
    func showAlertView(message: String) {
        if let keyWindow = UIWindow().key {
            keyWindow.showAlert(message: message)
        }
    }
    
    func showEmptyMsgView(message: String) {
        if let keyWindow = UIWindow().key {
            keyWindow.showEmptyMsg(message)
        }
    }
    
    func startLoading() {
        if let keyWindow = UIWindow().key {
            keyWindow.showLoader()
        }
    }
    
    func stopLoading() {
        if let keyWindow = UIWindow().key {
            keyWindow.stopLoader()
            
           if let view = keyWindow.viewWithTag(-100) {
                view.removeFromSuperview()
            }
        }
    }
    
    func checkInvalidTimeSlot(_ hallTiming : [HallTiming]) -> [HallTiming] {
        
        var newHallTiming = hallTiming
        var nextDaySlotHolder = hallTiming
        
        
        for j in 0..<newHallTiming.count{
            
            for k in 0..<newHallTiming[j].timeSlot.count{
                
                var timeSlot = newHallTiming[j].timeSlot[k]
                
                let startTime = (timeSlot.startTime ?? "").lowercased()
                let endTime   = (timeSlot.endTime ?? "").lowercased()
                
                let nextDayIndex = (j == 6) ? 0 : j+1
                
                if startTime.contains("pm") && endTime.contains("am"){
                    
                    timeSlot.endTime = "11:59 PM"
                    newHallTiming[j].timeSlot[k] = timeSlot
                }
                else if startTime.contains("am") && endTime.contains("am"){
                    
                    if startTime.compare(endTime) == .orderedDescending{
                        timeSlot.endTime = "11:59 PM"
                        newHallTiming[j].timeSlot[k] = timeSlot
                        
                        var nextDaySlot = TimeSlot()
                        nextDaySlot.startTime = "12:00 AM"
                        nextDaySlot.endTime   = endTime.uppercased()
                        
                        nextDaySlotHolder[nextDayIndex].timeSlot.append(nextDaySlot)
                        
                    }
                    else if startTime.compare(endTime) == .orderedAscending{
                        
                        if !startTime.hasPrefix("12") && endTime.hasPrefix("12"){
                            timeSlot.endTime = "11:59 PM"
                            newHallTiming[j].timeSlot[k] = timeSlot
                            
                            var nextDaySlot = TimeSlot()
                            nextDaySlot.startTime = "12:00 AM"
                            nextDaySlot.endTime   = endTime.uppercased()
                            
                            if (nextDaySlot.startTime ?? "").compare(nextDaySlot.endTime ?? "") != .orderedSame{
                                nextDaySlotHolder[nextDayIndex].timeSlot.append(nextDaySlot)
                            }
                        }
                    }
                }
                
            }
        }
        
        
        for i in 0..<newHallTiming.count{
            
            if nextDaySlotHolder[i].timeSlot.count > newHallTiming[i].timeSlot.count{
                
                var nextDaySlot = [nextDaySlotHolder[i].timeSlot.last ?? TimeSlot()]
                
                nextDaySlot.append(contentsOf: newHallTiming[i].timeSlot)
                
                newHallTiming[i].timeSlot = nextDaySlot
            }
            
        }
        
        return newHallTiming
    }

}


class KeyChain {
    
    class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]
        
        SecItemDelete(query as CFDictionary)
        
        return SecItemAdd(query as CFDictionary, nil)
    }
    
    class func load(key: String) -> Data? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]
        
        var dataTypeRef: AnyObject? = nil
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        
        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }
    
    class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)
        
        let swiftString: String = cfStr as String
        return swiftString
    }
}
