//
//  CancelPolicyModel.swift
//  #Restaurants
//
//  Created by Satveer on 20/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


// MARK: - DataClass
class CancelPolicyModel: NSObject {
    var cancellationPolicy = ""
    var refundRules = [RefundRule]()

    func setCancelPolicyModelData(dic: NSDictionary) -> CancelPolicyModel{
        let model = CancelPolicyModel()
        model.cancellationPolicy = dic["cancellation_policy"] as? String ?? ""
        model.refundRules = RefundRule().setRefundRuleData(array: dic["refund_rules"] as? NSArray ?? [])
        return model
    }
}

// MARK: - RefundRule
class RefundRule: NSObject {
    var id = 0
    var hours = 0
    var minutes = 0
    var percentage = 0
    var createdAt = ""
    var updatedAt = ""

    func setRefundRuleData(array: NSArray) -> [RefundRule] {
        
        var ruleArr = [RefundRule]()
        for i in 0..<array.count {
            let dic = array[i] as? NSDictionary ?? [:]
            let model = RefundRule()
            model.id = dic["model"] as? Int ?? 0
            model.hours = dic["model"] as? Int ?? 0
            model.minutes =  dic["model"] as? Int ?? 0
            model.percentage =  dic["model"] as? Int ?? 0
            model.createdAt =  dic["created_at"] as? String ?? ""
            model.updatedAt =  dic["updated_at"] as? String ?? ""
            ruleArr.append(model)
        }
        return ruleArr
    }
}
