//
//  CardModel.swift
//  #Restaurants
//
//  Created by Satveer on 19/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class CardModel : NSObject {
    
    var cardNumber = ""
    var customerId = ""
    var cardId = ""
    var isDefault : Bool = false
    var lastFourDigits : String = ""
    var brand : String = ""
    func getCard(_ response: NSDictionary?) -> CardModel {
        if let response = response {
            
            let this = CardModel()
            this.cardNumber = response.value(forKey: "last4") as? String ?? ""
            this.customerId = response.value(forKey: "customer") as? String ?? ""
            this.cardId = response.value(forKey: "id") as? String ?? ""
            this.brand = response.value(forKey: "brand") as? String ?? ""
            this.lastFourDigits = response.value(forKey: "last_four_digit") as? String ?? ""
            
            return this
        }
        return CardModel()
    }
    
}
