//
//  CartItemModel.swift
//  #Restaurants
//
//  Created by Satveer on 19/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class CartItem {
    var id: Int = 0
    var dishId : Int = 0
    var dishName: String = ""
    var price: String = ""
    var qty: String = ""
    var currencySymbol: String =  UserModel.shared.currencyCode //"$"
    var addOns = [cartItemAddOn]()
    var customOptions = [CartItemCustomOption]()
    var type : DishType = .veg
    var status : Bool = true
    var mealIds : [Int]?
    var serving : CartItemCustomOption?
    var servingName : String?
    var showTypesOnApp = Bool()
    var dishTypes = [DishType]()
    var variations = [String:Any]()
    var variationName : String?
    var variationserving: String?
    var rating = [String:Any]()
    var getvariationdata = [String:Any]()
    
    func cartItem(_ dic: NSDictionary) -> CartItem {
        
        let cartItem = CartItem()
        cartItem.id = dic.value(forKey: "id") as? Int ?? 0
        cartItem.dishId = dic.value(forKey: "dish_id") as? Int ?? 0
        cartItem.dishName = dic.value(forKey: "item_name") as? String ?? ""
        cartItem.price = (String(format: "%.2f", dic.value(forKey: "price") as? Double ?? 0.0))
        cartItem.qty = String(dic.value(forKey: "qty") as? Int ?? 0)
        cartItem.addOns = cartItemAddOn().getCartItemAddons(dic.value(forKey: "addons") as? NSArray ?? NSArray())
        cartItem.customOptions = CartItemCustomOption().getCartItemCustomOptions(dic.value(forKey: "options") as? NSArray ?? NSArray())
        cartItem.type = dic.value(forKey: "veg") as? String ?? "" == "Veg" ? .veg : .nonVeg
        cartItem.status = dic.value(forKey: "status") as? Bool ?? true
        cartItem.mealIds = dic.value(forKey: "meal_ids") as? [Int] ?? [Int]()
        cartItem.serving = CartItemCustomOption().cartItemCustomOptions(dic.value(forKey: "serving") as? NSDictionary ?? NSDictionary())
        cartItem.servingName = dic.value(forKey: "serving") as? String
        cartItem.variations = dic.value(forKey: "Veriations") as? [String:Any] ?? [:]
        cartItem.showTypesOnApp = dic.value(forKey: "showtypeonapp") as? Bool ?? false
        cartItem.variationName = dic.value(forKey: "variationName") as? String ?? ""
        cartItem.variationserving = dic.value(forKey: "variationserving") as? String ?? ""
        cartItem.rating = dic.value(forKey: "dish_rating") as? [String:Any] ?? [:]
        cartItem.getvariationdata = dic.value(forKey: "variation") as? [String:Any] ?? [:]
        let dishTypes = dic.value(forKey: "veg") as? String ?? "" //veg is the key used in backend for dishTypes
        if cartItem.getvariationdata.count != 0 {
            
            let option_id = cartItem.getvariationdata["option_id"] as? Int ?? 0
            let variation_serving = cartItem.getvariationdata["variation_serving"] as? String ?? ""
            let variation_serving_title = cartItem.getvariationdata["variation_serving_title"] as? String ?? ""
            cartItem.variations = ["variation_serving_title":variation_serving_title,"option_id":option_id,"variation_serving":variation_serving]
        }
        
        for dishType in  dishTypes.components(separatedBy: ",")
        {
            let dishType  = DishType(rawValue: dishType) ?? .undefined
            cartItem.dishTypes.append(dishType)
        }
        
        return cartItem
    }
    
    func getCartItems(_ arr: NSArray) -> [CartItem] {
        var arrCartItem = [CartItem]()
        for cartItem in arr {
            let cartDic = self.cartItem(cartItem as? NSDictionary ?? NSDictionary())
            arrCartItem.append(cartDic)
        }
        return arrCartItem
    }
}

struct cartItemAddOn {
    var id : Int?
    var name : String?
    var addOnId : Int?
    var price : String?
    
    init(id : Int? , name : String?,addOnId : Int?,price : String?) {
        self.id = id
        self.name = name
        self.addOnId = addOnId
        self.price = price
    }
    init(){
        
    }
    
    func cartItemAddOns(_ dic: NSDictionary) -> cartItemAddOn {
        var item = cartItemAddOn()
        item.name = dic.value(forKey:"addon")as? String ?? ""
        item.id = dic.value(forKey:"id")as? Int ?? 0
        if let id = dic.value(forKey:"addon_id")as? String{
            item.addOnId = Int(id) ?? 0
        }else{
            item.addOnId = dic.value(forKey: "addon_id") as? Int ?? 0
        }
        item.price = dic.value(forKey:"price") as? String ?? "0.00"
        return item
    }
    
    func getCartItemAddons(_ arr: NSArray) -> [cartItemAddOn] {
        var addOnArr = [cartItemAddOn]()
        for addOns in arr {
            let attributeDic = self.cartItemAddOns(addOns as? NSDictionary ?? NSDictionary())
            addOnArr.append(attributeDic)
        }
        return addOnArr
    }
}

struct ratingAddOn {
    var id : Int?
    var OrderItemId : Int?
    var Rating : Int?
    
    init(id : Int? , orderItemId : Int?,rating : Int?) {
        self.id = id
        self.OrderItemId = orderItemId
        self.Rating = rating
        
    }
    init(){
        
    }
    
    func ratingAddOnAddOns(_ dic: NSDictionary) -> ratingAddOn {
        var item = ratingAddOn()
        
        item.id = dic.value(forKey:"id")as? Int ?? 0
        item.OrderItemId = dic.value(forKey:"order_item_id")as? Int ?? 0
        item.Rating = dic.value(forKey:"rating")as? Int ?? 0
        return item
    }
    
//    func getCartItemAddons(_ arr: NSArray) -> [ratingAddOn] {
//        var addOnArr = [ratingAddOn]()
//        for addOns in arr {
//            let attributeDic = self.ratingAddOnAddOns(addOns as? NSDictionary ?? NSDictionary())
//            addOnArr.append(attributeDic)
//        }
//        return addOnArr
//    }
}


struct CartItemCustomOption {
    var id : Int?
    var ingredient : String?
    var ingredientId : Int?
    var option : String?
    var optionId : Int?
    
    init(id : Int? , ingredient : String?,ingredientId : Int?,option : String?,optionId : Int?) {
        self.id = id
        self.ingredient = ingredient
        self.ingredientId = ingredientId
        self.option = option
        self.optionId = optionId
    }
    init(){
        
    }
    
    func cartItemCustomOptions(_ dic: NSDictionary) -> CartItemCustomOption {
        var item = CartItemCustomOption()
        item.ingredient = dic.value(forKey:"ingredient")as? String ?? ""
        item.id = dic.value(forKey:"id")as? Int ?? 0
        if let id = dic.value(forKey:"ingredient_id")as? String{
            item.ingredientId = Int(id) ?? 0
        }else{
            item.ingredientId = dic.value(forKey: "ingredient_id") as? Int ?? 0
        }
        
        item.option = dic.value(forKey:"option")as? String ?? ""
        
        
        // Change for reorder
        
        if let optionName = dic.value(forKey: "option_name") as? String{
            item.option = optionName
        }
        
        //
        
        if let id = dic.value(forKey:"option_id")as? String{
            item.optionId = Int(id) ?? 0
        }else{
            item.optionId = dic.value(forKey: "option_id") as? Int ?? 0
        }
        return item
    }
    
    func getCartItemCustomOptions(_ arr: NSArray) -> [CartItemCustomOption] {
        var customOptionArr = [CartItemCustomOption]()
        for options in arr {
            let attributeDic = self.cartItemCustomOptions(options as? NSDictionary ?? NSDictionary())
            customOptionArr.append(attributeDic)
        }
        return customOptionArr
    }
}
