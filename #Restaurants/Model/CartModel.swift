//
//  CartModel.swift
//  #Restaurants
//
//  Created by Satveer on 19/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

enum CartCase {
    case cartItem
    case coupon
    case specialInstruction
    case pickUptime
    case cartTotal
    case deliveryOptions
    case address
    case defaultCard
}

struct CartStruct {
    var type: CartCase
    var data: Any?
}

class Cart: NSObject {
    var arr = [CartStruct]()
    var isCouponApplied : Bool?
    var totalPrice : String?
    var couponCode : String?
    var couponDiscount : String?
    var tax : String?
    var id : Int?
    var grandTotal : String?
    var defaultCardId : String?
    var hallId : Int?
    var taxDescription = [TaxDescription]()
    var specialInstruction : String = ""
    var pickUptime: String = ""
    var timeSelected : String?
    var currencySymbol = UserModel.shared.currencyCode//FIXME:- TEST "$"
    var cartCount : Int = 0
    var cartTotal : Double = 0.0
    var status : Bool?
    var hallName : String?
    var isAcceptingOrder : Bool?
    var delivery_charges = ""
    var order_type = ""

    func addData() {
        arr.append(CartStruct(type: .cartItem, data: nil))
        arr.append(CartStruct(type: .coupon, data: nil))
        arr.append(CartStruct(type: .specialInstruction, data: nil))
        arr.append(CartStruct(type: .pickUptime, data: nil))
        arr.append(CartStruct(type: .cartTotal, data: nil))
        arr.append(CartStruct(type: .deliveryOptions, data: nil))
        arr.append(CartStruct(type: .address, data: nil))
    }
    
    func getData(_ response: NSDictionary?) {
        if let response = response {
            self.hallName = response.value(forKey: "hall_name") as? String ?? ""
            self.specialInstruction = ""
            self.id = response.value(forKey:"id")as? Int ?? 0
            self.totalPrice = (String(format: "%.2f", response.value(forKey: "cart_total") as? Double ?? 0.0))
            self.couponDiscount = (String(format: "%.2f", response.value(forKey: "discount") as? Double ?? 0.0))
            self.couponCode = response.value(forKey: "coupon_code") as? String ?? ""
       
            if self.couponCode != ""
            {
                self.isCouponApplied = true
            }else{
                self.isCouponApplied = false
            }
            self.tax = (String(format: "%.2f", response.value(forKey: "tax")as? Double ?? 0.0))
            self.grandTotal = (String(format: "%.2f", response.value(forKey: "grand_total")as? Double ?? 0.0))
            self.hallId = response.value(forKey: "hall_id") as? Int ?? 0
            self.defaultCardId = response.value(forKey: "default_card_id") as? String ?? ""
           
            if let charges = response["delivery_charges"] as? String {
                self.delivery_charges = charges
            }
            else if let charges = response["delivery_charges"] as? Float {
                self.delivery_charges = String(format: "%.2f", charges)
            } else {
                self.delivery_charges = "0"
            }

            if self.delivery_charges.last == "0" {
                self.delivery_charges = String(delivery_charges.dropLast())
            }
            

            self.order_type = response.value(forKey: "order_type") as? String ?? ""

            let arr = CartItem().getCartItems(response.value(forKey: "items") as? NSArray ?? NSArray())
            self.taxDescription = TaxDescription().getCartItemAddons(self.convertToDictionary(text: response.value(forKey: "tax_description") as? String ?? "") as NSArray? ?? NSArray())
            self.isAcceptingOrder = response.value(forKey: "is_accepting_orders") as? String == "yes" ? true : false
            self.cartTotal = 0.0
            self.cartCount = 0
            self.arr.append(CartStruct(type: .cartItem, data: arr))
            for cartItem in arr{
                self.cartCount += (Int(cartItem.qty) ?? 0)
                self.cartTotal = cartTotal + ((Double(cartItem.price) ?? 0.0) * (Double(cartItem.qty) ?? 0.0))
            }
            if arr.count != 0 {
                self.arr.append(CartStruct(type: .coupon, data: nil))
                self.arr.append(CartStruct(type: .specialInstruction, data: nil))
                self.arr.append(CartStruct(type: .pickUptime, data: nil))
                self.arr.append(CartStruct(type: .cartTotal, data: nil))
                self.arr.append(CartStruct(type: .deliveryOptions, data: nil))
                self.arr.append(CartStruct(type: .address, data: nil))
                self.arr.append(CartStruct(type: .defaultCard, data: nil))
            }
        }
    }

    func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

class Card : NSObject {
    
    var cardNumber = ""
    var customerId = ""
    var cardId = ""
    var isDefault : Bool = false
    var lastFourDigits : String = ""
    var brand : String = ""
    func getCard(_ response: NSDictionary?) -> Card{
        if let response = response {
           
           let this = Card()
           this.cardNumber = response.value(forKey: "last4") as? String ?? ""
           this.customerId = response.value(forKey: "customer") as? String ?? ""
           this.cardId = response.value(forKey: "id") as? String ?? ""
           this.brand = response.value(forKey: "brand") as? String ?? ""
           this.lastFourDigits = response.value(forKey: "last_four_digit") as? String ?? ""
            
           return this
        }
        return Card()
    }
}

struct TaxDescription{
    var taxPercentage : Double?
    var title : String?
    var amount = 0.0
    var currencySymbol = UserModel.shared.currencyCode//"$"
    
    init(taxPercentage : Double? , title : String?) {
        self.taxPercentage = taxPercentage
        self.title = title
    }
    init(){
        
    }
    
    func getTaxDescription(_ dic: NSDictionary) -> TaxDescription {

        var item = TaxDescription()
        item.taxPercentage = dic.value(forKey:"percentage")as? Double ?? 0.0
        item.title = dic.value(forKey:"title")as? String ?? ""
        item.amount =  dic.value(forKey:"amount") as? Double ?? 0.0
        
        return item
    }
    
    func getCartItemAddons(_ arr: NSArray) -> [TaxDescription] {
        var taxArr = [TaxDescription]()
        for tax in arr {
            let taxDic = self.getTaxDescription(tax as? NSDictionary ?? NSDictionary())
            taxArr.append(taxDic)
        }
        return taxArr
    }
}
