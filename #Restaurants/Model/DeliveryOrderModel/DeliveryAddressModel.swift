//
//  DeliveryAddressModel.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
class RestaurantAddressModel: NSObject {
    
    var address_line_1 = ""
    var address_line_2 = ""
    var city = ""
    var country = ""
    var hall_id = 0
    var rest_id = 0
    var state = ""
    var zip = ""
    var location_title = ""
    class func setRestaurntAddressData(dic: [String: Any])-> RestaurantAddressModel {
        let model = RestaurantAddressModel()
        model.address_line_1 = dic["address_line_1"] as? String ?? ""
        model.address_line_2 = dic["address_line_2"] as? String ?? ""
        model.city = dic["city"] as? String ?? ""
        model.country = dic["country"] as? String ?? ""
        model.hall_id = dic["hall_id"] as? Int ?? 0
        model.rest_id = dic["rest_id"] as? Int ?? 0
        model.state = dic["state"] as? String ?? ""
        model.zip = dic["zip"] as? String ?? ""
        model.location_title = dic["location_title"] as? String ?? ""
        return model
        
    }
}

class DeliveryAddressModel: NSObject {
    
    var city = ""
    var created_at = ""
    var default_address = 0
    var firstname = ""
    var email = ""
    var house_no = ""
    var id = 0
    var lastname = ""
    var lat = 0.0
    var lng = 0.0
    var state = ""
    var street1 = ""
    var street2 = ""
    var title = ""
    var updated_at = ""
    var user_id = 0
    var zipcode = ""
    let userdefault = UserDefaults.standard
    
    func setDeliveryAddressData(array: NSArray)-> [DeliveryAddressModel] {
        
        var addresses = [DeliveryAddressModel]()
        for i in 0..<array.count {
            let dic = array[i] as? NSDictionary ?? [:]
            print(dic)
            let model = DeliveryAddressModel()
            model.city = dic["city"] as? String ?? ""
            model.created_at = dic["created_at"] as? String ?? ""
            model.default_address = dic["default_address"] as? Int ?? 0
            model.firstname = dic["firstname"] as? String ?? ""
            model.house_no = dic["house_no"] as? String ?? ""
            model.id = dic["id"] as? Int ?? 0
            model.lastname = dic["lastname"] as? String ?? ""
            if let lat = dic["lat"] as? Double, let lng = dic["lng"] as? Double {
                model.lat = lat
                model.lng = lng
            }
            else {
                model.lat = Double(dic["lat"] as? String ?? "") ?? 0.0
                model.lng = Double(dic["lng"] as? String ?? "") ?? 0.0
            }
            model.state = dic["state"] as? String ?? ""
            model.street1 = dic["street1"] as? String ?? ""
            model.street2 = dic["street2"] as? String ?? ""
            model.title = dic["title"] as? String ?? ""
            model.updated_at = dic["updated_at"] as? String ?? ""
            model.user_id = dic["user_id"] as? Int ?? 0
            model.zipcode = dic["zipcode"] as? String ?? ""
            model.email = dic["email"] as? String ?? ""
            
            addresses.append(model)
            
        }
        return addresses
        
    }
    
    func setDefaultAddressData(dic: NSDictionary)-> DeliveryAddressModel {
        let model = DeliveryAddressModel()
        model.city = dic["city"] as? String ?? ""
        model.created_at = dic["created_at"] as? String ?? ""
        model.default_address = dic["default_address"] as? Int ?? 0
        model.firstname = dic["firstname"] as? String ?? ""
        model.house_no = dic["house_no"] as? String ?? ""
        model.id = dic["id"] as? Int ?? 0
        model.lastname = dic["lastname"] as? String ?? ""
        if let lat = dic["lat"] as? Double, let lng = dic["lng"] as? Double {
            model.lat = lat
            model.lng = lng
        }
        else {
            model.lat = Double(dic["lat"] as? String ?? "") ?? 0.0
            model.lng = Double(dic["lng"] as? String ?? "") ?? 0.0
        }
        model.state = dic["state"] as? String ?? ""
        model.street1 = dic["street1"] as? String ?? ""
        model.street2 = dic["street2"] as? String ?? ""
        model.title = dic["title"] as? String ?? ""
        model.updated_at = dic["updated_at"] as? String ?? ""
        model.user_id = dic["user_id"] as? Int ?? 0
        model.zipcode = dic["zipcode"] as? String ?? ""
        model.email = dic["email"] as? String ?? ""
        
        return model
        
    }
    
    func saveSelectedDeliveryAddress(address: DeliveryAddressModel) {
        userdefault.set(address.city, forKey: "address_city")
        userdefault.set(address.created_at, forKey: "address_created_at")
        userdefault.set(address.default_address, forKey: "address_default_address")
        userdefault.set(address.firstname, forKey: "address_firstname")
        userdefault.set(address.house_no, forKey: "address_house_no")
        userdefault.set(address.id, forKey: "address_id")
        userdefault.set(address.lastname, forKey: "address_lastname")
        userdefault.set(address.lat, forKey: "address_lat")
        userdefault.set(address.lng, forKey: "address_lng")
        userdefault.set(address.state, forKey: "address_state")
        userdefault.set(address.street1, forKey: "address_street1")
        userdefault.set(address.street2, forKey: "address_street2")
        userdefault.set(address.title, forKey: "address_title")
        userdefault.set(address.updated_at, forKey: "address_updated_at")
        userdefault.set(address.user_id, forKey: "address_user_id")
        userdefault.set(address.zipcode, forKey: "address_zipcode")
        userdefault.set(address.email, forKey: "address_email")
    }
    
    func getSavedDeliveryAddress() -> DeliveryAddressModel {
        let model = DeliveryAddressModel()
        model.city =  userdefault.value(forKey: "address_city") as? String ?? ""
        model.created_at = userdefault.value(forKey: "address_created_at") as? String ?? ""
        model.default_address = userdefault.value(forKey: "address_default_address") as? Int ?? 0
        model.firstname = userdefault.value(forKey: "address_firstname") as? String ?? ""
        model.house_no = userdefault.value(forKey: "address_house_no") as? String ?? ""
        model.id = userdefault.value(forKey: "address_id") as? Int ?? 0
        model.lastname = userdefault.value(forKey: "address_lastname") as? String ?? ""
        model.lat = userdefault.value(forKey: "address_lat") as? Double ?? 0.0
        model.lng = userdefault.value(forKey: "address_lng") as? Double ?? 0.0
        model.state = userdefault.value(forKey: "address_state") as? String ?? ""
        model.street1 = userdefault.value(forKey: "address_street1") as? String ?? ""
        model.street2 = userdefault.value(forKey: "address_street2") as? String ?? ""
        model.title = userdefault.value(forKey: "address_title") as? String ?? ""
        model.updated_at = userdefault.value(forKey: "address_updated_at") as? String ?? ""
        model.user_id = userdefault.value(forKey: "address_user_id") as? Int ?? 0
        model.zipcode = userdefault.value(forKey: "address_zipcode") as? String ?? ""
        model.email = userdefault.value(forKey: "address_email") as? String ?? ""
        
        return model
    }
    
    func removeSavedDeliveryAddress() {
        userdefault.removeObject(forKey: "address_city")
        userdefault.removeObject(forKey: "address_created_at")
        userdefault.removeObject(forKey: "address_default_address")
        userdefault.removeObject(forKey: "address_firstname")
        userdefault.removeObject(forKey: "address_house_no")
        userdefault.removeObject(forKey: "address_id")
        userdefault.removeObject(forKey: "address_lastname")
        userdefault.removeObject(forKey: "address_lat")
        userdefault.removeObject(forKey: "address_lng")
        userdefault.removeObject(forKey: "address_state")
        userdefault.removeObject(forKey: "address_street1")
        userdefault.removeObject(forKey: "address_street2")
        userdefault.removeObject(forKey: "address_title")
        userdefault.removeObject(forKey: "address_updated_at")
        userdefault.removeObject(forKey: "address_user_id")
        userdefault.removeObject(forKey: "address_zipcode")
        userdefault.removeObject(forKey: "address_email")
    }
}
