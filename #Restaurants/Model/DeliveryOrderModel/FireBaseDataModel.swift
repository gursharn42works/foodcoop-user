//
//  FireBaseDataModel.swift
//  #Restaurants
//
//  Created by Satveer on 11/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class FireBaseDataModel {
    var angle = 0.0
    var lat = 0.0
    var lng = 0.0
    
    func setFireBaseModelData(dic: NSDictionary) -> FireBaseDataModel {
        let mdoel = FireBaseDataModel()
        mdoel.angle = dic["angle"] as? Double ?? 0.0
        mdoel.lat = dic["lat"] as? Double ?? 0.0
        mdoel.lng = dic["lng"] as? Double ?? 0.0
        return mdoel
    }
}
