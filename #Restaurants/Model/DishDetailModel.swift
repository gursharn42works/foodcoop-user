//
//  DishDetailModel.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

struct SubCatagory{
    var id : Int?
    var name : String?
    var dishes : [DishModel]?
    
    func subCatagoryItems(_ dic: NSDictionary) -> SubCatagory{
        var subCatagoryItem = SubCatagory()
        subCatagoryItem.id = dic.value(forKey: "id") as? Int ?? 0
        subCatagoryItem.name = dic.value(forKey: "name") as? String ?? ""
        subCatagoryItem.dishes = DishModel().getDishItems(dic.value(forKey: "dishes") as? NSArray ?? NSArray())
        return subCatagoryItem
    }
    
    func getHomeCatagoryDetail(_ arr: NSArray) -> [SubCatagory] {
        var subCatagoryItemsArr = [SubCatagory]()
        for subCatagoryItem in arr {
            let catagoryDic = self.subCatagoryItems(subCatagoryItem as? NSDictionary ?? NSDictionary())
            subCatagoryItemsArr.append(catagoryDic)
        }
        return subCatagoryItemsArr
    }
}

class DishModel : NSObject{
    var id : Int?
    var name : String?
    var price : String?
    var type : DishType = .veg
    
    var dishTypes = [DishType]()
    var showTypesOnApp = Bool()
    var preperationTime : String?
    var productImage : String?
    var productDescription : String?
    var longDescription: String?
    var ingredients : String?
    var isFavourite : Bool?
    var calories : Int?
    var attributes : [AttributeModelModel]?
    var isBestSeller = false
    var addons = [AddOn]()
    var customizations = [Customization]()
    var hallId : Int?
    var hallName : String?
    var isDishAvailable : Bool?
    var mealIds : [Int]?
    var mealNames : [String]?
    // var isMealClosed : Bool?
    var serving : [DishServingModel]?
    var isCustomizable : Bool?
    var isAcceptingOrder : Bool?
    var typeCommaSeparated : String?
    var dish_type = ""
    var variationService = ""
    var variations = [VariationsModel]()
    
    init(id : Int? , name : String?,price : String?,type : DishType?,preperationTime : String?,productImage : String?,isFavourite : Bool?,calories : Int? , AttributeModelModels : [AttributeModelModel]?,productDescription : String?,isBestSeller : Bool?,addons : [AddOn],customizations : [Customization],hallId : Int?,hallName : String?,ingredients : String?,isDishAvailable : Bool?,mealIds : [Int]?,mealNames : [String]?,serving : [DishServingModel]?,isCustomizable : Bool?){
        self.id = id
        self.name = name
        self.price = price
        self.type = .veg
        self.preperationTime = preperationTime
        self.productImage = productImage
        self.isFavourite = isFavourite
        self.calories = calories
        self.attributes = AttributeModelModels
        self.productDescription = productDescription
        self.isBestSeller = isBestSeller ?? false
        self.customizations = customizations
        self.addons = addons
        self.hallId = hallId
        self.hallName = hallName
        self.ingredients = ingredients
        self.isDishAvailable = isDishAvailable
        self.mealIds = mealIds
        self.mealNames = mealNames
        self.serving = serving
        self.isCustomizable = isCustomizable
        
    }
    override init() {
        super.init()
    }
    
    func dishItem(_ dic: NSDictionary) -> DishModel {
        let item = DishModel()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.name = dic.value(forKey: "name") as? String ?? ""
        item.price = (String(format: "%.2f", dic.value(forKey: "price") as? Double ?? 0.0))
        item.typeCommaSeparated = dic.value(forKey: "veg") as? String ?? ""
        
        if item.typeCommaSeparated == "Veg"{
            item.type = .veg
        }else if item.typeCommaSeparated == "Non-Veg"{
            item.type = .nonVeg
        }else{
            item.type = .containsEgg
        }
        
        
        //Changed Dish Type
        item.showTypesOnApp = dic.value(forKey: "showtypeonapp") as? Bool ?? false
        
        for dishType in  item.typeCommaSeparated!.components(separatedBy: ",")
        {
            let dishType  = DishType(rawValue: dishType) ?? .undefined
            item.dishTypes.append(dishType)
        }
        //
        
        item.mealIds = dic.value(forKey: "meal_ids") as? [Int] ?? [0]
        item.mealNames = dic.value(forKey: "meal_names") as? [String] ?? [""]
        item.calories = dic.value(forKey: "calories") as? Int ?? 0
        item.isFavourite = dic.value(forKey: "favorited") as? Bool ?? false
        item.preperationTime = dic.value(forKey: "preparation_time") as? String ?? ""
        var image = dic.value(forKey: "image") as? String ?? ""
        if image == "https://restaurantordersapp.com/app/"{
            image = ""
        }
        
        //let imageNoSpace = image //.replacingOccurrences(of: " ", with: "%20")
        item.productImage = image
        item.attributes = AttributeModelModel().getDishAttributeModelModel(dic.value(forKey: "AttributeModelModels") as? NSArray ?? NSArray())
        item.productDescription = dic.value(forKey: "description")as? String ?? ""
        item.isBestSeller = dic.value(forKey: "best_seller")as? Bool ?? false
        item.customizations = Customization().getdishCustomization(dic.value(forKey: "customizable_options") as? NSArray ?? NSArray())
        item.addons = AddOn().getDishAddOns(dic.value(forKey: "addons") as? NSArray ?? NSArray())
        item.hallId = dic.value(forKey: "hall_id") as? Int ?? 0
        item.hallName = dic.value(forKey: "hall_name") as? String ?? ""
        item.ingredients = dic.value(forKey: "ingredients") as? String ?? ""
        item.isDishAvailable = dic.value(forKey: "status") as? Bool ?? false
        
        item.isCustomizable = dic.value(forKey: "customizable") as? Bool
        item.serving = DishServingModel().getDishServingModelOptions( dic.value(forKey: "serving") as? NSArray ?? NSArray())
        let defaultServing = DishServingModel(id: 0, price: item.price ?? "", serving: (dic.value(forKey: "serving_default") as? String ?? ""), isDefault: true)
        item.serving?.insert(defaultServing, at: 0)
        item.isAcceptingOrder = dic.value(forKey: "is_accepting_orders") as? String == "yes" ? true : false
        
        let dishAvailable = item.isDishAvailable ?? false
        
        if !dishAvailable{
            print("Unavailable dish: \(self.name)")
        }
        
        item.longDescription = dic.value(forKey: "long_description") as? String ?? ""
        item.dish_type = dic.value(forKey: "dish_type") as? String ?? ""
        item.variationService =  dic.value(forKey: "variation_serving") as? String ?? ""
        item.variations = VariationsModel.setData(array: dic.value(forKey: "variations") as? NSArray ?? [])
        
        item.name  =  item.name?.capitalizeFirstInEachWord()
        
        return item
    }
    
    func getDishItems(_ arr: NSArray) -> [DishModel] {
        var dishItems = [DishModel]()
        for item in arr {
            let dishesDic = self.dishItem(item as? NSDictionary ?? NSDictionary())
            //if let status = dishesDic.isDishAvailable, status == true{
                dishItems.append(dishesDic)
            //}
        }
        return dishItems
    }

//    func totalPrice() -> Double{
//
//        var prizeForOptions = 0.0
//
//        var isMultiSelect = false
//        for customization in self.customizations {
//            if customization.type == .multipleSelection {
//                isMultiSelect = true
//            }
//
//            prizeForOptions += customization.priceOfSelectedOptions()
//        }
//
//        var servingPrice = 0.0
//
//        if isMultiSelect == false {
//            for i in 0..<(self.serving?.count ?? 0) {
//                if self.serving?[i].isDefault ?? false{
//                    servingPrice = Double(self.serving?[i].price ?? "") ?? 0.0
//                }
//            }
//        }
//
////        let actualPrice = servingPrice
////
////        var variationPrice = self.calculateVariationServingPrice()
////        if variationPrice >= actualPrice {
////            variationPrice = variationPrice-Double(actualPrice)
////        }
//
//        if servingPrice != 0 {
//            prizeForOptions = servingPrice
//            servingPrice = 0
//        }
//
//
//        return /*(Double(self.price ?? "") ?? 0.0) + */ prizeForOptions + servingPrice //+ variationPrice
//    }
    
    func calculateVariationServingPrice() -> (Double, Bool) {
        if variations.count > 0 {
            var isVariations = false

            let servings = self.variations.filter({$0.servings.count > 0 })
            
            if servings.count == 0 {
                var price = 0.0
                for variation in self.variations {
                    if variation.isDefault {
                        price += variation.priceInDouble
                        isVariations = true
                    }
                }
                return (price,isVariations)
            } else {
                var price = 0.0
                for variation in self.variations {
                    for serve in variation.servings {
                        if serve.isDefault {
                            price += serve.priceInDouble
                            isVariations = true
                        }
                    }
                }
                return (price,isVariations)
            }
        }
        return (0.0,true)
    }
    
    func totalPrice() -> Double{
        
        var prizeForOptions = 0.0
        
        for customization in self.customizations {
            prizeForOptions += customization.priceOfSelectedOptions()
        }
        
        let servings = self.variations.filter({$0.servings.count > 0 })

        var servingPrice = 0.0
        
        let variationPrice = self.calculateVariationServingPrice()

        if servings.count == 0 && variationPrice.0 == 0 && self.variations.count == 0 {
            for i in 0..<(self.serving?.count ?? 0){
                if self.serving?[i].isDefault ?? false{
                    servingPrice = Double(self.serving?[i].price ?? "") ?? 0.0
                }
            }
        }
        
        
        return /*(Double(self.price ?? "") ?? 0.0) + */ prizeForOptions + servingPrice + variationPrice.0
    }

    func servingPrice() -> Double{
        var servingPrice = 0.0
        
        if self.variations.count == 0 {
            for i in 0..<(self.serving?.count ?? 0){
                if self.serving?[i].isDefault ?? false{
                    servingPrice = Double(self.serving?[i].price ?? "") ?? 0.0
                }
            }
        }
        
        return servingPrice
    }
//    func servingPrice() -> Double{
//        var servingPrice = 0.0
//
//        for i in 0..<(self.serving?.count ?? 0){
//            if self.serving?[i].isDefault ?? false{
//                servingPrice = Double(self.serving?[i].price ?? "") ?? 0.0
//            }
//        }
//
//        return servingPrice
//    }
    
    func areAllRequiredCustomizationsSelected() -> (success: Bool, message: String) {
        
        for customization in self.customizations {
            
            let requiredCustomizationsSelected = customization.areMinimumCustomizationsSelected()
            
            if !requiredCustomizationsSelected{
                return (false, Messages.selectRequiredCustomizations)
            }
        }
        
        return (true, "")
    }
    
//    func checkRequiredVariationsSelected()-> String {
//        
//        if self.variations.count > 0 {
//            let servings = self.variations.filter({$0.servings.count > 0 })
//            let variationsSelected = self.variations.filter({$0.isDefault })
//            let servingSelected = servings.filter({$0.isDefault })
//
//            if servings.count == 0 && variationsSelected.count == 0 {
//                return Messages.selectRequiredCustomizations
//            } else if servings.count > 0 && servingSelected.count == 0 {
//                return Messages.selectRequiredCustomizations
//            }
//        }
//        
//        return ""
//
//    }
}

struct AttributeModelModel {
    var id : Int?
    var name : String?
    var value = [String]()
    
    init(id : Int? , name : String?,value : [String]) {
        self.id = id
        self.name = name
        self.value = value
    }
    init(){
        
    }
    
    func dishItemsAtributes(_ dic: NSDictionary) -> AttributeModelModel {
        var item = AttributeModelModel()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.name = dic.value(forKey: "name") as? String ?? ""
        let values = dic.value(forKey: "values") as? [String] ?? [String]()
        for str in values{
            item.value.append(str)
        }
        return item
    }
    
    func getDishAttributeModelModel(_ arr: NSArray) -> [AttributeModelModel] {
        var dishAttributeModelModelArr = [AttributeModelModel]()
        for AttributeModelModel in arr {
            let AttributeModelModelDic = self.dishItemsAtributes(AttributeModelModel as? NSDictionary ?? NSDictionary())
            dishAttributeModelModelArr.append(AttributeModelModelDic)
        }
        return dishAttributeModelModelArr
    }
}

struct AddOn {
    var id : Int?
    var addon : String?
    var price : String?
    var isSelected : Bool?
    
    init(id : Int? , addon : String?,price : String?) {
        self.id = id
        self.addon = addon
        self.price = price
    }
    init(){
        
    }
    
    func dishAddOns(_ dic: NSDictionary) -> AddOn {
        var item = AddOn()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.addon = dic.value(forKey: "addon") as? String ?? ""
        item.price = (String(format: "%.2f", dic.value(forKey: "price") as? Double ?? 0.0))
        return item
    }
    
    func getDishAddOns(_ arr: NSArray) -> [AddOn] {
        var dishAddOnsArr = [AddOn]()
        for addOn in arr {
            let addOnDic = self.dishAddOns(addOn as? NSDictionary ?? NSDictionary())
            dishAddOnsArr.append(addOnDic)
        }
        return dishAddOnsArr
    }
}

enum CustomizationGroupType: String{
    case customization = "Customization"
    case addOn = "Addon"
}

struct Customization {
    enum CustomizationType {
        case singleSelection
        case multipleSelection
    }
    
    var id : Int?
    var ingredient : String?
    var type : CustomizationType?
    var groupType: CustomizationGroupType?
    var maxSelectionAllowed : Int?
    var options = [Option]()
    var price : String?
    var allowPricePerItem : Bool?
    
    
    init(id : Int? , ingredient : String?,type : CustomizationType?,maxSelectionAllowed : Int?,price : String?,allowPricePerItem : Bool?) {
        self.id = id
        self.ingredient = ingredient
        self.type = type
        self.maxSelectionAllowed = maxSelectionAllowed
        self.price = price
        self.allowPricePerItem = allowPricePerItem
    }
    init(){
        
    }
    
    func dishCustomization(_ dic: NSDictionary) -> Customization {
        var item = Customization()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.ingredient = dic.value(forKey: "ingredient") as? String ?? ""
        let maxSelection = dic.value(forKey: "max_allowed_options") as? Int ?? 0
        item.options = Option().getCustomizationOptions(dic.value(forKey: "options") as? NSArray ?? NSArray())
        if item.options.count < maxSelection
        {
            item.maxSelectionAllowed = item.options.count
        }else{
            item.maxSelectionAllowed = maxSelection
        }
        item.type = item.maxSelectionAllowed ?? 0 > 1 ? .multipleSelection : .singleSelection
        item.groupType = CustomizationGroupType(rawValue: dic.value(forKey: "group_type") as? String ?? "")
        item.allowPricePerItem = dic.value(forKey: "allow_price_per_item") as? Bool ?? false
        if item.type == .singleSelection{
            if item.allowPricePerItem ?? false{
                item.type = .multipleSelection
            }
        }
        
        item.price = (String(format: "%.2f", dic.value(forKey: "price") as? Double ?? 0.0))
        
        if item.groupType == CustomizationGroupType.addOn && (item.maxSelectionAllowed ?? 0 == 0){
            item.type = .multipleSelection
            item.maxSelectionAllowed  = 10
        }
        
        //        for var customOption in item.options{
        //            if customOption.isDefault ?? false{
        //                customOption.isSelected = true
        //            }
        //        }
        
        
        return item
    }
    
    func getdishCustomization(_ arr: NSArray) -> [Customization] {
        var dishCustomizationArr = [Customization]()
        for customization in arr {
            let customizationDic = self.dishCustomization(customization as? NSDictionary ?? NSDictionary())
            dishCustomizationArr.append(customizationDic)
        }
        return dishCustomizationArr
    }
    
//    func priceOfSelectedOptions() -> Double {
//
//        var selectedOptions = [Option]()
//
//        var priceForSelectedOptions = 0.0
//
//        for option in self.options{
//            if option.isDefault ?? false{
//                selectedOptions.append(option)
//            }
//        }
//
//        selectedOptions = selectedOptions.sorted(by: { (option1, option2) -> Bool in
//            return option1.price?.compare(option2.price ?? "") == ComparisonResult.orderedDescending
//        })
//
//        let selectedOptionCount = selectedOptions.count
//
////        print("groupType: \(groupType?.rawValue)")
//
//        if groupType != CustomizationGroupType.addOn  && selectedOptions.count >= self.maxSelectionAllowed ?? 0 &&  groupType != CustomizationGroupType.customization{
//
//            for _ in 0..<(self.maxSelectionAllowed ?? 0){
//                if selectedOptions.count > 0{
//                    selectedOptions.removeLast()
//                }
//            }
//        }
//
//        for option in selectedOptions{
//
//            if groupType != CustomizationGroupType.addOn && groupType != CustomizationGroupType.customization {
//                if selectedOptionCount > (self.maxSelectionAllowed ?? 0){
//                    priceForSelectedOptions += option.priceInDouble ?? 0.0
//                }
//            }else{
//                priceForSelectedOptions += option.priceInDouble ?? 0.0
//            }
//        }
//
//        return priceForSelectedOptions
//    }
    
    func priceOfSelectedOptions() -> Double {
        
        var selectedOptions = [Option]()
        
        var priceForSelectedOptions = 0.0
        
        for option in self.options{
            if option.isDefault ?? false{
                selectedOptions.append(option)
            }
        }
        
//        selectedOptions = selectedOptions.sorted(by: { (option1, option2) -> Bool in
//            return "\(option1.price!)".compare("\(option2.priceInDouble!)") == ComparisonResult.orderedAscending
//        })
        
        
        selectedOptions = selectedOptions.sorted(by: { $0.priceInDouble! > $1.priceInDouble! })

        let selectedOptionCount = selectedOptions.count
        
//        print("groupType: \(groupType?.rawValue)")
        
        
//        var price = [Double]()
//        for option in selectedOptions {
//            price.append(option.priceInDouble ?? 0)
//        }
//        let maxValue = price.max()
//        print(maxValue)
//
//        if let index = selectedOptions.firstIndex(where: {$0.priceInDouble! == maxValue!}) {
//
//        }
        
        for option in selectedOptions {
            print(option.priceInDouble)
        }

        if groupType != CustomizationGroupType.addOn  && selectedOptions.count >= self.maxSelectionAllowed ?? 0{
            
            for _ in 0..<(self.maxSelectionAllowed ?? 0){
                if selectedOptions.count > 0{
                    selectedOptions.removeLast()
                }
            }
        }
        
        for option in selectedOptions{
            
            if groupType != CustomizationGroupType.addOn{
                if selectedOptionCount > (self.maxSelectionAllowed ?? 0){
                    priceForSelectedOptions += option.priceInDouble ?? 0.0
                }
            }else{
                priceForSelectedOptions += option.priceInDouble ?? 0.0
            }
        }
        
        return priceForSelectedOptions
    }
    
    func areMinimumCustomizationsSelected() -> Bool{
        
        if groupType != CustomizationGroupType.addOn {
            
            var selecetedItems = 0
            
            for option in self.options{
                if option.isDefault ?? false && option.isAvailable ?? false {
                    selecetedItems += 1
                }
            }
            
            print("selecetedItems: \(selecetedItems) self.maxSelectionAllowed: \(self.maxSelectionAllowed)")
            
            if selecetedItems < (self.maxSelectionAllowed ?? 0){
                return false
            }
        }
        
        return true
    }
    
}

struct Option {
    var id : Int?
    var name : String?
    var isDefault : Bool?
    var price : String?
    var priceInDouble : Double?
    var isAvailable : Bool?
    
    init(id : Int? , name : String?,isDefault : Bool?, price : String?) {
        self.id = id
        self.name = name
        self.isDefault = isDefault
        self.price = price
    }
    init(){
        
    }
    
    func dishCustomizationOption(_ dic: NSDictionary) -> Option {
        var item = Option()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.name = dic.value(forKey: "name") as? String ?? ""
        item.isDefault = dic.value(forKey: "default_option") as? Bool ?? false
        
        item.priceInDouble =  dic.value(forKey: "price") as? Double ?? 0.0
        
        item.price =  (item.priceInDouble ?? 0.0) > 0.0 ? (String(format: "\(UserModel.shared.currencyCode)%.2f", item.priceInDouble ?? 0.0)) : ""
        item.isAvailable = dic.value(forKey: "available") as? Bool ?? false
        
        return item
    }
    
    func getCustomizationOptions(_ arr: NSArray) -> [Option] {
        var dishCustomizationOptionArr = [Option]()
        for option in arr {
            let optionDic = self.dishCustomizationOption(option as? NSDictionary ?? NSDictionary())
            dishCustomizationOptionArr.append(optionDic)
        }
        return dishCustomizationOptionArr
    }
}

struct DishServingModel {
    
    var id : Int?
    var price : String?
    var serving : String?
    var isDefault : Bool?
    
    func DishServingModelOption(_ dic: NSDictionary) -> DishServingModel {
        var item = DishServingModel()
        item.id = dic.value(forKey: "id") as? Int ?? 0
        item.serving = dic.value(forKey: "serving") as? String ?? ""
        item.price = (String(format: "%.2f", dic.value(forKey: "price") as? Double ?? 0.0))
        item.isDefault = false
        return item
    }
    
    func getDishServingModelOptions(_ arr: NSArray) -> [DishServingModel] {
        var dishCustomizationOptionArr = [DishServingModel]()
        for option in arr {
            let optionDic = self.DishServingModelOption(option as? NSDictionary ?? NSDictionary())
            dishCustomizationOptionArr.append(optionDic)
        }
        return dishCustomizationOptionArr
    }
}

class VariationsModel: NSObject {
    var id = 0
    var price = ""
    var priceInDouble = 0.0
    var servings = [Servings]()
    var variation = ""
    var isDefault = false
   class func setData(array: NSArray)-> [VariationsModel] {
        
        var modelArr = [VariationsModel]()
        for i in 0..<array.count {
            let dic = array[i] as? [String: Any] ?? [:]
            let model = VariationsModel()
            model.id = dic["id"] as? Int ?? 0
            let price = dic["price"] as? Double ?? 0.0
            model.price = (String(format: "\(UserModel.shared.currencyCode)%.2f", price))
            model.priceInDouble = price
            model.servings = Servings.setData(array: dic["servings"] as? NSArray ?? [])
            model.variation = dic["variation"] as? String ?? ""
//            model.isDefault = i==0 ? true:false
            modelArr.append(model)
        }
        
        return modelArr
    }
}

class Servings: NSObject {
    var id = 0
    var price = ""
    var priceInDouble = 0.0
    var serving = ""
    var isDefault = false

    class func setData(array: NSArray)-> [Servings] {
        
        var modelArr = [Servings]()
        for i in 0..<array.count {
            let dic = array[i] as? [String: Any] ?? [:]
            let model = Servings()
            model.id = dic["id"] as? Int ?? 0
            let price = dic["price"] as? Double ?? 0.0
            model.price = (String(format: "\(UserModel.shared.currencyCode)%.2f", price))
            model.priceInDouble = price
            model.serving = dic["serving"] as? String ?? ""
           // model.isDefault = i==0 ? true:false
            modelArr.append(model)
        }
        
        return modelArr
    }
}


