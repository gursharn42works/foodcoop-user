//
//  FavoriteDishModel.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FavoriteDishModel: NSObject {
    var id = 0
    var name = ""
    var price = 0.0
    var veg = ""
    var preparationTime = ""
    var image = ""
    var favorited = false
    var calories = ""
    var dishDescription = ""
    var ingredients = ""
    var bestSeller = 0
    var customizable = false
    var showtypeonapp = 0
    var status = 0
    
    func setFavoriteItemsData(array: NSArray) -> [FavoriteDishModel] {
        
        var modelArr = [FavoriteDishModel]()
        
        for i in 0..<array.count {
            let dic = array[i] as? [String: Any] ?? [:]
            let model = FavoriteDishModel()

            model.id = dic["id"] as? Int ?? 0
            model.name = dic["name"] as? String ?? ""
            model.price = dic["price"] as? Double ?? 0.0
            
            model.veg = dic["veg"] as? String ?? ""
            model.preparationTime = dic["preparation_time"] as? String ?? ""
            model.image = dic["image"] as? String ?? ""
            model.favorited = dic["favorited"] as? Bool ?? false
            model.calories = dic["calories"] as? String ?? ""
            model.dishDescription = dic["description"] as? String ?? ""
            model.ingredients = dic["ingredients"] as? String ?? ""
            model.bestSeller = dic["best_seller"] as? Int ?? 0
            model.customizable = dic["customizable"] as? Bool ?? false
            model.showtypeonapp = dic["showtypeonapp"] as? Int ?? 0
            model.status = dic["status"] as? Int ?? 0
            
            modelArr.append(model)
        }
        return modelArr
    }
}
