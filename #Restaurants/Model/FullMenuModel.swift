//
//  FullMenuModel.swift
//  #Restaurants
//
//  Created by Satveer on 29/07/20.
//  Copyright © 2020 42works. All rights reserved.
//


import Foundation

class FullMenuModel: NSObject {
    var categories = [MenuCategories]()
    var filters = MenuFilters()
    var RestDetails = RestaurantDetails()
    
    func setFullMenuData(data: [String: Any]) -> FullMenuModel{

        let categories = data["categories"] as? NSArray ?? []
        let filters = data["filters"] as? NSDictionary ?? [:]
        let restDetails = data["restaurant"] as? NSDictionary ?? [:]
        
        let model = FullMenuModel()
        model.categories = self.getCategories(categoriesArr: categories)
        model.filters = self.getFilters(filters: filters)
        model.RestDetails = self.getRestaurantDetails(restDetailDic: restDetails)
        
        return model
    }
    
    func getRestaurantDetails(restDetailDic: NSDictionary) -> RestaurantDetails{

        let cateArr = RestaurantDetails()
        let catDic = restDetailDic
            
        cateArr.id = catDic["id"] as? Int ?? 0
        cateArr.name = catDic["name"] as? String ?? ""
        let image = catDic["logo"] as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        cateArr.logo = imageNoSpace
        let thumb = catDic["thumb"] as? String ?? ""
        let thumbNoSpace = thumb.replacingOccurrences(of: " ", with: "%20")
        cateArr.thumb = thumbNoSpace
        cateArr.rating = catDic["rating"] as? Float ?? 0.0
        cateArr.is_accepting_orders = catDic["is_accepting_orders"] as? String ?? ""
        cateArr.city = catDic["city"] as? String ?? ""
        
        return cateArr
    }
    
    
    func getCategories(categoriesArr: NSArray) -> [MenuCategories]{

        var cateArr = [MenuCategories]()

        for i in 0..<categoriesArr.count {
            let catDic = categoriesArr[i] as? NSDictionary ?? [:]
            
            let category = MenuCategories()
            
            category.id = catDic["id"] as? Int ?? 0
            category.name = catDic["name"] as? String ?? ""
            category.descript_ion = catDic["description"] as? String ?? ""
            category.dishes = self.getDishes(dishes: catDic["dishes"] as? NSArray ?? [])

            cateArr.append(category)
        }
        
        return cateArr
    }
    
    
    func getFilters(filters: NSDictionary) -> MenuFilters {
        var sorts = [String]()
        let sortArr = filters["sort"] as? NSArray ?? []
        for i in 0..<sortArr.count {
            sorts.append(sortArr[i] as? String ?? "")
        }
        let mdoel = MenuFilters()
        mdoel.sort = sorts
        return mdoel
    }
    
    func getDishes(dishes: NSArray) -> [MenuDishes] {
        
        var menuDishesArr = [MenuDishes]()
        
        for i in 0..<dishes.count {
            let dic = dishes[i] as? NSDictionary ?? [:]
            
            let menuDishes = MenuDishes()
            
            menuDishes.attributes = self.getAttributes(attributes: dic["attributes"] as? NSArray ?? [])
            
            menuDishes.best_seller = dic["best_seller"] as? Int ?? 0
            menuDishes.calories = dic["calories"] as? String ?? ""
            menuDishes.customizable = dic["customizable"] as? Int ?? 0 == 1 ? true:false
            menuDishes.descript_ion = dic["description"] as? String ?? ""
            menuDishes.favorited = dic["favorited"] as? Int ?? 0 == 1 ? true:false
            menuDishes.hall_id = dic["hall_id"] as? Int ?? 0
            menuDishes.id = dic["id"] as? Int ?? 0
            menuDishes.image = dic["image"] as? String ?? ""
            menuDishes.ingredients = dic["ingredients"] as? String ?? ""
            menuDishes.meal_ids = dic["meal_ids"] as? NSArray ?? []
            menuDishes.meal_names = dic["meal_names"] as? NSArray ?? []
            menuDishes.name = dic["name"] as? String ?? ""
            menuDishes.preparation_time = dic["preparation_time"] as? Int ?? 0
            menuDishes.price = String(format: "%.2f", dic["price"] as? Double ?? 0.0)
            menuDishes.showtypeonapp = dic["showtypeonapp"] as? Int ?? 0
            menuDishes.status = dic["status"] as? Int ?? 0
            menuDishes.veg = dic["veg"] as? String ?? ""
            menuDishes.recommended = dic["recommended"] as? Int ?? 0
            
            let rating = dic["dish_rating"] as? Double ?? 0.0
            menuDishes.rating = rating.rounded()
            menuDishesArr.append(menuDishes)
        }
        return menuDishesArr
    }
    
    func getAttributes(attributes: NSArray) -> [DishesAttributes] {

        var attributesArr = [DishesAttributes]()
        for i in 0..<attributes.count {
            
            let dic = attributes[i] as? NSDictionary ?? [:]
            let model = DishesAttributes()
            
            model.id = dic["id"] as? Int ?? 0
            model.name = dic["name"] as? String ?? ""
            attributesArr.append(model)
        }
        
        return attributesArr
    }
}

class RestaurantDetails: NSObject {
    var id = 0
    var name = ""
    var thumb = ""
    var logo = ""
    var is_accepting_orders = ""
    var rating : Float?
    var city = ""
    
}


class MenuCategories: NSObject {
    var id = 0
    var name = ""
    var descript_ion = ""
    var dishes = [MenuDishes]()
}

class MenuDishes: NSObject {
    var attributes = [DishesAttributes]()
    var best_seller = 0
    var calories = ""
    var customizable = false
    var descript_ion = ""
    var favorited = false
    var hall_id = 0
    var id = 0
    var image = ""
    var ingredients = "";
    var meal_ids = NSArray()
    var meal_names = NSArray()
    var name = ""
    var preparation_time = 0
    var price = ""
    var showtypeonapp = 0
    var status = 0
    var veg = ""
    var recommended = 0
    var rating = 0.0

}

class DishesAttributes: NSObject {
    var id = 0
    var name = ""
}

class MenuFilters: NSObject {
    var sort = [String]()
}

enum DishType : String{
    case veg             = "Vegetarian"
    case nonVeg             = "Non-Vegetarian"
    case containsEgg     = "Contains Egg"
    case glutenFree         = "Glutten Free"
    case vegan             = "Vegan"
    case undefined
    
    var index : Int{
        switch self {
        case .veg:
            return 0
        case .nonVeg:
            return 1
        case .containsEgg:
            return 2
        case  .glutenFree:
            return 3
        case .vegan:
            return 4
        default:
            return -1
        }
    }
}
