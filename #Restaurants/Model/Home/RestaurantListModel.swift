//
//  RestaurantListModel.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RestaurantListModel: NSObject {
    var locations = [Location]()
    var currency = ""

    func setRestaurantListModelData(dic: NSDictionary) -> RestaurantListModel{
        let model = RestaurantListModel()

        model.locations = Location().setRestaurantListData(arr: dic["locations"] as? NSArray ?? [])
        model.currency = dic["currency"] as? String ?? ""

        return model
    }
}

// MARK: - Location
class Location: NSObject {
    var id = 0
    var name = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var email1 = ""
    var designation = ""
    var username = ""
    var newPasswordRequest = ""
    var domain = ""
    var subscriptionID = ""
    var chargeID = ""
    var stripeCustomerID = ""
    var fbToken = ""
    var block = ""
    var showLocation = false
    var studentID = ""
    var locationTitle = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var city = ""
    var country = ""
    var phone = ""
    var phone2 = ""
    var phone3 = ""
    var profilePic = ""
    var logo = ""
    var state = ""
    var zip = ""
    var lat = ""
    var lng = ""
    var restaurantName = ""
    var businessLegalName = ""
    var businessTagline = ""
    var einNo = ""
    var isAcceptingOrders = ""
    var isTimingsDefault = ""
    var locationDescription = ""
    var taxes = ""
    var currencyCode = ""
    var location = ""
    var facebook = ""
    var google = ""
    var deviceType = ""
    var yelpRating = ""
    var colorChoice = ""
    var cuisineType = ""
    var delAvailability = ""
    var radiusDelivery = ""
    var deliveryCharge = ""
    var minOrderValue = ""
    var loc = ""
    var refCode = ""
    var delvdoc = ""
    var defaultTip = ""
    var stripeID = ""
    var defaultCardID = ""
    var role = ""
    var confirmationCode = ""
    var verified = false
    var clientKey = ""
    var secretKey = ""
    var clientDB = ""
    var parent = ""
    var createdAt = ""
    var updatedAt = ""
    var deletedAt = ""

    func setRestaurantListData(arr: NSArray) -> [Location] {

        var locationArr = [Location]()

        for i in 0..<arr.count {
            let dic = arr[i] as? NSDictionary ?? [:]

            let model = Location()
            model.id = dic["id"] as? Int ?? 0
            model.name = dic["name"] as? String ?? ""
            model.firstName = dic["first_name"] as? String ?? ""
            model.lastName = dic["last_name"] as? String ?? ""
            model.email = dic["email"] as? String ?? ""
            model.email1 = dic["email1"] as? String ?? ""
            model.designation = dic["designation"] as? String ?? ""
            model.username = dic["username"] as? String ?? ""
            model.newPasswordRequest = dic["new_password_request"] as? String ?? ""
            model.domain = dic["domain"] as? String ?? ""
            model.subscriptionID =  dic["subscription_id"] as? String ?? ""
            model.chargeID = dic["charge_id"] as? String ?? ""
            model.stripeCustomerID = dic["stripe_customer_id"] as? String ?? ""
            model.fbToken =  dic["fb_token"] as? String ?? ""
            model.block = dic["block"] as? String ?? ""
            model.showLocation = dic["show_location"] as? Bool ?? false
            model.studentID = dic["student_id"] as? String ?? ""
            model.locationTitle = dic["location_title"] as? String ?? ""
            model.addressLine1 = dic["address_line_1"] as? String ?? ""
            model.addressLine2 = dic["address_line_2"] as? String ?? ""
            model.city = dic["min_order_value"] as? String ?? ""
            model.country = dic["min_order_value"] as? String ?? ""
            model.phone = dic["min_order_value"] as? String ?? ""
            model.phone2 = dic["phone_2"] as? String ?? ""
            model.phone3 = dic["phone_3"] as? String ?? ""
            model.profilePic = dic["profile_pic"] as? String ?? ""
            model.logo = dic["logo"] as? String ?? ""
            model.state = dic["state"] as? String ?? ""
            model.zip = dic["zip"] as? String ?? ""
            model.lat = dic["lat"] as? String ?? ""
            model.lng = dic["lng"] as? String ?? ""
            model.restaurantName = dic["restaurant_name"] as? String ?? ""
            model.businessLegalName = dic["business_legal_name"] as? String ?? ""
            model.businessTagline = dic["business_tagline"] as? String ?? ""
            model.einNo = dic["ein_no"] as? String ?? ""
            model.isAcceptingOrders = dic["is_accepting_orders"] as? String ?? ""
            model.isTimingsDefault = dic["is_timings_default"] as? String ?? ""
            model.locationDescription = dic["description"] as? String ?? ""
            model.taxes = dic["taxes"] as? String ?? ""
            model.currencyCode = dic["currency_code"] as? String ?? ""
            model.location = dic["location"] as? String ?? ""
            model.facebook = dic["facebook"] as? String ?? ""
            model.google = dic["google"] as? String ?? ""
            model.deviceType = dic["device_type"] as? String ?? ""
            model.yelpRating = dic["yelp_rating"] as? String ?? ""
            model.colorChoice = dic["color_choice"] as? String ?? ""
            model.cuisineType = dic["cuisine_type"] as? String ?? ""
            model.delAvailability = dic["del_availability"] as? String ?? ""
            model.radiusDelivery = dic["radius_delivery"] as? String ?? ""
            model.deliveryCharge = dic["delivery_charge"] as? String ?? ""
            model.minOrderValue = dic["min_order_value"] as? String ?? ""
            model.loc = dic["lco"] as? String ?? ""
            model.refCode = dic["ref_code"] as? String ?? ""
            model.delvdoc = dic["delvdoc"] as? String ?? ""
            model.defaultTip = dic["default_tip"] as? String ?? ""
            model.stripeID = dic["stripe_id"] as? String ?? ""
            model.defaultCardID = dic["default_card_id"] as? String ?? ""
            model.role = dic["role"] as? String ?? ""
            model.confirmationCode = dic["confirmation_code"] as? String ?? ""
            model.verified = dic["verified"] as? Bool ?? false
            model.clientKey = dic["client_key"] as? String ?? ""
            model.secretKey = dic["secret_key"] as? String ?? ""
            model.clientDB = dic["client_db"] as? String ?? ""
            model.parent = dic["parent"] as? String ?? ""
            model.createdAt = dic["created_at"] as? String ?? ""
            model.updatedAt = dic["updated_at"] as? String ?? ""
            model.deletedAt = dic["deleted_at"] as? String ?? ""

            locationArr.append(model)
        }

        return locationArr
    }
}
