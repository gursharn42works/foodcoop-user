//
//  HomeModel.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

struct HomeModel {
    
    var announcement : Announcement?
    var banners = [HomeBanner]()
    var halls = [Hall]()
    var restaurantsArr = [Restaurants]()
    var tagsArr = [Tags]()
    var productCatagory = [ProductCatagory]()
    var page = 1
   // var filter = FilterManager()
    var cartCount = 0
    var cartPrice = 0.0
    var secretKey : String = ""
    var publishableKey = ""
    var trinity_KeyId = ""
    var trinity_secretId = ""
    var meatCoop_Keyid = ""
    var meatCoop_secretId = ""
    var address : DeliveryAddressModel?
    var addressWithinRadius : Bool?
    
    // filter : FilterManager
   // self.filter = filter
    init(banners : [HomeBanner] , halls : [Hall], productCatagory : [ProductCatagory], address : DeliveryAddressModel,restaurantsArr : [Restaurants], tags : [Tags] ) {
        self.banners = banners
        self.halls = halls
        self.productCatagory = productCatagory
        self.address = address
        self.restaurantsArr = restaurantsArr
        self.tagsArr = tags
    }
    init(){
        
    }
    
    func homeProductItems(_ dic: NSDictionary) -> HomeModel {
        var product = HomeModel()
        product.banners = HomeBanner().getBannerArr(dic.value(forKey: "banners")as? NSArray ?? NSArray())
        product.halls = Hall().getHallDetail(dic.value(forKey: "halls") as? NSArray ?? NSArray())
        product.productCatagory = ProductCatagory().getHomeCatagoryDetail(dic.value(forKey: "categories")as? NSArray ?? NSArray())
       // product.filter = FilterManager().filterManager(dic.value(forKey: "filters") as? NSDictionary ?? NSDictionary())
        product.restaurantsArr = Restaurants().getRestaurantArr(dic.value(forKey: "restaurants")as? NSArray ?? NSArray())
        product.tagsArr = Tags().getTagArr(dic.value(forKey: "tags")as? NSArray ?? NSArray())
        
        product.cartCount = dic.value(forKey: "cart_count") as? Int ?? 0
        product.cartPrice = dic.value(forKey: "cart_total") as? Double ?? 0.0
        product.secretKey = dic.value(forKey: "secret_key") as? String ?? ""
        UserDefaults.standard.set(product.secretKey, forKey: UserDefault.secretKey)
        product.address = DeliveryAddressModel().setDefaultAddressData(dic: dic["address"] as? NSDictionary ?? [:])
        product.addressWithinRadius = dic.value(forKey: "withinRadius") as? Bool
        
        UserModel.shared.publishableKey = dic.value(forKey: "publishable_key") as? String ?? ""
        
        product.trinity_KeyId = dic.value(forKey: "trinityfoods_razor_key_id") as? String ?? ""
        product.trinity_secretId = dic.value(forKey: "trinityfoods_razor_key_secret") as? String ?? ""
        UserModel.shared.trinity_KeyId = product.trinity_KeyId
        
        product.meatCoop_Keyid = dic.value(forKey: "meatcoop_razor_key_id") as? String ?? ""
        product.meatCoop_secretId = dic.value(forKey: "meatcoop_razor_key_secret") as? String ?? ""
        UserModel.shared.meatCoop_KeyId = product.meatCoop_Keyid

        if let announcement = dic["annoucement"] as? NSDictionary{
            product.announcement = Announcement().getAnnouncement(announcement)
        }
    
        return product
    }
}

struct ProductCatagory{
    var id : Int?
    var catagoryName : String?
    var sub_categories : [SubCatagory]?

    init(id : Int? , catagoryName : String?,sub_categories : [SubCatagory]?) {
        self.id = id
        self.catagoryName = catagoryName
        self.sub_categories = sub_categories
    }
    
    init(){
        
    }
    
    func catagoryItems(_ dic: NSDictionary) -> ProductCatagory {
        var product = ProductCatagory()
        product.id = dic.value(forKey: "id") as? Int ?? 0
        product.catagoryName = dic.value(forKey: "name") as? String ?? ""
        product.sub_categories = SubCatagory().getHomeCatagoryDetail(dic.value(forKey: "sub_categories") as? NSArray ?? NSArray())
        
        return product
    }
    
    func getHomeCatagoryDetail(_ arr: NSArray) -> [ProductCatagory] {
        var homeCatagory = [ProductCatagory]()
        for catagory in arr {
            let catagoryDic = self.catagoryItems(catagory as? NSDictionary ?? NSDictionary())
            homeCatagory.append(catagoryDic)
        }
        return homeCatagory
    }
}

struct Hall {
    var id : Int?
    var name : String?
    var image : String?
    var timing : [HallTiming]!
    var isClosed : Bool?
    var mealTiming : [MealTime]?
    var isAcceptingOrder : Bool?
    
    init(id : Int, name : String,image : String,timing : [HallTiming],mealTiming : [MealTime],isAcceptingOrder : Bool) {
        self.id = id
        self.name = name
        self.image = image
        self.timing = timing
        self.mealTiming = mealTiming
        self.isAcceptingOrder = isAcceptingOrder
    }
    init() {
        
    }
    func hallItems(_ dic: NSDictionary) -> Hall {
        var item = Hall()
        item.name = dic.value(forKey: "name")as? String ?? ""
        item.id = dic.value(forKey: "id")as? Int ?? 0
        let image = dic.value(forKey: "logo")as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        item.image = imageNoSpace
        let hallTiming = HallTiming().getHallTimingArr(dic.value(forKey: "timing")as? NSArray ?? NSArray())
        item.timing = Utility.shared.checkInvalidTimeSlot(hallTiming)
        
        item.mealTiming = MealTime().getMealTiming(dic.value(forKey: "meal_timing")as? NSArray ?? NSArray())
        
        for i in 0 ..< (item.timing?.count ?? 0){
            item.timing?[i].id = i + 1
            if item.timing?[i].timeSlot.count == 0{
                item.isClosed = true
            }
        }
        item.isAcceptingOrder = dic.value(forKey: "is_accepting_orders") as? String == "yes" ? true : false
        
        return item
    }
    
    func getHallDetail(_ arr : NSArray) -> [Hall] {
        var hallArr = [Hall]()
        for hallItem in arr {
            let hallDic = self.hallItems(hallItem as? NSDictionary ?? NSDictionary())
            hallArr.append(hallDic)
        }
        return hallArr
    }
}


struct Announcement {
    
    var title : String?
    var message : String?
    var timing : String?
    
    func getAnnouncement(_ dic : NSDictionary) -> Announcement{
           var announcement = Announcement()
           announcement.title = dic.value(forKey: "title")as? String ?? ""
            announcement.message = dic.value(forKey: "annoucement_text")as? String ?? ""
         announcement.timing = dic.value(forKey: "date_time")as? String ?? ""
           return announcement
       }
}

struct HomeBanner
{
    var id : Int?
    var bannerImage : String?
    var title : String?
    var description : String?
    
    func getBanners(_ dic : NSDictionary) -> HomeBanner{
        var bannerItem = HomeBanner()
        let image = dic.value(forKey: "banner")as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        bannerItem.bannerImage = imageNoSpace
        bannerItem.id = dic.value(forKey: "id")as? Int ?? 0
        bannerItem.title = dic.value(forKey: "title")as? String ?? ""
        bannerItem.description = dic.value(forKey: "description")as? String ?? ""
        return bannerItem
    }
    
    func getBannerArr(_ arr : NSArray) -> [HomeBanner] {
        var bannerArr = [HomeBanner]()
        for item in arr {
            let bannerDic = self.getBanners(item as? NSDictionary ?? NSDictionary())
            bannerArr.append(bannerDic)
        }
        return bannerArr
    }
}

struct Restaurants {
    var id : Int?
    var name : String?
    var logo : String?
    var thumb : String?
    var bg_image : String?
    var rating : Float?
    var time : String?
    var address : String?
    
    func getRestaurants(_ dic : NSDictionary) -> Restaurants{
        var restaurantItem = Restaurants()
        let image = dic.value(forKey: "logo")as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        let thumb = dic.value(forKey: "thumb")as? String ?? ""
        let thumbNoSpace = thumb.replacingOccurrences(of: " ", with: "%20")
        restaurantItem.logo = imageNoSpace
        restaurantItem.id = dic.value(forKey: "id")as? Int ?? 0
        restaurantItem.name = dic.value(forKey: "name")as? String ?? ""
        restaurantItem.thumb = thumbNoSpace
        restaurantItem.rating = dic.value(forKey: "rating")as? Float ?? 0.0
        restaurantItem.time = dic.value(forKey: "mins")as? String ?? ""
        restaurantItem.address = dic.value(forKey: "address_line_1")as? String ?? ""
        let bg_image = dic.value(forKey: "bg_image")as? String ?? ""
        restaurantItem.bg_image = bg_image
        return restaurantItem
    }
    
    func getRestaurantArr(_ arr : NSArray) -> [Restaurants] {
        var restaurantArr = [Restaurants]()
        for item in arr {
            let restDic = self.getRestaurants(item as? NSDictionary ?? NSDictionary())
            restaurantArr.append(restDic)
        }
        return restaurantArr
    }
    
}

struct Tags {
    var id : Int?
    var name : String?
    var logo : String?
    
    
    func getTags(_ dic : NSDictionary) -> Tags{
        var tagsItem = Tags()
        let image = dic.value(forKey: "logo")as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        tagsItem.logo = imageNoSpace
        tagsItem.id = dic.value(forKey: "id")as? Int ?? 0
        tagsItem.name = dic.value(forKey: "name")as? String ?? ""
        return tagsItem
    }
    
    func getTagArr(_ arr : NSArray) -> [Tags] {
        var tagsArr = [Tags]()
        for item in arr {
            let restDic = self.getTags(item as? NSDictionary ?? NSDictionary())
            tagsArr.append(restDic)
        }
        return tagsArr
    }
    
}

struct HallTiming
{
    
    var day : String!
    var id : Int = 0
    var timeSlot = [TimeSlot]()
    
    
    func getHallTiming(_ dic : NSDictionary) -> HallTiming{
        var timing = HallTiming()
        timing.day = dic.value(forKey: "day")as? String ?? ""
        timing.timeSlot = TimeSlot().getHallTimeSlotArr(dic.value(forKey: "range") as? NSArray ?? NSArray())
        
        return timing
    }
    
    func getHallTimingArr(_ arr : NSArray) -> [HallTiming] {
        var timingArr = [HallTiming]()
        for item in arr {
            let timingDic = self.getHallTiming(item as? NSDictionary ?? NSDictionary())
            timingArr.append(timingDic)
        }
        return timingArr
    }
}

struct TimeSlot
{
    
    var endTime : String!
    var startTime : String!
    var mealName : String!
    
    func getHallTimeSlot(_ dic : NSDictionary) -> TimeSlot{
        var timing = TimeSlot()
        timing.endTime = dic.value(forKey: "end_time")as? String ?? ""
        timing.startTime = dic.value(forKey: "start_time")as? String ?? ""
        return timing
    }
    
    func getHallTimeSlotArr(_ arr : NSArray) -> [TimeSlot] {
        var timingArr = [TimeSlot]()
        for item in arr {
            let timingDic = self.getHallTimeSlot(item as? NSDictionary ?? NSDictionary())
            timingArr.append(timingDic)
        }
        return timingArr
    }
}

struct MealTime {
    
    var id : Int?
    var name : String?
    var timing : [HallTiming]?
    
    init(id : Int, name : String,image : String,timing : [HallTiming]) {
        self.id = id
        self.name = name
        self.timing = timing
    }
    
    init() {
        
    }
    
    func hallItems(_ dic: NSDictionary) -> MealTime {
        var item = MealTime()
        item.name = dic.value(forKey: "name")as? String ?? ""
        item.id = dic.value(forKey: "id")as? Int ?? 0
        let mealTiming = HallTiming().getHallTimingArr(dic.value(forKey: "timing")as? NSArray ?? NSArray())
        item.timing = Utility.shared.checkInvalidTimeSlot(mealTiming)
        return item
    }
    
    func getMealTiming(_ arr : NSArray) -> [MealTime] {
        var hallArr = [MealTime]()
        for hallItem in arr {
            let hallDic = self.hallItems(hallItem as? NSDictionary ?? NSDictionary())
            hallArr.append(hallDic)
        }
        return hallArr
    }
}

struct SearchItems {
    var id : Int?
    var name : String?
    var logo : String?
    var image : String?
    var thumb : String?
    var bg_image : String?
    var rating : Float?
    var address_line_1 : String?
    var address_line_2 : String?
    var city : String?
    var state : String?
    var zip : String?
    var country : String?
    var restaurants_tagged : Int?
    var type : String?
    
    
    
    func getSearchItems(_ dic : NSDictionary) -> SearchItems{
        var tagsItem = SearchItems()
        let logo = dic.value(forKey: "logo")as? String ?? ""
        let logoNoSpace = logo.replacingOccurrences(of: " ", with: "%20")
        let thumb = dic.value(forKey: "thumb")as? String ?? ""
        let thumbNoSpace = thumb.replacingOccurrences(of: " ", with: "%20")
        let image = dic.value(forKey: "image")as? String ?? ""
        let imageNoSpace = image.replacingOccurrences(of: " ", with: "%20")
        tagsItem.logo = logoNoSpace
        tagsItem.id = dic.value(forKey: "id")as? Int ?? 0
        tagsItem.name = dic.value(forKey: "name")as? String ?? ""
        tagsItem.image = imageNoSpace
        tagsItem.thumb = thumbNoSpace
        tagsItem.bg_image = dic.value(forKey: "bg_image")as? String ?? ""
        tagsItem.rating = dic.value(forKey: "rating")as? Float ?? 0.0
        tagsItem.address_line_1 = dic.value(forKey: "address_line_1")as? String ?? ""
        tagsItem.address_line_2 = dic.value(forKey: "address_line_2")as? String ?? ""
        tagsItem.city = dic.value(forKey: "city")as? String ?? ""
        tagsItem.state = dic.value(forKey: "state")as? String ?? ""
        tagsItem.zip = dic.value(forKey: "zip")as? String ?? ""
        tagsItem.country = dic.value(forKey: "country")as? String ?? ""
        tagsItem.restaurants_tagged = dic.value(forKey: "restaurants_tagged")as? Int ?? 0
        tagsItem.type = dic.value(forKey: "type")as? String ?? ""
        return tagsItem
    }
    
    func getSearchItemsArr(_ arr : NSArray) -> [SearchItems] {
        var tagsArr = [SearchItems]()
        for item in arr {
            let restDic = self.getSearchItems(item as? NSDictionary ?? NSDictionary())
            tagsArr.append(restDic)
        }
        return tagsArr
    }
    
}
