//
//  NotificationModel.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    
    var id: Int
    var notification_type: String
    var message: String
    var title: String
    var student_id: Int
    var hall_id: Int
    var data: NotificationData?
    var is_read: Int
    var status: String
    var created_at: String
    var updated_at: String
    var location_title: String
    
    init(id: Int,notification_type: String,message: String,title:String,student_id: Int,hall_id: Int,data: NotificationData,is_read: Int,status: String,created_at: String,updated_at: String,location_title: String) {
        
         self.id = id
         self.notification_type = notification_type
         self.message = message
         self.title = title
         self.student_id = student_id
         self.hall_id = hall_id
         self.data = data
         self.is_read = is_read
         self.status = status
         self.created_at = created_at
         self.updated_at = updated_at
         self.location_title = location_title

    }
}

class NotificationData: NSObject {
    var push_type: String
    var order_id: Int
    var status: String
    
    init(push_type: String,order_id: Int,status: String) {
        self.push_type = push_type
        self.order_id = order_id
        self.status = status
    }
}

