//
//  OrderHistoryModel.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


//struct OngoingOrderModel: Codable {
//    let success: Bool
//    let data: OngoingOrderModelDataClass
//    let message: String
//}
//
//// MARK: - DataClass
//struct OngoingOrderModelDataClass: Codable {
//    let orders: [OngoingOrder]
//}
//
//// MARK: - Order
//struct OngoingOrder: Codable {
//    let id, hallID: Int
//    let customerPhone, customerName, orderNumber: String
//    let cancelRequest, orderTotal: Int
//    let grandTotal: Double
//    let discount: Int
//    let tax: Double
//    let taxDescription, status: String
//    let reason: JSONNull?
//    let createdAt: OngoingCreatedAt
//    let startTime, endTime, couponCode, firstname: JSONNull?
//    let lastname, street1, street2, city: JSONNull?
//    let state, zipcode, houseNo: JSONNull?
//    let averageDeliveryTimeHrs, averageDeliveryTimeMins: Int
//    let instructions, timing: String
//    let isImmediate: Int
//    let thumb, areaRating, notes: JSONNull?
//    let locationTitle: String
//    let items: [OngoingItem]
//    let average: String
//    let card: OngoingCard
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case hallID = "hall_id"
//        case customerPhone = "customer_phone"
//        case customerName = "customer_name"
//        case orderNumber = "order_number"
//        case cancelRequest = "cancel_request"
//        case orderTotal = "order_total"
//        case grandTotal = "grand_total"
//        case discount, tax
//        case taxDescription = "tax_description"
//        case status, reason
//        case createdAt = "created_at"
//        case startTime = "start_time"
//        case endTime = "end_time"
//        case couponCode = "coupon_code"
//        case firstname, lastname, street1, street2, city, state, zipcode
//        case houseNo = "house_no"
//        case averageDeliveryTimeHrs = "average_delivery_time_hrs"
//        case averageDeliveryTimeMins = "average_delivery_time_mins"
//        case instructions, timing
//        case isImmediate = "is_immediate"
//        case thumb
//        case areaRating = "area_rating"
//        case notes
//        case locationTitle = "location_title"
//        case items, average, card
//    }
//}
//
//// MARK: - Card
//struct OngoingCard: Codable {
//    let lastFourDigit, brand: String?
//
//    enum CodingKeys: String, CodingKey {
//        case lastFourDigit = "last_four_digit"
//        case brand
//    }
//}
//
//// MARK: - CreatedAt
//struct OngoingCreatedAt: Codable {
//    let date: String
//    let timezoneType: Int
//    let timezone: String
//
//    enum CodingKeys: String, CodingKey {
//        case date
//        case timezoneType = "timezone_type"
//        case timezone
//    }
//}
//
//// MARK: - Item
//struct OngoingItem: Codable {
//    let id: Int
//    let isDeleted: Bool
//    let dishID: Int
//    let itemName, veg: String
//    let price: Int
//    let serving: String
//    let addons, options: [JSONAny]
//    let qty: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case isDeleted = "is_deleted"
//        case dishID = "dish_id"
//        case itemName = "item_name"
//        case veg, price, serving, addons, options, qty
//    }
//}
//
////MARK:- Previous Order Model
//// MARK: - Empty
//struct PreviousOrderModel: Codable {
//    let success: Bool
//    let data: PreviousDataClass
//    let message: String
//}
//
//struct PreviousDataClass: Codable {
//    let orders: [PreviousOrder]
//}
//
//// MARK: - Order
//struct PreviousOrder: Codable {
//    let id, hallID: Int
//    let customerPhone, customerName, orderNumber: String
//    let cancelRequest, orderTotal: Int
//    let grandTotal: Double
//    let discount: Int
//    let tax: Double
//    let taxDescription, status: String
//    let reason: JSONNull?
//    let createdAt: PreviousCreatedAt
//    let startTime, endTime, couponCode, firstname: JSONNull?
//    let lastname, street1, street2, city: JSONNull?
//    let state, zipcode, houseNo: JSONNull?
//    let averageDeliveryTimeHrs, averageDeliveryTimeMins: Int
//    let instructions, timing: String
//    let isImmediate: Int
//    let thumb, areaRating, notes: JSONNull?
//    let locationTitle: String
//    let items: [PreviousItem]
//    let average: String
//    let card: PreviousCard
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case hallID = "hall_id"
//        case customerPhone = "customer_phone"
//        case customerName = "customer_name"
//        case orderNumber = "order_number"
//        case cancelRequest = "cancel_request"
//        case orderTotal = "order_total"
//        case grandTotal = "grand_total"
//        case discount, tax
//        case taxDescription = "tax_description"
//        case status, reason
//        case createdAt = "created_at"
//        case startTime = "start_time"
//        case endTime = "end_time"
//        case couponCode = "coupon_code"
//        case firstname, lastname, street1, street2, city, state, zipcode
//        case houseNo = "house_no"
//        case averageDeliveryTimeHrs = "average_delivery_time_hrs"
//        case averageDeliveryTimeMins = "average_delivery_time_mins"
//        case instructions, timing
//        case isImmediate = "is_immediate"
//        case thumb
//        case areaRating = "area_rating"
//        case notes
//        case locationTitle = "location_title"
//        case items, average, card
//    }
//}
//
//// MARK: - Card
//struct PreviousCard: Codable {
//    let lastFourDigit, brand: String?
//
//    enum CodingKeys: String, CodingKey {
//        case lastFourDigit = "last_four_digit"
//        case brand
//    }
//}
//
//// MARK: - CreatedAt
//struct PreviousCreatedAt: Codable {
//    let date: String
//    let timezoneType: Int
//    let timezone: String
//
//    enum CodingKeys: String, CodingKey {
//        case date
//        case timezoneType = "timezone_type"
//        case timezone
//    }
//}
//
//// MARK: - Item
//struct PreviousItem: Codable {
//    let id: Int
//    let isDeleted: Bool
//    let dishID: Int
//    let itemName, veg: String
//    let price: Int
//    let serving: String
//    let addons, options: [JSONAny]
//    let qty: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case isDeleted = "is_deleted"
//        case dishID = "dish_id"
//        case itemName = "item_name"
//        case veg, price, serving, addons, options, qty
//    }
//}
