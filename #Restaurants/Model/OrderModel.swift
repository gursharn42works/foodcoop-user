//
//  OrderModel.swift
//  #Restaurants
//
//  Created by Satveer on 19/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

enum OrderType{
    case ongoing
    case previous
}

enum PaymentMethod{
    case payAtStore
    case payByCard
    case brainTree
    case Razorypay
    
}

struct Order {
    var statusOrderId: String?
    var id: String
    var number: String
    var delieveryDate: String
    var customerNumber: String
    var description: String
    var totalPrice: String
    var items: [CartItem]
    var status: String
    var is_rating = false
    var orderPlacedDate: String
    var pickupDate: String
    var specialNotes: String
    var timing: String = ""
    var hall : String = ""
    var type : OrderType = .ongoing
    var orderTotal : String = ""
    var tax : String = ""
    var taxDescription = [TaxDescription]()
    var couponDiscount : String = ""
    var isCouponApplied : Bool = false
    var card = Card()
    var thumb : String = ""
    var isImmediate : Bool = false
    var currencySymbol =  UserModel.shared.currencyCode //"$"
    var hallId : Int = 0
    var cancelRequest : Int = 0
    var cancellationReason : String?
    var orderStartTime : String?
    var orderEndTime : String?
    var order_type = ""
    var delivery_charges = ""
    var estimatedTime : String?
    var phone = ""
    var delivery_rating = 3
    var order_rating = 0
    var delivery_comment = ""
    var order_comment = ""
    var average_delivery_time_hrs = ""
    var average_delivery_time_mins = ""
    var deliveryAddress = DeliveryAddressModel()
    var deliveryBoy = DeliveryBoyModel()
    var restaurantDetail = RestaurantDetail()
    var restaurantAddressModel = RestaurantAddressModel()
    //New additions
    
    var locationTitle = ""
    var orderPlacedOn = ""
    var orderStatus : OrderStatusEnum  = .pendingConfirmation
    var paymentMethod: PaymentMethod = .payByCard
    
    init() {
        id = ""
        number = ""
        delieveryDate = ""
        customerNumber = ""
        description = ""
        totalPrice = ""
        hall = ""
        items = [CartItem]()
        status = ""
        orderPlacedDate = ""
        pickupDate = ""
        specialNotes = ""
        type = .ongoing
        thumb = ""
        hallId = 0
        cancelRequest = 0
        
        locationTitle = ""
        orderPlacedOn = ""
        orderStatus   = .pendingConfirmation
        paymentMethod  = .payByCard
        
    }
    
    //get Order
    func getOrder(_ dic: Any) -> Order {
        var this = Order()
        if let dic = dic as? NSDictionary {
            print(dic)
            this.estimatedTime = dic.value(forKey: "average") as? String
            this.average_delivery_time_hrs = "\(dic.value(forKey: "average_delivery_time_hrs") as? Int ?? 0)"
            this.average_delivery_time_mins = "\(dic.value(forKey: "average_delivery_time_mins") as? Int ?? 0)"

            
            this.statusOrderId = (dic.value(forKey: "order_number") as? String ?? "")
            this.id = "\(dic.value(forKey: "id") as? Int ?? 0)"
            this.cancellationReason = dic.value(forKey: "reason") as? String ?? ""
            //let grandTotal = dic.value(forKey: "grand_total") as? String ?? "0.0"
            this.totalPrice = (String(format: "%.2f", dic.value(forKey: "grand_total") as? Double ?? 0.0))
            this.number = "Order No. : " + (dic.value(forKey: "order_number") as? String ?? "")
            this.cancelRequest = dic.value(forKey: "cancel_request") as? Int ?? 0
         
            
            if let rating = dic["rating"] as? NSDictionary {
                this.delivery_rating = rating["delivery_rating"] as? Int ?? 0
                this.order_rating = rating["order_rating"] as? Int ?? 0
                this.delivery_comment = rating.value(forKey: "delivery_comment") as? String ?? ""
                this.order_comment = rating.value(forKey: "order_comment") as? String ?? ""
            } else {
                this.delivery_rating = dic["delivery_rating"] as? Int ?? 0
                this.order_rating = dic["order_rating"] as? Int ?? 0
                this.delivery_comment = dic.value(forKey: "delivery_comment") as? String ?? ""
                this.order_comment = dic.value(forKey: "order_comment") as? String ?? ""
            }

            this.is_rating = dic.value(forKey: "is_rating") as? Bool ?? false

            
            var arrayItemName = [String]()
            this.items = CartItem().getCartItems(dic.value(forKey: "items") as? NSArray ?? NSArray())
            for item in this.items{
                arrayItemName.append(item.dishName)
            }

            this.phone = dic.value(forKey: "customer_phone") as? String ?? ""
            this.orderStartTime = dic.value(forKey: "start_time") as? String
            this.orderEndTime = dic.value(forKey: "end_time") as? String
            this.description = arrayItemName.joined(separator: ", ")
            this.timing = dic.value(forKey: "timing") as? String ?? ""
            this.status = dic.value(forKey: "status") as? String ?? ""
            this.specialNotes = dic.value(forKey: "instructions") as? String ?? ""

            this.hall = dic.value(forKey: "restaurant_name") as? String ?? ""
            //let orderTotal = dic.value(forKey: "order_total") as? String ?? "0.0"
            this.orderTotal = (String(format: "%.2f", dic.value(forKey: "order_total") as? Double ?? 0.0))
            
            //let tax = dic.value(forKey: "tax") as? String ?? "0.0"
            this.tax = (String(format: "%.2f", dic.value(forKey: "tax") as? Double ?? 0.0))
            this.taxDescription = TaxDescription().getCartItemAddons(self.convertToDictionary(text: dic.value(forKey: "tax_description") as? String ?? "") as NSArray? ?? NSArray())
            //let couponDiscount = dic.value(forKey: "discount") as? String ?? "0.00"
            this.couponDiscount = (String(format: "%.2f", dic.value(forKey: "discount") as? Double ?? 0.0))
            if this.couponDiscount == "0.00"{
                this.isCouponApplied = false
            }else{
                this.isCouponApplied = true
            }
            this.card = Card().getCard(dic.value(forKey: "card")as? NSDictionary ?? NSDictionary())
            this.thumb = dic.value(forKey: "thumb") as? String ?? ""
            this.isImmediate = dic.value(forKey: "is_immediate") as? Bool ?? false
            this.order_type = dic.value(forKey: "order_type") as? String ?? ""
            this.restaurantAddressModel = RestaurantAddressModel.setRestaurntAddressData(dic: dic["rest"] as? [String: Any] ?? [:])

            if let charges = dic["delivery_charges"] as? String {
                this.delivery_charges = charges
            }
            else if let charges = dic["delivery_charges"] as? Float {
                this.delivery_charges = String(format: "%.2f", charges)
            } else {
                this.delivery_charges = "0"
            }

           // if this.isImmediate
           // {
          //      this.delieveryDate = "Immediate Pickup"
          //  }else{
                if let dateDic = dic.value(forKey: "timing") as? String{
                    let dateToShow = dateDic//GSD dateDic.UTCToLocal()
                    this.delieveryDate = dateToShow.convertDateFormat("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy, hh:mm a")
                }
         //   }
            this.type = this.status == OrderStatusEnum.completed.rawValue || this.status == OrderStatusEnum.cancelled.rawValue ? .previous : .ongoing
            this.hallId = dic.value(forKey: "hall_id") as? Int ?? 0
            
            //additions
            
            this.locationTitle = dic.value(forKey: "location_title") as? String ?? ""
            this.orderPlacedOn =  self.getOrderPickupTime(from: dic)//dic.value(forKey: "created_at") as? String ?? ""
            
            if this.orderPlacedOn != "" {
                this.orderPlacedOn =  this.orderPlacedOn.convertDateFormat("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy, hh:mm a")
            }
            
            let status =  dic.value(forKey: "status") as? String ?? ""
            
            this.orderStatus   = OrderStatusEnum(rawValue: status) ?? .pendingConfirmation
        
            let paymentMethod = dic.value(forKey: "payment_method") as? String ?? ""
            
            switch paymentMethod {
            case "Razorypay":
            this.paymentMethod = .Razorypay
            case "pay_on_pickup", "Cash on Delivery":
                this.paymentMethod = .payAtStore
            case "Braintree":
                this.paymentMethod = .brainTree
            default:
                this.paymentMethod = .payByCard
            }

            
            this.deliveryAddress = DeliveryAddressModel().setDefaultAddressData(dic: dic["delivery_address"] as? NSDictionary ?? [:])
            
            this.deliveryBoy = DeliveryBoyModel().setDefaultAddressData(dic: dic["delivery_boy"] as? NSDictionary ?? [:])
            
            this.restaurantDetail = RestaurantDetail().setRestaurantData(dic: dic["restaurant_detail"] as? NSDictionary ?? [:])
            
        }
        
        return this
    }
    
    func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    fileprivate func getOrderPickupTime(from dict: NSDictionary) -> String{
        
        if let createdDate = dict.value(forKey: "created_at") as? String{
            return createdDate
        }
        
        if let creationInfo = dict.value(forKey: "created_at") as? [String : Any]{
            return creationInfo["date"] as? String ?? ""
        }
        return ""
        /*
        "created_at" =             {
                       date = "2020-06-18 13:06:52.000000";
                       timezone = UTC;
                       "timezone_type" = 3;
                   };
        */
        
    }
    
}

class DeliveryBoyModel {
    var city = ""
    var delivery_boy_status = ""
    var delivery_boy_id = 0
    var delivery_id = 0
    var firstname = ""
    var lastname = ""
    var phone = ""
    var photo = ""
    var state = ""
    var street1 = ""
    var street2 = ""
    var zipcode = 0
    var deliveryBoyId = 0

    func setDefaultAddressData(dic: NSDictionary)-> DeliveryBoyModel {
        let model = DeliveryBoyModel()
        model.city = dic["city"] as? String ?? ""
        model.delivery_boy_status = dic["delivery_boy_status"] as? String ?? ""
        model.delivery_boy_id = dic["delivery_boy_id"] as? Int ?? 0
        model.firstname = dic["first_name"] as? String ?? "".capitalized
        model.lastname = dic["last_name"] as? String ?? "".capitalized
        model.phone = dic["phone"] as? String ?? ""
        model.photo = dic["photo"] as? String ?? ""
        model.state = dic["state"] as? String ?? ""
        model.street1 = dic["street1"] as? String ?? ""
        model.street2 = dic["street2"] as? String ?? ""
        model.zipcode = dic["zipcode"] as? Int ?? 0
        model.deliveryBoyId = dic["delivery_boy_id"] as? Int ?? 0
        model.delivery_id = dic["delivery_id"] as? Int ?? 0

        return model
        
    }
}

class RestaurantDetail {
    var city = ""
    var lat = 0.0
    var lng = 0.0
    var logo = ""
    var phone = ""
    var street1 = ""
    var street2 = ""
    var zipcode = 0

    func setRestaurantData(dic: NSDictionary)-> RestaurantDetail {
        let model = RestaurantDetail()
        model.city = dic["city"] as? String ?? ""
        
        if let lat = dic["lat"] as? Double, let lng = dic["lng"] as? Double {
            model.lat = lat
            model.lng = lng
        }
        else {
            model.lat = Double(dic["lat"] as? String ?? "") ?? 0.0
            model.lng = Double(dic["lng"] as? String ?? "") ?? 0.0
        }
        
        model.logo = dic["logo"] as? String ?? ""
        model.phone = dic["phone"] as? String ?? ""
        model.street1 = dic["street1"] as? String ?? ""
        model.street2 = dic["street2"] as? String ?? ""
        model.zipcode = dic["zipcode"] as? Int ?? 0
        
        return model
        
    }
}
