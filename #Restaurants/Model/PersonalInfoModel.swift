//
//  PersonalInfoModel.swift
//  #Restaurants
//
//  Created by Satveer on 10/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


class PersonalInfoModel: NSObject {
    var id = 0
    var name = ""
    var email = ""
    var studentID = ""
    var city = ""
    var country = ""
    var phone = ""
    var profilePic = ""
    var isAcceptingOrder = ""
    var stripeID = ""
    var defaultCardID = ""

    class func setData(dic: [String: Any]) -> PersonalInfoModel {
        let model = PersonalInfoModel()
        model.id = dic["id"] as? Int ?? 0
        model.name = dic["name"] as? String ?? ""
        model.email = dic["email"] as? String ?? ""
        model.studentID = dic["student_id"] as? String ?? ""
        model.city = dic["city"] as? String ?? ""
        model.country = dic["country"] as? String ?? ""
        model.phone = dic["phone"] as? String ?? ""
        model.profilePic = dic["profile_pic"] as? String ?? ""
        model.isAcceptingOrder = dic["is_accepting_orders"] as? String ?? ""
        model.stripeID = dic["stripe_id"] as? String ?? ""
        model.defaultCardID = dic["default_card_id"] as? String ?? ""
        
        return model
    }
}
