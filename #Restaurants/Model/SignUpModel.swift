//
//  SignUpModel.swift
//  #Restaurants
//
//  Created by Satveer on 27/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation

// MARK: - Empty
struct SignUpModel: Codable {
    let success: Bool
    let data: DataSignUpModelClass
    let message: String
}

// MARK: - DataClass
struct DataSignUpModelClass: Codable {
    let id: Int
    let name, email: String
    let studentID: JSONNull?
    let city, country, phone: String
    let stripeID, defaultCardID: JSONNull?

    enum CodingKeys: String, CodingKey {
        case id, name, email
        case studentID = "student_id"
        case city, country, phone
        case stripeID = "stripe_id"
        case defaultCardID = "default_card_id"
    }
}

