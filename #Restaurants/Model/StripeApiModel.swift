//
//  StripeApiModel.swift
//  #Restaurants
//
//  Created by Satveer on 19/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class StripeApiModel: NSObject {
    var cardsFetched = [CardModel]()
    var responseHandle:(([CardModel])->())?
    
    //MARK:- Call Api
    
    func callApi(){
        if UserModel.shared.stripe_id != "" {
           self.getSavedCardsList(UserModel.shared.stripe_id)
        } else {
            self.generateToken { (str) in
                
            }
        }
    }
    
    //MARK:- Stripe Services
    func generateToken(completion: @escaping(String) -> Void) {
        let dic = ["description": "Random"] as [String : Any] // "source":"tok_amex",
        
        ServiceManager.shared.postStripeRequest(url: ApiName.getStripeCustomerId, params: dic as NSDictionary) { (status, message, dict) in
            if status ?? false {
                BaseVC().updateProfileBackground(dict?.value(forKey: "id") as? String ?? "",cardId: UserModel.shared.card_id)
                let customerID = dict?.value(forKey: "id") as? String ?? ""
                self.getSavedCardsList(customerID)
                completion(customerID)
            } else {
               completion("")

            }
        }
    }
    
    func getSavedCardsList(_ id : String = ""){
        var customerId = ""
        var cardDetail = [CardModel]()
        customerId = id
        if customerId == "" {
            self.generateToken { (str) in
                
            }
            return
        }
        
        let url = "https://api.stripe.com/v1/customers/\(customerId)/sources?object=card&limit=5"
       
        ServiceManager.shared.getStripeRequest(url: url, params: nil) { (status, message, dict) in
            if status ?? false {
                if let cardsList = dict?.value(forKey: "data") as? [NSDictionary] {
                    for data in cardsList {
                       cardDetail.append(CardModel().getCard(data))
                    }
                }else{

                }
            }
            self.responseHandle?(cardDetail)
        }
    }
    
    
}
