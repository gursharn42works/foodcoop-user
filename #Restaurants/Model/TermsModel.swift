//
//  TermsModel.swift
//  #Restaurants
//
//  Created by Satveer on 27/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
// MARK: - TermsModel
struct TermsModel: Codable {
    let success: Bool
    let data: TermsModelDataClass
    let message: String
}

// MARK: - DataClass
struct TermsModelDataClass: Codable {
    let page: Page
}

// MARK: - Page
struct Page: Codable {
    let htmlData: String
    let id: Int
    let title, slug: String

    enum CodingKeys: String, CodingKey {
        case htmlData = "html_data"
        case id, title, slug
    }
}
