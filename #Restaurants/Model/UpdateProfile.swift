//
//  UpdateProfile.swift
//  Magic
//
//  Created by Apple1 on 24/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
class UpdateProfile{ 
    
    var name: String?
    var phone: String?
    var studentID: String?
    var stripeId : String?
    var cardId : String?
    
    func serverData() -> [String: Any] {
        var serverDic = [String: String]()
        serverDic["name"] = name
        serverDic["phone"] = phone
        serverDic["student_id"] = studentID
        serverDic["stripe_id"] = stripeId
        serverDic["default_card_id"] = cardId
        return serverDic
    }
    
    //MARK:- Form Validations
    private func isValidData() -> (Bool, String) {
        var message = ""
        if name == "" {
            message = Messages.emptyName
        }else if (phone?.count) ?? 0 < 10 {
            message = Messages.invalidMobile
        }
        return message == "" ? (true, message) : (false, message)
    }
    
    //MARK:- Validation
    func isFormValid() -> (Bool, String, [String: Any]?) {
        return (isValidData().0, isValidData().1, isValidData().0 ? serverData(): nil)
    }
    
}
