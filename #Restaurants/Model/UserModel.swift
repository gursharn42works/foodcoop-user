//
//  UserModel.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation

enum PaymentOption: String{
    case payOnPickup  = "pay_on_pickup"
    case payOnline    = "razorpay"//"Stripe"
    case paypal       = "Braintree"
    case cod          = "Cash on Delivery"
    
}

class UserModel: NSObject {
    static var shared = UserModel()
    
    var paymentOption : PaymentOption = .payOnline//.payOnPickup
    let userDefaults = UserDefaults.standard
    var user: UserDataModel?
    var device_id = UIDevice.current.identifierForVendor!.uuidString
    var fcm_token = ""
    var appliedFilter = ""
    
    var hallTimings: [HallTiming] = []
    var ongoingOrders: [Order] = []
    var cartItemsCount = 0
    var notificationCount = 0
    var cartItemsPrice = 0.00
    let showTaxes  = true
    
    var user_id = Int()
    var currencyCode = "₹"
    var user_name = ""
    var country = ""
    var is_accepting_orders = ""
    var profile_pic = ""
    var phone = ""
    var email = ""
    var student_id = ""
    var isAnnounceMentShown = false 
    var stripe_id = ""
    var card_id = ""
    var isLoggedin = false

    var hall_id: Int {
        set { userDefaults.set(newValue, forKey: "hall_id") } get { userDefaults.integer(forKey: "hall_id") } }
    
    var hall_name: String {
        set { userDefaults.set(newValue, forKey: "hall_name") } get { userDefaults.string(forKey: "hall_name") ?? "" } }
        
    var selectedLocation: Int {
        set { userDefaults.set(newValue, forKey: "selectedLocation") } get { userDefaults.integer(forKey: "selectedLocation") }}
    
    var apple_data: [String: Any] {
        set { userDefaults.set(newValue, forKey: "apple_data") } get { userDefaults.dictionary(forKey: "apple_data") ?? [:]  } }
    var apple_id: String {
        set { userDefaults.set(newValue, forKey: "apple_id") } get { userDefaults.string(forKey: "apple_id") ?? "" } }
    
    var login_Skipped: Bool {
        set { userDefaults.set(newValue, forKey: "login_Skipped") } get { return userDefaults.bool(forKey: "login_Skipped") } }
    
    var secret_key: String {
        set { userDefaults.set(newValue, forKey: "secret_key") } get { userDefaults.string(forKey: "secret_key") ?? "" } }
    
    var publishableKey: String {
        set { userDefaults.set(newValue, forKey: "publishableKey") } get { userDefaults.string(forKey: "publishableKey") ?? "" } }
    
    var trinity_KeyId: String {
        set { userDefaults.set(newValue, forKey: "trinityKey") } get { userDefaults.string(forKey: "trinityKey") ?? "" } }
    
    var meatCoop_KeyId: String {
        set { userDefaults.set(newValue, forKey: "meatCoopKey") } get { userDefaults.string(forKey: "meatCoopKey") ?? "" } }

    
    func savePersonalInfoData(user_name: String, profile_pic: String, phone: String) {
        self.user_name = user_name
        self.profile_pic = profile_pic
        self.phone = phone
        userDefaults.set(user_name, forKey: "user_name")
        userDefaults.set(profile_pic, forKey: "profile_pic")
        userDefaults.set(phone, forKey: "phone")

    }
    
    func saveUserData(user_id: Int, apple_id: String, currency_code: String, user_name: String, country: String, is_accepting_orders: String, profile_pic: String,phone: String,email: String,stripe_id: String,card_id: String) {
        self.user_id = user_id
        self.apple_id = apple_id
        self.currencyCode = currency_code == "" ? "₹":currency_code
        self.user_name = user_name
        self.country = country
        self.profile_pic = profile_pic
        self.phone = phone
        self.email = email
        self.stripe_id = stripe_id
        userDefaults.set(stripe_id, forKey: "stripe_id")

        self.card_id = card_id
        self.saveUser()
    }
    
    func saveUser() {
        userDefaults.set(user_id, forKey: "user_id")
        userDefaults.set(apple_id, forKey: "apple_id")
        userDefaults.set(currencyCode, forKey: "currency_code")
        userDefaults.set(user_name, forKey: "user_name")
        userDefaults.set(country, forKey: "country")
        userDefaults.set(is_accepting_orders, forKey: "is_accepting_orders")
        userDefaults.set(profile_pic, forKey: "profile_pic")
        userDefaults.set(phone, forKey: "phone")
        userDefaults.set(email, forKey: "email")
        userDefaults.set(stripe_id, forKey: "stripe_id")
        userDefaults.set(card_id, forKey: "card_id")
     
    }
    
    func removeValueFromSkipLogin() {
        userDefaults.removeObject(forKey: "login_Skipped")
    }
    
    func getUserData() -> Bool {
        user_id = userDefaults.integer(forKey: "user_id")
        apple_id = userDefaults.string(forKey: "apple_id") ?? ""
        currencyCode = userDefaults.string(forKey: "currency_code") ?? ""
        user_name = userDefaults.string(forKey: "user_name") ?? ""
        country = userDefaults.string(forKey: "country") ?? ""
        is_accepting_orders = userDefaults.string(forKey: "is_accepting_orders") ?? ""
        profile_pic = userDefaults.string(forKey: "profile_pic") ?? ""
        phone = userDefaults.string(forKey: "phone") ?? ""
        email = userDefaults.string(forKey: "email") ?? ""
        stripe_id = userDefaults.string(forKey: "stripe_id") ?? ""
        card_id = userDefaults.string(forKey: "card_id") ?? ""

        if user_id != 0 {
            return true
        }
        return false
    }
    
    func removeUserData() {
        userDefaults.removeObject(forKey: "user_id")
        userDefaults.removeObject(forKey: "apple_id")
        userDefaults.removeObject(forKey: "currency_code")
        userDefaults.removeObject(forKey: "user_name")
        userDefaults.removeObject(forKey: "country")
        userDefaults.removeObject(forKey: "is_accepting_orders")
        userDefaults.removeObject(forKey: "profile_pic")
        userDefaults.removeObject(forKey: "phone")
        userDefaults.removeObject(forKey: "email")
        userDefaults.removeObject(forKey: "hall_id")
        userDefaults.removeObject(forKey: "selectedLocation")
        userDefaults.removeObject(forKey: "apple_data")
        userDefaults.removeObject(forKey: "forKey")
        userDefaults.removeObject(forKey: "login_Skipped")
        userDefaults.removeObject(forKey: "stripe_id")
        userDefaults.removeObject(forKey: "card_id")

        userDefaults.removeObject(forKey: UserDefault.localCart)
        self.cartItemsCount = 0
        self.cartItemsPrice = 0        
        
        print(self.getUserData())
        
    }
    
}

class UserDataModel: NSObject {
    var id: Int!
    var name, city, country, phone: String!
    var email: String!
    var studentID: String!
    var stripeID, defaultCardID, isAcceptingOrders, profilePic: String!
    
    func setData(dic: [String: Any])  {
        
        self.id = dic["id"] as? Int ?? 0
        self.name = dic["name"] as? String ?? ""
        self.city = dic["city"] as? String ?? ""
        self.country = dic["country"] as? String ?? ""
        self.phone = dic["phone"] as? String ?? ""
        self.email = dic["email"] as? String ?? ""
        self.studentID = dic["studentID"] as? String ?? ""
        self.stripeID = dic["student_id"] as? String ?? ""
        self.defaultCardID = dic["default_card_id"] as? String ?? ""
        self.isAcceptingOrders = dic["isAcceptingOrders"] as? String ?? ""
        self.profilePic = dic["profile_pic"] as? String ?? ""
        
        UserModel.shared.saveUserData(user_id: self.id, apple_id: "", currency_code: "", user_name: self.name, country: self.country, is_accepting_orders: self.isAcceptingOrders, profile_pic: self.profilePic, phone: self.phone, email: self.email,stripe_id:self.stripeID, card_id: "")
        
    }
    
}

class FaceBookModel: NSObject {
    
    var city: String
    var country: String
    var default_card_id: String
    var email: String
    var id: Int
    var is_accepting_orders: String
    var name: String
    var phone: String
    var profile_pic: String
    var stripe_id: String
    var student_id: String
    
    init(city: String, country: String, default_card_id: String,email: String,id: Int,is_accepting_orders: String,name: String,phone: String,profile_pic: String,stripe_id: String,student_id: String) {
        
        self.city = city
        self.country = country
        self.default_card_id = default_card_id
        self.email = email
        self.id = id
        self.is_accepting_orders = is_accepting_orders
        self.name = name
        self.phone = phone
        self.profile_pic = profile_pic
        self.stripe_id = stripe_id
        self.student_id = student_id
    }
}


