//
//  NotificationManager.swift
//  #Restaurants
//
//  Created by Satveer on 29/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging

class NotificationManager: NSObject ,UNUserNotificationCenterDelegate {
    
    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Got token data! \(deviceToken)")
    }

    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        ServiceManager.shared.hideLoader = true

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        ServiceManager.shared.hideLoader = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: userInfo)
        
        let push_type = userInfo["push_type"] as? String ?? ""
        let order_id  = userInfo["order_id"] as? String ?? ""
        let body = userInfo["body"] as? String ?? ""
      
        if push_type == "order" && order_id != "" {
            if body.contains("Please provide us feedback") {
                BaseVC().pushToViewWhenClcikOnNotifcation(order_id: order_id, isFeedbackView: true)
            } else {
                BaseVC().pushToViewWhenClcikOnNotifcation(order_id: order_id, isFeedbackView: false)
            }
        }
        
        completionHandler()
    }

}

extension NotificationManager : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserModel.shared.fcm_token = fcmToken
        
        self.updateFcmTokenService(fcmToken: fcmToken)
        
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func updateFcmTokenService(fcmToken: String) {
        let parameters = ["device_id" : UserModel.shared.device_id,"token" : fcmToken,"user_id" : UserModel.shared.user_id] as [String : Any]
        ServiceManager.shared.postRequest(url: ApiName.updateFcmToken, parameters: parameters) { (jsonData, success) in
            print(success)
        }
    }
}
