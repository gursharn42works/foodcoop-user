//
//  CartViewPopUp.swift
//  #Restaurants
//
//  Created by Satveer on 08/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

extension BaseVC: BottomCartViewDelegate {
    func addBottomCartView(_ superView : UIView, _ tabBarHeight : CGFloat = 50) {
        
        self.removeCartBottomView(superView)
        if UserModel.shared.cartItemsCount > 0 {
            self.removeCartBottomView(superView)
            if UserModel.shared.cartItemsCount > 0{
                let bottomCartView = BottomCartView()
                let bounds = UIScreen.main.bounds
                let height = bounds.size.height
                if UserModel.shared.ongoingOrders.count > 0 && Utility.shared.isSearchViewController == false {
                    var extraHeight = 0.0
                    if height > 736.0 && tabBarController?.tabBar.isHidden == true {
                        extraHeight = 30
                    }
                    bottomCartView.frame = CGRect(x: 0, y: self.view.frame.size.height - 122.5 - tabBarHeight - CGFloat(extraHeight), width: self.view.frame.size.width, height: 50)
                }else{
                    
                    if Utility.shared.isSearchViewController == true {
                        bottomCartView.frame = CGRect(x: 0, y: self.view.frame.size.height - 100 - tabBarHeight, width: self.view.frame.size.width, height: 50)
                    }else {
                        
                        if height > 736.0 {
                            if tabBarController?.tabBar.isHidden == true {
                                bottomCartView.frame = CGRect(x: 0, y: self.view.frame.size.height - 80 - tabBarHeight, width: self.view.frame.size.width, height: 50)
                            }else {
                                bottomCartView.frame = CGRect(x: 0, y: self.view.frame.size.height - 50 - tabBarHeight, width: self.view.frame.size.width, height: 50)
                            }
                            
                        }else {
                            bottomCartView.frame = CGRect(x: 0, y: self.view.frame.size.height - 50 - tabBarHeight, width: self.view.frame.size.width, height: 50)
                        }
                        
                    }
                    
                }
                
                 bottomCartView.quantityLabel.text = "\(UserModel.shared.cartItemsCount)"
                bottomCartView.tag = -10004
                bottomCartView.delegate = self
                bottomCartView.priceLabel.smartDecimalText =  UserModel.shared.currencyCode /*"$"*/ + (String(format: "%.2f", UserModel.shared.cartItemsPrice))
                superView.addSubview(bottomCartView)
            }
        }
    }
    
    func removeCartBottomView(_ superView: UIView?) {
        guard let superView = superView else {
            return
        }
        for view in superView.subviews {
            if let temp = view as? BottomCartView , temp.tag == -10004{
                view.removeFromSuperview()
            }
        }
    }
    
    func moveToCartViewController() {
        if let cartVC = Router.viewController(with: .cartVC, .home) as? CartViewController {
            cartVC.hidesBottomBarWhenPushed = true
            cartVC.delegate = self
            cartVC.isFromBottomCartView = true
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(cartVC , animated: true)
        }
    }
    
    @objc func updateValueOnBottomCartView() {
        print("updateValueOnBottomCartView")
    }

}
