//
//  OrderViewPopUP.swift
//  #Restaurants
//
//  Created by Satveer on 08/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

extension BaseVC {
    func addBottomOrderView(_ superView : UIView, _ tabBarHeight : CGFloat = 50) {
        
        self.removeBottomOrderView(superView)
        
        if UserModel.shared.ongoingOrders.count > 0 {
            
          let  bottomOrderView = BottomOrderView()
            var extraHeight = 0.0
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            if height > 736.0 && tabBarController?.tabBar.isHidden == true {
                extraHeight = 30.0
            }
            bottomOrderView.frame = CGRect(x: 0, y: self.view.frame.size.height - 60 - tabBarHeight - CGFloat(extraHeight), width: self.view.frame.size.width, height: 70)
            bottomOrderView.backgroundColor = UIColor.green
            bottomOrderView.delegate = self
            bottomOrderView.tag = -10003
            superView.addSubview(bottomOrderView)
            bottomOrderView.pageControl.numberOfPages = UserModel.shared.ongoingOrders.count
            bottomOrderView.ongoingOrder = UserModel.shared.ongoingOrders
            bottomOrderView.orderCollectionView.reloadData()
        }
    }
    
    func removeBottomOrderView(_ superView: UIView?) {
        guard let superView = superView else {
            return
        }
        for view in superView.subviews {
            if let temp = view as? BottomOrderView , temp.tag == -10003{
                view.removeFromSuperview()
            }
        }
    }
    
    func moveToOrderStatusView(order_id: Int) {
        if let orderStatusVC = Router.viewController(with: .deliveryOrderDetailVC, .profile) as? DeliveryOrderDetailVC {
            orderStatusVC.order.id = "\(order_id)"
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(orderStatusVC , animated: true)
        }
    }
}
