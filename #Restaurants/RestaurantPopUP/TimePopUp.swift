//
//  TimePopUp.swift
//  #Restaurants
//
//  Created by Satveer on 07/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

extension BaseVC {
   
    func moveToTimingVC() {
        if let restaurantTimeVC = Router.viewController(with: .restaurantTimeVC, .product) as? RestaurantTimeVC {
            restaurantTimeVC.modalPresentationStyle = .overCurrentContext
            self.window?.rootViewController?.present(restaurantTimeVC, animated: false, completion: nil)
        }
    }
    
    func addRestaurantClosed(_ superview: UIView, _ tabBarHeight : CGFloat = 50,_ shouldShow: Bool = false,_  action:@escaping () -> ()){
      //  self.removeRestaurantClosed(superview)
        
        if shouldShow{
            let restaurantClosed = RestaurantClosed()
            
            restaurantClosed.frame = CGRect(x: 0, y: self.view.frame.size.height - self.yConstantForRestaurantClosed() - tabBarHeight, width: self.view.frame.size.width, height: 72)
            superview.addSubview(restaurantClosed)
            restaurantClosed.tag = 10002
            restaurantClosed.openTimingsClosure = {
                action()
            }
        }
    }
    
    func removeRestaurantClosed(_ superView: UIView?) {
        guard let superView = superView else {
            return
        }
        for view in superView.subviews {
            if let temp = view as? RestaurantClosed , temp.tag == 10002 {
                view.removeFromSuperview()
            }
        }
    }
    
//    func removeRestaurantClosed(_ superView: UIView?) {
//        self.restaurantClosed.removeFromSuperview()
////        guard let superView = superView else {
////            return
////        }
////        for view in superView.subviews {
////            if let temp = view as? RestaurantClosed{
////                temp.removeFromSuperview()
////            }
//       // }
//    }
    
    func getRestaurantTimes(completion: @escaping([HallTiming],Bool)-> Void){
        
        let url =  ApiName.getTiming + "?\(Constant.shared.timezone)=\(TimeZone.current.identifier)"
        ServiceManager.shared.getRequest(url: url, parameters: [:]) { (jsonData,bool) in
            if let data = jsonData {
               let hallsArr = self.createRestaurantTimeModel(jsonData: data)
                self.getSelectedHallTime(hallsArr) { (hallTiming,success) in
                    completion(hallTiming,true)
                }
            }
        }
    }
    
    private func createRestaurantTimeModel(jsonData: Data?) -> [Hall] {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] , let data = jsonDic["data"] as? [String: Any]{
            return Hall().getHallDetail(data["halls"] as? NSArray ?? [])
        }
        return []
    }
    
    func getSelectedHallTime(_ halls: [Hall],completion: ([HallTiming], Bool) -> Void) {
        let localCartDic = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]
        let hallid = localCartDic!["hall_id"] as? Int ?? 0
        for hall in halls {
            //
            if hallid == hall.id {
                self.checkRstaurantClosed(timing: hall.timing) { (success) in
                    completion(hall.timing,success)
                }
                break
            }
        }
    }
    
    func checkRstaurantClosed(timing: [HallTiming], completion:(Bool) -> Void) {
        UserModel.shared.hallTimings = timing
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE"
        let currentDay = dateFormatterPrint.string(from: Date())
        
        for time in timing {
            if(time.day.caseInsensitiveCompare(currentDay) == ComparisonResult.orderedSame){
                if self.matchTime(timing: time) {
//                    isRestaurantClose = true
                    completion(true)
                } else {
                    completion(false)
                }
                break
            }
        }
    }
    
    func matchTime(timing: HallTiming) -> Bool {
        
        for time in timing.timeSlot {
            let currentTime = Date().getTime()
            let currentDateTime = ((Date().getDate())) + " at " + currentTime
            let finalCurrentDateTime = currentDateTime.getTimeWithDate()
            
            let hallendTimeString = (Date().getDate()) + " at " + time.endTime
            let hallendTimeDate = hallendTimeString.getTimeWithDate()
            
            let hallStartTimeString = (Date().getDate()) + " at " + time.startTime
            let hallStartTimeDate = hallStartTimeString.getTimeWithDate()
            
            if finalCurrentDateTime == hallStartTimeDate || finalCurrentDateTime == hallendTimeDate{
                return false
            }
            if finalCurrentDateTime > hallStartTimeDate && finalCurrentDateTime < hallendTimeDate{
                return false
            }
            if finalCurrentDateTime < hallStartTimeDate || finalCurrentDateTime > hallendTimeDate{
                return true
            }
            
        }
        return true
    }
}
