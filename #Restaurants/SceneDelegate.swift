//
//  SceneDelegate.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import Firebase
import FirebaseMessaging
import UserNotifications
import Braintree

var sceneDelegate = UIApplication.shared.delegate as? SceneDelegate
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var scene: UIWindowScene?
    let appUrlScheme = "com.hashtagrestaurants.app.payments"//"com.hashtagrestaurants.app.payments"

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let scene = (scene as? UIWindowScene) else { return }
        sceneDelegate = self
        self.setCustomSettings()
        self.scene = scene
        self.setRootViewController()
        window?.overrideUserInterfaceStyle = .light
    }
    
    func setCustomSettings() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    UserDefaults.standard.set(UIDevice.current.identifierForVendor!.uuidString, forKey: UserDefault.deviceId)
        BTAppSwitch.setReturnURLScheme(appUrlScheme)

    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        self.showUpdateVersionAlertView()
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        self.showUpdateVersionAlertView()

    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let context = URLContexts.first else {
            fatalError("Open url called without a context. This should never happen.")
        }
        
        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: context.url,
            sourceApplication: nil,
            annotation: nil
        )
    }
    
}

extension SceneDelegate {
    func setRootViewController() {
        self.moveToSplashView()
    }
    

    
    func moveToSplashView() {
        DispatchQueue.main.async {
            if let windowScene = self.scene {
                self.window = UIWindow(windowScene: windowScene)
                if let magicTabBar = Router.viewController(with: .splash, .login) as? SplashVC {
                    let rootNC = UINavigationController(rootViewController: magicTabBar)
                    rootNC.setNavigationBarHidden(true, animated: false)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
                }
            }
        }
    }
}

extension SceneDelegate {
    
    
    func showUpdateVersionAlertView() {
        if self.appUpdateAvailable() {
            let alert = UIAlertController(title: "New Version Available", message:"There is a new version available for download! Please update the app by visiting the Apple Store.", preferredStyle: UIAlertController.Style.alert)
            
            let actionPreview = UIAlertAction(title: "Update", style: .default) { (action) in
                
                let urlStr = "https://apps.apple.com/us/app/restaurants/id1519302274?ls=1"
                
                if let url = URL(string: urlStr) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            
            alert.addAction(actionPreview)
            window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    func appUpdateAvailable() -> Bool
    {
        guard let info = Bundle.main.infoDictionary,
              let identifier = info["CFBundleIdentifier"] as? String
        else {
            return true
        }
        
        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=\(identifier)"
        var upgradeAvailable = false
        // Get the main bundle of the app so that we can determine the app's version number
        if let infoDictionary = Bundle.main.infoDictionary {
            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
            guard let urlOnAppStore = URL(string: storeInfoURL) else {
                return false
            }
            do {
                let dataInJSON = try Data(contentsOf: urlOnAppStore)
                
                if let dict: NSDictionary = try? JSONSerialization.jsonObject(with: dataInJSON, options: .allowFragments) as! NSDictionary {
                    if let results:NSArray = dict["results"] as? NSArray {
                        if results.count > 0, let version = (results[0] as AnyObject).value(forKey: "version") as? String {
                            // Get the version number of the current version installed on device
                            if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
                                // Check if they are the same. If not, an upgrade is available.
                                print("\(version)")
                                if version > currentVersion {
                                    upgradeAvailable = true
                                }
                            }
                        }
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        return upgradeAvailable
    }
    
}
