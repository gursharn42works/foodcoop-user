//
//  FacebookManager.swift
//  #Restaurants
//
//  Created by Satveer on 27/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import FBSDKLoginKit

class FacebookManager {
    
    class func openFacebookManager(_ onCompletion: @escaping (NSDictionary) -> Void) {
        Utility.shared.startLoading()
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["public_profile","email"/*,"user_friends","user_birthday"*/], from: nil) { (result, error) in

            if error != nil {
                print(error as Any)
                Utility.shared.stopLoading()
                onCompletion([:])
            }else{
                if AccessToken.current != nil {
                    let accessToken = AccessToken.current
                    self.getFBUserData((accessToken?.tokenString)!, onCompletion: { (dic)  in
                        Utility.shared.stopLoading()
                        onCompletion(dic)
                    })
                    loginManager.logOut()
                }else{
                    Utility.shared.stopLoading()
                    onCompletion([:])
                }
            }
        }
    }
    
    class func getFBUserData(_ token: String, onCompletion: @escaping (NSDictionary) -> Void) {
        
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    let dict = result as! NSDictionary
                    if dict["email"] == nil  {
                        Utility.shared.showAlertView(message: "Email is required to login.")
                    }
                    print(dict)
                    onCompletion(dict)
                } else {
                    onCompletion([:])
                }
            })
        }
    }
}
