//
//  CancellableDelayTask.swift
//  Magic
//
//  Created by Apple1 on 07/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class CancellableDelayedTask {
    
	var cancelled = false
    
	func run(delay: Double, task: @escaping () -> Void ) {
        let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            if !self.cancelled {
                task()
            }
        }
    }
    
	func cancel() {
        cancelled = true
    }
}


//func delay(_ delay:Double, closure:@escaping ()->()) {
//    DispatchQueue.main.asyncAfter(
//        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
//}
