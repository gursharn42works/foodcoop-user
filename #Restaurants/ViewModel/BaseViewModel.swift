//
//  BaseViewModel.swift
//  Magic
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class BaseViewModel {
    
    //MARK:- Variables
    var isLoading: Bool = false {
        didSet {
            showNoInternetScreen = false
            updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var showEmptyMsg: Bool? {
        didSet {
            showEmptyMsgClausre?()
        }
    }
    var isEndRefresh : Bool? {
        didSet {
            endRefreshClausre?()
        }
    }
    var showNoInternetScreen : Bool? {
        didSet {
            showNoInternetView?()
        }
    }
    
    //MARK:- Clausers
    var endRefreshClausre:(()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var reloadTableViewClosure: (()->())?
    var showEmptyMsgClausre: (()->())?
    var showNoInternetView: (()->())?
    var responseRecieved: ((NSDictionary)->())?
    
    init() {
        
    }
}
