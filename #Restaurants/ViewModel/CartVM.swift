//
//  CartVM.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class CartVM: NSObject {
    
    func getBrainTreeClientToken(completion: @escaping(String) -> Void) {
        ServiceManager.shared.callGetRequest(url: ApiName.brainTreeClientToken, parameters: [:]) { (jsonData) in
          
            let cartTokenModel = try? JSONDecoder().decode(CartTokenModel.self, from: jsonData)
            if let model = cartTokenModel {
                completion(model.data.token)
            } else {
                completion("")
            }
        }
    }
    
    func getDefaultAddressList(completion: @escaping(DeliveryAddressModel) -> Void) {
        self.getDefaultAddressRequest(url: ApiName.defaultDeliveryAddress, parameters: ["user_id":UserModel.shared.user_id]) { (jsonData) in
            completion(self.createDefaultAddressDataModel(jsonData: jsonData))
        }
    }
    
    func getDefaultAddressRequest(url: String, parameters:[String: Any], completion: @escaping(Data?) -> Void) {
        Utility.shared.startLoading()

        let methodName = url.replacingOccurrences(of: " ", with: "%20")
        
        request(URL(string: ServerUrl.link + methodName)!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [HeaderKey.headerKeyName: HeaderKey.customHeaderKey]).responseJSON { (response) in
            print(response)
            Utility.shared.stopLoading()

            if let data = response.data {
                completion(data)
            }
        }
    }
    
    private func createDefaultAddressDataModel(jsonData: Data?) -> DeliveryAddressModel {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                if response["success"] as? Int ?? 0 != 0 {
                    return DeliveryAddressModel().setDefaultAddressData(dic: data["addresses"] as? NSDictionary ?? [:])
                }
            }
        }
        return DeliveryAddressModel()
    }
}



// MARK: - Empty
struct CartTokenModel: Codable {
    let success: Bool
    let data: CartTokenDataClass
    let message: String
}

// MARK: - DataClass
struct CartTokenDataClass: Codable {
    let token: String
}
