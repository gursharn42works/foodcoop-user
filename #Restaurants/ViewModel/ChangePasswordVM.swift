//
//  ChangePasswordVM.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ChangePasswordVM: NSObject {
    
}
extension ChangePasswordVM {
    func changePassword(currentPassword: String, newPassword: String, confirmPassword: String, completion: @escaping(Bool)-> Void){
        
        if self.isValidData(currentPassword, newPassword, confirmPassword) { return  }
        
        let parameters = Parameters.changePasswordParameters(newPassword, oldpassord: currentPassword, confirmNewPwd: confirmPassword)
        
        ServiceManager.shared.callPostRequest(url: ApiName.changePassword, parameters: parameters) { (jsonData) in
            let changePasswordModel = try? JSONDecoder().decode(ChangePasswordModel.self, from: jsonData)
            if let model = changePasswordModel {
                Utility.shared.showAlertView(message: model.message)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

extension ChangePasswordVM {
    
    private func isValidData(_ currentPassword: String, _ newPassword: String, _ confirmPassword: String) -> Bool {
        if currentPassword == "" {
            Utility.shared.showAlertView(message: Messages.emptyPwd)
            return true
        } else if newPassword == ""{
            Utility.shared.showAlertView(message: Messages.emptyPwd)
            return true
        }else if confirmPassword == ""{
            Utility.shared.showAlertView(message: Messages.emptyConfirmPwd)
            return true
        }else if newPassword.length < 8 {
            Utility.shared.showAlertView(message: Messages.newPwdLength)
            return true
        }else if newPassword != confirmPassword{
            Utility.shared.showAlertView(message: Messages.pwdNotMatched)
            return true
        }

        return false
    }
}

// MARK: - ChangePasswordModel
struct ChangePasswordModel: Codable {
    let success: Bool
    let data: [JSONAny]
    let message: String
}
