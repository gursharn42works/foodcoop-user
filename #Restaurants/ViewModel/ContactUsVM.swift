//
//  ContactUsVM.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ContactUsVM: NSObject {

}

// MARK: - ContactUsModel
struct ContactUsModel: Codable {
    let success: Bool
    let data, message: String
}

extension ContactUsVM {
    func submitContactUs(name: String, email: String, subject: String, message: String, completion: @escaping()-> Void){
       
        if self.isValidData(name, email, subject, message) {
            return
        }
        let dic = [Constant.shared.name : name , Constant.shared.hall_id : UserModel.shared.hall_id,Constant.shared.email : email,Constant.shared.subject : subject,Constant.shared.message : message] as [String : Any]
        
        ServiceManager.shared.callPostRequest(url: ApiName.contactUs, parameters: dic) { (jsonData) in
           let contactUsModel = try? JSONDecoder().decode(ContactUsModel.self, from: jsonData)
            if let model = contactUsModel {
                Utility.shared.showAlertView(message: model.message)
                completion()
            } else {
                completion()
            }
        }
    }
    
    
}

extension ContactUsVM {
    
    private func isValidData(_ name: String,_ email: String,_ subject: String, _ message: String) -> Bool {
        if name == "" {
            Utility.shared.showAlertView(message: Messages.emptyName)
            return true
        }
        else if email == "" {
            Utility.shared.showAlertView(message: Messages.emptyEmail)
            return true
        }
        else if !email.isValidEmail() {
            Utility.shared.showAlertView(message: Messages.invalidEmail)
            return true
        }
        else if subject == "" {
            Utility.shared.showAlertView(message: Messages.emptySubject)
            return true
        }
        else if message == "" {
            Utility.shared.showAlertView(message: Messages.emptyMessage)
            return true
        }

        return false
    }
    
}
