//
//  AddDeliveryAddressVM.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class AddDeliveryAddressVM: NSObject {
    
    var states = ["AN:Andaman and Nicobar Islands",
                  "AP:Andhra Pradesh",
                  "AR:Arunachal Pradesh",
                  "AS:Assam",
                  "BR:Bihar",
                  "CH:Chandigarh",
                  "CT:Chhattisgarh",
                  "DN:Dadra and Nagar Haveli",
                  "DD:Daman and Diu",
                  "DL:Delhi",
                  "GA:Goa",
                  "GJ:Gujarat",
                  "HR:Haryana",
                  "HP:Himachal Pradesh",
                  "JK:Jammu and Kashmir",
                  "JH:Jharkhand",
                  "KA:Karnataka",
                  "KL:Kerala",
                  "LA:Ladakh",
                  "LD:Lakshadweep",
                  "MP:Madhya Pradesh",
                  "MH:Maharashtra",
                  "MN:Manipur",
                  "ML:Meghalaya",
                  "MZ:Mizoram",
                  "NL:Nagaland",
                  "OR:Odisha",
                  "PY:Puducherry",
                  "PB:Punjab",
                  "RJ:Rajasthan",
                  "SK:Sikkim",
                  "TN:Tamil Nadu",
                  "TS:Telangana",
                  "TR:Tripura",
                  "UP:Uttar Pradesh",
                  "UK:Uttarakhand",
                  "WB:West Bengal"]
    
    func callAddAddressService(_ addressId: Int,_ addressTitle: String, houseNum: String, street1: String, street2: String, city: String, state: String, zip: String, default_address: Int, lat: Double, lng: Double, completion: @escaping(Bool) -> Void) {
        
        //MARK:- Check Fields Data
        if self.isValidData(addressTitle, houseNum, street1, street2, city, state, zip) { return }
        
        let parameters = Parameters.addDeliveryAddressParameters(addressId,addressTitle: addressTitle, houseNum, street1, street2, city, state, zip, default_address, lat: lat, lng: lng)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: addressId != 0 ? ApiName.deliveryAddressUpdate:ApiName.deliveryAddressAdd, parameters: parameters) { (jsonData) in
            
            if self.createAddAddressDataModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createAddAddressDataModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
                if let message = response["message"] as? String{
                    Utility.shared.showAlertView(message: message)
                    return true
                }
        }
        return false
    }
}

extension AddDeliveryAddressVM {
    
     func isValidData(_ addressTitle: String, _ houseNum: String, _ street1: String, _ street2: String, _ city: String,_ state: String, _ zip: String) -> Bool {
        if addressTitle == "" {
            Utility.shared.showAlertView(message: Messages.emptyAddressTitle)
            return true
        }
        else if houseNum == "" {
            Utility.shared.showAlertView(message: Messages.emptyHouseNum)
            return true
        }
        else if street1 == "" {
            Utility.shared.showAlertView(message: Messages.emptyStreetOne)
            return true
        }
        else if street2 == "" {
            Utility.shared.showAlertView(message: Messages.emptyStreetTwo)
            return true
        }
        else if city == "" {
            Utility.shared.showAlertView(message: Messages.emptyCity)
            return true
        }
        else if state == "" {
            Utility.shared.showAlertView(message: Messages.emptyState)
            return true
        }
        else if zip == "" {
            Utility.shared.showAlertView(message: Messages.emptyZipcode)
            return true
        }
        else if zip == "" {
            Utility.shared.showAlertView(message: Messages.emptyZipcode)
            return true
        }
        
        return false
    }
    
}
