//
//  DeliveryAddressVM.swift
//  #Restaurants
//
//  Created by Satveer on 04/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class DeliveryAddressVM: NSObject {
    
   // var selectedAddressRow  = 0
    var controller: DeliveryAddressVC?
    var addresses = [DeliveryAddressModel]()
    var deliveryAddressModel = DeliveryAddressModel()
    
    func getDeliveryAddressesList(completion: @escaping(Bool)-> Void){
        ServiceManager.shared.callGetRequest(url: ApiName.deliveryAddressList, parameters: [Constant.shared.user_id: UserModel.shared.user_id]) { (jsonData) in
            if self.createDeliveryAddressDataModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createDeliveryAddressDataModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                if response["success"] as? Int ?? 0 != 0 {
                    self.addresses = DeliveryAddressModel().setDeliveryAddressData(array: data["addresses"] as? NSArray ?? [])
                    self.sortArrayWithBool()
                    return true
                }
            }
        }
        return false
    }
    
    func sortArrayWithBool() {
        var addressesArr = [DeliveryAddressModel]()
        for address in self.addresses {
            if address.default_address == 1 {
                addressesArr.insert(address, at: 0)
            } else {
                addressesArr.append(address)
            }
        }
        
        self.addresses = addressesArr
    }
    
    func callDeleteAddressService(_ addressId: Int, completion: @escaping(Bool) -> Void) {
        ServiceManager.shared.callPostRequest(url: ApiName.deliveryDeleteAddress, parameters: ["id": addressId]) { (jsonData) in
                completion(true)
        }
    }
    
    func callAddAddressApi(_ addressId: Int,_ addressTitle: String, houseNum: String, street1: String, street2: String, city: String, state: String, zip: String, default_address: Int,lat: Double, lng: Double, completion: @escaping(Bool) -> Void) {
        
        //MARK:- Check Fields Data
        let parameters = Parameters.addDeliveryAddressParameters(addressId,addressTitle: addressTitle, houseNum, street1, street2, city, state, zip, default_address,lat: lat, lng: lng)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: addressId != 0 ? ApiName.deliveryAddressUpdate:ApiName.deliveryAddressAdd, parameters: parameters) { (jsonData) in
            completion(true)

        }
    }
}

extension DeliveryAddressVM: UITableViewDataSource, UITableViewDelegate, DeliveryAddressListCellDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.deliveryAddressListCell, for: indexPath) as! DeliveryAddressListCell
        cell.deleteBtn.tag = indexPath.row
        cell.setDefaultBtn.tag = indexPath.row
        cell.delegate = self
        
        
        let address = self.addresses[indexPath.row]
        cell.addresstitleLbl.text = address.title.capitalizeFirstInEachWord()
        cell.addressDescLbl.text = "\(address.house_no), \(address.street1), \(address.street2), \(address.city.capitalized), \(address.state.capitalized), \(address.zipcode)."
        let isDefault = address.default_address == 1 ? true:false
        
        let defaultTitle = isDefault ? "   Default   " : " Set as Default "
        let attributeString = isDefault ? NSMutableAttributedString(string: defaultTitle,
                                                                    attributes: attrWithoutUnderline) : NSMutableAttributedString(string: defaultTitle,
                                                                                                                                  attributes: attrWithUnderline)
        cell.setDefaultBtn.setAttributedTitle(attributeString, for: .normal)
        
        cell.tickWidth.constant = 0

        if isDefault {
            cell.defaultBtnWidth.constant = 60
            cell.setDefaultBtn.backgroundColor = AppColor.primaryColor
            cell.setDefaultBtn.setTitleColor(UIColor.white, for: .normal)
        } else {
            cell.defaultBtnWidth.constant = 96
            cell.setDefaultBtn.backgroundColor = UIColor.white
            cell.setDefaultBtn.setTitleColor(AppColor.primaryColor, for: .normal)
        }
        
        //MARK: Get Last saved address from userdefault. If address not save the show tick on 0 index
        let savedAddress = DeliveryAddressModel().getSavedDeliveryAddress()
        if savedAddress.id == address.id {
            cell.tickWidth.constant = 20
        } else if indexPath.row == 0 && savedAddress.id == 0 {
            cell.tickWidth.constant = 20
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //MARK:- Save selected address in userdefault
        deliveryAddressModel.saveSelectedDeliveryAddress(address: self.addresses[indexPath.row])
        self.controller?.addressListTableView.reloadData()

    }
    
    func defaultClickFunc(index: Int) {
        if self.addresses.count == 0 {
            self.controller?.getDeliveryAddressList()
            return
        }
        let address = self.addresses[index]
        self.callAddAddressApi(address.id,address.title, houseNum: address.house_no, street1: address.street1, street2: address.street2, city: address.city, state: address.state, zip: address.zipcode, default_address: 1, lat: address.lat, lng: address.lng) { (success) in
            if success {
                //MARK:- Save selected address in userdefault
                self.deliveryAddressModel.saveSelectedDeliveryAddress(address: self.addresses[0])
                self.controller?.getDeliveryAddressList()
            }
        }
    }
    
    func crossClickFunc(index: Int) {
        let deletionAlert  = UIAlertController(title: "Please Verify!", message: Messages.addressDeletionMessage, preferredStyle: .alert)
        
        deletionAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        deletionAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            
            self.callDeleteAddressService(self.addresses[index].id) { (success) in
                
                let savedAddress = DeliveryAddressModel().getSavedDeliveryAddress()
                if savedAddress.id == self.addresses[index].id {
                    //MARK:- Save selected address in userdefault
                    self.deliveryAddressModel.saveSelectedDeliveryAddress(address: self.addresses[0])
                }
                
                self.addresses.remove(at: index)
                if self.addresses.count == 0 {
                    //MARK:- remove selected address in userdefault
                    self.deliveryAddressModel.removeSavedDeliveryAddress()
                    self.controller?.isMoveBack = true
                }
                
//                else {
//                    let savedAddress = DeliveryAddressModel().getSavedDeliveryAddress()
//                    if savedAddress.id == self.addresses[index-1].id {
//                        //MARK:- Save selected address in userdefault
//                        self.deliveryAddressModel.saveSelectedDeliveryAddress(address: self.addresses[0])
//                    }
//                }
                
                
                if index == 0 {
                    self.defaultClickFunc(index: 0)
                }else {
                    self.controller?.getDeliveryAddressList()
                }
            }
        }))
        
        self.controller?.present(deletionAlert, animated: true, completion: nil)
        
    }
    
    
    
    
}
