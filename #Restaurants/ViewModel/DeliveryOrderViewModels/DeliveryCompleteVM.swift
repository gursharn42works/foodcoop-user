//
//  DeliveryCompleteVM.swift
//  #Restaurants
//
//  Created by Satveer on 08/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class DeliveryCompleteVM: NSObject {

    func callRateDeliveryApi(delivery_id: String,order_id: String, rating: String, comment: String, order_rating: String, order_comment: String, dishrating: String,rest_Id: String,subject: String,message:String, completion:@escaping(Bool) -> Void) {
      
        
        let parameters = Parameters.rateDeliveryParameters(delivery_id, order_id, rating, comment, order_rating, order_comment, dishrating, rest_Id, subject, message)
        print(parameters)
        
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: ApiName.rateDelivery, parameters: parameters) { (jsonData) in
            completion(self.checkDeliveryRatingSuccesful(jsonData))
        }
    }
    
    private func checkDeliveryRatingSuccesful(_ jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
                if let message = response["message"] as? String, let success = response["success"] as? Int, success == 1 {
                    Utility.shared.showAlertView(message: message)
                    return true
                } else {
                    return false
                }
            
        }
        return false
    }
}
