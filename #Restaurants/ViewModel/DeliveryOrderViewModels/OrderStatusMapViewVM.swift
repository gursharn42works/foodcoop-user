//
//  OrderStatusMapViewVM.swift
//  #Restaurants
//
//  Created by Satveer on 06/01/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class OrderStatusMapViewVM: NSObject {

    func callDistanceApi(_ res_id: Int,completion: @escaping(Order)-> Void){
        //MARK:- Call Api
        let url = ApiName.distance
        
        let parameters = [Constant.shared.user_id: UserModel.shared.user_id, "res_id": res_id] as [String : Any]
        
        ServiceManager.shared.callGetRequest(url: url, parameters: parameters) { (jsonData) in
            completion(self.createOrderStatusModel(jsonData: jsonData))
        }
    }
    
    private func createOrderStatusModel(jsonData: Data?) -> Order {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            return Order().getOrder(jsonDic["data"] as? NSDictionary ?? [:])
        }
        return Order()
    }
}
