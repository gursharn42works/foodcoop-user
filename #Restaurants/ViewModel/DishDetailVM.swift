//
//  DishDetailVM.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol DishDetailVMDelage: class {
    func updateCartData(isCartUpdate: Bool)
}
class DishDetailVM: NSObject {
    var delegate: DishDetailVMDelage?
    var dish = DishModel()
    var isLocalCartUpdate : Bool?
    var localCartItemIndex : Int?

}

extension DishDetailVM {
    func getDishDetail(_ dish_id: Int,completion: @escaping(Bool)-> Void){
        //MARK:- Call Api
        let url = createUrl(dish_id)
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            
            if self.createDishDetailModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    //MARK:- Create Url
    func createUrl(_ dish_id: Int) -> String {
        if UserModel.shared.user_id != 0 {
            return ApiName.getDishDetail + "\(dish_id)" + "?\(Constant.shared.user_id)=\(UserModel.shared.user_id)"+"&\(Constant.shared.dish_id)=\(dish_id)"
        }
        else {
            return ApiName.getDishDetail + "\(dish_id)?"+"\(Constant.shared.dish_id)=\(dish_id)"
        }
    }
    
    private func createDishDetailModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let json = responseJSON as? NSDictionary, let data = json["data"] as? NSDictionary {
            self.dish = DishModel().dishItem(data["dish"] as? NSDictionary ?? [:])
            print(self.dish)
            return true
        }
        return false
    }
}

extension DishDetailVM {
    //MARK:- Add to Local cart
    func localAddToCart(_ dish : DishModel, _ count : Int?, _ price : Double) {
        
        var localCart = [String : Any]()
        var localCartItem = [[String : Any]]()
        // var commonMealIds = [String]()
        
        if let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]{
            
            localCart = savedLocalCart
            localCartItem = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
        }
        print(localCartItem)
        var cartItem = [String : Any]()
        print(price)
        cartItem["dish_id"] = dish.id ?? 0
        cartItem["item_name"] = dish.name ?? ""
        cartItem["qty"] = count ?? 0
        cartItem["status"] = dish.isDishAvailable
        cartItem["showtypeonapp"] = dish.showTypesOnApp
        cartItem["veg"] = dish.typeCommaSeparated ?? ""
        cartItem["price"] = dish.totalPrice()
        
        var localCartAddon = [[String : Any]]()
        var localcartVeriation = [String:Any]()
        var addOnId = [Int]()
        var variationname = String()
        var veriationService = String()
        var veriationAvailable = false
        // add veriations
        for item in dish.variations{
            
            if dish.variationService == "serving" {
                for serving in item.servings {
                    if serving.isDefault == true {
                        var addVariations = [String : Any]()
                        addVariations["variation"] = item.variation
                        variationname = item.variation
                        addVariations["variation_serving_title"] = serving.serving
                        addVariations["variation_serving"] = dish.variationService
                        cartItem["variationName"] = item.variation
                        cartItem["variationserving"] = serving.serving
                        veriationService = serving.serving
                        addVariations["option_id"] = serving.id
                        localcartVeriation = addVariations
                    }
                }
            }else {
                var addVariations = [String : Any]()
                if item.isDefault == true {
                    variationname = item.variation
                    veriationService = ""
                    addVariations["variation"] = item.variation
                    addVariations["variation_serving_title"] = ""
                    cartItem["variationName"] = item.variation
                    cartItem["variationserving"] = ""
                    addVariations["variation_serving"] = dish.variationService
                    addVariations["option_id"] = item.id
                    localcartVeriation = addVariations
                }
                
            }
            
            
        }
        
        if dish.variations.count == 0 {
            var addVariations = [String : Any]()
            addVariations["option_id"] = 0
            localcartVeriation = addVariations
        }
        
        // Add Addons to local cart
        
        for item in dish.addons{
            if item.isSelected ?? false{
                var cartAddon = [String : Any]()
                cartAddon["addon_id"] = item.id
                cartAddon["addon"] = item.addon
                cartAddon["price"] = item.price
                addOnId.append(item.id ?? 0)
                localCartAddon.append(cartAddon)
            }
        }
        
        cartItem["addons"] = localCartAddon
        cartItem["addOnId"] = addOnId
        cartItem["Veriations"] = localcartVeriation
        // Add customizations
        
        var localCartOptions = [[String : Any]]()
        var optionId = [Int]()
        for customization in dish.customizations{
            for option in customization.options{
                if option.isDefault ?? false && option.isAvailable ?? false {
                    var cartCustomization = [String : Any]()
                    cartCustomization["ingredient_id"] = customization.id
                    cartCustomization["ingredient"] = customization.ingredient
                    cartCustomization["option"] = option.name
                    cartCustomization["option_id"] = option.id
                    optionId.append(option.id ?? 0)
                    localCartOptions.append(cartCustomization)
                }
            }
        }
        
        cartItem["optionId"] = optionId
        cartItem["options"] = localCartOptions
      
        // Add Serving
        var localCartServing = [String : Any]()
        var servingId = [Int]()
        for serving in dish.serving ?? [DishServingModel](){
            if serving.isDefault ?? false{
                localCartServing["ingredient"] = serving.serving
                localCartServing["option_id"] = serving.id
                servingId.append(serving.id ?? 0)
            }
        }
        cartItem["servingId"] = servingId
        cartItem["serving"] = localCartServing
       // cartItem["veg"] = dish.type == .veg ? "Veg" : "Non-veg"
        
        var index = -1
        for i in 0..<localCartItem.count{
            let item = localCartItem[i]
            if ((item["dish_id"] as? Int) == (cartItem["dish_id"] as? Int)){
                let status = checkIfAddonsAlreadyExists((item["addOnId"] as? [Int]) ?? [Int](), newCartItemAddonId: (cartItem["addOnId"] as? [Int]) ?? [Int]())
                let statusOptions = checkIfOptionIdAlreadyExists((item["optionId"] as? [Int]) ?? [Int](), newCartItemOptionId: (cartItem["optionId"] as? [Int]) ?? [Int]())
                let statusServing = checkIfServingIdAlreadyExists((item["servingId"] as? [Int]) ?? [Int](), newCartItemServingId: (cartItem["servingId"] as? [Int]) ?? [Int]())
                if dish.dish_type == "variable" {
                    
                    if (item["variationName"] as? String ?? "" ) == variationname {
                        if veriationService != "" {
                            if item["variationserving"] as! String == veriationService {
                                veriationAvailable = true
                                if status && statusOptions && statusServing{
                                    index = i
                                }
                            }
                        }else {
                            veriationAvailable = true
                            if status && statusOptions && statusServing{
                                index = i
                            }
                        }
                       
                    }
                }else {
                    if status && statusOptions && statusServing{
                        index = i
                    }
                }
                
                
               
            }
        }
        if index != -1
        {
            var item = localCartItem[index]
            if isLocalCartUpdate ?? false
            {
                if index == localCartItemIndex
                {
                    item["qty"] = count
                    localCartItem[index] = item
                }else{
                    
                    if dish.dish_type == "variable" {
                        if veriationAvailable {
                            let qty = item["qty"] as? Int ?? 0
                            let newQty = qty + (count ?? 0)
                            item["qty"] = newQty
                        }else {
                            localCartItem.remove(at: localCartItemIndex ?? 0)
                            localCartItem[localCartItemIndex ?? 0] = cartItem
                        }
                    }else {
                        let qty = item["qty"] as? Int ?? 0
                        let newQty = qty + (count ?? 0)
                        item["qty"] = newQty
                        localCartItem[index] = item
                        localCartItem.remove(at: localCartItemIndex ?? 0)
                    }
                    
                    
                }

            }else{
                
                if dish.dish_type == "variable" {
                    if veriationAvailable {
                        
                        let qty = item["qty"] as? Int ?? 0
                            let newQty = qty + (count ?? 0)
                            item["qty"] = newQty
                            localCartItem[index] = item
                        
                    }else {
                        localCartItem.append(cartItem)
                    }
                }else {
                    let qty = item["qty"] as? Int ?? 0
                    let newQty = qty + (count ?? 0)
                    item["qty"] = newQty
                    localCartItem[index] = item
                }
                
                
               
            }

        }else{
            if isLocalCartUpdate ?? false{
                localCartItem[localCartItemIndex ?? 0] = cartItem
            }else{
                
                localCartItem.append(cartItem)
            }
        }
        
        localCart["items"] = localCartItem
        var localCartCount = 0
        var localcartTotal = 0.0
        for items in localCartItem{
            localCartCount += (items["qty"] as? Int) ?? 0
            localcartTotal += (((items["price"] as? Double) ?? 0.0) * (Double((items["qty"] as? Int) ?? 0)))
        }
        print(localcartTotal)
        localCart["cart_total"] = localcartTotal
        localCart["cart_count"] = localCartCount
        localCart["hall_id"] = dish.hallId
        localCart["hall_name"] = dish.hallName
        localCart["isCouponApplied"] = false
        localCart["is_accepting_orders"] = dish.isAcceptingOrder == true ? "yes" : "no"
        
        print(localCart)
        UserModel.shared.cartItemsCount = localCartCount
        UserModel.shared.cartItemsPrice = localCart["cart_total"] as? Double ?? 0.0
        UserDefaults.standard.set(localCart, forKey: "localCart")
        UserDefaults.standard.synchronize()
        self.delegate?.updateCartData(isCartUpdate: true)
    }
    
    
    func checkIfAddonsAlreadyExists(_ existingCartItemAddonId : [Int], newCartItemAddonId : [Int]) -> Bool{
        if existingCartItemAddonId.count != newCartItemAddonId.count{
            return false
        }
        let existingAddons = existingCartItemAddonId.sorted()
        let newAddons = newCartItemAddonId.sorted()
        if existingAddons == newAddons{
            return true
        }
        return false
    }
    
    func checkIfOptionIdAlreadyExists(_ existingItemOptionId : [Int], newCartItemOptionId : [Int]) -> Bool{
        let existingOptions = existingItemOptionId.sorted()
        let newOptions = newCartItemOptionId.sorted()
        
        if existingOptions == newOptions{
            return true
        }
        return false
    }
    
    func checkIfServingIdAlreadyExists(_ existingItemServingId : [Int], newCartItemServingId : [Int]) -> Bool{
        let existingOptions = existingItemServingId.sorted()
        let newOptions = newCartItemServingId.sorted()
        if existingOptions == newOptions{
            return true
        }
        return false
    }
}
