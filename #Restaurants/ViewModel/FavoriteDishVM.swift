//
//  FavoriteDishVM.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol FavoriteDishVMDelegate: class {
    func reloadFavouriteTableView()
    func moveToDishDetailFromFavorite(dish_id: Int)
}

class FavoriteDishVM: NSObject {
    var favoriteDishs: [FavoriteDishModel] = []
    var tableView = UITableView()
    var delegate: FavoriteDishVMDelegate?
}

extension FavoriteDishVM: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.favoriteDishs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.hallDishCell, for: indexPath)as! HallDishCell
        
        self.showCellData(self.favoriteDishs[indexPath.row], cell)
        cell.favBtn.tag = indexPath.row

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.favoriteDishs[indexPath.row].status == 0 {
            Utility.shared.showAlertView(message: Messages.dishNotAvailable)
        } else {
            self.delegate?.moveToDishDetailFromFavorite(dish_id: self.favoriteDishs[indexPath.row].id)
        }
    }
    
    private func showCellData(_ dish: FavoriteDishModel?, _ cell: HallDishCell?) {
        
        cell?.containerView.alpha = dish?.status == 0 ? 0.5 : 1
        cell?.favBtn.addTarget(self, action: #selector(self.favBtn_Click(_:)), for: .touchUpInside)
        
        cell?.favBtn.isSelected = dish?.favorited ?? false
        cell?.productImage.set_image(dish?.image ?? "", placeholder: "")
        cell?.dishName.text = dish?.name ?? ""
        cell?.price.smartDecimalText = UserModel.shared.currencyCode  /*"$"*/ + "\(String(format: "%.2f",dish?.price ?? 0))"
        
        cell?.calories.numberOfLines = dish?.customizable == false ?  3 : 2
        cell?.calories.text = dish?.dishDescription
        
        if Utility.shared.favDishArr.contains(dish?.id ?? 0) {
            cell?.favDish.image = #imageLiteral(resourceName: "heart filled")
        } else {
            cell?.favDish.image = dish?.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
        }
        
        cell?.allergies.isHidden = dish?.customizable == true ? false : true
        cell?.allergies.text =  dish?.customizable == true ? "customizations available" : "" //GSD
        
        if dish?.showtypeonapp == 1 , let veg = dish?.veg {
            cell?.dishTypeStackView.showDishTypes(veg)
        } else {
            cell?.dishTypeStackView.isHidden = true
        }
        
    }
    
    @objc func favBtn_Click(_ sender : UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
            if UserModel.shared.user_id != 0 {
                let dish = self.favoriteDishs[sender.tag]
                
                sender.isSelected = !sender.isSelected
                cell.favDish.image = dish.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
                
                //MARK:- Remove unlike dish from array
                self.favoriteDishs.remove(at: indexPath.row)
                self.delegate?.reloadFavouriteTableView()
                
                self.likeUnlikeService(dish.id) { (succes) in
                }
            }
        }
    }
}

extension FavoriteDishVM {
    func getFavoriteList(limit: Int, page: Int,completion: @escaping(Bool)-> Void){
        //MARK:- Call Api
        let url = createUrl(limit, page)
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            if self.createFvoriteItemsModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }

        }
    }
    
    //MARK:- Create Url
    func createUrl(_ limit: Int,_ page: Int) -> String {
        if UserModel.shared.user_id != 0 {
            return ApiName.favouriteDishList + "?\(Constant.shared.user_id)=\(UserModel.shared.user_id )&\(Constant.shared.limit)=\(limit)&\(Constant.shared.page)=\(page)" + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)"
        }
        
        return ""
    }
    
    private func createFvoriteItemsModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                if response["success"] as? Int ?? 0 != 0 {
                    self.favoriteDishs = FavoriteDishModel().setFavoriteItemsData(array: data["dishes"] as? NSArray ?? [])
                    return true
                }
            }
        }
        return false
    }
}
