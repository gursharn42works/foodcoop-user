//
//  FilterVM.swift
//  #Restaurants
//
//  Created by Satveer on 30/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol FilterVMDelegate:class {
    func reloadTableView(selectedFilter: String)
}

class FilterVM: NSObject {
    var filters: MenuFilters?
    var selectedValue = ""
    var delegate: FilterVMDelegate?
}

extension FilterVM: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filters?.sort.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.filterCell, for: indexPath) as! FilterCell
        cell.nameLabel.text = filters?.sort[indexPath.row]
        
        cell.nameLabel.textColor = self.selectedValue == filters?.sort[indexPath.row] ?? "" ? AppColor.primaryColor : AppColor.darkTextColor
        cell.nameLabel.text =  self.selectedValue == filters?.sort[indexPath.row] ?? "" ? "\u{2713} \((filters?.sort[indexPath.row]) ?? "")" : "\u{2001} \((filters?.sort[indexPath.row]) ?? "")"

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedValue = filters?.sort[indexPath.row] ?? ""
        delegate?.reloadTableView(selectedFilter: filters?.sort[indexPath.row] ?? "")
    }
}
