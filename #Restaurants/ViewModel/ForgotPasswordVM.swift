//
//  ForgotPasswordVM.swift
//  #Restaurants
//
//  Created by Satveer on 11/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

// MARK: - Empty
struct ForgotPasswordModel: Codable {
    let success: Bool
    let data: [JSONAny]
    let message: String
}

class ForgotPasswordVM: NSObject {

    func callForgotPasswordService(email: String, completion:@escaping(Bool) -> Void) {
      
        //MARK:- Check Fields Data
        if self.isValidData(email){ return }
        
        let parameters = [Constant.shared.email: email]
        print(parameters)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: ApiName.forgotPwd, parameters: parameters) { (jsonData) in
            
            let forgotPasswordModel = try? JSONDecoder().decode(ForgotPasswordModel.self, from: jsonData)
            
            if let model = forgotPasswordModel {
                if model.success {
                    Utility.shared.showAlertView(message: model.message)
                    completion(true)
                }
            } else {
                completion(true)
            }
        }
    }
}

extension ForgotPasswordVM {
    
    private func isValidData(_ email: String) -> Bool {
        if email == "" {
            Utility.shared.showAlertView(message: Messages.emptyEmail)
            return true
        }
        else if !email.isValidEmail() {
            Utility.shared.showAlertView(message: Messages.invalidEmail)
            return true
        }
        return false
    }
    
}
