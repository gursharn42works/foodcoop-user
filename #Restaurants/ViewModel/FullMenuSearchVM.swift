//
//  FullMenuSearchVM.swift
//  #Restaurants
//
//  Created by Satveer on 02/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit


class FullMenuSearchVM: NSObject {
    var nibname = "HallDishCell"
    var dishes = [MenuDishes]()

    //MARK:- Methods are used to get home screen data
    func getFullMenuSearchDataData(search: String,completion: @escaping(Bool)-> Void){
        //MARK:- Call Api
        ServiceManager.shared.getRequest(url:"rest-search?rest_id=\(selectedHallid)&q=\(search)" , parameters: [:]) { (jsonData,bool) in
            
            if self.createHomeModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createHomeModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any], let data = jsonDic["data"] as? [String: Any]{
            self.dishes = FullMenuModel().getDishes(dishes: data["dishes"] as? NSArray ?? [])
            return true
        }
        return false
    }
}


