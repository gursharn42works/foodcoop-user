//
//  FullMenuVM.swift
//  #Restaurants
//
//  Created by Satveer on 29/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FullMenuVM: NSObject {
    var fullMenuModel: FullMenuModel?

    func getFullMenuData(filter: String,rest_id: Int,tag_id: Int, searchTxt : String,completion: @escaping(Bool)-> Void){
        ServiceManager.shared.callGetRequest(url: createUrl(filter: filter,rest_id: rest_id,tag_id: tag_id, search: searchTxt), parameters: [:]) { (jsonData) in
            if self.createFullMenuDataModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }

        }
    }
    
    //MARK:- Create Url
    private func createUrl(filter: String,rest_id: Int,tag_id: Int,search:String) -> String {
        if UserModel.shared.user_id != 0 {
            //ApiName.fullMenu + self.addFilters(filter) + "&\(Constant.shared.user_id)=\(UserModel.shared.user_id)&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)"
            return ApiName.RestFullMenu + "\(Constant.shared.restId)=\(rest_id)" + "&\(Constant.shared.tagId)=\(tag_id)" + "&q=\(search)" + "&\(Constant.shared.sort)=\(filter)"
        }else{
            
            //ApiName.fullMenu + self.addFilters(filter) + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)"
            return ApiName.RestFullMenu + "\(Constant.shared.restId)=\(rest_id)" + "&\(Constant.shared.tagId)=\(tag_id)" + "&q=\(search)" + "&\(Constant.shared.sort)=\(filter)"

        }
        
    }
    
    
    private func createFullMenuDataModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                print(data)
                if response["success"] as? Int ?? 0 != 0 {
                    self.fullMenuModel = FullMenuModel().setFullMenuData(data: data)
                    return true
                }
            }
        }
        return false
    }
}
