//
//  HomeVM.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

protocol HomeVMDelegate: class {
    func reloadPageControl(index: Int)
    func moveToDishDetailView(rest_id: Int,tag_Id: Int, type:String)
}

class HomeVM: NSObject {
    var delegate: HomeVMDelegate?
    var homeModel: HomeModel?
    var tableHeight:CGFloat = 0
    var tabSec = ["Restaurants Around You", "Select Your Favourite Dish"]
    var images = ["Burger","Dosa","icecream","noodels","Pizza","PlainDosa"]
}

extension HomeVM: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeModel?.banners.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.shared.homeBannerCell, for: indexPath) as! HomeBannerCell
        if let banner = homeModel?.banners[indexPath.row] {
            cell.showBanner(banner: banner)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.delegate?.reloadPageControl(index: indexPath.row)
    }
    
    
}

extension HomeVM: UITableViewDataSource, UITableViewDelegate,HomeItemsCellDelegate,RestItemsTableCellDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if homeModel?.announcement?.message == nil {
                return self.tableHeight
            }else {
                if indexPath.row == 0 {
                    return UITableView.automaticDimension
                }else {
                    return self.tableHeight
                }
            }
           
        
        }else {
            let tags = homeModel?.tagsArr.count ?? 0
            let finalHeight = (tags/4)*108
            return CGFloat(finalHeight + 85)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tabSec.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if homeModel?.announcement?.message == nil {
                return 1
            }else {
                return 2
            }
            
        }else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if homeModel?.announcement?.message == nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.homeItemsCell, for: indexPath) as! HomeItemsCell
                if let restArr = homeModel?.restaurantsArr  {
                        cell.restaurantsArr = restArr
                }
                cell.delegate = self
                cell.collectionView.reloadData()
                return cell
            }else {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.alertCell, for: indexPath) as! ShowAlertTableCell
                  
                    cell.alertTextLbl.text = homeModel?.announcement?.message
                    cell.timeLbl.text = homeModel?.announcement?.timing
                    return cell
                }else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.homeItemsCell, for: indexPath) as! HomeItemsCell
                    if let restArr = homeModel?.restaurantsArr  {
                            cell.restaurantsArr = restArr
                    }
                    cell.delegate = self
                    cell.collectionView.reloadData()
                    return cell
                }
            }
            
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemsCell", for: indexPath) as! RestItemsTableCell
            
            if let tags = homeModel?.tagsArr {
                cell.tagsArr = tags
            }
            cell.delegate = self
            cell.itemsCollectionView.reloadData()
            
            
            return cell
        }
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//          let headerView = UIView()
//        headerView.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 0.8)
//            let myLabel = UILabel()
//
//        myLabel.frame = CGRect(x: 8, y: 10, width: Int(headerView.frame.width), height: 30)
//            myLabel.font =  UIFont(name:"Poppins-Medium",size: 17.0) //UIFont.boldSystemFont(ofSize: 16) //
//            myLabel.text = tabSec[section]
//            myLabel.sizeToFit()
//            headerView.addSubview(myLabel)
//
//        return headerView
//    }
    
//     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//            return 40
//
//
//        }
    
    func moveToRestView(dish_id: Int, tag_id: Int, type: String) {
        self.delegate?.moveToDishDetailView(rest_id: dish_id, tag_Id: tag_id, type: type)
    }
    
    func moveToRestDetailView(rest_id: Int,tag_id : Int, type : String ) {
        self.delegate?.moveToDishDetailView(rest_id: rest_id, tag_Id: tag_id, type: type)
    }
}

extension HomeVM {
    //MARK:- Methods are used to get home screen data
    func getHomeData(completion: @escaping(Bool)-> Void){
        //MARK:- Call Api
        ServiceManager.shared.callGetRequest(url: createUrl(), parameters: [:]) { (jsonData) in
            
            if self.createHomeModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createHomeModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            self.homeModel = HomeModel().homeProductItems(jsonDic["data"] as? NSDictionary ?? [:])
            return true
        }
        return false
    }
    
    //MARK:- Create Url
    func createUrl() -> String {
        if UserModel.shared.user_id != 0 {
            return ApiName.getHomeProduct + "?\(Constant.shared.user_id)=\(UserModel.shared.user_id)&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&\(Constant.shared.timezone)=\(TimeZone.current.identifier)"+self.addFilters("")
        }
        else {
            return ApiName.getHomeProduct + "?\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&\(Constant.shared.timezone)=\(TimeZone.current.identifier)"+self.addFilters("")
        }
    }
    
}
//MARK:- Methods is used to get Ordering Data List
extension HomeVM {
    func getOrderingFromData(completion: @escaping(RestaurantListModel)-> Void){
        //MARK:- Call Api
        ServiceManager.shared.callGetRequest(url: ApiName.locationList, parameters: [:]) { (jsonData) in
            completion(self.createOrderringDataModel(jsonData: jsonData))
        }
    }
    
    private func createOrderringDataModel(jsonData: Data?) -> RestaurantListModel {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? NSDictionary {
            if let data = response["data"] as? NSDictionary{
                // let message = data["message"] as? String ?? ""
                if response["success"] as? Int ?? 0 != 0 {
                   return RestaurantListModel().setRestaurantListModelData(dic: data)
                    
                }
                
            }
        }
        return RestaurantListModel()
    }
}

extension HomeVM {
    
    func getDefaultAddressList(completion: @escaping(DeliveryAddressModel) -> Void) {
        self.getDefaultAddressRequest(url: ApiName.defaultDeliveryAddress, parameters: ["user_id":UserModel.shared.user_id]) { (jsonData) in
            completion(self.createDefaultAddressDataModel(jsonData: jsonData))
        }
    }
    
    func getDefaultAddressRequest(url: String, parameters:[String: Any], completion: @escaping(Data?) -> Void) {
        let methodName = url.replacingOccurrences(of: " ", with: "%20")
        
        request(URL(string: ServerUrl.link + methodName)!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: [HeaderKey.headerKeyName: HeaderKey.customHeaderKey]).responseJSON { (response) in
            print(response)
            if let data = response.data {
                completion(data)
            }
        }
    }
    
    private func createDefaultAddressDataModel(jsonData: Data?) -> DeliveryAddressModel {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                if response["success"] as? Int ?? 0 != 0 {
                    return DeliveryAddressModel().setDefaultAddressData(dic: data["addresses"] as? NSDictionary ?? [:])
                }
            }
        }
        return DeliveryAddressModel()
    }
}

