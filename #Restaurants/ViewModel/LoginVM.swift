//
//  LoginVM.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class LoginVM: NSObject {
    
    func callLoginService(_ email: String,_ password: String, completion:@escaping(Bool) -> Void) {
        //MARK:- Check Fields Data
        if self.isValidData(email, password) { return }
        let parameters = Parameters.loginParameters(email, password: password)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: ApiName.login, parameters: parameters) { (jsonData) in
            
            if self.createLoginModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createLoginModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                if response["success"] as? Int ?? 0 != 0 {
                    LoginModel().setData(dic: data)
                    return true
                }
            }
        }
        return false
    }
}

extension LoginVM {
    func appleLoginService(_ name: String,_ email: String, _ profile_pic: String, _ social_token: String, completion:@escaping(Bool) -> Void) {
        
        let parameters = Parameters.appleLoginParameters(email, name: name,profile_pic, social_token)
        
        print(parameters)
        //MARK:- Call Api
        ServiceManager.shared.loginWithMedia(url: ApiName.loginWithMedia, parameters: parameters) { (jsonData) in
                
            if self.createFacebookDataModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

extension LoginVM {
    func facebookLoginService(dic: NSDictionary, completion:@escaping(Bool) -> Void) {
        
        let parameters = Parameters.facebookLoginParameters(data: dic)
        
        print(parameters)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: ApiName.loginWithMedia, parameters: parameters) { (jsonData) in
            
            if self.createFacebookDataModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
            
        }
    }
    
    private func createFacebookDataModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            let dic = jsonDic["data"] as? [String: Any] ?? [:]
          let data = FaceBookModel.init(city: dic["city"] as? String ?? "", country: dic["country"] as? String ?? "", default_card_id: dic["default_card_id"] as? String ?? "", email: dic["email"] as? String ?? "", id: dic["id"] as? Int ?? 0, is_accepting_orders: dic["is_accepting_orders"] as? String ?? "", name: dic["name"] as? String ?? "", phone: dic["phone"] as? String ?? "", profile_pic: dic["profile_pic"] as? String ?? "", stripe_id: dic["stripe_id"] as? String ?? "", student_id: dic["student_id"] as? String ?? "")
            
            UserModel.shared.saveUserData(user_id: data.id, apple_id: "", currency_code: "", user_name: data.name, country: data.country, is_accepting_orders: data.is_accepting_orders, profile_pic: data.profile_pic, phone: data.phone, email: data.email,stripe_id: data.stripe_id, card_id: data.default_card_id)
            
            if jsonDic["success"] as? Int ?? 0 != 0 {
                return true
            } else {
                return false
            }

        }
        
        return false

    }
}

extension LoginVM {
    //MARK:- Check user data
    func checkUserData(_ loginModel: LoginModel,completion: @escaping(Bool)-> Void) {
            let role = loginModel.role
            
            if role == Constant.shared.role {
                Utility.shared.showAlertView(message: Messages.invalidLoginDetail)
                return
            }
            self.getUserData(loginModel) { (success) in
                if success {
                    completion(success)
                }
            }
    }
    
    //MARK:- Get user data according user id
    private func getUserData(_ loginModel: LoginModel,completion: @escaping(Bool)-> Void) {
        
        ServiceManager.shared.callGetRequest(url: ApiName.userProfileDetail + "\(loginModel.id ?? 0)", parameters: [:]) { (jsonData) in
            
            self.saveUserData(data: jsonData)
            completion(true)

        }
    }
}

extension LoginVM {
    
    private func isValidData(_ email: String, _ password: String) -> Bool {
        if email == "" {
            Utility.shared.showAlertView(message: Messages.emptyEmail)
            return true
        }
        else if !email.isValidEmail() {
            Utility.shared.showAlertView(message: Messages.invalidEmail)
            return true
        }
        else if password == "" {
            Utility.shared.showAlertView(message: Messages.emptyPwd)
            return true
        }
        return false
    }
    
}


