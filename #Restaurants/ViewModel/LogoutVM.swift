//
//  LogoutVM.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


class LogoutVM: NSObject {
    func logoutUser(completion: @escaping(Bool) -> Void) {
        let params = ["\(Constant.shared.device_id)" : UserModel.shared.device_id,"\(Constant.shared.user_id)" : UserModel.shared.user_id] as [String : Any]
        
        ServiceManager.shared.callPostRequest(url: ApiName.logoutUser + "\(UserModel.shared.user_id)" + "/logout", parameters: params) { (jsonData) in
            
            let logOutModel = try? JSONDecoder().decode(LogOutModel.self, from: jsonData)

            if let model = logOutModel {
                Utility.shared.showAlertView(message: model.message)
            }
            completion(true)
        }
    }
}

// MARK: - Empty
struct LogOutModel: Codable {
    let success: Bool
    let data, message: String
}
