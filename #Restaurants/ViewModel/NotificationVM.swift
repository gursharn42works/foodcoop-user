//
//  NotificationVM.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol NotificationVMDelagte: class {
    func moveToOrderDetailOnClick(order_id: Int,status: String)
}

class NotificationVM: NSObject {
    var notifications: [NotificationModel] = []
    var delegate: NotificationVMDelagte?
}

extension NotificationVM: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.notificationCell, for: indexPath)as! NotificationCell
        
        let notifications = self.notifications[indexPath.row]
        
        cell.titleLbl.text = "\(notifications.data?.order_id ?? 0)" != "0" ? "Order No. #" + "\(notifications.data?.order_id ?? 0)" : notifications.title
        cell.detailLbl.text = notifications.message
        cell.timeLbl.text = notifications.created_at.convertDateFormat("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy, hh:mm a")

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.notifications[indexPath.row].data
        self.delegate?.moveToOrderDetailOnClick(order_id: data?.order_id ?? 0, status: data?.status ?? "")
    }
}

extension NotificationVM {
    func getNotifications(limit: Int,page: Int,completion: @escaping(Bool)-> Void){

        let dic = [Constant.shared.limit : limit , Constant.shared.page : page]
        let url = ApiName.notificationList+"\(UserModel.shared.user_id)"
        
        ServiceManager.shared.callPostRequest(url: url, parameters: dic) { (jsonData) in
        
            if self.makeDataToNSDictionary(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func makeDataToNSDictionary(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let responseJSON = responseJSON as? [String: Any] {
           // print(responseJSON)
            
            let data = responseJSON["data"] as? [String:Any] ?? [:]
            let notifications = data["notifications"] as? NSArray ?? []
            self.notifications.removeAll()
            for i in 0..<notifications.count {
                let dic = notifications[i] as? NSDictionary ?? [:]
                let data = dic["data"] as? NSDictionary ?? [:]
                let object = NotificationModel.init(id: dic["id"] as? Int ?? 0, notification_type: dic["notification_type"] as? String ?? "", message: dic["message"] as? String ?? "", title: dic["title"] as? String ?? "", student_id: dic["student_id"] as? Int ?? 0, hall_id: dic["hall_id"] as? Int ?? 0, data: NotificationData.init(push_type: data["push_type"] as? String ?? "", order_id: data["order_id"] as? Int ?? 0, status: data["status"] as? String ?? ""), is_read: dic["is_read"] as? Int ?? 0, status: dic["status"] as? String ?? "", created_at: dic["created_at"] as? String ?? "", updated_at: dic["updated_at"] as? String ?? "", location_title: dic["location_title"] as? String ?? "")
                
                self.notifications.append(object)
            }
            return true
        } else {
            return false
        }
    }
    
    func deleteNotifications(completion: @escaping(Bool)-> Void){
        //MARK:- Call Api
        let url = ApiName.notificationStatus+"\(UserModel.shared.user_id)"+"/0"
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in

            if self.makeDataToNSDictionary(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }

        }
    }
}

