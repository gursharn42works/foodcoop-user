//
//  OrderHistoryVM.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class OrderHistoryVM: NSObject {
}

extension OrderHistoryVM {
    func getHistoryOrders(_ type: String, limit: Int, page: Int,completion: @escaping(Data)-> Void){
        //MARK:- Call Api
        let url = createUrl(type, limit, page)
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            Utility.shared.stopLoading()
                completion(jsonData)
        }
    }
    
    //MARK:- Create Url
    func createUrl(_ type: String,_  limit: Int,_ page: Int) -> String {
        if UserModel.shared.user_id != 0 {
            return "\(ApiName.orderList)?\(Constant.shared.user_id)=\(UserModel.shared.user_id)&\(Constant.shared.type)=\(type)&\(Constant.shared.limit)=\(limit)&\(Constant.shared.page)=\(page)"
        }
        return ""
    }
}


