//
//  OrderStatusVM.swift
//  #Restaurants
//
//  Created by Satveer on 05/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class OrderStatusVM: NSObject {
}

extension OrderStatusVM {
    func getOrderDetails(_ order_id: String,completion: @escaping(Order)-> Void){
        //MARK:- Call Api
        let url = ApiName.orderDetail+"?\(Constant.shared.order_id)=\(order_id)"
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            completion(self.createOrderStatusModel(jsonData: jsonData))
        }
    }
    
    private func createOrderStatusModel(jsonData: Data?) -> Order {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let jsonDic = responseJSON as? [String: Any] {
            return Order().getOrder(jsonDic["data"] as? NSDictionary ?? [:])
        }
        return Order()
    }
}
