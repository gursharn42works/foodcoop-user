//
//  PersonalInfoVM.swift
//  #Restaurants
//
//  Created by Satveer on 04/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PersonalInfoVM: NSObject {
    
    func saveChangesInformation(_ name: String, _ phone: String, completion:@escaping (PersonalInfoModel?) -> Void) {
        
        if self.isValidData(name, phone, email: "") { return }
        
        let url = ApiName.updateUserProfile + "\(UserModel.shared.user_id)"
        let parameters = Parameters.personalInfoParameters(name, phone)
        
        ServiceManager.shared.callPostRequest(url: url, parameters: parameters) { (jsonData) in
            self.saveUserData(data: jsonData)
                do {
                let jsonDic = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String:AnyObject]
                    if let dic = jsonDic {
                        
                        let model = PersonalInfoModel.setData(dic: dic["data"] as? [String: Any] ?? [:])
                        completion(model)
                    }

                } catch {
                    completion(nil)

                }
        }
    }
}

extension PersonalInfoVM {
    func uploadUserImage(data: Data,completion: @escaping(Bool)-> Void){
        let url = ApiName.uploadImage + "\(UserModel.shared.user_id)"
        ServiceManager.shared.uploadImage(url: url, parameters: ["image" : data]) { (jsonData) in
            
                self.saveUserData(data: jsonData)
            
            let responseJSON = try? JSONSerialization.jsonObject(with: jsonData, options: [])
            if let response = responseJSON as? [String: Any] {
                    Utility.shared.showAlertView(message: response["message"] as? String ?? "")
                
            }

            completion(true)
        }
    }
}

extension PersonalInfoVM {
    
    private func isValidData(_ name: String, _ mobile: String, email: String) -> Bool {
        if name == "" {
            Utility.shared.showAlertView(message: Messages.emptyName)
            return true
        }
        else if mobile == "" {
            Utility.shared.showAlertView(message: Messages.emptyMobile)
            return true
        }
        else if mobile.count < 10 {
            Utility.shared.showAlertView(message: Messages.invalidMobile)
            return true
        }
        return false
    }
}


