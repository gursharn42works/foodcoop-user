//
//  RestaurantListVM.swift
//  #Restaurants
//
//  Created by 42works on 22/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RestaurantListVM: NSObject {
    var addressModel: RestaurantListModel?
}

extension RestaurantListVM: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressModel?.locations.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.restaurantListCell, for: indexPath) as! RestaurantListCell
        
        if let location = addressModel?.locations[indexPath.row] {
            cell.locationTitleLbl.text = location.locationTitle
          
            var addressArr = [String]()
            addressArr.append(location.addressLine1)
            addressArr.append(location.addressLine2)
            addressArr.append(location.city )
            addressArr.append(location.state)
            addressArr.append(location.zip)
            
            var address = ""
            for i in 0..<addressArr.count {
                let add = addressArr[i]
                if add != "" {
                    if i < addressArr.count-1 {
                        address.append("\(add), ")
                    } else {
                        address.append(add)
                    }
                }
            }
            cell.addressLineLbl.text = address
        }
        cell.cellBtn.tag = indexPath.row
        cell.iconImgView.isHidden = indexPath.row != UserModel.shared.selectedLocation
//        if indexPath.row == (addressModel?.locations.count ?? 0)-1 {
//        }
        
        return cell
    }
    
    
    
}
