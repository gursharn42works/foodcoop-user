//
//  RestaurantSearchVM.swift
//  #Restaurants
//
//  Created by 42works on 31/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol RestaurantSearchVMDelegate : class {
    func moveToDishDetailFromSearch(dish_id: Int)
    func reloadSearchViewTableView()
    func onFavClickTap(dishModel: DishModel)
    func getSearchPaginationData()
    func jumpToMenu(rest_Id: Int)
}

class RestaurantSearchVM : NSObject  {
    var tableView = UITableView()
    var delegate: RestaurantSearchVMDelegate?

    var AllRestaurants = [Restaurants]()

   // var query = ""
    var limit = 20
    var page = 1
    var category_id = 0
}

extension RestaurantSearchVM : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllRestaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell", for: indexPath)as? RestaurantsTableViewCell
        let restImage  = AllRestaurants[indexPath.row].thumb
        let bg_image = AllRestaurants[indexPath.row].bg_image
        cell?.restName.text = AllRestaurants[indexPath.row].name
        let rating = AllRestaurants[indexPath.row].rating ?? 0.0
        cell?.ratingLbl_Out.text = "\(rating)"
        cell?.ratingView.isHidden = rating == 0.0 ? true:false
        
        if bg_image == "" {
            cell?.restImage.set_image(restImage!, placeholder: "dishBackground_bg")
            //cell?.restImage.sd_setImage(with: URL(string: restImage ?? ""), placeholderImage: UIImage(named: "dishBackground_bg"))
        }else {
            //cell?.restImage.sd_setImage(with: URL(string: bg_image ?? ""), placeholderImage: UIImage(named: "dishBackground_bg"))
            cell?.restImage.set_image(bg_image!, placeholder: "dishBackground_bg")
        }
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let RestId = AllRestaurants[indexPath.row].id
        self.delegate?.jumpToMenu(rest_Id: RestId!)
    
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
                return UITableView.automaticDimension
    
        }
}

extension RestaurantSearchVM {
    func getSearchData(_ tagId : Int, restStr : String ,showDefault : Bool, _ resetFilter : Bool = false,completion: @escaping(Bool)-> Void){
      

        //MARK:- Call Api
        let url = createUrl(tagId: tagId, searchtxt: restStr)
        ServiceManager.shared.getRequest(url: url, parameters: [:]) { (jsonData,success) in
           
            if self.createSearchDishModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }

        }
    }
    
    private func createSearchDishModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? NSDictionary {
            if let data = response.value(forKey: "data")as? NSDictionary{
                if let dishes = data.value(forKey: "restaurants")as? NSArray
                {
                    let tagsArr = Restaurants().getRestaurantArr(dishes)
                    AllRestaurants = tagsArr

                }
            }
            return true
        }
        return false
    }
    
    //MARK:- Create Url
    func createUrl(tagId: Int,searchtxt: String) -> String {
        if UserModel.shared.user_id != 0 {
            
          //  ApiName.searchDish + self.addSearchFilters("") + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&\(Constant.shared.user_id)=\(UserModel.shared.user_id )&q=\(query)&limit=\(limit)&page=\(page)&category_id=\(category_id)"
            
            return ApiName.RestWithtag + "tag_id=\(tagId)&q=\(searchtxt)"
        }
        else {
            //ApiName.searchDish + self.addSearchFilters("") + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&q=\(query)&limit=\(limit)&page=\(page)&category_id=\(category_id)"
            
            return ApiName.RestWithtag + "tag_id=\(tagId)&q=\(searchtxt)" //&from_restaurant=true
        }
    }
    
    private func addSearchFilters(_ filter: String) -> String {
        return "?dietary=\("")&cuisine=\("")&sort=\(filter)"
    }
    
}

extension RestaurantSearchVM : UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.reloadProducts(scrollView)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.reloadProducts(scrollView)
        }
    }

    func reloadProducts(_ scrollView: UIScrollView) {
        if scrollView == tableView {
//            if scrollView.frame.size.height + scrollView.contentOffset.y >= scrollView.contentSize.height, self.dishesArr.count % limit == 0 {
//                self.delegate!.getSearchPaginationData()
//
//            }
        }
    }

}
