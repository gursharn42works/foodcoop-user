//
//  RestaurantTimeVM.swift
//  #Restaurants
//
//  Created by Satveer on 06/08/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RestaurantTimeVM: NSObject {
    var hallTiming: [HallTiming] = []
    var tableView = UITableView()
}

extension RestaurantTimeVM: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let timingHeaderView = tableView.dequeueReusableCell(withIdentifier: Constant.shared.productCatagoryCell) as! ProductCatagoryCell
      timingHeaderView.catagoryName.text = timingHeaderView.getTimeHeading(section)
      timingHeaderView.arrowDown.isHidden = true
      return timingHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if hallTiming.count == 0 {
                return 1
            }else if hallTiming[section].timeSlot.count == 0 {
                return 1
            }else{
                return hallTiming[section].timeSlot.count
            }
        default:
            if hallTiming.count == 0 {
                return 1
            }else if hallTiming[section].timeSlot.count == 0 {
                return 1
            }else{
                return hallTiming[section].timeSlot.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.restaurantTimeCell, for: indexPath) as! RestaurantTimeCell
        
        if hallTiming.count > 0 && hallTiming[indexPath.section].timeSlot.count > 0 {
            cell.timeLbl.text = "\(hallTiming[indexPath.section].timeSlot[indexPath.row].startTime ?? "") - \(hallTiming[indexPath.section].timeSlot[indexPath.row].endTime ?? "")"
        }
        else {
            cell.timeLbl.text = "Restaurant is Closed."
        }

        return cell
    }
}

