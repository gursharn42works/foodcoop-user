//
//  SearchVM.swift
//  #Restaurants
//
//  Created by Satveer on 31/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol SearchVMDelegate:class {
    func moveToDishDetailFromSearch(dish_id: Int, type: String, itemname: String)
    func reloadSearchViewTableView()
    func onFavClickTap(dishModel: DishModel)
    func getSearchPaginationData()
}

class SearchVM: NSObject  {
    var tableView = UITableView()
    var delegate: SearchVMDelegate?
    var dishesArr = [DishModel]()
    var allDishesArr = [DishModel]()
    var AllSearchValues = [SearchItems]()

   // var query = ""
    var limit = 20
    var page = 1
    var category_id = 0
}

extension SearchVM: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.AllSearchValues.count == 0 {
                self.tableView.setEmptyMessage("No results found")
            } else {
                self.tableView.restore()
            }
        return self.AllSearchValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell", for: indexPath)as? SearchTableViewCell
        let restImage  = AllSearchValues[indexPath.row].thumb ?? ""
        let logo = AllSearchValues[indexPath.row].logo ?? ""
        let image = AllSearchValues[indexPath.row].image ?? ""
        
        cell?.titleName.text = AllSearchValues[indexPath.row].name
        
        
        if AllSearchValues[indexPath.row].type == "tag" {
            cell?.image_Out.set_image(logo, placeholder: "dishBackground_bg")
            cell?.image_Out.layer.cornerRadius = (cell?.image_Out.frame.height)! / 2
            let count = AllSearchValues[indexPath.row].restaurants_tagged ?? 0
            cell?.subTitleName.text = "\(count) Restaurants"
        }else if AllSearchValues[indexPath.row].type == "dish" {
            cell?.image_Out.set_image(image, placeholder: "dishBackground_bg")
            cell?.image_Out.layer.cornerRadius = (cell?.image_Out.frame.height)! / 2
            cell?.subTitleName.text = AllSearchValues[indexPath.row].type
        }else {
            cell?.image_Out.set_image(restImage, placeholder: "dishBackground_bg")
            cell?.image_Out.layer.cornerRadius = 10
            cell?.subTitleName.text = AllSearchValues[indexPath.row].city
        }
        
    
        return cell! 
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if !(dishesArr[indexPath.row].isDishAvailable ?? true){
//            Utility.shared.showAlertView(message: Messages.dishNotAvailable)
//        } else {
        let type = AllSearchValues[indexPath.row].type
        let id = AllSearchValues[indexPath.row].id ?? 0
        switch type {
        case "tag":
            self.delegate?.moveToDishDetailFromSearch(dish_id: id, type: "tag", itemname: AllSearchValues[indexPath.row].name ?? "" )
            
        case "restaurant":
            self.delegate?.moveToDishDetailFromSearch(dish_id: id, type: "restaurant", itemname: AllSearchValues[indexPath.row].name ?? "")
            
        case "dish":
            self.delegate?.moveToDishDetailFromSearch(dish_id: id, type: "dish", itemname: AllSearchValues[indexPath.row].name ?? "")
            
        default:
            print("Have you done something new?")
        }
        
        
       // }
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
                return UITableView.automaticDimension
    
        }
}

extension SearchVM {
    func getSearchData(_ query : String ,showDefault : Bool, _ resetFilter : Bool = false,completion: @escaping(Bool)-> Void){
        Utility.shared.startLoading()
        if !resetFilter || !showDefault {
            if query.trim().length < 1 {
                return
            }
        }
        //MARK:- Call Api
        let url = createUrl(query: query)
        ServiceManager.shared.getRequest(url: url, parameters: [:]) { (jsonData,success) in
            Utility.shared.stopLoading()
            if self.createSearchDishModel(query, showDefault: showDefault, resetFilter, jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }

        }
    }
    
    private func createSearchDishModel(_ query : String ,showDefault : Bool, _ resetFilter : Bool = false, jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? NSDictionary {
            if let data = response.value(forKey: "data")as? NSDictionary{
                if let dishes = data.value(forKey: "tags")as? NSArray
                {
                    
                    let tagsArr = SearchItems().getSearchItemsArr(dishes)
                    let restArr = SearchItems().getSearchItemsArr(data.value(forKey: "restaurants")as? NSArray ?? NSArray())
                    let dishesarr = SearchItems().getSearchItemsArr(data.value(forKey: "dishes")as? NSArray ?? NSArray())
                    AllSearchValues = tagsArr + dishesarr + restArr
//                    if showDefault
//                    {
//                        for item in dishes {
//                            let dishDic = DishModel().dishItem(item as? NSDictionary ?? NSDictionary())
//                           // if let status = dishDic.isDishAvailable { //status == true
//                                self.dishesArr.append(DishModel().dishItem(item as? NSDictionary ?? NSDictionary()))
//                           // }
//                        }
//                        if self.page == 1{
//                            self.allDishesArr = self.dishesArr
//                        }
//                        return true
//                    }
//                    else{
//                        self.dishesArr = DishModel().getDishItems(dishes)
//                        return true
//
//                    }
                }
            }
            return true
           // self.searchTablevView.reloadData()
            //self.updateLabel()
        }
        return false
    }
    
    //MARK:- Create Url
    func createUrl(query: String) -> String {
        if UserModel.shared.user_id != 0 {
            
          //  ApiName.searchDish + self.addSearchFilters("") + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&\(Constant.shared.user_id)=\(UserModel.shared.user_id )&q=\(query)&limit=\(limit)&page=\(page)&category_id=\(category_id)"
            
            return ApiName.searchDish + "?q=\(query)"
        }
        else {
            //ApiName.searchDish + self.addSearchFilters("") + "&\(Constant.shared.hall_id)=\(UserModel.shared.hall_id)&q=\(query)&limit=\(limit)&page=\(page)&category_id=\(category_id)"
            
            return ApiName.searchDish + "?q=\(query)" //&from_restaurant=true
        }
    }
    
    private func addSearchFilters(_ filter: String) -> String {
        return "?dietary=\("")&cuisine=\("")&sort=\(filter)"
    }
    
}

extension SearchVM: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       // self.reloadProducts(scrollView)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
      //      self.reloadProducts(scrollView)
        }
    }

    func reloadProducts(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if scrollView.frame.size.height + scrollView.contentOffset.y >= scrollView.contentSize.height, self.dishesArr.count % limit == 0 {
                self.delegate!.getSearchPaginationData()

            }
        }
    }

}
