//
//  SignUpVM.swift
//  #Restaurants
//
//  Created by 42works on 24/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SignUpVM: NSObject {
    
    func callSignUpService(_ name: String,_ email: String, phone: String, password: String, confirmPassword: String, isTermsAccepted: Bool, completion:@escaping(Bool) -> Void) {
      
        //MARK:- Check Fields Data
        if self.isValidData(name, email, phone, password, confirmPassword, isTermsAccepted: isTermsAccepted){ return }
        
        let parameters = Parameters.signUpParameters(name, email, phone, password: password, confirmPassword)
        print(parameters)
        //MARK:- Call Api
        ServiceManager.shared.callPostRequest(url: ApiName.register, parameters: parameters) { (jsonData) in
                        
            if self.createSignUpModel(jsonData: jsonData) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func createSignUpModel(jsonData: Data?) -> Bool {
        let responseJSON = try? JSONSerialization.jsonObject(with: jsonData!, options: [])
        if let response = responseJSON as? [String: Any] {
            if let data = response["data"] as? [String: Any]{
                // let message = data["message"] as? String ?? ""
                if response["success"] as? Int ?? 0 != 0 {
                    LoginModel().setData(dic: data)
                    return true
                }
                
            }
        }
        return false
    }
}

extension SignUpVM {
    
    private func isValidData(_ name: String, _ email: String, _ phone: String,_ password: String, _ confirmPassword: String,isTermsAccepted: Bool) -> Bool {
        if name == "" {
            Utility.shared.showAlertView(message: Messages.emptyName)
            return true
        }
        if email == "" {
            Utility.shared.showAlertView(message: Messages.emptyEmail)
            return true
        }
        else if !email.isValidEmail() {
            Utility.shared.showAlertView(message: Messages.invalidEmail)
            return true
        }
        else if phone == "" {
            Utility.shared.showAlertView(message: Messages.emptyMobile)
            return true
        }
        if phone.length < 10 {
            Utility.shared.showAlertView(message: Messages.invalidMobile)
            return true
        }
        else if password == "" {
            Utility.shared.showAlertView(message: Messages.emptyPwd)
            return true
        }
        else if password.length < 8 {
            Utility.shared.showAlertView(message: Messages.minCharPWd)
            return true
        }
        else if confirmPassword == "" {
            Utility.shared.showAlertView(message: Messages.emptyConfirmPwd)
            return true
        }
        else if confirmPassword.length < 8{
            Utility.shared.showAlertView(message: Messages.minCharPWd)
            return true
        }
        else if confirmPassword != password {
            Utility.shared.showAlertView(message: Messages.confirmPwdAlertMsg)
            return true
        }
        else if isTermsAccepted == false {
            Utility.shared.showAlertView(message: Messages.termsNotSelected)
            return true
        }
        
        return false
    }
}

