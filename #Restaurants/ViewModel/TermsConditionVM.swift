//
//  TermsConditionVM.swift
//  #Restaurants
//
//  Created by 42works on 24/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class TermsConditionVM: NSObject {
    
    func getContent(_ string : String, completion:@escaping (TermsModel?) -> Void){
        
        let url = ApiName.staticPages + "?slug=\(string)"
        
        ServiceManager.shared.callGetRequest(url: url, parameters: [:]) { (jsonData) in
            
            let termsModel = try? JSONDecoder().decode(TermsModel.self, from: jsonData)
            if let model = termsModel {
                completion(model)
            } else {
                completion(termsModel)
            }
        }
    }
}
