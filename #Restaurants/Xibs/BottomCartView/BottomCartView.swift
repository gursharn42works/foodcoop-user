//
//  HeaderView.swift
//  Expand Track
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol BottomCartViewDelegate: class {
    func moveToCartViewController()
}

class BottomCartView: UIView {
    		
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var openCartBtn : UIButton!

    var delegate: BottomCartViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("BottomCartView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
 
    
    @IBAction func openCart(_ sender : UIButton){
        self.delegate?.moveToCartViewController()
    }
}
