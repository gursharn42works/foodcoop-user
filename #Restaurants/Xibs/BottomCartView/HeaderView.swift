//
//  HeaderView.swift
//  Expand Track
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class BottomCart: UIView {
    //MARK:- Variables
    var isAlreadyBack: Bool = false
    var backTapClausre:(()->())?
    var logoutClausre:(()->())?
    var profileTapClausre:(()->())?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageBack: UIImageView!
    @IBOutlet weak var constarintLogoLeading: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.imageBack.transform = CGAffineTransform(rotationAngle: .pi)
    }
    
    @IBAction func backTap() {
        backTapClausre?()
//        isAlreadyBack = false
//        if !isAlreadyBack {
//            (AppDel.window?.rootViewController as? UINavigationController)?.popViewController(animated: true)
//        }
//        isAlreadyBack = true
    }
    
    @IBAction func logoutTap() {
        logoutClausre?()
    }
    
    @IBAction func profileTap() {
        profileTapClausre?()
    }
}
