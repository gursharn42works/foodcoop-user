//
//  CancellationPolicyView.swift
//  #Restaurants
//
//  Created by Satveer on 11/08/20.
//  Copyright © 2020 Apple1. All rights reserved.
//

import UIKit
import WebKit

protocol CancellationPolicyViewDelegate: class {
    func callCancelliationApi(order_id: String)
}

class CancellationPolicyView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var confirmBtnHeight: NSLayoutConstraint!
    @IBOutlet var textViewHeiht: NSLayoutConstraint!
    @IBOutlet var textViewBackViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var Confirm_Btn_Out: UIButton!
    var delegate: CancellationPolicyViewDelegate?
    
    var order_id = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CancellationPolicyView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        self.textView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

    }
    
    @IBAction func confirm_Click(_ sender: UIButton) {
        self.delegate?.callCancelliationApi(order_id: self.order_id)
    }
    
    @IBAction func cross_Click(_ sender: UIButton) {
        self.isHidden = true
    }
    
    func showHtmlDataOnWebView(string: String) {
//        DispatchQueue.main.async {
            //			let str = string+string
            //	self.scrollView.setContentOffset(CGPoint.zero, animated: true)
            self.textView.attributedText = string.htmlToAttributedString
            self.textView.translatesAutoresizingMaskIntoConstraints = true
            self.textView.sizeToFit()
            self.textView.isScrollEnabled = false
            self.textView.font = UIFont.systemFont(ofSize: 15)
            self.textViewHeiht.constant = self.textView.contentSize.height

//            if self.textView.contentSize.height < self.frame.size.height-100 {
//                self.textViewHeiht.constant = self.textView.contentSize.height
//
//                if self.isDeviceIsIphoneX() {
//                    if self.textView.contentSize.height < 200 {
//                        self.textViewBackViewHeight.constant = 210
//                    } else {
//                        self.textViewBackViewHeight.constant = self.textView.contentSize.height+94
//                    }
//                } else {
//
//                    if self.textView.contentSize.height < 200 {
//                        self.textViewBackViewHeight.constant = 200
//                    } else {
//                        self.textViewBackViewHeight.constant = self.textView.contentSize.height
//                    }
//                }
//            } else {
            //    self.textViewHeiht.constant = self.textView.contentSize.height
                
              //  if self.isDeviceIsIphoneX() {
               //     self.textViewBackViewHeight.constant = self.frame.size.height-175
//                } else {
//                    self.textViewBackViewHeight.constant = self.frame.size.height-115
//                }
                
          //  }
      //  }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey] {
                let newsize  = newvalue as! CGSize
                self.textViewHeiht.constant = newsize.height
                if self.textView.contentSize.height > self.frame.size.height-200 {
                    self.textViewHeiht.constant = self.frame.size.height-200
                    self.textView.isScrollEnabled = true
                } else {
                    self.textView.isScrollEnabled = false
                }
                
                self.textViewBackViewHeight.constant = self.textViewHeiht.constant+60

//                DispatchQueue.main.async {
                    self.confirmBtnHeight.constant = 40
//                }

            }
        }
    }

    private func isDeviceIsIphoneX() -> Bool {
//        if self.frame.width == 414 && self.frame.height == 896 || self.frame.width == 375 && self.frame.height == 812  {
//
//            print("iPhone X")
//            return true
//        } else {
//
//            print("not iPhone X")
//            return false
//
//        }
        switch UIScreen.main.nativeBounds.height {
                case 960:
                    return false
                case 1136:
                    return false
                case 1334:
                    return false
                case 1792:
                    return true
                case 1920, 2208:
                    return true
                case 2436:
                    return true
                case 2688:
                    return true
                default:
                    return false
                }
        
    }
    
}

