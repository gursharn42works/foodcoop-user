//
//  CartMessageView.swift
//  #Restaurants
//
//  Created by Satveer on 02/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol CartMessageViewDelegate: class {
    func moveToHomeViewOnBrowseBtnClick()
}
class CartMessageView: UIView {
    
    @IBOutlet var contentView: UIView!
    
    var delegate: CartMessageViewDelegate?
    
    var order_id = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CartMessageView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func browseBtnClick(_ sender: UIButton) {
        self.delegate?.moveToHomeViewOnBrowseBtnClick()
    }
        
}
