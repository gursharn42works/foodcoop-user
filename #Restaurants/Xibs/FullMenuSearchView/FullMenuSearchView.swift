//
//  FullMenuSearchView.swift
//  #Restaurants
//
//  Created by Satveer on 03/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol FullMenuSearchViewDelegate: class {
    func openReapeatItemView(dishs: [MenuDishes], index: Int, itemCount: Int, childController: UITableView)
    func updateViewDataOnMinusBtnClick()
    func moveToMenuDetailView(dishId: Int)
    func reloadView()
}

class FullMenuSearchView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTxt: UITextField!

    var fullMenuSearchVM = FullMenuSearchVM()
    var nibname = "HallDishCell"
    var delegate: FullMenuSearchViewDelegate?
    var selectedIndex = 0
    var itemCount = 1
    var searchString = ""
    var isHideView = false
    var readmoreindex = NSMutableArray()
    var emptyText = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("FullMenuSearchView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        tableView.register(UINib(nibName: nibname, bundle: nil), forCellReuseIdentifier: nibname)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        searchTxt.addTarget(self, action: #selector(searchTextChanged(_:)), for: .editingChanged)
        self.tableView.separatorColor = UIColor.clear
       // self.getMenuSearchData(searchString)
    }
    override open func layoutSubviews() {
        super.layoutSubviews()
        self.setSearchViewShadow()
    }

    
    @objc func searchTextChanged(_ sender: UITextField) {
        searchString = sender.text ?? ""
        getMenuSearchData(searchString)
    }

    
    func getMenuSearchData(_ search: String) {
        if search != "" {
            emptyText = search
            self.tableView.isHidden = false
            self.fullMenuSearchVM.getFullMenuSearchDataData(search: search) { (success) in
                if success {
                    self.tableView.reloadData()
                }
            }
        }
        else {
            emptyText = search
                self.fullMenuSearchVM.dishes.removeAll()
            self.tableView.isHidden = true
                self.tableView.reloadData()
            
            
        }
        
    }
    
    @IBAction func crossBtnClick(_ sender : Any){
        searchTxt.text = ""
        self.isHideView = true
        self.isHidden = true
    }
    
    func setSearchViewShadow() {
        let shadow = UIBezierPath(roundedRect: searchView.bounds, cornerRadius: 10).cgPath
        searchView.layer.shadowRadius = 5
        searchView.layer.shadowColor = UIColor.black.cgColor
        searchView.layer.shadowOpacity = 0.3
        searchView.layer.masksToBounds = false
        searchView.layer.shadowPath = shadow
        
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async {
            textField.resignFirstResponder()  }
        
        return true
    }

}

extension FullMenuSearchView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.fullMenuSearchVM.dishes.count == 0 {
            if emptyText == "" {
                self.tableView.setEmptyMessage("")
            }else {
                self.tableView.setEmptyMessage("No results found")
            }
            } else {
                    self.tableView.restore()
            }
        return self.fullMenuSearchVM.dishes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shared.hallDishCell, for: indexPath) as! HallDishCell

        self.showCellData(self.fullMenuSearchVM.dishes[indexPath.row], cell)
        cell.favBtn.tag = indexPath.row
        cell.addBtn_Out.tag = indexPath.row
        cell.minusBtn_Out.tag = indexPath.row
        cell.plusBtn_Out.tag = indexPath.row
        cell.readMoreBtn_Out.tag = indexPath.row
        return cell
    }
    
    
    private func showCellData(_ dish: MenuDishes?, _ cell: HallDishCell?) {
        
        let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        
        if savedLocalCart.count > 0 {
            
            let hallId = savedLocalCart["hall_id"] as? Int ?? 0
            let cartData = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
            if hallId == dish!.hall_id {
                if cartData.count > 0 {
                    if cartData.contains(where: { ($0["dish_id"] as! Int) == dish?.id }) {
                        let cartArr = cartData.filter{ ($0["dish_id"] as! Int) == dish?.id }
                        cell?.addBtn_Out.isHidden = true
                        let qty = cartArr[0]["qty"] as! Int
                        cell?.itemCountLbl.text = "\(qty)"
                        print(true)
                    }else {
                        cell?.addBtn_Out.isHidden = false
                       print(false)
                    }
                }
            }
            
        }
        
        cell?.containerView.alpha = dish?.status == 0 ? 0.5 : 1
        cell?.favBtn.addTarget(self, action: #selector(self.favBtn_Click(_:)), for: .touchUpInside)
        cell?.plusBtn_Out.addTarget(self, action: #selector(self.plusBtnClick(_:)), for: .touchUpInside)
        cell?.minusBtn_Out.addTarget(self, action: #selector(self.minusBtn_Click(_:)), for: .touchUpInside)
        cell?.addBtn_Out.addTarget(self, action: #selector(self.addBtnClick(_:)), for: .touchUpInside)
        cell?.readMoreBtn_Out.addTarget(self, action: #selector(self.readMoreClick(_:)), for: .touchUpInside)
        
        cell?.favBtn.isSelected = dish?.favorited ?? false
        cell?.productImage.set_image(dish?.image ?? "", placeholder: #imageLiteral(resourceName: "dishPlaceholder_300_200"))
        cell?.dishName.text = dish?.name.firstUppercased ?? ""
        cell?.price.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish?.price ?? "")"
      
        cell?.ratingLbl_Out.isHidden = dish?.rating == 0.0 ? true : false
        cell?.starImg_Out.isHidden = dish?.rating == 0.0 ? true : false
        cell?.starWidthConst.constant = dish?.rating == 0.0 ? 0 : 15
        cell?.ratingLbl_Out.text = String(dish?.rating ?? 0.0)

        //cell?.calories.numberOfLines = dish?.customizable == false ?  3 : 2
        cell?.calories.text = dish?.descript_ion
        if readmoreindex.contains(dish?.id ?? -1) {
            cell?.calories.numberOfLines = 0
            cell?.readMoreBtn_Out.isHidden = true
        } else {
            cell?.calories.numberOfLines = 2
            cell?.readMoreBtn_Out.isHidden = false

        }
        
        if (cell?.calories.retrieveTextHeight())! < 40.0 {
            cell?.readMoreBtn_Out.isHidden = true
        }
        
        cell?.favDish.image = dish?.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
        // }
        // cell?.bestSellerImg.isHidden = dish.isBestSeller

        cell?.bestSeller_Out.isHidden = dish?.best_seller == 0 ? true : false
        cell?.bestsellerWidthConst.constant = cell?.bestSeller_Out.isHidden == true ? 0 : 50
        cell?.recommended_Out.isHidden = dish?.recommended == 0 ? true : false

        cell?.allergies.isHidden = dish?.customizable == true ? false : true
        cell?.allergies.text =  dish?.customizable == true ? "customisable" : "" //GSD
        
        if let subviews = cell?.dishTypeStackView.arrangedSubviews {
            for dishIndicator in subviews {
                dishIndicator.isHidden = true
            }
        }
        
        if let veg = dish?.veg ,let isOn = dish?.showtypeonapp, isOn == 1 {
            cell?.dishTypeStackView.showDishTypes(veg)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.fullMenuSearchVM.dishes[indexPath.row].status == 0 {
            Utility.shared.showAlertView(message: Messages.dishNotAvailable)
        } else {
            if self.fullMenuSearchVM.dishes[indexPath.row].customizable {
                self.delegate?.moveToMenuDetailView(dishId: self.fullMenuSearchVM.dishes[indexPath.row].id)
            }
            
        }
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func favBtn_Click(_ sender : UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
            if UserModel.shared.user_id != 0 {
                let dish = self.fullMenuSearchVM.dishes[sender.tag]
                
                sender.isSelected = !sender.isSelected
                cell.favDish.image = dish.favorited == true ? #imageLiteral(resourceName: "heart filled") : #imageLiteral(resourceName: "heart unfill")
                
                self.fullMenuSearchVM.dishes[sender.tag].favorited = !self.fullMenuSearchVM.dishes[sender.tag].favorited

                self.tableView.reloadData()
                self.likeUnlikeService(dish.id) { (succes) in
                }
            }
        }
    }
    
    @objc func readMoreClick(_ sender : UIButton) {
    
        let dishid = self.fullMenuSearchVM.dishes[sender.tag].id
            self.readmoreindex.add(dishid)
           self.tableView.reloadData()
        
    }
    
    @objc func addBtnClick(_ sender : UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let dish = self.fullMenuSearchVM.dishes[sender.tag]
        
        let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
        if savedLocalCart.count > 0 {
            let hallId = savedLocalCart["hall_id"] as? Int ?? 0
            if hallId != dish.hall_id {
                self.showAlert(dishIndex: sender.tag)
                return
            }
        }
        
        if self.fullMenuSearchVM.dishes[sender.tag].customizable {
            self.delegate?.moveToMenuDetailView(dishId: self.fullMenuSearchVM.dishes[sender.tag].id)
        }else {
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
                
                    
                    
                    let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any] ?? [:]
                    if savedLocalCart.count > 0 {
                        let hallId = savedLocalCart["hall_id"] as? Int ?? 0
                        if hallId != dish.hall_id {
                            return
                        }
                    }
                    cell.addBtn_Out.isHidden = true
                    cell.itemCountLbl.text = "1"
                    self.localAddToCart(dish,itemCount,"add")
                }
        }
        
        self.delegate?.reloadView()
    
        
    }
    
    @objc func plusBtnClick(_ sender : UIButton) {
        if self.fullMenuSearchVM.dishes[sender.tag].customizable {
            self.delegate?.openReapeatItemView(dishs: self.fullMenuSearchVM.dishes, index: sender.tag, itemCount: self.itemCount, childController: self.tableView)
        }else {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
               
                    let dish = self.fullMenuSearchVM.dishes[sender.tag]
                var count = Int(cell.itemCountLbl.text!)
                      count = count! + 1
                    
                    if (itemCount < 101) {
                        cell.itemCountLbl.text = "\(count!)";
                        self.localAddToCart(dish,count,"add")
                    } else {
            
                    }
            }
        }
        
        self.delegate?.reloadView()
        
    }
    
    @objc func minusBtn_Click(_ sender : UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
            
                let dish = self.fullMenuSearchVM.dishes[sender.tag]
                
            var count = Int(cell.itemCountLbl.text!)
                  count = count! - 1
                
                if count == 0 {
                    cell.addBtn_Out.isHidden = false
                self.localAddToCart(dish,count,"sub")
                    return
                }
                if itemCount < 100 {
                    cell.itemCountLbl.text = "\(count!)";
                    self.localAddToCart(dish,count,"sub")
                }
            
        }
        
        self.delegate?.updateViewDataOnMinusBtnClick()
        self.delegate?.reloadView()
    }
    
    
    //MARK:- Add to Local cart
    func localAddToCart(_ dish : MenuDishes, _ count : Int?, _ type : String) {
        
        var localCart = [String : Any]()
        var localCartItem = [[String : Any]]()
        // var commonMealIds = [String]()
        
        if let savedLocalCart = UserDefaults.standard.value(forKey: UserDefault.localCart) as? [String : Any]{
            
            localCart = savedLocalCart
            localCartItem = savedLocalCart["items"] as? [[String : Any]] ?? [[String : Any]]()
        }
        
        var cartItem = [String : Any]()
        
        cartItem["dish_id"] = dish.id
        cartItem["item_name"] = dish.name
        cartItem["price"] = Double(dish.price) //GSD price
        cartItem["qty"] = count ?? 0
       // cartItem["status"] = dish.isDishAvailable
        cartItem["showtypeonapp"] = dish.showtypeonapp
        cartItem["veg"] = dish.veg
        cartItem["Veriations"] = ["option_id":0]
       // cartItem["veg"] = dish.type == .veg ? "Veg" : "Non-veg"
        if localCartItem.count != 0 {
            
            if type == "add" {
                
                var Fresult = Bool()
                _ = localCartItem.filter { dict in
                    if (cartItem["dish_id"] as? Int) == dict["dish_id"] as? Int {
                        if let index = localCartItem.firstIndex(where: {$0["dish_id"] as! Int == (cartItem["dish_id"] as? Int)!}) {
                            var item = localCartItem[index]
                            let newQty = (count ?? 0)
                            item["qty"] = newQty
                            localCartItem[index] = item
                        }
                        Fresult = true
                        return true
                    }
                    Fresult = false
                    return false
                }
                
                if Fresult == false {
                    localCartItem.append(cartItem)
                }
                
            }else {
                
                _ = localCartItem.filter { dict in
                    if (cartItem["dish_id"] as? Int) == dict["dish_id"] as? Int {
                        if let index = localCartItem.firstIndex(where: {$0["dish_id"] as! Int == (cartItem["dish_id"] as? Int)!}) {
                            var item = localCartItem[index]
                            let newQty = (count ?? 0)
                            if count == 0 {
                                localCartItem.remove(at:index)
                            }else {
                                item["qty"] = newQty
                                localCartItem[index] = item
                            }
                        }
                        
                        return true
                    }
                    
                    return false
                }
                
            }
            
        }else {
            localCartItem.append(cartItem)
        }
       
        
        localCart["items"] = localCartItem
        var localCartCount = 0
        var localcartTotal = 0.0
        for items in localCartItem{
            localCartCount += (items["qty"] as? Int) ?? 0
            localcartTotal += (((items["price"] as? Double) ?? 0.0) * (Double((items["qty"] as? Int) ?? 0)))
        }
        print(localcartTotal)
        localCart["cart_total"] = localcartTotal
        localCart["cart_count"] = localCartCount
        localCart["hall_id"] = dish.hall_id
        localCart["hall_name"] = UserModel.shared.hall_name
        localCart["isCouponApplied"] = false
      //  localCart["is_accepting_orders"] = dish.isAcceptingOrder == true ? "yes" : "no"
        
        print(localCart)
        UserModel.shared.cartItemsCount = localCartCount
        UserModel.shared.cartItemsPrice = localCart["cart_total"] as? Double ?? 0.0
        UserDefaults.standard.set(localCart, forKey: UserDefault.localCart)
        UserDefaults.standard.synchronize()
        
        if localCartItem.count == 0 {
            UserModel.shared.cartItemsCount = 0
            UserModel.shared.cartItemsPrice = 0
            UserDefaults.standard.removeObject(forKey: "localCart")
            UserDefaults.standard.synchronize()
        }
        //self.delegate?.updateCartData(isCartUpdate: true)
    }
    
    func showAlert(dishIndex: Int) {
        let dish = self.fullMenuSearchVM.dishes[dishIndex]

        let refreshAlert = UIAlertController(title: "Item already in cart", message: "Your cart contains items from different restaurant. Would you like to reset your cart.", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            UserModel.shared.cartItemsCount = 0
            UserModel.shared.cartItemsPrice = 0
            UserDefaults.standard.removeObject(forKey: UserDefault.localCart)
            UserDefaults.standard.synchronize()
            self.localAddToCart(dish,self.itemCount,"add")
            self.delegate?.reloadView()
            
            let indexPath = IndexPath(row: dishIndex, section: 0)
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? HallDishCell {
                cell.addBtn_Out.isHidden = true
                cell.itemCountLbl.text = "1"
            }

            print("Handle Ok logic here")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        refreshAlert.view.tintColor = .black

        self.window?.rootViewController?.present(refreshAlert, animated: true, completion: nil)
    }
    

}
