//
//  LoaderView.swift
//  #Restaurants
//
//  Created by 42works on 21/07/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftGifOrigin

class LoaderView: UIView {
   
    @IBOutlet weak var activityView: UIView!
    var contentView: UIView!
    var loader: NVActivityIndicatorView!

    var nibName: String {
        return String(describing: type(of: self))
    }
    
    //MARK:
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        contentView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = bounds
        self.addSubview(contentView)
    }
    
    func addLoader() {
       // let size = self.activityView.frame.size
        //loader = NVActivityIndicatorView(frame: CGRect(x:(size.width/2)-35 , y: (size.height/2)-35, width: 70, height: 70), type: .ballSpinFadeLoader, color: #colorLiteral(red: 0.8761098981, green: 0.2143658996, blue: 0.1991876662, alpha: 1), padding: 10)
        
        //loader.startAnimating()
        let imagegif = UIImageView()
        imagegif.frame = CGRect(x:0 , y: 0, width: 60, height: 60)
        imagegif.image = UIImage.gif(name: "RedGif2")
        activityView.addSubview(imagegif)
    }
    
}
