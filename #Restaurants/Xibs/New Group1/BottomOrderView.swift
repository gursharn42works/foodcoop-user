//
//  BottomOrderView.swift
//  Restaurant42
//
//  Created by Apple1 on 29/03/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import Foundation

import UIKit

protocol BottomOrderViewDelegate: class {
    func moveToOrderStatusView(order_id: Int)
    func openCancelliationPolicyView(order_id: String)
}

class BottomOrderView: UIView {
    
    //MARK:- Variables
    
	@IBOutlet var viewtemp: UIView!
    @IBOutlet var contentView: UIView!
    var delegate: BottomOrderViewDelegate?
    
    @IBOutlet weak var orderCollectionView : UICollectionView!{
        didSet{
            orderCollectionView.tag = -1
            orderCollectionView.delegate = self
            //orderCollectionView.dataSource = self
            orderCollectionView.register(UINib(nibName: "OrderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OrderCollectionCell")
        }
    }
    
	@IBOutlet weak var pageControl : UIPageControl!{
        didSet{
            pageControl.hidesForSinglePage = true
        }
    }
    
    var ongoingOrder: [Order] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("BottomOrderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
	
        self.pageControl.numberOfPages = ongoingOrder.count
        self.orderCollectionView.delegate = self
        self.orderCollectionView.dataSource = self
    }
 
}

extension BottomOrderView : UICollectionViewDataSource,UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) ->
        Int {
            return ongoingOrder.count 
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCollectionCell", for: indexPath) as! OrderCollectionCell
        cell.config(ongoingOrder[indexPath.row])
        cell.cancelBtn.tag = indexPath.row
        cell.cancelBtn.tag = indexPath.row
        cell.cancelBtn.addTarget(self, action: #selector(canceOrder_Click(_:)), for: .touchUpInside)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        self.delegate?.moveToOrderStatusView(order_id: Int(ongoingOrder[indexPath.row].id) ?? 0)
    }

    @objc func canceOrder_Click(_ sender: UIButton) {
        self.delegate?.openCancelliationPolicyView(order_id: "\(ongoingOrder[sender.tag].id)")
    }
}

extension BottomOrderView : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.size.width, height: 60.0)
    }
}

