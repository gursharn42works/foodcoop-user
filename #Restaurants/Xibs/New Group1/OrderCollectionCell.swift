//
//  OrderCollectionCell.swift
//  Restaurant42
//
//  Created by Apple1 on 29/03/2019.
//  Copyright © 2019 Apple1. All rights reserved.
//

import UIKit

class OrderCollectionCell: UICollectionViewCell {

    //MARK:- IBOutlets
    
    @IBOutlet weak var pendingConfirmation : UIImageView!
    @IBOutlet weak var orderConfirmed : UIImageView!
    @IBOutlet weak var orderDispatched : UIImageView!
    @IBOutlet weak var orderDelievered : UIImageView!
    @IBOutlet weak var orderStatusLabel : UILabel!
    @IBOutlet weak var estimatedTimeLabel : UILabel!
    @IBOutlet weak var cancelBtn : UIButton!
    
    //MARK:- Variables
   var order = Order()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- Private Function
    
    private func checkStatus(_ order : Order){

        switch order.orderStatus {
            
        case .pendingConfirmation:
            pendingConfirmation.backgroundColor = AppColor.greenColor
            break
            
        case .confirmed, .preparingOrder:
            pendingConfirmation.backgroundColor = AppColor.greenColor
            orderConfirmed.backgroundColor = AppColor.greenColor
            break
            
        case .readyForPickup:
            pendingConfirmation.backgroundColor = AppColor.greenColor
            orderConfirmed.backgroundColor = AppColor.greenColor
            orderDispatched.backgroundColor = AppColor.greenColor
            break
            
        case .completed:
            pendingConfirmation.backgroundColor = AppColor.greenColor
            orderConfirmed.backgroundColor = AppColor.greenColor
            orderDispatched.backgroundColor = AppColor.greenColor
            orderDelievered.backgroundColor = AppColor.greenColor
            break
            
        default:
            break
        }
    }
    
    private func showOrderStatus(_ status: String){
        let status  = OrderStatusEnum(rawValue: status) ?? .pendingConfirmation

        if status != .cancelled{
            for i in 0..<5{
                let imageViewTag = i+10
                if let iv = self.viewWithTag(i+10)  as? UIImageView{
                    if imageViewTag <= status.highlightTillIndex + 10 {
                        iv.backgroundColor = AppColor.greenColor
                    }
                }
            }
        }
    }
    
    //MARK:- Function
    
    func config(_ order : Order){
        self.order = order
        //FIXME:- Implement new order status logic self.checkStatus(order)
        
        let orderStatus = OrderStatusEnum(rawValue: order.status) ?? .pendingConfirmation

        if orderStatus == .completed || orderStatus == .cancelled{
            self.cancelBtn.isHidden = true
        } else {
            self.cancelBtn.isHidden = false
        }
        self.showOrderStatus(order.status)
        
        self.orderStatusLabel.text = "Order #\(order.statusOrderId ?? "") at \(AppStrings.appName)."
        
        if orderStatus != .pendingConfirmation && order.order_type == Constant.shared.DELIVERABLE {
            if order.paymentMethod == .payByCard {
                if orderStatus == .completed || orderStatus == .cancelled{
                    self.cancelBtn.isHidden = true
                } else {
                    self.cancelBtn.isHidden = false
                }
            } else {
                self.cancelBtn.isHidden = true
            }
        }
        else if orderStatus != .pendingConfirmation && order.paymentMethod == .payAtStore {
            self.cancelBtn.isHidden = true
        }
            
        if order.paymentMethod == .payAtStore {
            self.cancelBtn.isHidden = true
        }
            let estimatedDelieveryTimeArr = order.estimatedTime?.convertDateFormat("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd hh:mm a").split(separator: " ")
            
            if estimatedDelieveryTimeArr?.count ?? 0 > 2{
                let estimatedTime = "\(estimatedDelieveryTimeArr?[1] ?? "") \(estimatedDelieveryTimeArr?[2] ?? "")"
                if order.status != "pending_confirmation"{
                    if order.order_type == Constant.shared.DELIVERABLE {
                        estimatedTimeLabel.text = "Estimated Delivery Time: \(estimatedTime)"
                    } else {
                        estimatedTimeLabel.text = "Estimated Pickup Time: \(estimatedTime)"
                    }
                }else{
                    estimatedTimeLabel.text = "Waiting for confirmation"
                }
                
            }else{
                if order.status == "pending_confirmation"{
                    estimatedTimeLabel.text = "Waiting for confirmation"
                }
            }
        //}
    }
    
}

