//
//  RepeatItemView.swift
//  #Restaurants
//
//  Created by Satveer on 03/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

protocol RepeatItemViewDelegate: class {
    func moveToDishDetailViewFromReapeatItemView(isAddNew: Bool)
}

class RepeatItemView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var dishNameLbl: UILabel!
    @IBOutlet var customizationLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    
    var delegate: RepeatItemViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("RepeatItemView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func showRepeatViewData(dish: MenuDishes) {
        self.dishNameLbl.text = dish.name
        customizationLbl.text = dish.descript_ion //GSD
        print(dish.attributes)
        self.priceLbl.smartDecimalText = "\(UserModel.shared.currencyCode)\(dish.price)"
        
        
    }
    
    @IBAction func addNewBtnClick(_ sender: UIButton) {
        self.delegate?.moveToDishDetailViewFromReapeatItemView(isAddNew: true)
    }
    
    @IBAction func reapeatLastBtnClick(_ sender: UIButton) {
        self.delegate?.moveToDishDetailViewFromReapeatItemView(isAddNew: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.bottomView {
            self.isHidden = true
        }
    }
    
}
