//
//  RestCollectionViewCell.swift
//  #Restaurants
//
//  Created by 42works on 27/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class RestCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    
    @IBOutlet weak var itemName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    
        
    }

}
