//
//  RestItemsTableCell.swift
//  #Restaurants
//
//  Created by 42works on 27/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit


protocol RestItemsTableCellDelegate: class {
    func moveToRestView(dish_id: Int,tag_id: Int,type : String)
}

class RestItemsTableCell: UITableViewCell {
    
    
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    var dishArr = ["Dosa","Burger","Dosa","Burger","Dosa","Burger","Dosa","Burger"]
    var itemName = ["Dosa","Salad","Pasta","Burger","Pizza","Noodels","Sushi","Chicken"]
    
    var tagsArr = [Tags]()
    var delegate:RestItemsTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.itemsCollectionView.register(UINib(nibName: "RestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollCell")
        self.itemsCollectionView.delegate = self
        self.itemsCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension RestItemsTableCell : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollCell", for: indexPath) as! RestCollectionViewCell
    cell.itemImage.set_image(tagsArr[indexPath.row].logo ?? "", placeholder: "dishBackground_bg")
        cell.itemName.text = tagsArr[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize  {
        //CGSize(width: (collectionView.frame.size.width/2)-30, height: collectionView.frame.size.height)
       // return CGSize(width: 80, height: 100)
        var collectionViewSize = collectionView.frame.size
                  collectionViewSize.width = 80//collectionViewSize.width/3.0 //Display Three elements in a row.
                  collectionViewSize.height = 100//collectionViewSize.height/4.0
                  return collectionViewSize
            
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let Tag = tagsArr[indexPath.row].id ?? 0
        self.delegate?.moveToRestView(dish_id: 0,tag_id: Tag,type : "Dish")
    }
    
    
}
