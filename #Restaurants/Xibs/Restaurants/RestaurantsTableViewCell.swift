//
//  RestaurantsTableViewCell.swift
//  #Restaurants
//
//  Created by 42works on 25/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var restImage: UIImageView!
    @IBOutlet weak var restName: UILabel!
    
    @IBOutlet weak var ratingLbl_Out: UILabel!
    @IBOutlet weak var ratingView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
