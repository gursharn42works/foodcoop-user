//
//  ShowAlertTableCell.swift
//  #Restaurants
//
//  Created by 42works on 31/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class ShowAlertTableCell: UITableViewCell {
    @IBOutlet weak var alertTextLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
