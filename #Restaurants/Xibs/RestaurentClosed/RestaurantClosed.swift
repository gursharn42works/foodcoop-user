//
//  RestaurantClosed.swift
//  #Restaurants
//
//  Created by Thirtyfour on 29/06/20.
//  Copyright © 2020 Apple1. All rights reserved.
//

import UIKit

class RestaurantClosed: UIView {
	//MARK:- Variables
		
	@IBOutlet var contentView: UIView!
    @IBOutlet weak var tvMessage: UITextView!
 
    var hallTiming:[HallTiming] = []
    var nibName = "RestaurantClosed"
    var openTimingsClosure:(()->())?

    override init(frame: CGRect) {
		   super.init(frame: frame)
		   commonInit()
	   }
	   
	   required init?(coder aDecoder: NSCoder) {
		   super.init(coder: aDecoder)
		   commonInit()
	   }
	   
	   private func commonInit() {
		   Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
		   addSubview(contentView)
		   contentView.frame = self.bounds
		   contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		  
		self.tvMessage.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
	
        let attributedString = NSMutableAttributedString(attributedString: self.tvMessage.attributedText)
		
		let range = (Messages.restaurantClosed as NSString).range(of: "here")
		
		attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        

		attributedString.addAttribute(NSAttributedString.Key.link, value: "https://www.google.com", range: range)
	 
		self.tvMessage.attributedText = attributedString
//        self.tvMessage.font = UIFont.boldSystemFont(ofSize: 15)

		self.tvMessage.delegate = self
	}
}

extension RestaurantClosed : UITextViewDelegate
{
	func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        self.openTimingsClosure?()
		return false
	}
}
