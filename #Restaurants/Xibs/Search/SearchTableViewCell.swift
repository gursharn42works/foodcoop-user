//
//  SearchTableViewCell.swift
//  #Restaurants
//
//  Created by 42works on 26/05/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var image_Out: UIImageView!
    @IBOutlet weak var subTitleName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
