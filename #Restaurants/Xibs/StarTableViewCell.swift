//
//  StarTableViewCell.swift
//  #Restaurants
//
//  Created by 42works on 05/06/21.
//  Copyright © 2021 42works. All rights reserved.
//

import UIKit
import Cosmos

class StarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemnameLbl: MediumLabel!
    
    @IBOutlet weak var itemSubLbl: UILabel!
    
    @IBOutlet weak var starRatingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
